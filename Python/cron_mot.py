#!/usr/bin/env python3

'''
    A cron monitor.

    Designed to be executed once a day ,
    monitor all the changes to all users' crontab ,
    remove all the entries 30 days older.

    Run as root recommended.
    Need to run the INIT command the first time:
        script_name init
'''

import hashlib , os , pickle , sys

et_lifetime = 30        # The lifetime of an entry in day.
crontab_path = '/var/spool/cron'
db_path = './db'        # Edit this to the path you want to save the DB.

file_db = {}            # Contain the lifetime of
                        # all users' crontab's entry.

file_rec = {}           # Temporary DB , contain line number for each
                        # entry , used to compare with the REAL
                        # DB and update the REAL DB as well as users'
                        # crontabs.

def db_init ():
    read_file()

    for file in file_rec.keys():
        file_db[file] = {}
        for entry in file_rec[file].keys():
            file_db[file][entry] = 0
    
    save_db()
    exit()
    
def load_db ():
    global file_db

    try:
        with open(db_path , 'rb') as f:
            file_db = pickle.load(f)
    except IOError:
        db_init()

def update_db ():
    '''
        Update the REAL DB based on then scan
        result(file_rec).

        Skip the empty table.
    '''

    def check_new_file ():
        global file_db , file_rec

        return \
            set(file_rec.keys()).difference(set(file_db.keys()))

    def check_old_file ():
        global file_db , file_rec
        
        return \
            set(file_db.keys()).difference(set(file_rec.keys()))
    
    def check_new_entry ():
        global file_db , file_rec
        nonlocal file

        return \
            set(file_rec[file].keys()).difference(set(file_db[file].keys()))

    def check_old_entry ():
        global file_db , file_rec
        nonlocal file
    
        # check entries that have been removed
        old_entry = \
            set(file_db[file].keys()).difference(set(file_rec[file].keys()))

        # check the outdated entry
        for et in set(file_db[file].keys()).difference(old_entry):
            if file_db[file][et] >= et_lifetime:
                old_entry.add(et)

        return old_entry

    def update_db_file (new_file , old_file):
        '''
            Add/Del files to the DB
        '''

        nonlocal file

        def add_file ():
            global file_db , file_rec
            nonlocal file , new_file

            for file in new_file:
                file_db[file] = {}
                for entry in file_rec[file].keys():
                    file_db[file][entry] = 0

        def del_file ():
            global file_db
            nonlocal file , old_file

            for file in old_file:
                del file_db[file]

        add_file()
        del_file()
    
    def update_db_entry (new_entry , old_entry):
        '''
            Add/Del entries to the DB
        '''

        nonlocal file

        def add_entry ():
            global file_db
            nonlocal file , new_entry

            for n_et in new_entry:
                file_db[file][n_et] = 0

        def del_entry ():
            global file_db
            nonlocal file , old_entry

            for o_et in old_entry:
                del file_db[file][o_et]

        def update_timestamp ():
            global file_db
            nonlocal file

            for et in file_db[file].keys():
                file_db[file][et] += 1

        add_entry()
        del_entry()
        update_timestamp()
    
    def update_file ():
        global file_db , file_rec
        nonlocal file

        with open(file , 'w') as f:
            for entry in file_db[file].keys():
                f.write(file_rec[file][entry] + '\n')


    # Update the file part of the DB first
    new_file = check_new_file()
    old_file = check_old_file()
    update_db_file(new_file , old_file)

    for file in file_db.keys():
        # Update the entry part of the DB
        new_entry = check_new_entry()
        old_entry = check_old_entry()
        update_db_entry(new_entry , old_entry)

        update_file()

def save_db ():
    with open(db_path , 'wb') as f:
        pickle.dump(file_db , f)

def rec_entry (f_name , f):
    '''
        Record each entry in F for
        update.
    '''

    for entry in f:
        et = str.join(' ' , entry.split())      # Adjust entry
        e_hash = entry_hash(et)
        file_rec[f_name][e_hash] = et           # Record adjusted entry

def entry_hash (entry):
    return hashlib.sha512(entry.encode()).digest()

def read_file ():
    for file in os.listdir(crontab_path):
        f_name = crontab_path + '/' + file
        with open(f_name) as f:
            file_rec[f_name] = {}
            rec_entry(f_name , f)

if len(sys.argv) > 1 and sys.argv[1] == 'init':
    db_init()

load_db()
read_file()
update_db()
save_db()
