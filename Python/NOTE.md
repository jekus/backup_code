#Note for Python3

* In interactive mode , the last printed expression
will be store in a special variable '_'.
```
	1 + 3	->	4
	_		->	4
```

* Raw string : r'123\n' or R'123\n'

* Basic list comprehension:
```
	[x**2 for x in range(10)]
	[(x,y) for x in [1,2,3] for y in [3,1,4] if x != y]

	>>> matrix = [[1,2,3],[4,5,6],[7,8,9]]
	>>> [[row[i] for row in matrix] for i in range(3)]
	->  [[1,4,7],[2,5,8],[3,6,9]]
```

* Loop over (idx , val) pairs:
```
    >>> for i , v in enumerate(['a','b','c'])
    ...     print(i , v)
    ...

    ->
    	0   a
		1   b
        2   c
```

* Loop over multiple items at the same time:
```
	>>> for x , y in zip(range(1 , 10) , range(2 , 20)):
	...	print(x+y-2)
```

* Reverse a sequence:
```
	>>> for i in reversed(range(1 , 10 , 2)):
	...	print(i)

    ->
    	9
		7
		5
		3
		1
```

* When import a module , use '_' before a name can 'HIDE' it.

* To reload a module , the reloaded module **MUST** be **LOADED** before
```
	>>> import imp
	>>> imp.reload(imp)
```

* Multiple variable assignment:
```
	a , b , c = (1 , 2 , 3)
		is equivalent to:
	a , b , c = 1 , 2 , 3 or even
	a , b , c = ((1 , 2 , 3))
```

* Assignments do not copy data -- they just **BIND** names to object.
* The same is true for deletions. It just removes the name from object.

* The difference between 'eval' and 'exec' [**TRY IT**]:
```
	exec('1+1')
	eval('1+1')
	exec('1+1;1+2')
	eval('1+1;1+2')
```