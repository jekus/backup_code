#!/usr/bin/env python3

'''
    Use of Argument Packing and Unpacking
'''

# arg is a tuple , key is a dict
def foo (name , *arg , **key):
    print("Hello" , name)
    print("Extra positional argument:" , arg)
    print("Extra key argument:" , key)
    print()

foo("Jekus Neky" , '32432' , 4324 , [21 , 434 , 34] , a=123 , b=456 , boom=True)

l = ["HHL" , 2]
foo(*l)

d = {"Jekus" : 123 , "XYT" : 456}
foo(*l , **d)
