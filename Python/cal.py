#!/usr/bin/env python3

'''
    A simple calculator
'''

class CAL_Exception (Exception):
    def __init__ (self , msg):
        self.msg = msg

class TokenError (CAL_Exception):
    pass
class ZeroDivError (CAL_Exception):
    pass


def tokenize (src):
    
    def classify_tk ():

        def get_num ():
            nonlocal src_idx , src
            nonlocal cur_tk
            cur_tk["TYPE"] = "N"

            for src_idx in range(src_idx+1 , len(src)):
                if not src[src_idx].isdigit():
                    return
            src_idx+=1

        def get_op_paren ():
            nonlocal src_idx , src
            nonlocal cur_tk
            cur_tk["TYPE"] = src[src_idx]
            src_idx+=1


        nonlocal src_idx , src
        tk_head = src[src_idx]

        if tk_head.isdigit():
            return get_num()
        elif tk_head in "+-*/()":
            return get_op_paren()
        else:
            raise TokenError("Invalid token")

    def build_tk ():
        nonlocal tk_start , src_idx , src
        nonlocal cur_tk
        cur_tk["REP"] = src[tk_start:src_idx]

    def skip_blank ():
        nonlocal src_idx , src
        while src_idx < len(src) and src[src_idx].isspace():
            src_idx += 1
    
    
    global tk_list , tk_idx
    tk_list = []
    tk_idx = -1
    cur_tk = {"REP": "" , "TYPE": ""}

    src_idx = 0
    while src_idx < len(src):
        skip_blank()
        tk_start = src_idx
        classify_tk()
        build_tk()
        tk_list.append(cur_tk.copy())
    tk_list.append({"REP": "" , "TYPE": "EOF"})


def expr (GET = True , in_subexpr = False):

    def get_prim (GET):
        global tk_list , tk_idx
        if GET: tk_idx+=1

        res = 0
        if tk_list[tk_idx]["TYPE"] == "(":
            res = expr(GET , True)
            if tk_list[tk_idx]["TYPE"] != ")":
                raise TokenError("Parenthesis not matched")
            tk_idx+=1
        elif not in_subexpr and tk_list[tk_idx]["TYPE"] == ")":
            raise TokenError("Parenthesis not matched")
        elif tk_list[tk_idx]["TYPE"] == "+":
            res = get_prim(GET)
        elif tk_list[tk_idx]["TYPE"] == "-":
            res = -get_prim(GET)
        elif tk_list[tk_idx]["TYPE"] == "N":
            res = int(tk_list[tk_idx]["REP"])
            tk_idx+=1
        return res
    
    def get_term (GET):
        global tk_list , tk_idx

        res = get_prim(GET)
        while True:
            if tk_list[tk_idx]["TYPE"] == '*':
                res *= get_prim(GET)
            elif tk_list[tk_idx]["TYPE"] == '/':
                try:
                    res /= get_prim(GET)
                except ZeroDivisionError:
                    raise ZeroDivError("Can't divide 0")
            else:
                return res


    global tk_list , tk_idx

    res = get_term(GET)
    while True:
        if tk_list[tk_idx]["TYPE"] == "+":
            res += get_term(GET)
        elif tk_list[tk_idx]["TYPE"] == "-":
            res -= get_term(GET)
        else:
            return res


def cal ():
    while True:
        try:
            exp = input("> ")
            tokenize(exp)
            print(expr())
        except TokenError as tk_err:
            print(tk_err.msg)
        except ZeroDivError as zd_err:
            print(zd_err.msg)
        except EOFError:
            print()
            return

if __name__ == "__main__":
    import readline
    cal()
