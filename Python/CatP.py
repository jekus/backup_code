#!/usr/bin/env python3

import urllib.request as R , re

url = input("URL: ")
url = R.Request(url)
url.add_header("User-Agent","Firefox/21.0")
html = R.urlopen(url).read().decode(errors="ignore")

result = re.findall("\"http[^()<>]*jpg\"",html)

i = 1
for u in result:
    print("Downloading {}.jpg".format(i))
    R.urlretrieve(u[1:len(u)-1],"/home/jekus/Pictures/{}.jpg".format(i))
    i += 1
