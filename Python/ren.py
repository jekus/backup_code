#!/usr/bin/env python3

import sys , shutil

if len(sys.argv) < 3:
    print("Usage: script_name old_name new_name ...")
    exit()

shutil.move(sys.argv[1] , sys.argv[2])
if len(sys.argv) > 3:
    for ref in sys.argv[3:]:
        with open(ref , 'r+') as f:
            rst = f.read().replace(sys.argv[1] , sys.argv[2])
            f.truncate(0)
            f.seek(0)
            f.write(rst)
