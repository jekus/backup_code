#!/usr/bin/sudo /usr/bin/bash

iptables -t nat -D POSTROUTING -o ppp0 -j MASQUERADE
iptables -D FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -D FORWARD -i wlp2s0 -o ppp0 -j ACCEPT

pkill hostapd
ip addr flush dev wlp2s0
ip link set dev wlp2s0 down
