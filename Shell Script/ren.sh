#!/usr/bin/bash

# Do the operation in the current work diretory by default.

# Usage: script_name old_name new_name [...(a list of files which refer the old name)]

old_name=$1
new_name=$2
mv $1 $2
shift 2

sed -i s/$old_name/$new_name/g $@
