#!/usr/bin/sudo /usr/bin/bash

# I enable 'dnsmasq' on system boot ,
# so the following code doesn't include
# something like 'dhcpd'.

iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i wlp2s0 -o ppp0 -j ACCEPT

ip link set dev wlp2s0 up
ip addr add dev wlp2s0 192.168.1.1/24 broadcast 192.168.1.255
hostapd /etc/hostapd.conf -B
