// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

(LISTEN_KBD)
    @KBD
    D=M
    @CLEAR_SCREEN
    D;JEQ
    @FILL_SCREEN
    D;JGT

    @LISTEN_KBD
    0;JMP

(CLEAR_SCREEN)
    @have_clear
    D=M
    @LISTEN_KBD
    D;JGT

    @SCREEN
    D=A
    (CLEAR_LOOP)
        @R0
        M=D
        @KBD
        D=D-A
        @CLEAR_END
        D;JGE

        @R0
        D=M
        A=D
        M=0
        D=D+1
        
        @CLEAR_LOOP
        0;JMP

    (CLEAR_END)
        @have_clear
        M=1
        @have_fill
        M=0
        @LISTEN_KBD
        0;JMP


(FILL_SCREEN)
    @have_fill
    D=M
    @LISTEN_KBD
    D;JGT

    @SCREEN
    D=A
    (FILL_LOOP)
        @R0
        M=D
        @KBD
        D=D-A
        @FILL_END
        D;JGE

        @R0
        D=M
        A=D
        M=-1
        D=D+1
        
        @FILL_LOOP
        0;JMP

    (FILL_END)
        @have_fill
        M=1
        @have_clear
        M=0
        @LISTEN_KBD
        0;JMP
