function [error_train , error_val] = ...
    learningCurveRand(X , y , Xval , yval , lambda)

m = size(X , 1);
error_train = zeros(m , 1);
error_val = zeros(m , 1);

Rep_select_n = 50;  % The number of time to select i examples.
for i = 1:m
    total_error_train = 0;
    total_error_val = 0;
    for _ = 1:Rep_select_n
        new_ex_idx = randperm(m , i);
        new_X = [];
        new_Y = [];
        for idx = 1:length(new_ex_idx)
            new_X(end+1,:) = X(idx,:);
            new_Y(end+1,:) = y(idx,:);
        end

        theta = trainLinearReg(new_X , new_Y , lambda);
        [total_error_train] += linearRegCostFunction(new_X , new_Y , theta , 0);
        [total_error_val] += linearRegCostFunction(Xval , yval , theta , 0);
    end

    error_train(i) = total_error_train / Rep_select_n;
    error_val(i) = total_error_val / Rep_select_n;
end
