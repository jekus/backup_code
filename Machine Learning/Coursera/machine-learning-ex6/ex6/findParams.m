clear; clc; close;

load('ex6data3.mat');

C = -1;
S = -1;
count = 0;
error_rate = 1;
H = waitbar(0 , "Searching ...");

cands = [0.1 1];
c_n = length(cands);
for C_idx = 1:c_n
    for S_idx = 1:c_n
        C_val = cands(C_idx);
        S_val = cands(S_idx);
        model= svmTrain(X, y, C_val, @(x1, x2) gaussianKernel(x1, x2, S_val));

        cur_error_rate = mean(double(svmPredict(model , Xval) != yval)); 
        if cur_error_rate < error_rate
            error_rate = cur_error_rate;
            C = C_val;
            S = S_val;
        end
        
        ++count;
        waitbar(count / (c_n ^ 2) , H);
    end
end

delete(H);