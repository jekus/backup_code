#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>

#define isparen(ch) (ch == '(' || ch == ')')
#define isop(ch) (ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '^')

#define data_outfmt "%Ld"
typedef long long int Data_t;

typedef enum {
    L_PAREN = '(' , R_PAREN = ')' ,
    PLUS = '+' , MINUS = '-' , MUL = '*' , DIV = '/' ,
    EXPT = '^' ,
    INT = 150 , Eof , ERR
}TK_c;

enum {
    PAREN_UM , UK_TK , PARSE_ERR ,
    DIV_ZERO
};

const char *err_msg [] = {
    "Parenthesis not matched" ,
    "Unknown token" ,
    "Error in parsing expression" ,
    "Division by zero"
};

static struct {
    char *rep;
    TK_c c;
}cur_tk;

const char * prompt = "> ";
static int buf_idx , buf_l;
static char *buf;
int l_paren_n;

Data_t expr (void);

Data_t
f_pow (Data_t base , Data_t n)
{
    Data_t res = 1;
    while(n)
    {
        if(n & 1) res *= base;
        base *= base;
        n >>= 1;
    }
    return res;
}

void
reset_state (void)
{
    buf_idx = 0;
    buf_l = strlen(buf);
    l_paren_n = 0;
}

void
skip_useless (void)
{
    while(buf_idx < buf_l && isblank(buf[buf_idx]))
        ++buf_idx;
}

int
empty_line (void)
{
    skip_useless();
    return buf_idx >= buf_l;
}

void
get_int ()
{
    cur_tk.c = INT;
    int bg_pos = buf_idx++;
    while(buf_idx < buf_l && isdigit(buf[buf_idx]))
        ++buf_idx;

    int s = buf_idx - bg_pos;
    cur_tk.rep = malloc(sizeof(char) * (s+1));
    memcpy(cur_tk.rep , buf+bg_pos , sizeof(char) * s);
    cur_tk.rep[s] = '\0';
}

void
get_op_pa (void)
{
    cur_tk.c = buf[buf_idx++];
}

void
get_err (void)
{
    cur_tk.c = ERR;
    cur_tk.rep = err_msg[UK_TK];
}

void
get_next_tk (void)
{
    if(cur_tk.c == INT)
        free(cur_tk.rep);
    skip_useless();
    if(buf_idx >= buf_l)
    {
        cur_tk.c = Eof;
        return;
    }

    char ch = buf[buf_idx];
    if(isdigit(ch))
        get_int();
    else if(isop(ch) || isparen(ch))
        get_op_pa();
    else
        get_err();
}

Data_t
prim (void)
{
    get_next_tk();
    int sign = 1;
    while(1)
    {
        switch(cur_tk.c)
        {
            case L_PAREN:
            {
                ++l_paren_n;
                Data_t res = expr();
                if(cur_tk.c == ERR) return 1;
                if(cur_tk.c != R_PAREN)
                {
                    cur_tk.c = ERR;
                    cur_tk.rep = err_msg[PAREN_UM];
                    return 1;
                }
                --l_paren_n;
                get_next_tk();
                return res * sign;
            }

            case PLUS:
                get_next_tk();
                break;
            case MINUS:
                sign = -sign;
                get_next_tk();
                break;

            case INT:
            {
                Data_t res = strtoll(cur_tk.rep , NULL , 10);
                get_next_tk();
                return res * sign;
            }

            case ERR: return 1;

            default:
                cur_tk.c = ERR;
                cur_tk.rep = err_msg[PARSE_ERR];
                return 0;
        }
    }
}

Data_t
m_pow (void)
{
    Data_t res = prim();

    while(1)
    {
        switch(cur_tk.c)
        {
            case EXPT:
                res = f_pow(res , m_pow());
                break;

            default: return res;
        }
    }

    return res;
}

Data_t
term (void)
{
    Data_t res = m_pow();

    while(1)
    {
        switch(cur_tk.c)
        {
            case MUL:
                res *= m_pow();
                break;

            case DIV:
            {
                Data_t d = m_pow();

                if(d == 0)
                {
                    cur_tk.c = ERR;
                    cur_tk.rep = err_msg[DIV_ZERO];
                    return 0;
                }
                else
                {
                    res /= d;
                    break;
                }
            }

            default: return res;
        }
    }

    return res;
}


Data_t
expr (void)
{
    Data_t res = term();

    while(1)
    {
        switch(cur_tk.c)
        {
            case PLUS:
                res += term();
                break;
            case MINUS:
                res -= term();
                break;

            case R_PAREN:
                if(l_paren_n)
                {
                    return res;
                }
                else
                {
                    cur_tk.c = ERR;
                    cur_tk.rep = err_msg[PAREN_UM];
                    return 0;
                }

            case ERR: case Eof:
                return res;

            default:
                cur_tk.c = ERR;
                cur_tk.rep = err_msg[PARSE_ERR];
                return 0;
        }
    }

    return res;
}

int
main (void)
{
    for(;(buf = readline(prompt));free(buf))
    {
        reset_state();
        if(!(*buf) || empty_line()) continue;

        Data_t res = expr();
        if(cur_tk.c == ERR)
        {
            puts(cur_tk.rep);
        }
        else
        {
            printf(data_outfmt "\n", res);
        }
    }

    return 0;
}
