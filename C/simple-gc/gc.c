#include <stdlib.h>
#include "vm.h"

void
mark (Object_t *obj)
{
    if(obj->marked) { return;}
    obj->marked = 1;

    if(obj->t == OBJ_PAIR)
    {
        mark(obj->fst);
        mark(obj->snd);
    }
}

void
unmark (Object_t *obj)
{
    if(!(obj->marked)) { return;}
    obj->marked = 0;

    if(obj->t == OBJ_PAIR)
    {
        unmark(obj->fst);
        unmark(obj->snd);
    }
}

static void
markall (VM *vm)
{
    for(int i = 0;i < vm->sk_ptr;++i)
        mark(vm->stack[i]);
}

static void
sweep (VM *vm)
{
    Object_t **ptr = &vm->fstobj;
    while(*ptr)
    {
        if(!((*ptr)->marked))
        {
            Object_t *fptr = *ptr;
            *ptr = fptr->next;
            free(fptr);
        }
        else
        {
            (*ptr)->marked = 0;
            ptr = &(*ptr)->next;
        }
    }
}

void
gc (VM *vm)
{
    markall(vm);
    sweep(vm);
    vm->maxObj_n = vm->Obj_n * 2;
}
