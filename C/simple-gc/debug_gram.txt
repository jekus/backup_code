program:    instr EOF
instr:      symbol args
symbol:     [a-zA-Z_]+
args:       Integer | Pair | @
Integer:    [0-9]+
Pair:       '(' Pair ',' Pair ')' | Integer

                First Set           Follow Set
program:    { symbol }              { }
instr:      { symbol }              { EOF }
symbol:     { symbol }              { Integer '(' EOF }
args:       { Integer '(' @ }       { EoF }
Integer:    { Integer }             { EoF ',' ')' }
Pair:       { '(' Integer }         { EoF ',' ')' }
