#include <stdio.h>
#include <stdlib.h>
#include "nerr.h"

void
display_error (const char *msg)
{
    printf("Error: %s\n", msg);
}

void
throw_error (const char *msg)
{
    printf("Error: %s\n", msg);
    exit(EXIT_SUCCESS);
}
