#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "vm.h"
#include "gc.h"
#include "nerr.h"

typedef void (*act_func) (Object_t * , void *);

static int
object_size (Object_t *obj)
{
    if(obj->marked) { return 0;}
    obj->marked = 1;

    int objs;
    switch(obj->t)
    {
        case OBJ_INT:
        {
            objs = sizeof(Int_t);
            break;
        }
        
        case OBJ_PAIR:
        {
            objs = object_size(obj->fst) +
                   object_size(obj->snd);
            break;
        }
    }

    obj->marked = 0;
    return objs;
}

static void
inspectele (Object_t *obj , void *p)
{
    printf("Object: ");
    int obj_size = object_size(obj);
    switch(obj->t)
    {
        case OBJ_INT:
        {
            printf("%p -> INT %10d Bytes", obj , obj_size);
            break;
        }

        case OBJ_PAIR:
        {
            printf("%p -> PAIR%10d Bytes", obj , obj_size);
            break;
        }
    }
    puts("");
    *(int *)p += obj_size;
}

static void
acc_objsize (Object_t *obj , void *p)
{
    *(int *)p += object_size(obj);
}

static void
tra_env (VM *vm , act_func func , void *v)
{
    for(Object_t *p = vm->fstobj;p;p = p->next)
    {
        if(!(p->marked))
        {
            func(p , v);
            mark(p);
        }
    }
    
    for(Object_t *p = vm->fstobj;p;p = p->next)
    {
        if(p->marked)
        {
            unmark(p);
        }
    }
}

static void
inspectSK (VM *vm)
{
    puts("");
    int total_mem = 0;
    for(int i = 0;i < vm->sk_ptr;++i)
    { inspectele(vm->stack[i] , &total_mem);}
    printf("- In Stack Memory Consumption: %d Bytes -\n", total_mem);

    total_mem = 0;
    tra_env(vm , acc_objsize , &total_mem);
    printf("- Total Memory Consumption: %d Bytes -\n\n", total_mem);
}


typedef long long int Int_t;

typedef enum{
    TK_LPAREN = '(' , TK_RPAREN = ')',
    TK_COMMA = ',' ,
    TK_SYMBOL = 257 ,
    TK_INTEGER ,
    TK_UK ,  TK_EOF
}tk_t;

typedef struct{
    tk_t t;
    char *rep;
}Token_t;

static Token_t tk;
static char *buf;
static int buf_idx;
static char cur_ch;

#define next_char() (cur_ch = buf[++buf_idx])

#define isuseless(ch)   (isspace(ch))
#define isparen(ch)     (ch == '(' || ch == ')')
#define iscomma(ch)     (ch == ',')
#define iseof(ch)       (ch == '\0')
#define issym(ch)       (isalpha(ch) || ch == '_')

static void
lex_setup (void)
{
    buf_idx = -1;
    tk.rep = NULL;
    next_char();
}

static void
skip_useless (void)
{
    while(isuseless(cur_ch))
    {
        next_char();
    }
}

static void
get_integer (void)
{
    tk.t = TK_INTEGER;
    while(isdigit(cur_ch))
    {
        next_char();
    }
}

static void
get_char (void)
{
    tk.t = cur_ch;
    next_char();
}

static void
get_eof (void)
{
    tk.t = TK_EOF;
    next_char();
}

static void
get_sym (void)
{
    tk.t = TK_SYMBOL;
    while(issym(cur_ch))
    {
        next_char();
    }
}

static void
get_uk (void)
{
    tk.t = TK_UK;
    next_char();
}

static void
tk_stringfy (int tk_start , int tk_end)
{
    int tk_size = tk_end - tk_start;
    char *rep = realloc(tk.rep , sizeof(char) * (tk_size+1));
    memcpy(rep , buf+tk_start , sizeof(char)*tk_size);
    rep[tk_size] = '\0';
    tk.rep = rep;
}

static void
get_next_token (void)
{
    skip_useless();

    int tk_start = buf_idx;
    if(isdigit(cur_ch))
    {
        get_integer();
    }
    else if(isparen(cur_ch))
    {
        get_char();
    }
    else if(iscomma(cur_ch))
    {
        get_char();
    }
    else if(iseof(cur_ch))
    {
        get_eof();
    }
    else if(issym(cur_ch))
    {
        get_sym();
    }
    else
    {
        get_uk();
    }

    tk_stringfy(tk_start , buf_idx);
}

/* Used for testing the lexical analyzer */
static void
lex_test (void)
{
    while(1)
    {
        get_next_token();
        switch(tk.t)
        {
            case TK_INTEGER:
            { printf("INTEGER: %s\n", tk.rep); break;}

            case TK_LPAREN:
            { printf("LPAREN: %s\n", tk.rep); break;}
            case TK_RPAREN:
            { printf("RPAREN: %s\n", tk.rep); break;}
            case TK_COMMA:
            { printf("COMMA: %s\n", tk.rep); break;}

            case TK_SYMBOL:
            { printf("SYMBOL: %s\n", tk.rep); break;}

            case TK_EOF:
            { printf("- EOF -\n"); return;}

            case TK_UK:
            { printf("UNKNOWN: %s\n", tk.rep); break;}
        }
    }
}

static int
empty_line (void)
{
    skip_useless();
    return (iseof(cur_ch) ? 1 : 0);
}


typedef enum{
    N_PROGRAM ,
    N_INSTR , N_ARGS ,
    N_SYMBOL , N_INTEGER ,
    N_PAIR
}node_t;

typedef struct ast_t{
    node_t t;
    char *rep;
    int cn;
    struct ast_t **child;
}AST_t;

static AST_t *
cons_ast_node (node_t t)
{
    AST_t *ast = malloc(sizeof(AST_t));
    ast->t = t;
    ast->cn = 0;
    ast->rep = NULL;
    ast->child = NULL;
    return ast;
}

static void
ast_del (AST_t *ast)
{
    for(int i = 0;i < ast->cn;++i)
    {
        ast_del(ast->child[i]);
    }

    switch(ast->t)
    {
        case N_SYMBOL:
        case N_INTEGER:
        {
            free(ast->rep);
            break;
        }

        default: break;
    }
    free(ast);
}

static void
ast_insert_node (AST_t *ast , AST_t *node)
{
    ++(ast->cn);
    AST_t **ptr = realloc(ast->child , sizeof(AST_t *) * (ast->cn));
    ast->child = ptr;
    ast->child[ast->cn-1] = node;
}

static void
token (tk_t t)
{
    if(tk.t != t)
    {
        throw_error("Error in parsing");
    }
    get_next_token();
}

static AST_t *
parse_integer (void)
{
    AST_t *ast = cons_ast_node(N_INTEGER);
    switch(tk.t)
    {
        case TK_INTEGER:
        {
            ast->rep = (char *)strdup(tk.rep);
            token(TK_INTEGER);
            break;
        }

        default: throw_error("Error in parsing"); break;
    }

    return ast;
}

static AST_t *
parse_pair (void)
{
    AST_t *ast = cons_ast_node(N_PAIR);
    switch(tk.t)
    {
        case TK_LPAREN:
        {
            token(TK_LPAREN);
            ast_insert_node(ast , parse_pair());
            token(TK_COMMA);
            ast_insert_node(ast , parse_pair());
            token(TK_RPAREN);
            break;
        }

        case TK_INTEGER:
        {
            ast_del(ast);
            return parse_integer();
        }
        
        default: throw_error("Error in parsing"); break;
    }

    return ast;
}

static AST_t *
parse_args (void)
{
    AST_t *ast = cons_ast_node(N_ARGS);
    switch(tk.t)
    {
        case TK_INTEGER:
        {
            ast_insert_node(ast , parse_integer());
            break;
        }

        case TK_LPAREN:
        {
            ast_insert_node(ast , parse_pair());
            break;
        }

        case TK_EOF: break;
        
        default: throw_error("Error in parsing"); break;
    }

    return ast;
}

static AST_t *
parse_symbol (void)
{
    AST_t *ast = cons_ast_node(N_SYMBOL);
    switch(tk.t)
    {
        case TK_SYMBOL:
        {
            ast->rep = (char *)strdup(tk.rep);
            token(TK_SYMBOL);
            break;
        }
        
        default: throw_error("Error in parsing"); break;
    }

    return ast;
}

static AST_t *
parse_instr (void)
{
    AST_t *ast = cons_ast_node(N_INSTR);
    switch(tk.t)
    {
        case TK_SYMBOL:
        {
            ast_insert_node(ast , parse_symbol());
            AST_t *subt = parse_args();
            if(!(subt->cn))
            {
                ast_del(subt);
            }
            else
            {
                ast_insert_node(ast , subt);
            }
            break;
        }

        default: throw_error("Error in parsing"); break;
    }

    return ast;
}

static AST_t *
parse_program (void)
{
    AST_t *ast = cons_ast_node(N_PROGRAM);
    switch(tk.t)
    {
        case TK_SYMBOL:
        {
            ast_insert_node(ast , parse_instr());
            break;
        }

        default: throw_error("Error in parsing"); break;
    }

    return ast;
}

static AST_t *
run_parser (void)
{
    get_next_token();
    AST_t *ast = parse_program();
    if(tk.t != TK_EOF) { throw_error("Error in parsing");}
    return ast;
}


static void do_ast_print (AST_t * , int , int);

static void
ast_print_pair (AST_t *ast)
{
    switch(ast->t)
    {
        case N_PAIR:
        {
            if(ast->cn > 1)
            {
                printf("(");
                ast_print_pair(ast->child[0]);
                printf(" , ");
                ast_print_pair(ast->child[1]);
                printf(")");
            }
            else
            {
                ast_print_pair(ast->child[0]);
            }
            break;
        }

        case N_INTEGER:
        {
            printf("%s", ast->rep);
            break;
        }

        default: break;
    }
}

static void
do_ast_print_sub (AST_t *ast , int lev , int ident_n)
{
    for(int i = 0;i < ast->cn;++i)
    {
        do_ast_print(ast->child[i] , lev+1 , ident_n+3);
    }
}

/* Print the AST in a human-readable form */
static void
do_ast_print (AST_t *ast , int lev , int ident_n)
{
    puts("");
    for(int i = ident_n-3;i > 0;--i)
    {
        putchar(' ');
    }
    if(lev)
    {
        printf("|- ");
    }

    switch(ast->t)
    {
        case N_PROGRAM:
        {
            printf("program");
            do_ast_print_sub(ast , lev , ident_n);
            break;
        }

        case N_INSTR:
        {
            printf("instr");
            do_ast_print_sub(ast , lev , ident_n);
            break;
        }

        case N_ARGS:
        {
            printf("args");
            do_ast_print_sub(ast , lev , ident_n);
            break;
        }
        
        case N_PAIR:
        {
            printf("pair -> ");
            ast_print_pair(ast);
            break;
        }

        case N_SYMBOL:
        {
            printf("symbol -> %s", ast->rep);
            break;
        }

        case N_INTEGER:
        {
            printf("integer -> %s", ast->rep);
            break;
        }
    }
}

static void
ast_print (AST_t *ast)
{
    do_ast_print(ast , 0 , 0);
    printf("\n\n");
}


typedef enum{
    E_INSTR , E_INTEGER ,
    E_PAIR , E_SYM ,
}expr_t;

typedef struct nexpr_t nexpr_t;

typedef struct ninstr_t{
    nexpr_t *sym;
    nexpr_t **args;
    int arg_n;
}ninstr_t;

typedef struct npair_t{
    nexpr_t *fst;
    nexpr_t *snd;
}npair_t;

struct nexpr_t{
    expr_t t;

    union{
        ninstr_t *instr;
        Int_t num;
        npair_t *pair;
        char *sym;
    };

    nexpr_t *next;
};

static nexpr_t * conv_ast (AST_t *);
static void nexpr_del (nexpr_t *);


static nexpr_t *
newNexpr (expr_t t)
{
    nexpr_t *e = malloc(sizeof(nexpr_t));
    e->t = t;
    e->next = NULL;
}

static void
nsym_del (nexpr_t *e)
{
    free(e->sym);
}

static void
ninstr_del (ninstr_t *ins)
{
    nexpr_del(ins->sym);
    for(int i = 0;i < ins->arg_n;++i)
    {
        nexpr_del(ins->args[i]);
    }
    free(ins);
}

static void
npair_del (npair_t *p)
{
    nexpr_del(p->fst);
    nexpr_del(p->snd);
    free(p);
}

static void
nexpr_del (nexpr_t *e)
{
    while(e)
    {
        switch(e->t)
        {
            case E_INTEGER: break;

            case E_INSTR:
            {
                ninstr_del(e->instr);
                break;
            }

            case E_PAIR:
            {
                npair_del(e->pair);
                break;
            }

            case E_SYM:
            {
                nsym_del(e);
                break;
            }

            default: throw_error("Error in destroying internal expression"); break;
        }

        nexpr_t *tmp = e;
        e = e->next;
        free(tmp);
    }
}

static nexpr_t *
decode_instr (AST_t *ast)
{
    nexpr_t *e = newNexpr(E_INSTR);
    e->instr = malloc(sizeof(ninstr_t));
    e->instr->sym = conv_ast(ast->child[0]);
    e->instr->args = NULL;
    e->instr->arg_n = ast->cn-1;
    if(ast->cn-1 > 0)
    {
        e->instr->args = malloc(sizeof(nexpr_t *) * (ast->cn-1));
        for(int i = 1;i < ast->cn;++i)
        {
            e->instr->args[i-1] = conv_ast(ast->child[i]);
        }
    }

    return e;
}

static nexpr_t *
decode_sym (AST_t *ast)
{
    nexpr_t *e = newNexpr(E_SYM);
    e->sym = (char *)strdup(ast->rep);
    return e;
}

static nexpr_t *
decode_int (AST_t *ast)
{
    nexpr_t *e = newNexpr(E_INTEGER);
    e->num = strtoll(ast->rep , NULL , 10);
    return e;
}

static nexpr_t *
decode_pair (AST_t *ast)
{
    nexpr_t *e = newNexpr(E_PAIR);
    e->pair = malloc(sizeof(npair_t));
    e->pair->fst = e->pair->snd = NULL;
    e->pair->fst = conv_ast(ast->child[0]);
    e->pair->snd = conv_ast(ast->child[1]);
    return e;
}

static nexpr_t *
conv_ast (AST_t *ast)
{
    switch(ast->t)
    {
        case N_PROGRAM:
        {
            nexpr_t *head = conv_ast(ast->child[0]);
            nexpr_t *ptr = head;
            for(int i = 1;i < ast->cn;++i)
            {
                nexpr_t *tmp = conv_ast(ast->child[i]);
                ptr->next = tmp;
                ptr = tmp;
            }
            return head;
        }

        case N_INSTR:   { return decode_instr(ast);}
        case N_ARGS:    { return conv_ast(ast->child[0]);}
        case N_SYMBOL:  { return decode_sym(ast);}
        case N_INTEGER: { return decode_int(ast);}
        case N_PAIR:    { return decode_pair(ast);}

        default: throw_error("Error in converting AST into internal expression"); break;
    }

    return NULL;
}

static void
builtin_push_arg (VM *vm , nexpr_t *v)
{
    switch(v->t)
    {
        case E_INTEGER:
        {
            vm_pushint(vm , v->num);
            break;
        }
        
        case E_PAIR:
        {
            builtin_push_arg(vm , v->pair->snd);
            builtin_push_arg(vm , v->pair->fst);
            vm_pushpair(vm);
            break;
        }
    }
}

static void
builtin_push (VM *vm , ninstr_t *ins)
{
    for(int i = 0;i < ins->arg_n;++i)
    {
        builtin_push_arg(vm , ins->args[i]);
    }
}

static void
builtin_pop (VM *vm , ninstr_t *ins)
{
    vm_pop(vm);
}

static void
builtin_inspect (VM *vm , ninstr_t *ins)
{
    inspectSK(vm);
}

static void
eval_instr (VM *vm , ninstr_t *ins)
{
    const char *sym = ins->sym->sym;
    if(!strcmp("push" , sym))
    { builtin_push(vm , ins);}
    else if(!strcmp("pop" , sym))
    { builtin_pop(vm , ins);}
    else if(!strcmp("inspect" , sym))
    { builtin_inspect(vm , ins);}
    else
    { display_error("Unknown command");}
}

void
vm_debug (VM *vm)
{
    const char *prompt = "> ";
    while((buf = readline(prompt)))
    {
        lex_setup();
        if(!empty_line())
        {
            add_history(buf);
            AST_t *ast = run_parser();
            nexpr_t *e = conv_ast(ast);
            eval_instr(vm , e->instr);
            nexpr_del(e);
            ast_del(ast);
        }
        free(buf);
    }
    puts("");
}

int
main (void)
{
    VM *vm = newVM();
    vm_debug(vm);
    delVM(vm);
}
