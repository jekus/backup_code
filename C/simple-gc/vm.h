#ifndef _VM_H_
#define _VM_H_

typedef long long int Int_t;

typedef enum{
    OBJ_INT , OBJ_PAIR
}OBJ_T;

typedef struct obj{
    OBJ_T t;
    unsigned char marked;

    union{
        Int_t n;

        struct{
            struct obj *fst;
            struct obj *snd;
        };
    };

    struct obj *next;
}Object_t;

#define MAX_SK_SIZE 256
#define INIT_MAXOBJ_N 10
typedef struct{
    Object_t *fstobj;
    Object_t *stack[MAX_SK_SIZE];
    int sk_ptr;

    int Obj_n;
    int maxObj_n;
}VM;

extern VM * newVM (void);
extern void delVM (VM *);
extern void vm_pushint (VM * , Int_t);
extern void vm_pushpair (VM *);
extern Object_t * vm_pop (VM *);

#endif
