#ifndef _NERR_H_
#define _NERR_H_

void display_error (const char *);
void throw_error (const char *);

#endif
