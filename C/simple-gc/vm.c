#include <stdlib.h>
#include "vm.h"
#include "gc.h"
#include "nerr.h"

static Object_t *
newObject (VM *vm , OBJ_T t)
{
    if(vm->Obj_n >= vm->maxObj_n) { gc(vm);}
    
    Object_t *obj = malloc(sizeof(Object_t));
    obj->t = t;
    obj->marked = 0;
    obj->next = vm->fstobj;
    vm->fstobj = obj;
    ++(vm->Obj_n);
    return obj;
}

static Object_t *
newInt (VM *vm , Int_t n)
{
    Object_t *obj = newObject(vm , OBJ_INT);
    obj->n = n;
    return obj;
}

static Object_t *
newPair (VM *vm)
{
    Object_t *obj = newObject(vm , OBJ_PAIR);
    obj->fst = vm_pop(vm);
    obj->snd = vm_pop(vm);
    return obj;
}

VM *
newVM (void)
{
    VM *vm = malloc(sizeof(VM));
    vm->fstobj = NULL;
    vm->sk_ptr = 0;
    vm->Obj_n = 0;
    vm->maxObj_n = INIT_MAXOBJ_N;
    return vm;
}

static void
delObj (Object_t *obj)
{
    free(obj);
}

void
delVM (VM *vm)
{
    Object_t *ptr = vm->fstobj;
    while(ptr)
    {
        Object_t *tmp = ptr;
        ptr = ptr->next;
        delObj(tmp);
    }
    free(vm);
}

static void
vm_push (VM *vm , Object_t *obj)
{
    if(vm->sk_ptr == MAX_SK_SIZE)
    { display_error("Stack overflow");}
    else
    { vm->stack[(vm->sk_ptr)++] = obj;}
}

void
vm_pushint (VM *vm , Int_t n)
{
    vm_push(vm , newInt(vm , n));
}

void
vm_pushpair (VM *vm)
{
    vm_push(vm , newPair(vm));
}

Object_t *
vm_pop (VM *vm)
{
    if(!(vm->sk_ptr))
    {
        display_error("Stack underflow");
        return NULL;
    }
    else
    { return vm->stack[--(vm->sk_ptr)];}
}
