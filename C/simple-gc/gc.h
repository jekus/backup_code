#ifndef _GC_H_
#define _GC_H_

#include "vm.h"

void mark (Object_t *);
void unmark (Object_t *);
void gc (VM *);

#endif
