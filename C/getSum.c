/*
    Calculate the sum of two integer
    without the use of '+' or '-'.
*/
#include <stdio.h>

static int
getSum (int sum , int carry)
{
    return !carry ? sum :
        getSum(sum ^ carry , (sum & carry) << 1);
}

int
main (void)
{
    int a , b;

    while(scanf("%d%d", &a , &b) == 2)
        printf("%d + %d is %d\n", a , b , getSum(a , b));

    return 0;
}
