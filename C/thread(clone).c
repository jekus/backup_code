#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/syscall.h>
#define STACK_SIZE (1024 * 1024)
#define errExit(str) do{perror(str);exit(1);}while(0)

static int childfunc(void * arg)
{
	printf("--------Child--------\nPID: %d\nTID: %ld (0x%X)\n---------------------\n",getpid(),syscall(__NR_gettid),(unsigned int)syscall(__NR_gettid));

	return 0;
}

int main (int argc , char ** argv)
{
	char *stack = malloc(STACK_SIZE);
	if (!stack)
		errExit("Line 18: Malloc Error");

	int pid;
	printf("--------Parent--------\nPID: %d\nTID: %ld (0x%X)\n----------------------\n\n",getpid(),syscall(__NR_gettid),(unsigned int)syscall(__NR_gettid));

	pid = clone(childfunc , stack + STACK_SIZE , SIGCHLD , NULL);
	if (pid == -1)
		errExit("Line 29: Clone Error");

	sleep(1);

	if (waitpid(pid , NULL , 0) == -1)
		errExit("Waitpid Error");
	printf("Child has terminated\n");
	exit(0);
}
