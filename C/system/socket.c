#include <sys/socket.h>
#include "sys.h"

int
Socket (int namespace , int style , int protocol)
{
    int skfd = socket(namespace , style , protocol);
    if(skfd < 0)
        sys_err("Can't create socket\n");
    return skfd;
}

void
Bind (int sk , struct sockaddr *addr , socklen_t len)
{
    if(bind(sk , addr , len) < 0)
        sys_err("Can't bind the address to socket\n");
}

void
Connect (int sk , struct sockaddr *addr , socklen_t len)
{
    if(connect(sk , addr , len) < 0)
        sys_err("Can't connect to the server\n");
}

void
Listen (int sk , int n)
{
    if(listen(sk , n) < 0)
        sys_err("Can't listen on the socket\n");
}

int
Accept (int sk , struct sockaddr *addr , socklen_t *len_ptr)
{
    int conn_sk = accept(sk , addr , len_ptr);
    if(conn_sk < 0)
        sys_err("Can't accept the connection\n");
    return conn_sk;
}
