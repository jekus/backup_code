#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "sys.h"

#define PROMPT "> "

int
main (int argc , char **argv)
{
    int skfd;
    char *textbuf;
    struct sockaddr_in servaddr;

    if(argc != 2)
        sys_err("Usage: %s <ADDRESS>\n", *argv);
    skfd = Socket(AF_INET , SOCK_STREAM , 0);

    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(3333);
    if(inet_pton(AF_INET , argv[1] , &servaddr.sin_addr) <= 0)
        sys_err("Error in 'inet_pton()'\n");

    Connect(skfd , (struct sockaddr *)&servaddr , sizeof(servaddr));
    puts("- Connection established");
    while((textbuf = readline(PROMPT)) && strncmp(textbuf , "EOF" , 3))
        write(skfd , textbuf , sizeof(char) * strlen(textbuf)) ,
        free(textbuf);
    write(skfd , "EOF" , sizeof(char) * 3);

    return 0;
}
