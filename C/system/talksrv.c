#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include "sys.h"

#define REV_PREFIX "- "
#define BUF_SIZE 256

int
main (void)
{
    int listenfd , connfd;
    char textbuf[BUF_SIZE];
    struct sockaddr_in servaddr;

    listenfd = Socket(AF_INET , SOCK_STREAM , 0);
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(3333);

    Bind(listenfd , (struct sockaddr *)&servaddr , sizeof(servaddr));
    Listen(listenfd , 10);
    connfd = Accept(listenfd , NULL , NULL);
    
    int read_in;
    while((read_in = read(connfd , textbuf ,
        sizeof(char) * BUF_SIZE)) >= 0 &&
        memcmp(textbuf , "EOF" , sizeof(char) * 3))
            write(STDOUT_FILENO , textbuf , read_in) ,
            puts("");

    puts("\n- End-Of-File -");
    return 0;
}
