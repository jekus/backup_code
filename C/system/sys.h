#ifndef __SYS_H__
#define __SYS_H__

extern void sys_err (const char *fmt , ...);
extern int Socket (int namespace , int style , int protocol);
extern void Bind (int sk , struct sockaddr *addr , socklen_t len);
extern void Connect (int sk , struct sockaddr *addr , socklen_t len);
extern void Listen (int sk , int n);
extern int Accept (int sk , struct sockaddr *addr , socklen_t *len_ptr);

#endif
