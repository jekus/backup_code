#include <apue.h>
#define RWX (S_IRUSR | S_IWUSR | S_IXUSR)
#define PATH_MAX 4096

int main (void)
{
	char pathname[PATH_MAX];
	for(int i = 1 ; i <= 1000 ; i++)
	{
		if(getcwd(pathname , PATH_MAX) != NULL)
			puts(pathname);
		else
			err_quit("pathname has longer than 'PATH_MAX'");

		if(mkdir("XYT" , RWX) < 0)
			err_quit("can't creat directory");
		if(chdir("XYT") < 0)
			err_quit("can't change directory");
	}

	exit(0);
}
