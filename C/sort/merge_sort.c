#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/*
    All 'begin' , 'mid' , 'end' below
    are all the VALID index of 'ar'.
*/


static void
Merge (
    int *ar , int begin , int mid ,
    int end , int *work_ar)
{
    int l_index = begin , r_index = mid+1;
    for(int index = begin;index <= end;++index)
        if(r_index >= end+1 || (l_index <= mid &&
            ar[l_index] <= ar[r_index]))
                work_ar[index] = ar[l_index++];
        else
            work_ar[index] = ar[r_index++];
}


/* Top-Down merge sort */
static void
TopDownSplitMerge (
    int *ar , int begin ,
    int end , int *work_ar)
{
    if(begin < end)
    {
        int mid = begin + (end - begin)/2;  /* prevent overflow */
        TopDownSplitMerge(ar , begin , mid , work_ar);
        TopDownSplitMerge(ar , mid+1 , end , work_ar);
        Merge(ar , begin , mid , end , work_ar);
        memcpy(ar+begin ,work_ar+begin ,
            sizeof(int) * (end-begin+1));
    }
}

void
TopDownMergeSort (int *ar , int size)
{
    int *work_ar = malloc(sizeof(int) * size);
    TopDownSplitMerge(ar , 0 , size-1 , work_ar);
    free(work_ar);
}
/* --- */


#define min(x,y) ((x) > (y) ? (y) : (x))
void
BottomUpMergeSort (int *ar , int size)
{
    int *work_ar = malloc(sizeof(int) * size);

    for(int width = 1;width < size;width <<= 1)
    {
        for(int subar_idx = 0;subar_idx < size;subar_idx += (width << 1))
            Merge(ar , subar_idx ,
                min(subar_idx + width - 1 , size-1) ,
                min(subar_idx + (width << 1) - 1 , size-1) ,
                work_ar);

        memcpy(ar , work_ar ,
            sizeof(int) * size);
    }

    free(work_ar);
}
