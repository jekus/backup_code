#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <error.h>
#include <errno.h>

#define SIZE 100000000L

typedef void (*Sort_Func) (int * , int);

extern void TopDownMergeSort (int * , int);
extern void BottomUpMergeSort (int * , int);
extern void QuickSort (int * , int);

extern void gene_random_ar (int * , int , int);


static void
test (int *data , Sort_Func func ,
    const char *algo_name)
{
    gene_random_ar(data , SIZE , 1000000);

    long int s_time = clock();
    func(data , SIZE);
    printf("%s: %Lfs\n", algo_name ,
        (long double)(clock() - s_time)/CLOCKS_PER_SEC);
}

int
main (void)
{
    int *data = malloc(sizeof(int) * SIZE);
    if(!data)
        error(1 , errno , "ERROR");

    test(data , TopDownMergeSort ,
        "TopDownMergeSort");
    test(data , BottomUpMergeSort ,
        "BottomUpMergeSort");
    test(data , QuickSort ,
        "QuickSort");

    return 0;
}
