#include <stdint.h>

static void
swap (int *a , int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void
QuickSort (int *ar , int size)
{
    if(size < 2)
        return;

    int key = ar[size/2];
    swap(ar+size/2 , ar+size-1);
    int k_pos = 0;
    for(int idx = 0;idx < size-1;++idx)
        if(ar[idx] < key)
            swap(ar+idx , ar+k_pos) ,
            ++k_pos;

    swap(ar+k_pos , ar+size-1);
    QuickSort(ar , k_pos);
    QuickSort(ar+k_pos+1 , size-k_pos-1);
}
