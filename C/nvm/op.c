#include "nvm.h"
#include "op.h"
#include "nvm_error.h"

static void
check_sk_oprc (NVM_REC *vm , const char *op_name , int oprc)
{
    assert_expr(vm->eval_sk_pt >= oprc-1 ,
                "[%s] can't get enough data from stack" , op_name);
}

static WORD_T
fetch_data (NVM_REC *vm)
{
    int addr = ++(vm->inst_pt);
    return (vm->program[addr]);
}

static void
op_halt (NVM_REC *vm)
{
    vm_stop(vm);
}

static void
op_push (NVM_REC *vm)
{
    WORD_T data = fetch_data(vm);
    int sk_pt = ++(vm->eval_sk_pt);
    assert_expr(sk_pt < EVAL_SK_SIZE ,
                "[PUSH] evaluation stack overflow");
    vm->eval_stack[sk_pt] = data;
}

static void
op_pop (NVM_REC *vm)
{
    int sk_pt = --(vm->eval_sk_pt);
    assert_expr(sk_pt >= -1 ,
                "[POP] evaluation stack overflow");
}

static void
op_dup (NVM_REC *vm)
{
    WORD_T offset_from_top = fetch_data(vm);
    int dup_sk_pt = vm->eval_sk_pt - offset_from_top;
    assert_expr(dup_sk_pt >= 0 ,
                "[DUP] invalid stack index");

    int sk_pt = ++(vm->eval_sk_pt);
    assert_expr(sk_pt < EVAL_SK_SIZE ,
                "[DUP] evaluation stack overflow");
    vm->eval_stack[sk_pt] = vm->eval_stack[dup_sk_pt];
}

static void
op_rep (NVM_REC *vm)
{
    WORD_T offset_from_top = fetch_data(vm);
    int rep_sk_pt = vm->eval_sk_pt - offset_from_top;

    /*
       If 'vm->eval_sk_pt' is not a valid index ,
       'rep_sk_pt' must also be invalid.
    */
    assert_expr(rep_sk_pt >= 0 ,
                "[REP] invalid stack index");

    WORD_T cur_top = vm->eval_stack[vm->eval_sk_pt];
    vm->eval_stack[rep_sk_pt] = cur_top;

    if(offset_from_top != 0)
    {   --(vm->eval_sk_pt); }
}

static void
op_add (NVM_REC *vm)
{
    check_sk_oprc(vm , "ADD" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    --sk_pt;
    vm->eval_stack[sk_pt] = top_ele + sec_ele;
    vm->eval_sk_pt = sk_pt;
}

static void
op_mul (NVM_REC *vm)
{
    check_sk_oprc(vm , "MUL" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    --sk_pt;
    vm->eval_stack[sk_pt] = top_ele * sec_ele;
    vm->eval_sk_pt = sk_pt;
}

static void
op_sub (NVM_REC *vm)
{
    check_sk_oprc(vm , "SUB" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    --sk_pt;
    vm->eval_stack[sk_pt] = top_ele - sec_ele;
    vm->eval_sk_pt = sk_pt;
}

static void
op_div (NVM_REC *vm)
{
    check_sk_oprc(vm , "DIV" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];
    assert_expr(sec_ele != 0 ,
                "[DIV] division by zero");

    --sk_pt;
    vm->eval_stack[sk_pt] = top_ele / sec_ele;
    vm->eval_sk_pt = sk_pt;
}

static int
get_opargc (NVM_REC *vm)
{
    return (int)((vm->program[vm->inst_pt] & OP_ARGC_MASK) >> OP_ARGC_OFFSET);
}

static void
skip_nextinst (NVM_REC *vm)
{
    ++(vm->inst_pt);
    vm->inst_pt += get_opargc(vm);
}

static void
op_eq (NVM_REC *vm)
{
    check_sk_oprc(vm , "EQ" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    if(top_ele == sec_ele)
    {   vm->inst_pt = vm->inst_pt; }
    else
    {   skip_nextinst(vm); }
    vm->eval_sk_pt -= 2;
}

static void
op_lt (NVM_REC *vm)
{
    check_sk_oprc(vm , "LT" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    if(top_ele < sec_ele)
    {   vm->inst_pt = vm->inst_pt; }
    else
    {   skip_nextinst(vm); }
    vm->eval_sk_pt -= 2;
}

static void
op_lq (NVM_REC *vm)
{
    check_sk_oprc(vm , "LQ" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    if(top_ele <= sec_ele)
    {   vm->inst_pt = vm->inst_pt; }
    else
    {   skip_nextinst(vm); }
    vm->eval_sk_pt -= 2;
}

static void
op_and (NVM_REC *vm)
{
    check_sk_oprc(vm , "AND" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    --sk_pt;
    vm->eval_stack[sk_pt] = top_ele & sec_ele;
    vm->eval_sk_pt = sk_pt;
}

static void
op_or (NVM_REC *vm)
{
    check_sk_oprc(vm , "OR" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    --sk_pt;
    vm->eval_stack[sk_pt] = top_ele | sec_ele;
    vm->eval_sk_pt = sk_pt;
}
    
static void
op_xor (NVM_REC *vm)
{
    check_sk_oprc(vm , "XOR" , 2);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt] ,
           sec_ele = vm->eval_stack[sk_pt-1];

    --sk_pt;
    vm->eval_stack[sk_pt] = top_ele ^ sec_ele;
    vm->eval_sk_pt = sk_pt;
}

static void
op_not (NVM_REC *vm)
{
    check_sk_oprc(vm , "NOT" , 1);

    int sk_pt = vm->eval_sk_pt;
    WORD_T top_ele = vm->eval_stack[sk_pt];
    vm->eval_stack[sk_pt] = ~top_ele;
}

static void
op_goto (NVM_REC *vm)
{
    WORD_T addr = fetch_data(vm);
    vm->inst_pt = addr - 1;
}

static void
push_ret_frag (NVM_REC *vm)
{
    int sk_pt = ++(vm->ret_sk_pt);
    assert_expr(sk_pt < RET_SK_SIZE ,
                "[CALL] return stack overflow");
    RET_FRAG *rf = vm->ret_stack + sk_pt;
    rf->ret_eval_sk_pt = vm->eval_sk_pt;
    rf->ret_addr = vm->inst_pt;
}

static void
op_call (NVM_REC *vm)
{
    WORD_T new_addr = fetch_data(vm);
    push_ret_frag(vm);
    vm->inst_pt = new_addr - 1;
}

static void
op_ret (NVM_REC *vm)
{
    int sk_pt = vm->ret_sk_pt;
    assert_expr(sk_pt >= 0 ,
                "[RET] no return fragment remained");
    RET_FRAG *rf = vm->ret_stack + sk_pt;
    vm->eval_sk_pt = rf->ret_eval_sk_pt;
    vm->inst_pt = rf->ret_addr;
    --(vm->ret_sk_pt);
}


typedef void (*op_func) (NVM_REC *);

static op_func const op_set[] = {
    op_halt ,
    op_push , op_pop ,
    op_dup  , op_rep ,
    op_add  , op_mul ,
    op_sub  , op_div ,
    op_eq   , op_lt  ,
    op_lq   ,
    op_and  , op_or  ,
    op_xor  , op_not ,
    op_goto ,
    op_call , op_ret ,
};

void
exec_op (NVM_REC *vm)
{
    op_set[vm->cur_op](vm);
}
