#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define __USE_GNU
#include <search.h>
#undef __USE_GNU

#include "../nvm.h"
#include "../opcode.h"


/*
    Record the start point of a LABEL.
*/
#define LBLTB_SIZE      (80)
struct hsearch_data label_tb;

/*
   Record the start point of a procedure.
*/
#define PROCTB_SIZE     (80)
struct hsearch_data proc_tb;

/*
    Record the opcode of the
    corresponding OP.
*/
#define OPTB_SIZE       (80)
struct hsearch_data optb;

typedef struct{
    const char *op;
    const WORD_T opcode;
}OP_Pair;

static const OP_Pair optb_init_set[] = {
    {"halt"     ,   HALT    }   ,
    {"push"     ,   PUSH    }   ,   {"pop"      ,   POP     } ,
    {"dup"      ,   DUP     }   ,   {"rep"      ,   REP     } ,
    {"add"      ,   ADD     }   ,   {"mul"      ,   MUL     } ,
    {"sub"      ,   SUB     }   ,   {"div"      ,   DIV     } ,
    {"eq"       ,   EQ      }   ,
    {"lt"       ,   LT      }   ,   {"lq"       ,   LQ      } ,
    {"and"      ,   AND     }   ,   {"or"       ,   OR      } ,
    {"xor"      ,   XOR     }   ,   {"not"      ,   NOT     } ,
    {"goto"     ,   GOTO    }   ,   {"call"     ,   CALL    } ,
    {"ret"      ,   RET     }   ,
};


static void
optb_init (void)
{
    ENTRY et , *et_ptr;
    const int optb_size = sizeof(optb_init_set) / sizeof(OP_Pair);
    for(int idx = 0;idx < optb_size;++idx)
    {
        et.key = (void *)(optb_init_set[idx].op);
        et.data = (void *)&(optb_init_set[idx].opcode);
        hsearch_r(et , ENTER , &et_ptr , &optb);
    }
}

void
hashtb_init (void)
{
    memset(&label_tb , 0 , sizeof(struct hsearch_data));
    memset(&proc_tb , 0 , sizeof(struct hsearch_data));
    memset(&optb , 0 , sizeof(struct hsearch_data));
    hcreate_r(LBLTB_SIZE , &label_tb);
    hcreate_r(PROCTB_SIZE , &proc_tb);
    hcreate_r(OPTB_SIZE , &optb);
    optb_init();
}
