# NVM Assembly Language

<br />

## Syntax
**DEFINE:** @ = empty string

<table>
<tr>
<td align="center">symbol</td>
<td align="center">[a-zA-Z_]<sup>+</sup> [a-zA-Z0-9_]<sup>*</sup></td>
</tr>

<tr>
<td align="center">integer</td>
<td align="center">[0-9]<sup>+</sup></td>
</tr>

<tr>
<td align="center">label</td>
<td align="center">'.' symbol</td>
</tr>

<tr>
<td align="center">label_def</td>
<td align="center">label ':'</td>
</tr>

<tr>
<td align="center">proc_def</td>
<td align="center">symbol ':'</td>
</tr>

<tr>
<td align="center">operand</td>
<td align="center">integer | label | symbol | @</td>
</tr>

<tr>
<td align="center">instr_head</td>
<td align="center">(label_def | proc_def) '\n' | @</td>
</tr>

<tr>
<td align="center">instr</td>
<td align="center">symbol operand '\n' | @</td>
</tr>

<tr>
<td align="center">program</td>
<td align="center">instr_head<sup>*</sup> instr program EOF</td>
</tr>
</table>
