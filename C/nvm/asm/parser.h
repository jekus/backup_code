#ifndef _NVM_AS_PARSER_H_
#define _NVM_AS_PARSER_H_

#include "lex.h"
#include "ast.h"

extern ast_node * parse_program (InputRec * const _ir);

#endif
