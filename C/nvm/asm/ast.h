#ifndef _NVM_AS_AST_H_
#define _NVM_AS_AST_H_

typedef enum{
    /* Non-Terminal */
    N_PROGRAM       ,
    N_INSTR         ,   N_INSTR_HEAD    ,
    N_OPERAND       ,
    N_LABEL_DEF     ,   N_PROC_DEF      ,

    /* Terminal */
    N_LABEL         ,
    N_INTEGER       ,   N_SYMBOL        ,
}Node_t;

typedef struct ast_node{
    Node_t t;
    const char *rep;
    int cn;
    struct ast_node **n_child;
}ast_node;


extern ast_node *  cons_ast_node       (Node_t _t);
extern void        ast_del             (ast_node *_node);
extern void        ast_insert_child    (ast_node *_par , ast_node *_ch);

#endif
