/*
    Assembler for the NVM.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>

#define __USE_GNU
#include <search.h>

#include "../nvm.h"
#include "../nvm_error.h"
#include "../nvm_mem.h"
#include "../opcode.h"
#include "../neff.h"
#include "hashtb.h"
#include "lex.h"
#include "ast.h"
#include "parser.h"


typedef struct{
    WORD_T *outbuf;
    int outbuf_size;
    int outbuf_idx;
}OutRec;

InputRec *ir = NULL;
OutRec *or = NULL;

static char *infile = NULL;
static FILE *is = NULL;
static FILE *os = NULL;


static OutRec *
new_or (void)
{
#define DEF_OUTBUF_SIZE (1024)

    OutRec *newor = xmalloc(sizeof(OutRec));
    newor->outbuf = xmalloc(sizeof(WORD_T) * DEF_OUTBUF_SIZE);
    newor->outbuf_size = DEF_OUTBUF_SIZE;
    newor->outbuf_idx = 0;

#undef DEF_OUTBUF_SIZE
}

static void
parse_arg (int argc , char **argv)
{
    if(argc < 2)
    {
        is = stdin;
        os = stdout;
    }
    else
    {
        ++argv;
        FILE *in = fopen(*argv , "r");
        if(in)
        {
            is = in;
            infile = *argv;
            ++argv;
            if(*argv)
            {
                FILE *out = fopen(*argv , "wb+");
                if(out)
                {   os = out; }
                else
                {   nvm_error("[ARGS_PARSER] can't open file '%s'" , *argv); }
            }
            else
            {
                os = stdout;
            }
        }
        else
        {
            nvm_error("[ARGS_PARSER] can't open file '%s'" , *argv);
        }
    }
}

static void
resize_outbuf (void)
{
#define RESIZE_STEP     (512)

    int new_size = or->outbuf_size + RESIZE_STEP;
    or->outbuf = xrealloc(or->outbuf , sizeof(WORD_T) * new_size);
    or->outbuf_size = new_size;

#undef RESIZE_STEP
}

static void
push_out_loc (WORD_T item)
{
    int outbuf_size = or->outbuf_size;
    or->outbuf[(or->outbuf_idx)++] = item;
    if(or->outbuf_idx == outbuf_size)
    {
        resize_outbuf();
    }
}


static int program_unit = 0;

static void
def_label (const char *lb_name)
{
    WORD_T *lb_sp = xmalloc(sizeof(WORD_T));
    *lb_sp = program_unit;

    ENTRY et = {
        .key = (void *)n_strdup(lb_name) ,
        .data = lb_sp
    };
    ENTRY *et_ptr = NULL;
    hsearch_r(et , FIND, &et_ptr , &label_tb);
        
    if(et_ptr)
    {   nvm_error("label '%s' redefined" , lb_name); }
    else
    {   hsearch_r(et , ENTER , &et_ptr , &label_tb); }
}

static void
def_proc (const char *proc_name)
{
    WORD_T *proc_sp = xmalloc(sizeof(WORD_T));
    *proc_sp = program_unit;

    ENTRY et = {
        .key = (void *)n_strdup(proc_name) ,
        .data = proc_sp
    };
    ENTRY *et_ptr = NULL;
    hsearch_r(et , FIND , &et_ptr , &proc_tb);
        
    if(et_ptr)
    {   nvm_error("procedure '%s' redefined" , proc_name); }
    else
    {   hsearch_r(et , ENTER , &et_ptr , &proc_tb); }
}

static void
collect_ast_info (ast_node *node)
{
    for(int i = 0;i < node->cn;++i)
    {
        collect_ast_info(node->n_child[i]);
    }

    switch(node->t)
    {
        case N_LABEL_DEF:
            def_label(node->rep);
            break;

        case N_PROC_DEF:
            def_proc(node->rep);
            break;

        case N_LABEL:
        case N_INTEGER:
        case N_SYMBOL:
            ++program_unit;
            break;

        default:
            break;
    }
}

static void
validate_instr_head (ast_node *node)
{
    int proc_def_n = 0;
    for(int i = 0;i < node->cn;++i)
    {
        if(node->n_child[i]->t == N_PROC_DEF)
        {
            ++proc_def_n;
            if(proc_def_n > 1)
            {
                nvm_error("can't define more than one procedure "
                          "in the same location");
            }
        }
    }
}

static void
check_op_argc (ast_node *opr , int tar_argc ,
        const char *tp , ...)
{
    if(opr->cn != tar_argc)
    {
        va_list vl;
        va_start(vl , tp);
        nvm_verror(tp , vl);
        va_end(vl);
    }
}

static void
validate_op_halt (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'halt' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_push (ast_node *opr)
{
    check_op_argc(opr , 1 ,
            "incorrect argument number for 'push' - "
            "expect 1 , got %d" , opr->cn);

    switch(opr->n_child[0]->t)
    {
        case N_INTEGER:
            break;

        default:
            nvm_error("incorrect argument type for 'push'");
    }
}

static void
validate_op_pop (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'pop' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_dup (ast_node *opr)
{
    check_op_argc(opr , 1 ,
            "incorrect argument number for 'dup' - "
            "expect 1 , got %d" , opr->cn);

    switch(opr->n_child[0]->t)
    {
        case N_INTEGER:
            break;

        default:
            nvm_error("incorrect argument type for 'dup'");
    }
}

static void
validate_op_rep (ast_node *opr)
{
    check_op_argc(opr , 1 ,
            "incorrect argument number for 'rep' - "
            "expect 1 , got %d" , opr->cn);

    switch(opr->n_child[0]->t)
    {
        case N_INTEGER:
            break;

        default:
            nvm_error("incorrect argument type for 'dup'");
    }
}

static void
validate_op_add (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'add' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_mul (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'mul' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_sub (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'sub' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_div (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'div' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_eq (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'eq' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_lt (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'lt' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_lq (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'lq' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_and (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'and' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_or (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'or' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_xor (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'xor' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_op_not (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'not' - "
            "expect 0 , got %d" , opr->cn);
}

static void
validate_label (const char *lb_name)
{
    ENTRY et = {
        .key = (void *)lb_name ,
        .data = NULL
    };
    ENTRY *et_ptr = NULL;
    hsearch_r(et , FIND , &et_ptr , &label_tb);

    if(!et_ptr)
    {
        nvm_error("unknown label '%s'" , lb_name);
    }
}

static void
validate_op_goto (ast_node *opr)
{
    check_op_argc(opr , 1 ,
            "incorrect argument number for 'goto' - "
            "expect 1 , got %d" , opr->cn);

    switch(opr->n_child[0]->t)
    {
        case N_LABEL:
            validate_label(opr->n_child[0]->rep);
            break;

        default:
            nvm_error("incorrect argument type for 'goto'");
    }
}

static void
validate_proc (const char *proc_name)
{
    ENTRY et = {
        .key = (void *)proc_name ,
        .data = NULL
    };
    ENTRY *et_ptr = NULL;
    hsearch_r(et , FIND , &et_ptr , &proc_tb);

    if(!et_ptr)
    {
        nvm_error("unknown procedure '%s'" , proc_name);
    }
}

static void
validate_op_call (ast_node *opr)
{
    check_op_argc(opr , 1 ,
            "incorrect argument number for 'call' - "
            "expect 1 , got %d" , opr->cn);

    switch(opr->n_child[0]->t)
    {
        case N_SYMBOL:
            validate_proc(opr->n_child[0]->rep);
            break;

        default:
            nvm_error("incorrect argument type for 'call'");
    }
}

static void
validate_op_ret (ast_node *opr)
{
    check_op_argc(opr , 0 ,
            "incorrect argument number for 'ret' - "
            "expect 0 , got %d" , opr->cn);
}


typedef void (*validate_func) (ast_node *);

static validate_func validate_tb [] = {
    validate_op_halt  ,

    validate_op_push  ,  validate_op_pop  ,
    validate_op_dup   ,  validate_op_rep  ,

    validate_op_add   ,  validate_op_mul  ,
    validate_op_sub   ,  validate_op_div  ,

    validate_op_eq    ,  validate_op_lt   ,
    validate_op_lq    ,

    validate_op_and   ,  validate_op_or   ,
    validate_op_xor   ,  validate_op_not  ,

    validate_op_goto  ,  validate_op_call ,
    validate_op_ret   ,
};

static WORD_T
get_opcode (const char *op_name)
{
    ENTRY et = {
        .key = (void *)op_name,
        .data = NULL
    };
    ENTRY *et_ptr = NULL;
    hsearch_r(et , FIND , &et_ptr , &optb);

    return (et_ptr ?
            *(WORD_T *)(et_ptr->data) :
            (WORD_T)-1);
}

static void
validate_instr (ast_node *node)
{
    const char *opt = node->n_child[0]->rep;
    ast_node *opr = node->n_child[1];
    WORD_T opcode = get_opcode(opt);
    if(opcode == -1)
    {   nvm_error("unknown operator '%s'" , opt); }
    else
    {   validate_tb[opcode](opr); }
}

static void
validate_ast (ast_node *node)
{
    for(int i = 0;i < node->cn;++i)
    {
        ast_node *c_node = node->n_child[i];
        switch(c_node->t)
        {
            case N_INSTR_HEAD:
                validate_instr_head(c_node);
                break;

            case N_INSTR:
                validate_instr(c_node);
                break;
        }
    }
}

static void
gen_integer (ast_node *node)
{
    push_out_loc(strtoll(node->rep , NULL , 10));
}

static WORD_T
get_label_sp (const char *lb_name)
{
    ENTRY et = {
        .key = (void *)lb_name,
        .data = NULL
    };
    ENTRY *et_ptr = NULL;
    hsearch_r(et , FIND , &et_ptr , &label_tb);

    return *(WORD_T *)(et_ptr->data);
}

static void
gen_label (ast_node *node)
{
    const char *lb_name = node->rep;
    push_out_loc(get_label_sp(lb_name));
}

static WORD_T
get_proc_sp (const char *proc_name)
{
    ENTRY et = {
        .key = (void *)proc_name,
        .data = NULL
    };
    ENTRY *et_ptr = NULL;
    hsearch_r(et , FIND , &et_ptr , &proc_tb);

    if(et_ptr)
    {   return *(WORD_T *)(et_ptr->data); }
    else
    {   nvm_error("missing definition for procedure '%s'" , proc_name); }
}

static void
gen_proc (ast_node *node)
{
    const char *proc_name = node->rep;
    push_out_loc(get_proc_sp(proc_name));
}

static void
gen_operand (ast_node *node)
{
    for(int i = 0;i < node->cn;++i)
    {
        ast_node *opr = node->n_child[i];
        switch(opr->t)
        {
            case N_INTEGER:
                gen_integer(opr);
                break;

            case N_LABEL:
                gen_label(opr);
                break;

            case N_SYMBOL:
                gen_proc(opr);
                break;
        }
    }
}

static WORD_T
cons_opcode (WORD_T opcode , const int argc)
{
    WORD_T args_bits = (~(WORD_T)0 << OP_ARGC_BITS) | (WORD_T)argc;
    int args_offset = sizeof(WORD_T) * 8 - OP_ARGC_BITS;
    return ((args_bits << args_offset) | opcode);
}

static void
gen_instr (ast_node *node)
{
    int op_idx = (node->n_child[0]->t == N_SYMBOL ? 0 : 1);
    const char *op_name = node->n_child[op_idx]->rep;
    WORD_T opcode = get_opcode(op_name);
    push_out_loc(cons_opcode(opcode , node->n_child[op_idx+1]->cn));
    gen_operand(node->n_child[op_idx+1]);
}

static void
gen_code (ast_node *node)
{
    for(int i = 0;i < node->cn;++i)
    {
        ast_node *c_node = node->n_child[i];
        switch(c_node->t)
        {
            case N_INSTR:
                gen_instr(c_node);
                break;
        }
    }
}

static void
export_neff_header ()
{
    NeffHeader *new_h =
        new_neffhd(
                sizeof(NeffHeader) ,
                sizeof(WORD_T) * or->outbuf_idx ,
                get_proc_sp("main"));

    if(fwrite(new_h , sizeof(NeffHeader) ,
                1 , os) != 1)
    {
        nvm_error("error in writing NEFF header to file");
    }
}

static void
export_neff_program ()
{
    int outnum = or->outbuf_idx;
    if(fwrite(or->outbuf , sizeof(WORD_T) , outnum , os) != outnum)
    {   nvm_error("error in writing program to file"); }
    else
    {   or->outbuf_idx = 0; }
}

static void
export_neff (void)
{
    export_neff_header();
    export_neff_program();
}

static void
asm_init (int argc , char **argv)
{
    set_errprefix("nvm_as:");
    parse_arg(argc , argv);
    hashtb_init();
    program_unit = 0;

    ir = start_lex(is , infile);
    or = new_or();
}

static void
nvm_asm (int argc , char **argv)
{
    asm_init(argc , argv);
    ast_node *root = parse_program(ir);
    collect_ast_info(root);
    validate_ast(root);
    gen_code(root);
    ast_del(root);
    export_neff();
}

int
main (int argc , char **argv)
{
    nvm_asm(argc , argv);
    return 0;
}
