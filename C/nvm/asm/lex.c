#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "../nvm_error.h"
#include "../nvm_mem.h"
#include "lex.h"


struct inputrec{
    const char *buf;
    size_t buf_size;
    int buf_idx;
    char cur_ch;
    int cur_line;
};

Token_t cur_token = {T_EOF , NULL};


static InputRec *
new_ir (void)
{
    InputRec *ir = xmalloc(sizeof(InputRec));
    ir->buf = NULL;
    ir->buf_size = -1;
    ir->buf_idx = -1;
    ir->cur_ch = '\0';
    ir->cur_line = 1;
    return ir;
}

static int
iseof (InputRec * const ir)
{
    return (ir->buf_idx >= ir->buf_size);
}

static int
isnewline (InputRec * const ir)
{
    return (ir->cur_ch == '\n');
}

static void
next_char (InputRec * const ir)
{
    ++(ir->buf_idx);
    if(!iseof(ir))
    {   ir->cur_ch = ir->buf[ir->buf_idx]; }
}

static InputRec *
read_from_file (FILE *is , const char *infile)
{
    struct stat file_stat;
    if(stat(infile , &file_stat) == 0)
    {
        size_t buf_size = file_stat.st_size;
        void *buf = xmalloc(buf_size);
        if(fread(buf , buf_size , 1 , is) == 1)
        {
            InputRec *ir = new_ir();
            ir->buf = buf;
            ir->buf_size = buf_size;
            return ir;
        }
        else
        {
            nvm_error("[LEXER] error in reading file '%s' into buffer" , infile);
        }
    }
    else
    {
        nvm_error("[LEXER] can't get the size of file '%s'" , infile);
    }
}

static int
isblank_line (const char *line)
{
    const char *p = line;
    for(;isspace(*p);++p)
        ;
    return (*p == '\0');
}

static char *
read_nonblank_line (const char *prompt)
{
    char *line = readline(prompt);

    while(line && isblank_line(line))
    {
        free(line);
        line = readline(prompt);
    }

    return line;
}

static InputRec *
read_from_stdin (void)
{
    char *buf = NULL; 
    int buf_size = 0;
    char *line = NULL;

    while((line = read_nonblank_line(NULL)))
    {
        add_history(line);
        int line_l = strlen(line);
        int new_size = buf_size + line_l + 1;
        buf = xrealloc(buf , sizeof(char) * new_size);
        memcpy(buf+buf_size , line , sizeof(char) * line_l);
        buf_size = new_size;
        buf[buf_size-1] = '\n';

        free(line);
    }

    if(ferror(stdin))
    {   nvm_error("[LEXER] error in reading program from STDIN"); }
    else
    {
        InputRec *ir = new_ir();
        ir->buf = buf;
        ir->buf_size = buf_size;
        return ir;
    }
}

InputRec *
start_lex (FILE *is , const char *infile)
{
    if(infile)
    {
        InputRec *ir = read_from_file(is , infile);
        next_char(ir);
        return ir;
    }
    else
    {
        InputRec *ir = read_from_stdin();
        next_char(ir);
        return ir;
    }
}


static void skip_useless (InputRec * const);

static void
skip_space (InputRec * const ir)
{
    while(!iseof(ir) && !isnewline(ir)
            && isspace(ir->cur_ch))
    {
        next_char(ir);
    }
}

static void
skip_comment (InputRec * const ir)
{
#define is_ct_starter(ch)   (ch == '#')

    if(!iseof(ir) && is_ct_starter(ir->cur_ch))
    {
        next_char(ir);
        while(!iseof(ir) && !isnewline(ir))
        {   next_char(ir); }
        skip_useless(ir);
    }

#undef is_ct_starter
}

static void
skip_useless (InputRec * const ir)
{
    skip_space(ir);
    skip_comment(ir);
}

static void
get_label (InputRec * const ir)
{
    next_char(ir);
    if(!iseof(ir) && isalpha(ir->cur_ch))
    {
        cur_token.t = T_LABEL;
        while(!iseof(ir) &&
                (isalpha(ir->cur_ch) || isdigit(ir->cur_ch)))
        {
            next_char(ir);
        }

        if(ir->cur_ch == ':')
        {
            cur_token.t = T_LABEL_DEF;
            next_char(ir);
        }
    }
    else
    {
        nvm_error("syntax error - required 'LABEL'");
    }
}

static void
get_symbol (InputRec * const ir)
{
    cur_token.t = T_SYMBOL;
    while(!iseof(ir) &&
            (isalpha(ir->cur_ch) || isdigit(ir->cur_ch)))
    {   next_char(ir); }

    if(ir->cur_ch == ':')
    {
        cur_token.t = T_PROC_DEF;
        next_char(ir);
    }
}

static void
get_integer (InputRec * const ir)
{
    cur_token.t = T_INTEGER;
    while(!iseof(ir) && isdigit(ir->cur_ch))
    {   next_char(ir); }
}

static void
get_newline (InputRec * const ir)
{
    cur_token.t = T_NEWLINE;
    next_char(ir);
    ++(ir->cur_line);
}

static void
get_eof (InputRec * const ir)
{
    cur_token.t = T_EOF;
}

static void
classify_token (InputRec * const ir)
{
    if(ir->cur_ch == '.')
    {   get_label(ir); }
    else if(isalpha(ir->cur_ch))
    {   get_symbol(ir); }
    else if(isdigit(ir->cur_ch))
    {   get_integer(ir); }
    else
    {   nvm_error("unknown token %c" , ir->cur_ch); }
}

static void
strdump (InputRec * const ir , int start , int len)
{
    if(cur_token.rep)
    {
        free((void *)(cur_token.rep));
        cur_token.rep = NULL;
    }

    char *s = xmalloc(sizeof(char) * (len+1));
    memcpy(s , ir->buf+start , sizeof(char) * len);
    s[len] = '\0';
    cur_token.rep = s;
}

void
get_next_token (InputRec * const ir)
{
    skip_useless(ir);

    if(iseof(ir))
    {
        get_eof(ir);
    }
    else if(isnewline(ir))
    {
        get_newline(ir);
    }
    else
    {
        int start = ir->buf_idx;
        classify_token(ir);
        strdump(ir , start , ir->buf_idx - start);
    }
}


static const char * tkstr[] = {
    "LABEL"     ,  "LABEL_DEF"  ,
    "SYMBOL"    ,  "INTEGER"    ,
    "PROC_DEF"  ,
    "NEWLINE"   ,  "EOF"        ,
};

const char *
tokenstr (Token_c t)
{
    return tkstr[t];
}
