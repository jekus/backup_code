#ifndef _LEX_H_
#define _LEX_H_

typedef enum{
    T_LABEL      ,   T_LABEL_DEF   ,
    T_SYMBOL     ,   T_INTEGER     ,
    T_PROC_DEF   ,
    T_NEWLINE    ,   T_EOF         ,
}Token_c;

typedef struct{
    Token_c t;
    const char *rep;
}Token_t;

typedef struct inputrec InputRec;

extern InputRec *       start_lex       (FILE *_is , const char *_infile);
extern void             get_next_token  (InputRec * const _ir);
extern const char *     tokenstr        (Token_c _t);

extern Token_t cur_token;

#endif
