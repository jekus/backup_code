#include <stdlib.h>
#include "../nvm_error.h"
#include "../nvm_mem.h"
#include "ast.h"

ast_node *
cons_ast_node (Node_t t)
{
    ast_node *new_n = xmalloc(sizeof(ast_node));
    new_n->t = t;
    new_n->rep = NULL;
    new_n->cn = 0;
    new_n->n_child = NULL;

    return new_n;
}

void
ast_del (ast_node *node)
{
    for(int i = 0;i < node->cn;++i)
    {
        ast_del(node->n_child[i]);
    }

    switch(node->t)
    {
        case N_LABEL:
        case N_LABEL_DEF:
        case N_PROC_DEF:
        case N_INTEGER:
        case N_SYMBOL:
            free((void *)(node->rep));
            break;
    }

    if(node->n_child)
    {   free(node->n_child); }
    free(node);
}

void
ast_insert_child (ast_node *par , ast_node *ch)
{
    int cur_cn = par->cn;
    ast_node **p = xrealloc(par->n_child ,
                            sizeof(ast_node *) * (cur_cn + 1));
    p[cur_cn] = ch;
    par->n_child = p;
    ++(par->cn);
}
