#include <string.h>
#include "../nvm_error.h"
#include "../nvm_mem.h"
#include "lex.h"
#include "ast.h"
#include "parser.h"

static void
validate_token (InputRec * const ir , Token_c t)
{
    if(cur_token.t == t)
    {   get_next_token(ir); }
    else
    {   nvm_error("syntax error - required '%s'" , tokenstr(t)); }
}

static ast_node *
parse_label_def (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_LABEL_DEF);
    switch(cur_token.t)
    {
        case T_LABEL_DEF:
        {
            int len = strlen(cur_token.rep)-1;
            char *rep = xmalloc(sizeof(char) * len);
            memcpy(rep , cur_token.rep+1 , sizeof(char) * len);
            rep[len-1] = '\0';
            node->rep = rep;
            validate_token(ir , T_LABEL_DEF);
            break;
        }

        default:
            validate_token(ir , T_LABEL_DEF);
    }

    return node;
}

static ast_node *
parse_proc_def (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_PROC_DEF);
    switch(cur_token.t)
    {
        case T_PROC_DEF:
        {
            int len = strlen(cur_token.rep);
            char *rep = xmalloc(sizeof(char) * len);
            memcpy(rep , cur_token.rep , sizeof(char) * len);
            rep[len-1] = '\0';
            node->rep = rep;
            validate_token(ir , T_PROC_DEF);
            break;
        }
        
        default:
            validate_token(ir , T_PROC_DEF);
    }

    return node;
}

static ast_node *
parse_symbol (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_SYMBOL);
    switch(cur_token.t)
    {
        case T_SYMBOL:
        {
            node->rep = (char *)n_strdup(cur_token.rep);
            validate_token(ir , T_SYMBOL);
            break;
        }
        
        default:
            validate_token(ir , T_SYMBOL);
    }

    return node;
}

static ast_node *
parse_label (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_LABEL);
    switch(cur_token.t)
    {
        case T_LABEL:
        {
            int len = strlen(cur_token.rep);
            char *rep = xmalloc(sizeof(char) * len);
            memcpy(rep , cur_token.rep+1 , sizeof(char) * (len-1));
            rep[len-1] = '\0';
            node->rep = rep;
            validate_token(ir , T_LABEL);
            break;
        }

        default:
            validate_token(ir , T_LABEL);
    }

    return node;
}

static ast_node *
parse_integer (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_INTEGER);
    switch(cur_token.t)
    {
        case T_INTEGER:
        {
            node->rep = (char *)n_strdup(cur_token.rep);
            validate_token(ir , T_INTEGER);
            break;
        }

        default:
            validate_token(ir , T_INTEGER);
    }

    return node;
}

static ast_node *
parse_operand (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_OPERAND);
    while(1)
    {
        switch(cur_token.t)
        {
            case T_INTEGER:
                ast_insert_child(node , parse_integer(ir));
                break;

            case T_LABEL:
                ast_insert_child(node , parse_label(ir));
                break;

            case T_SYMBOL:
                ast_insert_child(node , parse_symbol(ir));
                break;

            case T_NEWLINE:
            case T_EOF:
                return node;

            default:
                nvm_error("syntax error - required "
                          "'INTEGER' , 'LABEL' or 'SYMBOL'");
        }
    }

    return node;
}

static ast_node *
parse_instr_head (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_INSTR_HEAD);
    while(1)
    {
        switch(cur_token.t)
        {
            case T_LABEL_DEF:
                ast_insert_child(node , parse_label_def(ir));
                break;

            case T_PROC_DEF:
                ast_insert_child(node , parse_proc_def(ir));
                break;

            case T_NEWLINE:
                validate_token(ir , T_NEWLINE);
                break;

            case T_SYMBOL:
            case T_EOF:
                return node;

            default:
                nvm_error("syntax error - required "
                          "'LABEL_DEF' or 'PROC_DEF'");
        }
    }

    return node;
}

static ast_node *
parse_instr (InputRec * const ir)
{
    ast_node *node = cons_ast_node(N_INSTR);
    switch(cur_token.t)
    {
        case T_SYMBOL:
            ast_insert_child(node , parse_symbol(ir));
            ast_insert_child(node , parse_operand(ir));
            break;

        case T_NEWLINE:
        case T_EOF:
            break;

        default:
            nvm_error("syntax error - required 'SYMBOL'");
    }

    return node;
}

ast_node *
parse_program (InputRec * const ir)
{
    get_next_token(ir);
    ast_node *node = cons_ast_node(N_PROGRAM);
    while(1)
    {
        switch(cur_token.t)
        {
            case T_LABEL_DEF:
            case T_PROC_DEF:
                ast_insert_child(node , parse_instr_head(ir));
                break;

            case T_SYMBOL:
                ast_insert_child(node , parse_instr(ir));
                validate_token(ir , T_NEWLINE);
                break;

            case T_NEWLINE:
                validate_token(ir , T_NEWLINE);
                break;

            case T_EOF:
                return node;

            default:
                nvm_error("syntax error - required "
                          "'LABEL_DEF' , 'PROC_DEF' or 'SYMBOL'");
        }
    }

    return node;
}
