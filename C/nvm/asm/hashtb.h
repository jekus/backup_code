#ifndef _HTB_H_
#define _HTB_H_

extern struct hsearch_data label_tb;
extern struct hsearch_data proc_tb;
extern struct hsearch_data optb;

extern void hashtb_init (void);

#endif
