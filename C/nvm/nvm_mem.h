#ifndef _NVM_MEM_H_
#define _NVM_MEM_H_

#include <stdio.h>

extern void * xmalloc (size_t _size);
extern void * xrealloc (void *_ptr , size_t _size);

extern char * n_strdup (const char *_str);

#endif
