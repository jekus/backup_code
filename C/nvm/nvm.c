/*
    A stack-based virtual machine.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include "nvm.h"
#include "nvm_error.h"
#include "nvm_mem.h"
#include "op.h"
#include "opcode.h"
#include "neff.h"

static void
loader (NVM_REC *vm , const char *file)
{
    assert_expr(file != NULL ,
                "[LOADER] no input file");
    
    FILE *in_neff = fopen(file , "rb");
    assert_expr(in_neff != NULL ,
                "[LOADER] can't load NEFF file '%s'" , in_neff);
    
    NeffHeader hd;
    assert_expr(fread(&hd , sizeof(NeffHeader) , 1 , in_neff) == 1 ,
                "[LOADER] error in reading NEFF header");
    assert_expr(fseek(in_neff , hd.pgar_os , SEEK_SET) == 0 ,
                "[LOADER] error in seeking program area");

    vm->program = xmalloc(hd.pgar_sz);
    assert_expr(fread(vm->program , hd.pgar_sz , 1 , in_neff) == 1 ,
                "[LOADER] error in reading program");

    vm->inst_pt = hd.pgsp-1;
    vm->program_size = hd.pgar_sz;

    fclose(in_neff);
}

static void
fetch_inst (NVM_REC *vm)
{
    ++(vm->inst_pt);
    if(vm->inst_pt >= vm->program_size)
    {   vm_stop(vm); }
}

static OP_T
get_op (NVM_REC *vm)
{
    return (vm->program[vm->inst_pt] & OP_MASK);
}

static void
decode_inst (NVM_REC *vm)
{
    vm->cur_op = get_op(vm);
}

static void
vm_start (NVM_REC *vm , int argc , char **argv)
{
    loader(vm , *++argv);
    while(1)
    {
        fetch_inst(vm);
        decode_inst(vm);
        exec_op(vm);
    }
}

void
vm_stop (NVM_REC *vm)
{
    printf("%Ld\n", vm->eval_stack[vm->eval_sk_pt]);
    exit(EXIT_SUCCESS);
}

static NVM_REC *
nvm_new (void)
{
    NVM_REC *vm = xmalloc(sizeof(NVM_REC));
    vm->eval_sk_pt = -1;
    vm->eval_stack = xmalloc(sizeof(WORD_T) * EVAL_SK_SIZE);
    vm->ret_sk_pt = -1;
    vm->ret_stack = xmalloc(sizeof(RET_FRAG) * RET_SK_SIZE);
    vm->inst_pt = -1;
    return vm;
}

void
main (int argc , char **argv)
{
    set_errprefix("nvm:");
    NVM_REC *vm = nvm_new();
    vm_start(vm , argc , argv);
}
