#ifndef _NVM_OPC_H_
#define _NVM_OPC_H_

/* The number of bits took up by OP */
#define OP_BITS         5
#define OP_MASK         ~(~(WORD_T)0 << OP_BITS)

/* The number of bits took up by OP ARG */
#define OP_ARGC_BITS    3
#define OP_ARGC_OFFSET  (sizeof(WORD_T) * 8 - OP_ARGC_BITS)
#define OP_ARGC_MASK    (~(~(WORD_T)0 << OP_ARGC_BITS) << OP_ARGC_OFFSET)

typedef enum{
    HALT    ,

    PUSH    ,   POP     ,
    DUP     ,   REP     ,

    ADD     ,   MUL     ,
    SUB     ,   DIV     ,

    EQ      ,   LT      ,
    LQ      ,

    AND     ,   OR      ,
    XOR     ,   NOT     ,

    GOTO    ,   CALL    ,
    RET     ,
}OP_T;

#endif
