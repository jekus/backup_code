# N Virtual Machine

<br />

## Overview
The N vitual machine is **stack-based**. It currently only support some basic operators and simple procedure call. Now , the project contains a simple assembler for the N virtual machine.

<br />

## Nvm Executable File Format (NEFF)
The N virtual machine accept **NEFF** as input. **NEFF** has only two parts now. One for header , another for program.

The **NEFF** header has the size defined by structure **NeffHeader** in `neff.h`.

Here's how the header looks like:

<table>
<tr>
<th>Program Area Offset<br />(in NEFF)</th>
<th>Program Area Size</th>
<th>Program Start Point<br />(in program area)</th>
</tr>

<tr>
<td align="center">4 Bytes</td>
<td align="center">4 Bytes</td>
<td align="center">4 Bytes</td>
</tr>
</table>

<br />

## Operator
**DEFINE:** ES = evaluation stack

<table>
<thead>
<tr>
<th>Operator</th>
<th>Description</th>
</tr>
</thead>

<tbody>
<tr>
<td align="center">halt</td>
<td>Stop the virtual machine</td>
</tr>

<tr>
<td align="center">push</td>
<td>Push an item onto <strong>ES</strong></td>
</tr>

<tr>
<td align="center">pop</td>
<td>Pop the top item from <strong>ES</strong></td>
</tr>

<tr>
<td align="center">dup</td>
<td>Duplicate an item in <strong>ES</strong> and place it on the top</td>
</tr>

<tr>
<td align="center">rep</td>
<td>Replace an item in <strong>ES</strong> with the top item</td>
</tr>

<tr>
<td align="center">add</td>
<td>Arithmetic operator <strong><code>+</code></strong> , add the top two items in <strong>ES</strong> , consume the two and place the result on the top</td>
</tr>

<tr>
<td align="center">mul</td>
<td>Arithmetic operator <strong><code>*</code></strong> , similar to <strong>add</strong></td>
</tr>

<tr>
<td align="center">sub</td>
<td>Arithmetic operator <strong><code>-</code></strong> , similar to <strong>add</strong></td>
</tr>

<tr>
<td align="center">div</td>
<td>Arithmetic operator <strong><code>/</code></strong> , similar to <strong>add</strong></td>
</tr>

<tr>
<td align="center">eq</td>
<td>Logical operator <strong><code>==</code></strong> , compare the top two items in <strong>ES</strong>. If the <strong>TOP</strong> is equivalent to the <strong>SECOND</strong> , execute the next instruction. Otherwise , skip it.</td>
</tr>

<tr>
<td align="center">lt</td>
<td>Logical operator <strong><code>&lt;</code></strong> , similar to <strong>eq</strong></td>
</tr>

<tr>
<td align="center">lq</td>
<td>Logical operator <strong><code>&lt;=</code></strong> , similar to <strong>eq</strong></td>
</tr>

<tr>
<td align="center">and</td>
<td>Bitwise <strong>AND</strong> , similar to <strong>add</strong></td>
</tr>

<tr>
<td align="center">or</td>
<td>Bitwise <strong>OR</strong> , similar to <strong>add</strong></td>
</tr>

<tr>
<td align="center">xor</td>
<td>Bitwise <strong>XOR</strong> , similar to <strong>add</strong></td>
</tr>

<tr>
<td align="center">not</td>
<td>Bitwise <strong>NOT</strong> , similar to <strong>add</strong></td>
</tr>

<tr>
<td align="center">goto</td>
<td>Jump to the address specified by the operand</td>
</tr>

<tr>
<td align="center">call</td>
<td>Call a procedure</td>
</tr>

<tr>
<td align="center">ret</td>
<td>Return from a procedure</td>
</tr>
</tbody>
</table>
