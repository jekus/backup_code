#ifndef _OP_H_
#define _OP_H_

#include "nvm.h"
#include "opcode.h"

extern void exec_op (NVM_REC *_vm);

#endif
