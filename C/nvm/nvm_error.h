#ifndef _NVM_ERR_H_
#define _NVM_ERR_H_

#include <stdarg.h>

extern void set_errprefix (const char *_errp);
extern void assert_expr (int _res , const char *_tp , ...);
extern void nvm_error (const char *_tp , ...);
extern void nvm_verror (const char *_tp , va_list vl);

#endif
