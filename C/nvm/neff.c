#include "neff.h"
#include "nvm_mem.h"

NeffHeader * new_neffhd (
                NEFF_PGAR_OS_T  pgar_os ,
                NEFF_PGAR_SZ_T  pgar_sz ,
                NEFF_PGSP_T     pgsp)
{
    NeffHeader *new_hd = xmalloc(sizeof(NeffHeader));
    new_hd->pgar_os = pgar_os;
    new_hd->pgar_sz = pgar_sz;
    new_hd->pgsp = pgsp;

    return new_hd;
}
