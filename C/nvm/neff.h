#ifndef _NEFF_H_
#define _NEFF_H_

#include <stdint.h>

/* The size (in bytes) of each segment in header */
#define NEFF_PGAR_OS_LEN    (4)
#define NEFF_PGAR_SZ_LEN    (4)
#define NEFF_PGSP_LEN       (4)

typedef int32_t NEFF_PGAR_OS_T;
typedef int32_t NEFF_PGAR_SZ_T;
typedef int32_t NEFF_PGSP_T;

typedef struct{
    NEFF_PGAR_OS_T  pgar_os;
    NEFF_PGAR_SZ_T  pgar_sz;
    NEFF_PGSP_T     pgsp;
}NeffHeader;

extern NeffHeader * new_neffhd (
                        NEFF_PGAR_OS_T  _pgar_os ,
                        NEFF_PGAR_SZ_T  _pgar_sz ,
                        NEFF_PGSP_T     _pgsp);

#endif
