#ifndef _NVM_H_
#define _NVM_H_

#include <stdint.h>
#include "opcode.h"

#define EVAL_SK_SIZE    (4096)
#define RET_SK_SIZE     (2048)

typedef int64_t WORD_T;

typedef struct{
    int ret_eval_sk_pt;
    int ret_addr;
}RET_FRAG;

typedef struct{
    int         eval_sk_pt;
    WORD_T      *eval_stack;

    int         ret_sk_pt;
    RET_FRAG    *ret_stack;

    int         inst_pt;
    WORD_T      *program;
    int         program_size;

    OP_T        cur_op;
}NVM_REC;

extern void vm_stop     (NVM_REC *_vm);

#endif
