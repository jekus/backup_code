#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "nvm_error.h"
#include "nvm_mem.h"


static const char * err_prefix = NULL;

void
set_errprefix (const char *p)
{
    if(err_prefix)
    {
        free((void *)err_prefix);
        err_prefix = NULL;
    }

    err_prefix = n_strdup(p);
}

static void
nvm_perror (const char *tp , va_list vl)
{
    fprintf(stderr , "%s ", err_prefix);
    vfprintf(stderr , tp , vl);
    fputc('\n' , stderr);
}

void
assert_expr (int res , const char *tp , ...)
{
    if(!res)
    {
        va_list vl;
        va_start(vl , tp);
        nvm_perror(tp , vl);
        va_end(vl);
        exit(EXIT_FAILURE);
    }
}

void
nvm_error (const char *tp , ...)
{
    va_list vl;
    va_start(vl , tp);
    nvm_perror(tp , vl);
    va_end(vl);
    exit(EXIT_FAILURE);
}

void
nvm_verror (const char *tp , va_list vl)
{
    nvm_perror(tp , vl);
    exit(EXIT_FAILURE);
}
