#include <stdlib.h>
#include <string.h>
#include "nvm_error.h"
#include "nvm_mem.h"

void *
xmalloc (size_t size)
{
    void *ptr = malloc(size);
    assert_expr(ptr != NULL ,
                "can't allocate %Ld Bytes memory" , size);
    return ptr;
}

void *
xrealloc (void *ptr , size_t size)
{
    void *new_ptr = realloc(ptr , size);
    assert_expr(new_ptr != NULL ,
                "can't allocate %Ld Bytes memory" , size);
    return new_ptr;
}

char *
n_strdup (const char *str)
{
    size_t s_size = sizeof(char) * (strlen(str)+1);
    char *new_s = xmalloc(s_size);
    memcpy(new_s , str , s_size);
    return new_s;
}
