#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 60000000
#define ELE_FIND 0

typedef long long int Ar_t;

/*
	The array must be sorted in increasing order.
	* ar[0] <= ar[1] <= ... <= ar[n - 1] *
*/
static int
binsearch (Ar_t x , Ar_t ar[] , Ar_t n)
{
	Ar_t low = 0 , high = n - 1 ,
		mid;
	clock_t start = clock();

	while(low <= high)
	{
		mid = (low + high) / 2;

		if(x <= ar[mid])
			high = mid - 1;
		else
			low = mid + 1;
	}

	if(x == ar[mid])
	{
		printf("%ld\n", clock() - start);
		return mid;
	}

	return -1;
}

static int
binsearch_f (Ar_t x , Ar_t ar[] , Ar_t n)
{
	Ar_t low = 0 , high = n - 1 ,
		mid;
	clock_t start = clock();

	while(low <= high)
	{
		mid = (low + high) / 2;

		if(x < ar[mid])
			high = mid - 1;
		else if(x > ar[mid])
			low = mid + 1;
		else
		{
			printf("%ld\n" , clock() - start);
			return mid;
		}
	}
	printf("%ld\n" , clock() - start);

	return -1;
}

static void
gene_ar (Ar_t ar[] , Ar_t n)
{
	for(Ar_t i = 0;i < n;++i)
		ar[i] = i;
}

int
main (void)
{
	Ar_t *ar = malloc(sizeof(Ar_t) * SIZE);
	if(!ar)
	{
		fprintf(stderr , "%s\n" , "Memory Allocation Failed");
		return EXIT_FAILURE;
	}

	gene_ar(ar , SIZE);
	printf("%d\n\n", binsearch(ELE_FIND , ar , SIZE));
	printf("%d\n\n\n", binsearch_f(ELE_FIND , ar , SIZE));

	return EXIT_SUCCESS;
}
