/*
	Return the value of x rotated to the right
	by n bit positions.
*/
#include <stdio.h>

typedef unsigned long long int Num_t;
extern void print_bin (Num_t);

Num_t
rightrot (Num_t x , int n)
{
	Num_t x_mask = 0;

	/*
		Create the mask for 'x' ,
		to get the 'n' bits.
	*/
	for(int idx = n - 1;idx >= 0;--idx)
		x_mask |= (1 << idx);
	x_mask &= x;
	x_mask <<= (sizeof(Num_t) * 8 - n);
	x >>= n;

	return (x | x_mask);
}

int
main (void)
{
	Num_t num;

	while(printf("\nEnter a number: ") >= 0 &&
		scanf("%Lu", &num) == 1)
	{
		print_bin(num);
		puts("");
		print_bin(rightrot(num , 10));
	}

	return 0;
}
