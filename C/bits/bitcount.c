/*
	Count the number of 1-bit in
	'x'.
*/
#include <stdio.h>

typedef unsigned long long int Num_t;
extern void print_bin (Num_t);

int
bitcount_1 (Num_t x)
{
	int count = 0;

	while(x)
	{
		if(x & 1)
			++count;
		x >>= 1;
	}

	return count;
}

int
bitcount_2 (Num_t x)
{
	int count = 0;

	while(x)
		x &= (x-1) ,
		++count;

	return count;
}

int
main (void)
{
	Num_t num;

	while(printf("Enter a number: ") >= 0 &&
		scanf("%Lu", &num) == 1)
	{
		print_bin(num);
		puts("");
		printf("Version_1: %d\nVersion_2: %d\n",
			bitcount_1(num) ,bitcount_2(num));
	}
	puts("");

	return 0;
}
