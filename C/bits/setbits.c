/*
	Set 'x' with the 'n' bits that begin at position
	'p' set to the rightmost 'n' bits of y.
*/
#include <stdio.h>

typedef unsigned long long int Num_t;
extern void print_bin (Num_t);

/*
	Assume that 'p' is greater than 'n'
*/
Num_t
setbits (Num_t x , int p , int n , Num_t y)
{
	/*
		Move the n-rightmost bits to
		the correct position.
	*/
	y <<= (p - n);

	Num_t x_mask , y_mask;
	/*
		Create the 'x_mask'.
		
		Work in two's complement number system.
		-1 will be all 1-bits.
	*/
	x_mask = (-1) << p;
	for(int idx = p - n - 1;idx >= 0;--idx)
		x_mask |= (1 << idx);
	y_mask = ~x_mask;

	x &= x_mask;
	y &= y_mask;
	
	return (x | y);
}

int
main (void)
{
	Num_t num_1 , num_2;

	printf("Enter a number: ");
	while(scanf("%Lu%Lu", &num_1 , &num_2) == 2)
	{
		print_bin(num_1);
		puts("");
		print_bin(num_2);
		puts("");
		print_bin(setbits(num_1 , 10 , 5 , num_2));
		puts("");
		printf("Enter a number: ");
	}
	puts("");

	return 0;
}
