/*
	Return 'x' with the 'n' bits that
	begin at position 'p' inverted.
	(1 to 0 , and vice versa)
*/
#include <stdio.h>

typedef unsigned long long int Num_t;
extern void print_bin (Num_t);

Num_t
invertbits (Num_t x , int p , int n)
{
	Num_t x_mask = 0;

	/*
		Create the mask for 'x'.
		The desired bits will be set
		to 1.
	*/
	for(int idx = n - 1;idx >= 0;--idx)
		x_mask |= (1 << idx);
	x_mask <<= (p - n);

	return (x ^ x_mask);
}

int
main (void)
{
	Num_t num;

	while(printf("\nEnter a number: ") >= 0 &&
		scanf("%Lu", &num) == 1)
	{
		print_bin(num);
		puts("");
		print_bin(invertbits(num , 10 , 5));
	}
	puts("");

	return 0;
}
