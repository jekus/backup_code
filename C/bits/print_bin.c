#include <stdio.h>

void
print_bin (unsigned long long int num)
{
	if(num)
	{
		print_bin(num >> 1);
		printf("%Lu", num & 1);
	}
}
