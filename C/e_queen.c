#include <stdio.h>
#include <string.h>
#include <math.h>

#define N 8	/* board size */
int m_count = 0;
int queen[N];

int
place_ok (int column , int row)
{
	for(int i = 0;i < row;++i)
		if(column == queen[i] ||
			(row - i) == abs(column - queen[i]))
				return 0;
	
	return 1;
}


void
add_queen (int row)
{
	if(row == N)
		++m_count;
	else
		for(int i = 0;i < N;++i)
			if(place_ok(i , row))
			{
				queen[row] = i;
				add_queen(row+1);
			}
}


int
main (void)
{
	add_queen(0);
	printf("%d\n",m_count);

	return 0;
}
