#include <apue.h>
#define BUFF 524288000

void alm (int signo)
{
	puts("Receive SIGALRM");
}

int main (void)
{
	char * buf = malloc(sizeof(char) * BUFF);
	FILE * fp;
	struct sigaction act;

	if((fp = fopen("hhl" , "w+")) == NULL)
	{
		printf("open error");
		exit(0);
	}

	for(int i = 0;i < BUFF;i++)
		buf[i] = 'H';

	act.sa_handler = alm;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask , SIGALRM);
	act.sa_flags = 0;
	sigaction(SIGALRM , &act , NULL);

	alarm(1);
	if(fwrite(buf , BUFF , 1 , fp) != 1)
	{
		printf("write error");
		exit(0);
	}
	alarm(0);

	puts("Done");

	if(fclose(fp) != 0)
		printf("close error");

	exit(0);
}
