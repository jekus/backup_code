#include <apue.h>
#define BUFF 100

void han (int signo)
{
	printf("receive SIGXFSZ");
}

Sigfunc * signal_intr(int signo , Sigfunc * func)
{
	struct sigaction act , oact;

	act.sa_handler = func;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
#ifdef SA_INTERRUPT
	act.sa_flags = SA_INTERRUPT;
#endif
	if(sigaction(signo , &act , &oact) < 0)
		return (SIG_ERR);
	return (oact.sa_handler);
}

int main (void)
{
	int n , wn;
	char buf[BUFF];

	signal_intr(SIGXFSZ , han);

	while((n = read(STDIN_FILENO , buf , BUFF)) > 0)
		if((wn = write(STDOUT_FILENO , buf , n)) != n)
			err_sys("write %d bytes",wn);
	
	if(n < 0)
		err_sys("read error");

	exit(0);
}
