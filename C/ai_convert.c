#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

static const char baseset[] =
	"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

static void
reverse (char s[])
{
	int tmp;
	int fir = 0 , sec = strlen(s)-1;

	while(fir < sec)
	{
		tmp = s[fir];
		s[fir] = s[sec];
		s[sec] = tmp;

		++fir;
		--sec;
	}
}

static int
atoi (char s[])
{
	int idx = 0;
	int num , sign;

	/* Skip the leading white space */
	while(isspace(s[idx]))
		++idx;

	/*
		Work under ASCII.

		',' is the only character between '+' and '-'.
	*/
	sign = (s[idx] == '+' || s[idx] == '-') ?
		',' - s[idx++] : 1;
	for(num = 0;isdigit(s[idx]);++idx)
		num = 10 * num + (s[idx] - '0');

	return sign * num;
}

static void
itoa (int n , char s[])
{
	int idx = 0 , sign = n;
	if(sign < 0)
		n = -n;

	do
	{
		s[idx++] = n % 10 + '0';
	}while((n /= 10));

	if(sign < 0)
		s[idx++] = '-';
	s[idx] = '\0';
	reverse(s);
}

/*
	'width' is the minimum field width(not include the sign character).
	If the number can't reach the width ,
	it will have leading blanks.
*/
static void
itoaw (int n , char s[] , int width)
{
	int idx = 0 , sign = n;
	if(sign < 0)
		n = -n;

	do
	{
		s[idx++] = n % 10 + '0';
	}while((n /= 10));

	for(int i = idx+1;i <= width;++i)
		s[idx++] = ' ';

	if(sign < 0)
		s[idx++] = '-';
	s[idx] = '\0';
	reverse(s);
}

/*
	Convert 'n' into a 'base' character
	representation in 's'.

	Can handle up to base 36.
*/
static void
itob (int n , char s[] , int base)
{
	int idx = 0 , sign = n;
	if(sign < 0)
		n = -n;

	do
	{
		s[idx++] = baseset[n % base];
	}while((n /= base));

	if(sign < 0)
		s[idx++] = '-';
	s[idx++] = '\0';
	reverse(s);
}

static double
atof (const char s[])
{
	double val;
	int power = 0 , exp = 0;
	int idx = 0 , sign;

	while(isspace(s[idx]))
		++idx;
	sign = (s[idx] == '+' || s[idx] == '-') ?
		',' - s[idx++] : 1;

	for(val = 0;isdigit(s[idx]);++idx)
		val = 10 * val + (s[idx] - '0');
	if(s[idx] == '.')
	{
		++idx;
		for(;isdigit(s[idx]);++idx , ++power)
			val = 10 * val + (s[idx] - '0');
		if(s[idx] == 'e' || s[idx] == 'E')
			exp = atoi(s+idx+1);
	}
	else if(s[idx] == 'e' || s[idx] == 'E')
		exp = atoi(s+idx+1);

	int ture_exp = power - exp;
	double res = sign * val;
	printf("%d\t%d\t%d\n", ture_exp , power , exp);
	if(ture_exp > 0)
		return res / pow(10 , ture_exp);
	else if(ture_exp < 0)
		return res * pow(10 , -ture_exp);
	else
		return res;
}

int
main (void)
{
	char buf[30];

	printf("%d\n", atoi("   -100"));
	itoaw(-12314 , buf , 10);
	puts(buf);
	itob(-12314 , buf , 30);
	puts(buf);
	printf("%lf\n", atof("1.23234e-4"));

	return 0;
}
