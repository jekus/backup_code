#include <stdio.h>
#include <string.h>

void
mstrcat (char *s , const char *t)
{
	s+=strlen(s);
	while(*s++ = *t++);
}

void
mstrcpy (char *s , const char *t)
{
	while(*s++ = *t++);
}

/*
	Return 1 if 't' occurs
	at the end of 's'.
*/
int
strend (const char *s , const char *t)
{
	int s_len = strlen(s) ,
		t_len = strlen(t);
	if(t_len > s_len || s[s_len-1] != t[t_len-1])
		return 0;

	s = s+s_len-2;
	for(const char *ptr = t+t_len-2;ptr >= t;--ptr , --s)
		if(*ptr != *s)
			return 0;

	return 1;
}

int
main (void)
{
	printf("%s\n", strend("abcd123" , "d123") ? "YES" : "NO");
	return 0;
}
