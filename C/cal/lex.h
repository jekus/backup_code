#ifndef _LEX_H_
#define _LEX_H_

#define GET 1
#define NOGET 0

/* 0 - 255 reserved for ASCII */
typedef enum{
	EoF = 256 , EoL ,
	NUMBER , SYMBOL
}Token_class;

typedef enum{
	ADD = '+' , SUB = '-' ,
	MUL = '*' , DIV = '/' ,
	ASSIGN = '=' ,
	POW = SYMBOL+1		/* '**' */ ,
	INCR				/* '++' */ ,
	DECR				/* '--' */ ,
	ADD_ASS				/* '+=' */ ,
	SUB_ASS				/* '-=' */ ,
	MUL_ASS				/* '*=' */ ,
	DIV_ASS				/* '/=' */ ,
	POW_ASS				/* '**=' */ ,
}OP_class;

typedef enum{
	L_PAREN = '(' , 
	R_PAREN = ')'
}Paren_class;

typedef struct{
	int class;
	char *rep;
}Token_t;

extern Token_t cur_token;

extern void set_eol (void);
extern void get_next_token (void);

#endif
