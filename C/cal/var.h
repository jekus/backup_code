#ifndef _VAR_H_
#define _VAR_H_

extern void var_init (void);

extern double * get_var (const char *var_name);
extern void set_var (const char *var_name , const double var_val);

#endif
