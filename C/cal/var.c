#include <stdlib.h>

#define __USE_XOPEN2K8
#include <string.h>
#undef __USE_XOPEN2K8

#define __USE_GNU
#include <search.h>
#undef __USE_GNU

#include <errno.h>
#include "error.h"
#include "xmalloc.h"
#include "var.h"

#define HTAB_SIZE	(1<<8)

typedef struct hsearch_data H_TAB;
static H_TAB htab;


void
var_init (void)
{
	memset(&htab , 0 , sizeof(H_TAB));

	if(!hcreate_r(HTAB_SIZE , &htab))
		error(err_msg[HTAB_CR_FAILED]);
}

static ENTRY *
get_entry (const char *var_name)
{
	ENTRY want_item = {var_name , NULL};
	ENTRY *get_item = NULL;

	hsearch_r(want_item , FIND ,
		&get_item , &htab);

	return get_item;
}

double *
get_var (const char *var_name)
{
	ENTRY *get_item = get_entry(var_name);
	return get_item ? (double *)(get_item->data) : NULL;
}

static void
do_set_var (
	const char *var_name ,
	const double var_val)
{
	double *var_ptr = xmalloc(sizeof(double));
	*var_ptr = var_val;

	ENTRY item = {strdup_n(var_name) , var_ptr};
	ENTRY *get_item;

	if(!hsearch_r(item , ENTER , &get_item , &htab))
		error(err_msg[HTAB_FILLED]);
}

static void
do_rep_var (
	ENTRY *get_item ,
	const double var_val)
{
	double get_val = *((double *)(get_item->data));

	if(get_val != var_val)
	{
		/*
			The same VAR with a different
			value.

			Change the value only.
		*/
		free(get_item->data);

		double *val_ptr = xmalloc(sizeof(double));
		*val_ptr = var_val;
		get_item->data = val_ptr;
	}
}

void
set_var (
	const char *var_name ,
	const double var_val)
{
	ENTRY *get_item = get_entry(var_name);

	if(get_item)
		do_rep_var(get_item , var_val);
	else
		do_set_var(var_name , var_val);
}
