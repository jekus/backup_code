#ifndef _MEM_MANA_H_
#define _MEM_MANA_H_

extern void *add_entry (void *addr);
extern int free_entry (void *addr);
extern void free_all_entry (void);

#endif
