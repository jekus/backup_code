#include <stdlib.h>

#define __USE_XOPEN2K8
#include <string.h>
#undef __USE_XOPEN2K8

#include "error.h"


void *
xmalloc (size_t size)
{
	void *ptr = malloc(size);
	if(!ptr)
		error(err_msg[MEM_ALLOC_FAILED]);

	return ptr;
}

char *
strdup_n (const char *str)
{
	char *new_str = strdup(str);
	if(!new_str)
		error(err_msg[MEM_ALLOC_FAILED]);

	return new_str;
}
