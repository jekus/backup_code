#ifndef _ERR_H_
#define _ERR_H_

#define throw_syn_err()				\
	error(err_msg[SYN_ERR])

typedef enum{
	SYN_ERR , DIV_ZERO , 
	UK_CHAR , MEM_ALLOC_FAILED ,
	HTAB_CR_FAILED , HTAB_FILLED ,
	UK_SYM
}Errmsg_class;

extern const char *err_msg[];

extern void error (const char *err_msg , ...);

#endif
