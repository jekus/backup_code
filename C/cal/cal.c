#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include "lex.h"
#include "var.h"

extern jmp_buf jmpbuf;
extern double expr (int);

int
main (void)
{
	var_init();

	while(1)
	{
		setjmp(jmpbuf);
		printf("%g\n", expr(GET));
	}

	return EXIT_SUCCESS;
}
