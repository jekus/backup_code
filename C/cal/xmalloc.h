#ifndef _XALLOC_H_
#define _XALLOC_H_

extern void * xmalloc (size_t size);
extern char * strdup_n (const char *str);

#endif
