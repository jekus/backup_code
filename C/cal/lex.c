#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <readline/readline.h>
#include "error.h"
#include "lex.h"

#define next_char()                         \
    (input_char = input_buf[++dot])
#define peek(n)                             \
    (input_buf[dot+n])

#define is_underscore(ch)                   \
    (ch == '_')
#define is_cmt_sym(ch)                      \
    (ch == '#')
#define is_dot(ch)                          \
    (ch == '.')
#define is_eol(ch)                          \
    (ch == '\0')
#define is_eof()                            \
    (input_buf == NULL)
#define is_incr(ch)                         \
    (ch == '+' && peek(1) == '+')
#define is_decr(ch)                         \
    (ch == '-' && peek(1) == '-')
#define is_pow(ch)                          \
    (ch == '*' && peek(1) == '*')
#define is_addass(ch)                       \
    (ch == '+' && peek(1) == '=')
#define is_subass(ch)                       \
    (ch == '-' && peek(1) == '=')
#define is_mulass(ch)                       \
    (ch == '*' && peek(1) == '=')
#define is_divass(ch)                       \
    (ch == '/' && peek(1) == '=')
#define is_powass(ch)                       \
    (is_pow(ch) && peek(2) == '=')
#define is_operator(ch)                     \
    (ch == ADD || ch == SUB ||              \
        ch == MUL || ch == DIV ||           \
        ch == ASSIGN || is_pow(ch) ||       \
        is_incr(ch) || is_decr(ch) ||       \
        is_addass(ch) || is_subass(ch) ||   \
        is_mulass(ch) || is_divass(ch) ||   \
        is_powass(ch))
#define is_paren(ch)                        \
    (ch == L_PAREN || ch == R_PAREN)

#define is_valid_char(ch)                   \
    (is_operator(ch) || is_paren(ch))

#define is_symbol(ch)                       \
    (isalpha(ch) || is_underscore(ch))
#define is_number(ch)                       \
    (isdigit(ch) ||                         \
        (is_dot(ch) && isdigit(peek(1))))

Token_t cur_token;

static const char *in_prompt = ">>> ";

static char *input_buf = NULL;
static int dot = 0;
static char input_char;
static int eof = 1;
static int eol = 1;

static void deal_with_eol (void);


void
set_eol (void)
{
    deal_with_eol();
}

static void
deal_with_eol (void)
{
    eol = 1;
    cur_token.class = EoL;
    cur_token.rep = NULL;
}

static void
get_input (void)
{
    READ_IN:;
    if(input_buf)
        free(input_buf);

    if(!(input_buf = readline(in_prompt)))
    {
        puts("");
        exit(EXIT_SUCCESS);
    }

    dot = -1;
    if(is_eol(next_char()))
        goto READ_IN;
    eof = 0;
    eol = 0;
}

static void
skip_blank (void)
{
    while(isspace(input_char))
        next_char();
}

static void
skip_comment (void)
{
    if(is_cmt_sym(input_char))
        while(!is_eol(next_char()) &&
            !is_eof());
}

static void
skip_useless (void)
{
    skip_blank();
    skip_comment();
}

static void
get_symbol (void)
{
    cur_token.class = SYMBOL;
    
    next_char();
    while(is_symbol(input_char) ||
        isdigit(input_char))
            next_char();
}

static void
get_number (void)
{
    cur_token.class = NUMBER;

    int have_dot = is_dot(input_char);
    if(have_dot)
        /* Cases like '.123' */
        while(isdigit(next_char()));
    else
    {
        /*
            Accept cases like:
            '123.'.

            '123.a' would be an symbol.
        */
        while(isdigit(next_char()) ||
            (!have_dot &&
                (have_dot = is_dot(input_char))));
        
        /*
            '123abc' is an symbol.
        */
        if(isalpha(input_char))
            get_symbol();
    }

    /*
        Whether more than one '.'
        appear in a number.
    */
    if(have_dot && is_dot(input_char))
        throw_syn_err();
}

static void
get_basic_ele (void)
{
    if(!is_valid_char(input_char))
        error(err_msg[UK_CHAR], input_char);

    cur_token.class = input_char;
    cur_token.rep = NULL;
    next_char();
}

static void
deal_with_operator (void)
{

    if(is_addass(input_char))
    {
        cur_token.class = ADD_ASS;
        next_char();
    }
    else if(is_subass(input_char))
    {
        cur_token.class = SUB_ASS;
        next_char();
    }
    else if(is_mulass(input_char))
    {
        cur_token.class = MUL_ASS;
        next_char();
    }
    else if(is_divass(input_char))
    {
        cur_token.class = DIV_ASS;
        next_char();
    }
    else if(is_powass(input_char))
    {
        cur_token.class = POW_ASS;
        next_char();
        next_char();
    }
    else if(is_pow(input_char))
    {
        cur_token.class = POW;
        next_char();
    }
    else if(is_incr(input_char))
    {
        cur_token.class = INCR;
        next_char();
    }
    else if(is_decr(input_char))
    {
        cur_token.class = DECR;
        next_char();
    }
    else
        cur_token.class = input_char;

    cur_token.rep = NULL;
    next_char();
}

static void
classify_token (void)
{
    if(is_eol(input_char))
        deal_with_eol();
    else if(is_symbol(input_char))
        get_symbol();
    else if(is_number(input_char))
        get_number();
    else if(is_operator(input_char))
        deal_with_operator();
    else
        get_basic_ele();
}

static void
do_stringfy (int start , int len)
{
    static char *rep_str = NULL;
    if(!(rep_str = realloc(rep_str , sizeof(char) * (len + 1))))
        error(err_msg[MEM_ALLOC_FAILED]);

    memcpy(rep_str , input_buf + start , sizeof(char) * len);
    rep_str[len] = '\0';
    cur_token.rep = rep_str;
}

static void
stringfy_token (int start , int len)
{
    switch(cur_token.class)
    {
        case SYMBOL:
        case NUMBER:
            do_stringfy(start , len);
    }
}

void
get_next_token (void)
{
    if(eol)
        get_input();
    skip_useless();

    int tk_start = dot;
    classify_token();
    stringfy_token(tk_start , dot - tk_start);
}
