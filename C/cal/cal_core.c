#include <stdio.h>
#include <stdlib.h>

#define __USE_XOPEN2K8
#include <string.h>
#undef __USE_XOPEN2K8

#include <setjmp.h>
#include <math.h>
#include "error.h"
#include "lex.h"
#include "xmalloc.h"
#include "mem_mana.h"
#include "var.h"

extern double expr (int);
static double prim (int);

/*
	Indicate the existence of
	left parenthesis.
*/
static int have_lp = 0;

/*
	Indicate the existence of
	operator before ASSIGN.
*/
static int have_op = 0;


static void
check_div_zero (double val)
{
	if(!val)
		error(err_msg[DIV_ZERO]);
}

static void
check_paren_err (void)
{
	switch(cur_token.class)
	{
		case L_PAREN:
		case R_PAREN:
			if(!have_lp)
				throw_syn_err();
	}
}

static void
check_tk_class (int class)
{
	if(cur_token.class != class)
		throw_syn_err();
}

/*
	Get the value of a
	variable.
*/
static double
get_id_val (const char *var_name)
{
	const double *val = get_var(var_name);
	if(!val)
		error(err_msg[UK_SYM]);

	return *val;
}

/*
	DECR is a special case of
	INCR.
*/
static double
do_preincr (int type)
{
	get_next_token();
	check_tk_class(SYMBOL);
	
	const char *var_name =
		add_entry(strdup_n(cur_token.rep));
	get_next_token();
	if(cur_token.class == ASSIGN)
		throw_syn_err();

	double val = get_id_val(var_name);
	double rst = val + (type == INCR ? 1 : -1);
	set_var(var_name , rst);

	free_entry(var_name);
	return rst;
}

static double
do_postincr (const char *var_name , int type)
{
	get_next_token();
	if(cur_token.class == ASSIGN)
		throw_syn_err();

	double val = get_id_val(var_name);
	double rst = val + (type == INCR ? 1 : -1);
	set_var(var_name , rst);

	return val;
}

static double
do_assign (const char *var_name)
{
	double val = expr(GET);
	set_var(var_name , val);
	return val;
}

static double
assign_to_id (const char *var_name)
{
	/*
		Deal with cases:
		'a + b = 1' , etc.
	*/
	if(have_op)
		throw_syn_err();
	return do_assign(var_name);
}

static double
addass_to_id (const char *var_name)
{
	/*
		Get the value of VAR itself.
	*/
	double val = get_id_val(var_name);
	/*
		Get the value after '+='.
	*/
	double rst = expr(GET);

	double sum = val + rst;
	set_var(var_name , sum);

	return sum;
}

static double
subass_to_id (const char *var_name)
{
	/*
		Get the value of VAR itself.
	*/
	double val = get_id_val(var_name);
	/*
		Get the value after '-='.
	*/
	double rst = expr(GET);

	double sum = val - rst;
	set_var(var_name , sum);

	return sum;
}

static double
mulass_to_id (const char *var_name)
{
	/*
		Get the value of VAR itself.
	*/
	double val = get_id_val(var_name);
	/*
		Get the value after '*='.
	*/
	double rst = expr(GET);

	double sum = val * rst;
	set_var(var_name , sum);

	return sum;
}

static double
divass_to_id (const char *var_name)
{
	/*
		Get the value of VAR itself.
	*/
	double val = get_id_val(var_name);
	/*
		Get the value after '/='.
	*/
	double rst = expr(GET);
	
	check_div_zero(rst);
	double sum = val / rst;
	set_var(var_name , sum);

	return sum;
}

static double
powass_to_id (const char *var_name)
{
	/*
		Get the value of VAR itself.
	*/
	double val = get_id_val(var_name);
	/*
		Get the value after '**='.
	*/
	double rst = expr(GET);
	
	check_div_zero(rst);
	double sum = pow(val , rst);
	set_var(var_name , sum);

	return sum;
}

static double
deal_with_symbol (void)
{
	double rst;
	const char *var_name =
		add_entry(strdup_n(cur_token.rep));

	get_next_token();
	switch(cur_token.class)
	{
		case ASSIGN:
			rst = assign_to_id(var_name);
			break;
		case ADD_ASS:
			rst = addass_to_id(var_name);
			break;
		case SUB_ASS:
			rst = subass_to_id(var_name);
			break;
		case MUL_ASS:
			rst = mulass_to_id(var_name);
			break;
		case DIV_ASS:
			rst = divass_to_id(var_name);
			break;
		case POW_ASS:
			rst = powass_to_id(var_name);
			break;

		case INCR:
		case DECR:
			rst = do_postincr(var_name , cur_token.class);
			break;

		case SYMBOL:
			throw_syn_err();

		default:
			rst = get_id_val(var_name);
	}

	free_entry(var_name);
	return rst;
}

static double
get_number (void)
{
	return atof(cur_token.rep);
}

static double
deal_with_lp (void)
{
	have_lp = 1;
	double rst = expr(GET);
	have_lp = 0;
	check_tk_class(R_PAREN);
	get_next_token();	/* eat ')' */

	switch(cur_token.class)
	{
		case ADD:
		case SUB:
		case MUL:
		case DIV:
		case POW:
		case EoL:break;

		default:
			throw_syn_err();
	}

	return rst;
}

static double
deal_with_number (void)
{
	double rst = get_number();
	get_next_token();
	return rst;
}

static double
prim (int get)
{
	if(get)
		get_next_token();

	switch(cur_token.class)
	{	
		case SYMBOL:
			return deal_with_symbol();
		case NUMBER:
			return deal_with_number();
		
		case ADD:
			return prim(GET);
		case SUB:
			return -prim(GET);
		
		case INCR:
		case DECR:
			return do_preincr(cur_token.class);

		case L_PAREN:
			return deal_with_lp();

		default:
			throw_syn_err();
	}
}

static double
term (int get)
{
	double rst = prim(get);

	while(1)
		switch(cur_token.class)
		{
			case MUL:
				have_op = 1;
				rst *= prim(GET);break;
			case DIV:
			{
				double pri_rst = prim(GET);
				check_div_zero(pri_rst);
				have_op = 1;
				rst /= pri_rst;break;
			}
			case POW:
				rst = pow(rst , prim(GET));
				break;

			default:
				return rst;
		}
}



double
expr (int get)
{
	double rst = term(get);

	/*
		Deal_with cases:
		'123(' , '123)' , etc.
	*/
	check_paren_err();

	while(1)
	{
		switch(cur_token.class)
		{
			case ADD:
				have_op = 1;
				rst += term(GET);break;
			case SUB:
				have_op = 1;
				rst -= term(GET);break;

			/*
				Deal with cases:
				'123 123' , etc.
			*/
			case NUMBER:
			/*
				Deal with cases:
				'123 =' , etc.
			*/
			case ASSIGN:
			/*
				These operator must appear
				after symbol.
			*/
			case ADD_ASS:
			case SUB_ASS:
			case MUL_ASS:
			case DIV_ASS:
			case POW_ASS:

			case SYMBOL:
				throw_syn_err();

			default:
				have_op = 0;
				return rst;
		}

		/*
			Deal with cases:
			'2+2(' , '3+3)' , etc.
		*/
		check_paren_err();
	}
}
