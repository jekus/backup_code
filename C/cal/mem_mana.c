#include <stdlib.h>
#include "xmalloc.h"
#include "mem_mana.h"

typedef struct mem_entry{
	void *addr;
	struct mem_entry *next;
}MEM_ENTRY;

static MEM_ENTRY *entry_list = NULL;


void *
add_entry (void *addr)
{
	MEM_ENTRY *new_entry =
		xmalloc(sizeof(MEM_ENTRY));

	new_entry->addr = addr;
	new_entry->next = entry_list;
	entry_list = new_entry;

	return addr;
}

int
free_entry (void *addr)
{
	MEM_ENTRY **tmp = &entry_list;

	while(*tmp && (*tmp)->addr != addr)
		tmp = &(*tmp)->next;
	if(!(*tmp))
		return 0;

	MEM_ENTRY *cur = *tmp;
	*tmp = (*tmp)->next;
	free(cur->addr);
	free(cur);
	return 1;
}

void
free_all_entry (void)
{
	MEM_ENTRY *next;

	while(entry_list)
	{
		next = entry_list->next;
		free(entry_list->addr);
		free(entry_list);
		entry_list = next;
	}
}
