#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>
#include "lex.h"
#include "mem_mana.h"
#include "error.h"

static const char *errmsg_prefix = "- ";

const char *err_msg[] = {
	"Syntax Error" , "Can't divide by 0" ,
	"Unknown character '%c'" ,
	"Memory Allocation Failed" ,
	"Failed in creating hashtable" ,
	"Hashtable has been filled" , 
	"Unknown symbol"
};

jmp_buf jmpbuf;


void
error (const char *err_msg , ...)
{
	printf("%s", errmsg_prefix);
	va_list ap;
	va_start(ap , err_msg);
	vprintf(err_msg , ap);
	puts("");
	va_end(ap);

	/*
		Set the End-Of-Line flag.
	*/
	set_eol();
	free_all_entry();
	longjmp(jmpbuf , 1);
}
