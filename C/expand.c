#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

enum{
	ALPHA , REVER_ALPHA ,
	DIGIT , REVER_DIGIT
};

static const char *charset[] = {
	"abcdefghijklmnopqrstuvwxyz" ,
	"zyxwvutsrqponmlkjihgfedcba" ,
	
	"0123456789" , "9876543210"
};


/*
	Copy the desired char set
	into 'dst'.

	If 'pure_beg' is ture ,
	it will copy the 'begin_ch' to 'dst' ,
	no copy otherwise.
*/
static void
copy_charset (
	char *dst , int *idx_ptr ,
	int chset , int begin_ch , int end_ch ,
	int pure_beg)
{
	char chset_start = *charset[chset];
	int copy_num = abs(begin_ch - end_ch) + 1;

	memcpy(dst ,
		charset[chset] + abs(begin_ch - chset_start) +
			(pure_beg ? 0 : 1) ,
		sizeof(char) * copy_num);

	*idx_ptr += copy_num;
}

static void
expand_alpha (
	const char **src_ptr , char *dst ,
	int *idx_ptr)
{
#define INCR 0
#define DECR 1

	int idx = *idx_ptr;
	const char *src = *src_ptr;

	/* The trend of change */
	int ce_trend = -1;
	/* Whether the point is a pure begin point */
	int pure_beg = 1;

	char begin_ch = *src++ , end_ch ,
		turning_ch /* the turning point */;

	/* Determine the expand range */
	if(*src == '-' && isalpha(*(src+1)))
	{
		/*
			Deal with cases like:
			'a-a-a-' , etc.
		*/
		do
		{
			turning_ch = *(src+1);
			src += 2;
		}while(*src == '-' &&
			isalpha(*(src+1)) &&
			turning_ch == begin_ch);

		if(begin_ch == turning_ch)
			end_ch = begin_ch;
		
		/* Build the initial trend of sequence */
		if(turning_ch > begin_ch)
			ce_trend = INCR;
		else if(turning_ch < begin_ch)
			ce_trend = DECR;
		
		/* Check whether the sequence is a hill */
		while(*src == '-' && isalpha(*(src+1)))
		{
			end_ch = *(src+1);
			src += 2;

			if(ce_trend == INCR && end_ch < turning_ch)
			{
				copy_charset(dst + idx , &idx ,
					ALPHA , begin_ch , turning_ch ,
					pure_beg);

				begin_ch = turning_ch;
				pure_beg = 0;
				/* The sequence starts to decrease */
				ce_trend = DECR;
			}
			else if(ce_trend == DECR && end_ch > turning_ch)
			{
				copy_charset(dst + idx , &idx ,
					REVER_ALPHA , begin_ch , turning_ch ,
					pure_beg);

				begin_ch = turning_ch;
				pure_beg = 0;
				/* The sequence starts to increase */
				ce_trend = INCR;
			}

			turning_ch = end_ch;
		}

		copy_charset(dst + idx , &idx ,
			(ce_trend == INCR ? ALPHA : REVER_ALPHA) ,
			begin_ch , end_ch , pure_beg);
	}
	else
	{
		/*
			Deal with cases like:
			'a-' , '-a-' , '-a' , etc.
		*/
		dst[idx++] = begin_ch;
		++src;
	}

	*idx_ptr = idx;
	*src_ptr = src;

#undef DECR
#undef INCR
}

static void
expand_digit (
	const char **src_ptr , char *dst ,
	int *idx_ptr)
{
	int idx = *idx_ptr;
	const char *src = *src_ptr;
	char begin_ch = *src++ , end_ch;
	
	while(*src == '-' && isdigit(*(src+1)))
		src += 2;
	end_ch = *(src-1);

	copy_charset(dst + idx , &idx ,
		DIGIT , begin_ch , end_ch , 1);

	*idx_ptr = idx;
	*src_ptr = src;
}

/*
	Expand shorthand notations like a-z in
	'src' into the abc...xyz , and store
	the result in 'dst'.

	Can handle cases like a-b-c , a-z0-9 , and -a-z.
	Leading or trailing '-' is taken literally.

	User must guarantee that 'dst' has enough space.
*/
static void
expand (const char *src , char *dst)
{
	int idx = 0;

	while(*src)
		if((isalpha(*src) && *(src+1) == '-') ||
			(*src == '-' && isalpha(*(src+1)) && ++src))
				expand_alpha(&src , dst , &idx);
		else if((isdigit(*src) && *(src+1) == '-') ||
			(*src == '-' && isdigit(*(src+1)) && ++src))
				expand_digit(&src , dst , &idx);
		else
			dst[idx++] = *src++;

	dst[idx] = '\0';
}

int
main (void)
{
	char *src = "I am a-a-a-k-k-k-k-a-a-a-a-a-a-f-a0-9-a-f b--";
	char dst[256];

	expand(src , dst);
	puts(dst);

	return 0;
}
