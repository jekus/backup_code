#include <stdio.h>

/*
    Convert the unprintable characters
    in 'str' into visible form.

    The result is stored in 'res'.
    And the user must guarantee that
    'res' has enough space.

    * newline to '\n' , for example. *
*/
static void
escape (char *res , const char *str)
{
    int idx = 0;

    while(*str)
    {
        switch(*str)
        {
            case '\a':
                res[idx++] = '\\';
                res[idx] = 'a';break;
            case '\b':
                res[idx++] = '\\';
                res[idx] = 'b';break;
            case '\t':
                res[idx++] = '\\';
                res[idx] = 't';break;
            case '\n':
                res[idx++] = '\\';
                res[idx] = 'n';break;
            case '\v':
                res[idx++] = '\\';
                res[idx] = 'v';break;
            case '\f':
                res[idx++] = '\\';
                res[idx] = 'f';break;
            case '\r':
                res[idx++] = '\\';
                res[idx] = 'r';break;

            default:
                res[idx] = *str;
        }
        
        ++idx;
        ++str;
    }

    res[idx] = '\0';
}

/* The reverse of 'escape'. */
static void
enter (char *res , const char *str)
{
    int idx = 0;

    while(*str)
    {
        if(*str == '\\')
            switch(*++str)
            {
                case 'a':
                    res[idx] = '\a';break;
                case 'b':
                    res[idx] = '\b';break;
                case 't':
                    res[idx] = '\t';break;
                case 'n':
                    res[idx] = '\n';break;
                case 'v':
                    res[idx] = '\v';break;
                case 'f':
                    res[idx] = '\f';break;
                case 'r':
                    res[idx] = '\r';break;

                /* Just a common escape */
                default:
                    res[idx++] = '\\';
                    res[idx] = *str;
            }
        else
            res[idx] = *str;

        ++idx;
        ++str;
    }

    res[idx] = '\0';
}

int
main (void)
{
    const char str[] = "Hello ,\nI am Jekus Neky\a\b\t.\nI like programming.\n";
    char buf_1[sizeof(str)+30] ,
         buf_2[sizeof(str)];

    puts(str);
    escape(buf_1 , str);
    puts(buf_1);
    enter(buf_2 , buf_1);
    putchar('\n');
    puts(buf_2);

    return 0;
}
