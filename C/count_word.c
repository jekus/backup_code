#include <stdio.h>
#include <ctype.h>

#define IN 0
#define OUT 1

int
main (void)
{
	int ch , wc = 0;
	int state = OUT;

	while((ch = getchar()) != EOF)
		if(isspace(ch))
			state = OUT;
		else if(state == OUT)
			state = IN , ++wc;
	
	printf("%d\n" , wc);

	return 0;
}
