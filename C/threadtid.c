#include <apue.h>
#include <pthread.h>

void * thr (void * arg)
{
	unsigned int tid;
	printf("Thread %d , PID: %u TID: %u (0x%X)\n",(int)arg , (unsigned int)getpid() , tid = (unsigned int)pthread_self() , tid);
	return(NULL);
}

int main (void)
{
	pthread_t tid;
	for (int i = 1;i <= 10;i++)
	{
		if(pthread_create(&tid , NULL , thr , (void *)i) != 0)
			err_quit("Line 15: pthread_create error");
	}
	sleep(1);
	exit(0);
}
