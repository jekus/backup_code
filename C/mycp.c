#include <apue.h>
#include <fcntl.h>

#define MAX_PATH 512
#define RWRR (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH )

int main (int argc , char * argv[])
{
	struct stat dst;
	int src , dt;
	char buf;
	char path[MAX_PATH+1];
	char *ptr , *temp;
	
	if(argc < 3)
		err_quit("Usage: %s src... dst",argv[0]);
	
	close(open(argv[argc - 1] , O_RDWR | O_CREAT | O_EXCL , RWRR));
	
	if(lstat(argv[argc - 1] , &dst) < 0)
		err_quit("can't stat %s",argv[argc - 1]);
	if(argc > 3 && S_ISDIR(dst.st_mode) == 0)
		err_quit("'%s' is not a directory",argv[argc - 1]);
	
	if(S_ISDIR(dst.st_mode))
	{
		strcpy(path , argv[argc - 1]);
		ptr = path + strlen(path);
		*ptr++ = '/';
		*ptr = 0;
	}
	else
	{
		if((src = open(argv[1] , O_RDONLY)) < 0)
		{
			err_sys("can't open %s : ",argv[1]);
			exit(0);
		}
		if((dt = open(argv[2] , O_RDWR | O_CREAT | O_TRUNC , RWRR)) < 0)
		{
			err_sys("can't open %s : ",argv[2]);
			exit(0);
		}
		while(read(src , &buf , 1) > 0)
			if(buf != 0)
				if(write(dt , &buf , 1) < 0)
					err_sys("error in writing %s",argv[2]);
		exit(0);
	}
	
	for(int i = 1;i < argc - 1;i++,*ptr = '\0')
	{
		if((src = open(argv[i] , O_RDONLY)) < 0)
		{
			err_sys("can't open %s : ",argv[i]);
			continue;
		}
		if(temp = strrchr(argv[i] , '/'))
			strcat(path , temp+1);
		else
			strcat(path , argv[i]);
		if((dt = open(path , O_RDWR | O_CREAT | O_TRUNC , RWRR)) < 0)
			err_sys("can't open %s : ",path);
		
		while(read(src , &buf , 1) > 0)
			if(buf != 0)
				if(write(dt , &buf , 1) < 0)
					err_sys("error in writing %s",path);

		if(close(src) < 0)
			err_sys("Error in closing %s",argv[i]);

		if(close(dt) < 0)
			err_sys("Error in closing %s",path);
	}

	exit(0);
}
