#include <stdio.h>
#include <stdlib.h>

void
Error (char *str)
{
	printf("\r%s\n", str);
	exit(1);
}
