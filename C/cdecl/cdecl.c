#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXTOKENLEN (64+1)
#define MAXTOKENS 128
#define DEF_OUT_SIZE (128+1)

enum type_tag{
	IDENTIFIER , QUALIFIER , TYPE
};

typedef struct token{
	char type;
	char string[MAXTOKENLEN];
}Token_t;

static int top = -1;
static Token_t stack[MAXTOKENS];
static Token_t this;	/* current token */

extern void Error (char *);

#define pop stack[top--]
#define push(s) stack[++top] = s

static enum type_tag
classify_string (void)
{
	char *s = this.string;

	if(!strcmp(s,"const"))
	{
		strcpy(s,"read-only");
		return QUALIFIER;
	}
	else if(!strcmp(s,"volatile"))
		return QUALIFIER;
	else if(!strcmp(s , "void") || !strcmp(s , "char") ||
		!strcmp(s , "signed") || !strcmp(s , "unsigned") ||
		!strcmp(s , "short") || !strcmp(s , "int") ||
		!strcmp(s , "long") || !strcmp(s , "float") ||
		!strcmp(s , "double") || !strcmp(s , "struct") ||
		!strcmp(s , "union") || !strcmp(s , "enum"))
			return TYPE;

	return IDENTIFIER;
}

static void
gettoken (void)
{
	char *p = this.string;

	while(isblank(*p = getchar()));

	if(isalnum(*p))
	{
		while(isalnum(*++p = getchar()));
		ungetc(*p , stdin);
		*p = '\0';
		this.type = classify_string();
		return;
	}

	if(*p == '*')
	{
		strcpy(this.string , "pointer to");
		this.type = '*';
		return;
	}

	this.string[1] = '\0';
	this.type = *p;
}

static void
read_to_first_identifier (void)
{
	gettoken();
	while(this.type != IDENTIFIER)
	{
		push(this);
		gettoken();
	}
	printf("%s is ",this.string);
	gettoken();
}


static void
deal_with_arrays (void)
{
	while(this.type == '[')
	{
		printf("array ");
		gettoken();		/* get ']' or number */
		if(isdigit(*this.string))
		{
			printf("0-%d ", atoi(this.string)-1);
			gettoken();		/* get ']' */
		}
		gettoken();
		printf("of ");
	}
}

static void
deal_with_function_args (void)
{
	int have_args = 0;

	printf("function ");
	while(this.type != ')')
	{
		if(this.type == TYPE)
		{
			if(!have_args)
			{
				printf("with args of ");
				have_args = 1;
			}
			printf("%s," , this.string);

			gettoken();
			if(this.type == ',')
				gettoken();
			if(this.type == ',')
				Error("invalid declaration");
			else
				continue;
		}
		gettoken();
	}
	printf("\b ");
	gettoken();
	printf("returning ");
}

static void
deal_with_pointers (void)
{
	while(stack[top].type == '*')
		printf("%s ",pop.string);
}

static void
deal_with_declarator (void)
{
	switch(this.type)
	{
		case '[': deal_with_arrays();break;
		case '(': deal_with_function_args();
	}
	deal_with_pointers();

	while(top >= 0)
	{
		if(stack[top].type == '(')
		{
			pop;
			gettoken();
			deal_with_declarator();
		}
		else
			printf("%s ",pop.string);
	}
}

int
main (void)
{	
	read_to_first_identifier();
	deal_with_declarator();
	puts("");
	return 0;
}
