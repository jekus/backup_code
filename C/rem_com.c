/*
	Remove all the comments in
	C source code. And print to
	STDOUT.

	Assume that the syntax is correct.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

static FILE *in_file = NULL;
static char *file_path = NULL;
static int is_file = 0;

void
set_in_file (int argc , char *argv[])
{
	if(argc < 2)
		in_file = stdin;
	else
	{
		in_file = fopen(argv[1] , "r");
		file_path = argv[1];
		is_file = 1;
	}
}

void
rem_com_in_file (void)
{
	struct stat file_stat;
	off_t file_size;
	stat(file_path , &file_stat);
	file_size = file_stat.st_size;

	char *file_buf = malloc(file_size);
	fread(file_buf , file_size , 1 , in_file);

	int new_idx = 0;
	char *new_buf = malloc(file_size);


#define in_range(pos) \
	(pos < file_size)
#define is_str_sym(ch) \
	(ch == '"')

#define is_comment_starter(idx) \
	(file_buf[idx] == '/' &&	\
		in_range(idx+1) &&		\
		file_buf[idx+1] == '*')
#define is_comment_stopper(idx)	\
	(file_buf[idx] == '*' && 	\
		in_range(idx+1) &&		\
		file_buf[idx+1] == '/')

	int in_str = 0;
	int in_com = 0;
	for(int idx = 0;idx < file_size;++idx)
	{
		if(!in_str)
			if(is_comment_starter(idx))
				in_com = 1;
			else if(is_comment_stopper(idx))
			{
				idx += 2;
				in_com = 0;
			}

		if(!in_com)
		{
			if(is_str_sym(file_buf[idx]))
				in_str = ~in_str;
			new_buf[new_idx++] = file_buf[idx];
		}
	}

#undef is_comment_stopper
#undef is_comment_starter
#undef in_range

	fwrite(new_buf , sizeof(char) * new_idx , 1 , stdout);
}

int
is_comment_starter (char ch)
{
	if(ch == '/')
		if(ungetc(getchar() , stdin) == '*')
		{
			getchar();
			return 1;
		}
	
	return 0;
}

int
is_comment_stopper (char ch)
{
	if(ch == '*')
		if(ungetc(getchar() , stdin) == '/')
		{
			getchar();
			return 1;
		}

	return 0;
}

void
rem_com_in_stdin (void)
{
	char ch;
	int in_str = 0;
	int in_com = 0;

	while((ch = getchar()) != EOF)
	{
		if(!in_str)
			if(is_comment_starter(ch))
				in_com = 1;
			else if(is_comment_stopper(ch))
			{
				ch = getchar();
				in_com = 0;
			}

		if(!in_com)
			if(is_str_sym(ch))
				in_str = ~in_str;
			else
				putchar(ch);
	}

#undef is_str_sym
}

void
rem_com (void)
{
	if(is_file)
		rem_com_in_file();
	else
		rem_com_in_stdin();
}

int
main (int argc , char *argv[])
{
	set_in_file(argc , argv);
	rem_com();

	return 0;
}
