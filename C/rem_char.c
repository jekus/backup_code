/*
	Remove all the character appear in
	str_2 in str_1.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char alpha_map[256];

void
set_alpha_map (const char *str_2)
{
	memset(alpha_map , 0 , sizeof(char) * 256);

	while(*str_2)
		alpha_map[*str_2++] = 1;
}

char *
squeeze (const char *str_1 , const char *str_2)
{
	set_alpha_map(str_2);

	int idx = 0;
	char *new_str = malloc(strlen(str_1) + 1);
	while(*str_1)
	{
		if(!alpha_map[*str_1])
			new_str[idx++] = *str_1;
		++str_1;
	}

	new_str[idx] = '\0';

	return new_str;
}

int
main (void)
{
	const char *str_1 = "aaaaa@@@@@@@@@@@@2aa34309g0934gaaaaasnblkkajdlkfjaaaaaaaaaaaawiojfiowajaaajfjkdslajklbanmcvhjshewuityerwiuhgaihiausdf";
	const char *str_2 = "aaaaafjjwhfhsdkaj@jhuiewurickjj39854u9834y5v9374095873498b5934-y9nviuesyo";

	puts(squeeze(str_1 , str_2));

	return 0;
}
