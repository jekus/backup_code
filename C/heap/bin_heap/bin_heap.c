#include <stdlib.h>
#include "bin_heap.h"

#define node_parent(node_i) ((node_i - 1) / 2)
#define node_lch(node_i)    (2 * node_i + 1)
#define node_rch(node_i)    (2 * node_i + 2)

Heap *
heap_create (Comp_Func comp , Destructor destor , unsigned long long init_undercont_size , unsigned long long undercont_size_step)
{
    Heap *heap = calloc(1 , sizeof(Heap));
    if(!heap)
        return NULL;
    heap->comp = comp;
    heap->destor = destor;

    void **Ar = malloc(sizeof(void *) * init_undercont_size);
    if(!Ar)
        return NULL;
    heap->Ar = Ar;
    heap->Ar_size = init_undercont_size;
    heap->Ar_size_step = undercont_size_step;

    return heap;
}

int
heap_destroy (Heap *heap)
{
    Destructor destor = heap->destor;
    if(destor)
        for(unsigned long long i = 0;i < heap->heap_size;++i)
            if(destor(heap->Ar[i]))
                return 1;
    free(heap->Ar);
    free(heap);
    return 0;
}

/*
    Shift up the last element
    in the heap to its place.
*/
static void
shift_up (Heap *heap)
{
    Comp_Func comp = heap->comp;
    void **Ar = heap->Ar;
    unsigned long long node_i = heap->heap_size;
    unsigned long long p_node_i = node_parent(node_i);
    void *target = Ar[heap->heap_size];
    while(node_i && comp(target , Ar[p_node_i]))
    {
        Ar[node_i] = Ar[p_node_i];
        node_i = p_node_i;
        p_node_i = node_parent(node_i);
    }
    Ar[node_i] = target;
}

int
heap_insert (Heap *heap , void *data)
{
    if(heap->heap_size + 1 > heap->Ar_size)
    {
        void **Ar = realloc(heap->Ar , sizeof(void *) * (heap->Ar_size + heap->Ar_size_step));
        if(!Ar)
            return 1;
        heap->Ar = Ar;
        heap->Ar_size += heap->Ar_size_step;
    }

    heap->Ar[heap->heap_size] = data;
    shift_up(heap);
    ++(heap->heap_size);

    return 0;
}

static void
shift_down (Heap *heap)
{
    unsigned long long
        node_i = 0 ,
        node_lch_i = node_lch(0) ,
        node_rch_i = node_rch(0);
    void **Ar = heap->Ar;
    Comp_Func comp = heap->comp;
    void *target = Ar[0];
    unsigned long long next_node_i;
    while(node_lch_i < heap->heap_size)
    {
        next_node_i = node_lch_i;
        if(node_rch_i < heap->heap_size && comp(Ar[node_rch_i] , Ar[node_lch_i]))
            ++next_node_i;
        if(comp(target , Ar[next_node_i]))
            break;

        Ar[node_i] = Ar[next_node_i];

        node_i = next_node_i;
        node_lch_i = node_lch(node_i);
        node_rch_i = node_rch(node_i);
    }
    Ar[node_i] = target;
}

void *
heap_extract (Heap *heap)
{
    void *data = heap->Ar[0];

    --(heap->heap_size);
    if(heap->heap_size)
    {
        heap->Ar[0] = heap->Ar[heap->heap_size];
        shift_down(heap);
    }

    return data;
}

unsigned long long
heap_size (Heap *heap)
{
    return heap->heap_size;
}
