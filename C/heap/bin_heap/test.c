#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "bin_heap.h"

#define Nums_Size 50
#define Init_undercont_size 512
#define undercont_size_step 512

int
comp (void *data_1 , void *data_2)
{
    return *(int *)data_1 > *(int *)data_2;
}

void
print_heap (Heap *heap)
{
    printf("heap_size: %Lu\tAr_size: %Lu\n", heap->heap_size , heap->Ar_size);
    int **Ar = (int **)heap->Ar;
    for(unsigned long long i = 0;i < heap->heap_size;++i)
    {
        printf("%d -> ", *Ar[i]);
        if(2*i+1 < heap->heap_size)
            printf("%d\t", *Ar[2*i+1]);
        if(2*i+2 < heap->heap_size)
            printf("%d", *Ar[2*i+2]);
        puts("");
    }
}

int
main (void)
{
    srand(time(NULL));
    
    Heap *heap = heap_create(comp , NULL , Init_undercont_size , undercont_size_step);
    print_heap(heap);
    for(int i = 0;i < Nums_Size;++i)
    {
        int *num = malloc(sizeof(int));
        *num = rand() % 100;
        heap_insert(heap , num);
    }
    print_heap(heap);

    for(int i = 0;i < Nums_Size/2;++i)
        heap_extract(heap);
    puts("");
    print_heap(heap);

    return 0;
}
