#ifndef _BIN_HEAP_H_
#define _BIN_HEAP_H_

typedef int (*Comp_Func) (void * , void *);
typedef int (*Destructor) (void *);

typedef struct{
    Comp_Func comp;
    Destructor destor;
    void **Ar;
    unsigned long long Ar_size;
    unsigned long long Ar_size_step;
    unsigned long long heap_size;
}Heap;

extern Heap *heap_create (Comp_Func comp , Destructor destor , unsigned long long init_undercont_size , unsigned long long undercont_size_step);
extern int heap_destroy (Heap *heap);
extern int heap_insert (Heap *heap , void *data);
extern void * heap_extract (Heap *heap);
extern unsigned long long heap_size (Heap *heap);

#endif
