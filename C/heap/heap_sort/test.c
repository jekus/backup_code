#include <stdio.h>

#define SIZE 5

void
p_ar (int *array , int len)
{
	puts("");
	for(int i = 0;i < len;++i)
		printf("%d\n" , array[i]);
}

int
main (void)
{
	int ar[SIZE]= {1 , 54 , -32 , 6345 , -1543};

	p_ar(ar , SIZE);
	min_heapify(ar , SIZE);
	p_ar(ar , SIZE);
	min_remove(ar , SIZE);
	p_ar(ar , SIZE);

	return 0;
}
