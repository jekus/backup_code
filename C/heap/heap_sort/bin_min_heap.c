#include <stdio.h>
#include <stdlib.h>

static void
shift_down (int *array , int idx , int num)
{
	int left = 2*idx+1 ,
		right = 2*idx+2;
	int tmp_idx;
	int target = array[idx];

	while(left < num)
	{
		tmp_idx = left;
		if(right < num && array[right] < array[left])
			++tmp_idx;
		if(array[tmp_idx] >= target)
			break;

		array[idx] = array[tmp_idx];

		idx = tmp_idx;
		left = 2*idx+1;
		right = left+1;
	}
	array[idx] = target;
}

static void
shift_up (int *array , int idx)
{
	int parent = (idx - 1)/2;
	int tmp = array[idx];

	while(idx && array[parent] > tmp)
	{
		array[idx] = array[parent];
		idx = parent;
		parent = (idx - 1)/2;
	}
	array[idx] = tmp;
}

void
min_heapify (int *array , int num)
{
	int idx = num>>1;
	for(;idx >= 0;--idx)
		shift_down(array , idx , num);
}

void
min_insert (int *array , int item , int num)
{
	array[num] = item;
	shift_up(array , num);
}

void min_remove (int *array , int num)
{
	array[0] = array[num-1];
	shift_down(array , 0 , num-1);
}

void
heap_sort (int *array , int num)
{
	min_heapify(array , num);
	while(--num)
	{
		array[num] ^= array[0];
		array[0] ^= array[num];
		array[num] ^= array[0];
		shift_down(array , 0 , num);
	}
}
