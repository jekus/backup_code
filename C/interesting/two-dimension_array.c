#include <stdio.h>
#include <stdlib.h>

#define p_addr(addr) printf("%p\n", addr)

int
main (void)
{
	int ar[2][2] = {{1 , 2} , {3 , 4}};

	p_addr(ar);
	p_addr(ar+1);
	p_addr(*ar);
	p_addr(*ar+1);

	return 0;
}
