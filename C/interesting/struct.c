#include <stdio.h>

struct foo {
	int array[10];
};
struct foo a;

int
main (void)
{
	printf("a: %p\n" , &a);
	printf("a.array: %p\n" , a.array);
	for(int i = 0;i < 10;++i)
		a.array[i] = i;
	for(int i = 0;i < 10;++i)
	{
		printf("%d\n" , (&a)[i]);
		printf("- %d\n" , ((int *)(&a))[i]);
	}

	return 0;
}
