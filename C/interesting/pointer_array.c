#include <stdio.h>

void
foo (void)
{
	puts("FOO!");
}

int
main (void)
{
	void (*a[1]) (void) = {foo};
	a[0]();
	(*(a[0]))();
	return 0;
}
