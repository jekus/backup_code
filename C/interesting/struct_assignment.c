#include <stdio.h>
#include <string.h>

#define MAXLEN 128

typedef struct{
	int num;
	char str[MAXLEN];
}FOO;

int
main (void)
{
	FOO a , b;

	printf("a.str: %p\nb.str: %p\n", a.str , b.str);
	a.num = 10;
	strcpy(a.str , "Jekus Neky");
	b = a;
	printf("a.str: %p\nb.str: %p\n", a.str , b.str);
	puts(a.str);
	puts(b.str);

	return 0;
}
