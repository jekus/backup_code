#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <readline/readline.h>
#include <string.h>
#include <ctype.h>
#include "lex.h"

#define LAYOUT_MASK					(1 << 1)
#define OPERATOR_MASK				(1 << 2)
#define DIGIT_MASK					(1 << 3)
#define UC_LETTER_MASK				(1 << 4)
#define LC_LETTER_MASK				(1 << 5)
#define SEPARATOR_MASK				(1 << 6)

#define bits_of(ch)					(charbits[(ch) & 0xff])

#define is_end_of_input(ch)			((ch) == '\0')
#define is_comment_starter(ch)		((ch) == '#')
#define is_comment_stopper(ch)		((ch) == '#' || (ch) == '\n')
#define is_underscore(ch)			((ch) == '_')
#define is_dot(ch)					((ch) == '.')
#define is_layout(ch)				(bits_of(ch) & LAYOUT_MASK)
#define is_operator(ch)				(bits_of(ch) & OPERATOR_MASK)
#define is_separator(ch)			(bits_of(ch) & SEPARATOR_MASK)
#define is_uc_letter(ch)			(bits_of(ch) & UC_LETTER_MASK)
#define is_lc_letter(ch)			(bits_of(ch) & LC_LETTER_MASK)
#define is_alpha(ch)				(is_uc_letter(ch) || is_lc_letter(ch))
#define is_digit(ch)				(bits_of(ch) & DIGIT_MASK)
#define is_alnum(ch)				(is_digit(ch) || is_alpha(ch))

static const char charbits[256] = {
	0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
	LAYOUT_MASK , LAYOUT_MASK , LAYOUT_MASK ,
	LAYOUT_MASK , LAYOUT_MASK ,
	0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
	0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
	LAYOUT_MASK , OPERATOR_MASK ,
	0 , 0 , 0 ,
	OPERATOR_MASK , OPERATOR_MASK , 0 ,
	SEPARATOR_MASK , SEPARATOR_MASK ,
	OPERATOR_MASK , OPERATOR_MASK , SEPARATOR_MASK ,
	OPERATOR_MASK , 0 , OPERATOR_MASK ,
	DIGIT_MASK , DIGIT_MASK , DIGIT_MASK ,
	DIGIT_MASK , DIGIT_MASK , DIGIT_MASK ,
	DIGIT_MASK , DIGIT_MASK , DIGIT_MASK , DIGIT_MASK ,
	0 , SEPARATOR_MASK , OPERATOR_MASK , OPERATOR_MASK ,
	OPERATOR_MASK , OPERATOR_MASK , OPERATOR_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK , UC_LETTER_MASK ,
	UC_LETTER_MASK , UC_LETTER_MASK ,
	SEPARATOR_MASK , 0 , SEPARATOR_MASK , OPERATOR_MASK ,
	0 , 0 ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK , LC_LETTER_MASK ,
	LC_LETTER_MASK , LC_LETTER_MASK ,
	SEPARATOR_MASK , OPERATOR_MASK , SEPARATOR_MASK ,
	OPERATOR_MASK , 0
};

#define next_char()	(input_char = input[++dot])

static char *input;
static int dot;				/* dot position in input */
static int input_char;		/* character at dot position */

Token_t Token;

void
start_lex (void)
{
	input = readline("INPUT: ");
	if(input)
	{
		dot = -1;
		next_char();
	}
	else
		input_char = '\0';
}

static void
skip_layout_comment (void)
{
	while(is_layout(input_char))
		next_char();
	
	while(is_comment_starter(input_char))
	{
		next_char();
		while(!is_comment_stopper(input_char))
		{
			if(is_end_of_input(input_char))
				return;
			next_char();
		}
		
		next_char();
		while(is_layout(input_char))
			next_char();
	}
}

static void
get_identifier (void)
{
	Token.class = IDENTIFIER;
	next_char();

	while(is_alnum(input_char))
		next_char();
	
	while(is_underscore(input_char))
	{
		next_char();
		while(is_alnum(input_char))
			next_char();
	}
}

static void
get_number (void)
{
	int have_dot = 0;
	Token.class = INTEGER;
	next_char();

	while(is_digit(input_char))
	{
		next_char();
		if(!have_dot && is_dot(input_char) &&
			is_digit(input[dot+1]))
		{
			Token.class = FLOAT;
			have_dot = 1;
			next_char();
		}
	}
}

static char *
stringfy (int start , int len)
{
	static char *str = NULL;

	str = realloc(str , sizeof(char) * len + 1);
	if(!str)
	{
		puts("Memory allocation failed");
		exit(EXIT_FAILURE);
	}

	for(int idx = 0;idx < len;)
		str[idx++] = input[start++];
	str[len] = '\0';

	return str;
}

static void
classify_token (void)
{
	int start_dot = dot;

	if(is_alpha(input_char))
		get_identifier();
	else if(is_digit(input_char))
		get_number();
	else if(is_operator(input_char))
	{
		Token.class = OPERATOR;
		next_char();
	}
	else if(is_separator(input_char))
	{
		Token.class = SEPARATOR;
		next_char();
	}
	else
	{
		Token.class = ERRONEOUS;
		next_char();
	}
	
	Token.repr = stringfy(start_dot , dot-start_dot);
}

void
get_next_token (void)
{
	skip_layout_comment();

	if(is_end_of_input(input_char))
	{
		Token.class = EoF;
		Token.repr = "<EoF>";
		return;
	}

	classify_token();
}
