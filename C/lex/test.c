#include <stdio.h>
#include "lex.h"

int
main (void)
{
	start_lex();

	do
	{
		get_next_token();
		switch(Token.class)
		{
			case IDENTIFIER:	printf("Identifier");break;
			case INTEGER:		printf("Integer");break;
			case FLOAT:			printf("Float");break;
			case OPERATOR:		printf("Operator");break;
			case SEPARATOR:		printf("Separator");break;
			case ERRONEOUS:		printf("Erroneous token");break;
			case EoF:			printf("End Of File");break;
		}
		printf(": %s\n", Token.repr);
	}while(Token.class != EoF);

	return 0;
}
