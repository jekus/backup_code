#ifndef _LEX_H_
#define _LEX_H_

/* 0 - 255 reserved for ASCII */
enum Token_class{
	EoF = 256 , IDENTIFIER ,
	INTEGER , FLOAT ,
	OPERATOR , SEPARATOR ,
	ERRONEOUS
};

typedef struct{
	char *file_name;
	int line_number;
	int char_number;
}Position_in_File;

typedef struct{
	int class;
	char *repr;
	Position_in_File pos;
}Token_t;

extern Token_t Token;

extern void start_lex (void);
extern void get_next_token (void);

#endif
