/*
	Generate a random-repeated sequence ,
	'abcabcjj4ig4jabcabcabc' , etc.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <readline/readline.h>

#define OCCUR_RATE 	0.1
#define BOUND		((1 - OCCUR_RATE) * RAND_MAX)

void
gene_random_seq (FILE *output_file)
{
	srand(time(NULL) * rand());

	int seq_len = rand() % 100;
	for(int idx = 0;idx < seq_len;++idx)
		/*
			generate the printable
			ASCII character.
		*/
		putc(rand() % 94 + 33 , output_file);
	
	fflush(output_file);
}

void
gene_seq (const char *rep_str , const int rep_times , FILE *output_file)
{
	srand(time(NULL));

	int occur_times = 0;	/* occur times of the sequence */
	int need_occur = 0;		/* the sequence occurs or not */

	while(occur_times < rep_times)
	{
		need_occur = (rand() > BOUND);
		if(need_occur)
		{
			fputs(rep_str , output_file);
			++occur_times;
		}

		/*
			Generate other random sequence
			instead of the 'rep_str'
		*/
		gene_random_seq(output_file);
	}

	putc('\n' , output_file);
	fflush(output_file);
}

void
open_output (FILE **output_file , char *file_path)
{
	FILE *file = fopen(file_path , "w+");
	if(!file)
	{
		fputs("Error in open file" , stderr);
		exit(EXIT_FAILURE);
	}

	*output_file = file;
}

int
main (int argc , char *argv[])
{
	FILE *output_file = stdout;
	if(argc > 1)
		open_output(&output_file , argv[1]);

	char *rep_str = readline("The repeated sequence: ");
	if(!rep_str)
	{
		fputs("Error while reading\n" , stderr);
		return EXIT_FAILURE;
	}

	int rep_times = -1;
	fputs("Repeated times: " , stdout);
	scanf("%d", &rep_times);
	if(rep_times < 0)
	{
		fputs("Not a valid integer\n" , stderr);
		return EXIT_FAILURE;
	}

	gene_seq(rep_str , rep_times , output_file);
	
	return EXIT_SUCCESS;
}
