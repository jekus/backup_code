#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#define FILE_PATH 	"./1.txt"
#define TAR_SEQ		"abcabc"

char *
read_file (void)
{
	struct stat in_file;
	if(stat(FILE_PATH , &in_file))
	{
		fputs("Can't get the stat of file\n" , stderr);
		exit(EXIT_FAILURE);
	}

	off_t file_size = in_file.st_size;
	char *file_buffer = malloc(file_size);
	if(!file_buffer)
	{
		fputs("Memory allocation failed\n" , stderr);
		exit(EXIT_FAILURE);
	}

	FILE *file = fopen(FILE_PATH , "r");
	if(!file)
	{
		fputs("Can't open file\n" , stderr);
		exit(EXIT_FAILURE);
	}

	if(fread(file_buffer , file_size , 1 , file) != 1)
	{
		fputs("Error in reading file\n" , stderr);
		exit(EXIT_FAILURE);
	}

	return file_buffer;
}

int
count_seq (char *file_buffer)
{
	char *have_seq = strstr(file_buffer , TAR_SEQ);

	if(!have_seq)
		return 0;
	
	return 1 + count_seq(have_seq+1);
}

int
main (int argc , char *argv[])
{
	char *file_buffer = read_file();
	int seq_num = count_seq(file_buffer);

	printf("%d\n", seq_num);

	return EXIT_SUCCESS;
}
