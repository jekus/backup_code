#include <stdio.h>
#include <stdlib.h>

#define SIZE 100000

extern void gene_random_ar (int [] , int , int);

static void
shellsort (int vec[] , int n)
{
	int gap , i , j , tmp;

	for(gap = n / 2;gap;gap >>= 1)
		for(i = gap;i < n;++i)
			for(j = i - gap;j >= 0 && vec[j] > vec[j+gap];j -= gap)
			{
				tmp = vec[j];
				vec[j] = vec[j+gap];
				vec[j+gap] = tmp;
			}
}

int
main (void)
{
	int *ar = malloc(sizeof(int) * SIZE);
	
	gene_random_ar(ar , SIZE , 1000);
	shellsort(ar , SIZE);

	return 0;
}
