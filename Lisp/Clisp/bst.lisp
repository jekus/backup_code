(defstruct (node (:print-function
                    (lambda (n s d)
                        (format s "#TREE_NODE<~A>" (node-ele n)))))
    ele (lc NIL) (rc NIL))

(defun bst-insert (obj v lt)
    (if (null v)
        (make-node :ele obj)
        (let ((ele (node-ele v)))
            (if (eql ele obj)
                v
                (if (funcall lt obj ele)
                    (make-node
                        :ele ele
                        :lc (bst-insert obj (node-lc v) lt)
                        :rc (node-rc v))
                    (make-node
                        :ele ele
                        :lc (node-lc v)
                        :rc (bst-insert obj (node-rc v) lt)))))))

(defun bst-find (obj v lt)
    (if (null v)
        NIL
        (let ((ele (node-ele v)))
            (if (eql obj ele)
                v
                (if (funcall lt obj ele)
                    (bst-find obj (node-lc v) lt)
                    (bst-find obj (node-rc v) lt))))))

(defun bst-min (v)
    (and v
        (or (bst-min (node-lc v)) v)))

(defun bst-max (v)
    (and v
        (or (bst-max (node-rc v)) v)))

(defun bst-remove (obj v lt)
    (if (null v)
        NIL
        (let ((ele (node-ele v)))
            (if (eql obj ele)
                (copy-subtree v)
                (if (funcall lt obj ele)
                    (make-node
                        :ele ele
                        :lc (bst-remove obj (node-lc v) lt)
                        :rc (node-rc v))
                    (make-node
                        :ele ele
                        :lc (node-lc v)
                        :rc (bst-remove obj (node-rc v) lt)))))))

(defun copy-subtree (v)
    (if (null (node-lc v))
        (if (null (node-rc v))
            NIL
            (copy-rtree v))
        (if (null (node-rc v))
            (copy-ltree v)
            (let ((c (random 2)))
                (if (zerop c)
                    (make-node
                        :ele (node-ele (node-lc v))
                        :lc (copy-subtree (node-lc v))
                        :rc (copy-rtree v))
                    (make-node
                        :ele (node-ele (node-rc v))
                        :lc (copy-ltree v)
                        :rc (copy-subtree (node-rc v))))))))

(defun copy-ltree (v)
    (if (null (node-lc v))
        NIL
        (make-node
            :ele (node-ele (node-lc v))
            :lc (node-lc (node-lc v))
            :rc (node-rc (node-lc v)))))

(defun copy-rtree (v)
    (if (null (node-rc v))
        NIL
        (make-node
            :ele (node-ele (node-rc v))
            :lc (node-lc (node-rc v))
            :rc (node-rc (node-rc v)))))

(defun bst-preorder (func v)
    (when v
        (funcall func (node-ele v))
        (bst-preorder func (node-lc v))
        (bst-preorder func (node-rc v))))

(defun bst-inorder (func v)
    (when v
        (bst-preorder func (node-lc v))
        (funcall func (node-ele v))
        (bst-preorder func (node-rc v))))

(defun bst-postorder (func v)
    (when v
        (bst-preorder func (node-lc v))
        (bst-preorder func (node-rc v))
        (funcall func (node-ele v))
        NIL))
