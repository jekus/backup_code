(defstruct (node :print-function
                 (lambda (n s d) (format s "#TREE<~A>" (node-ele n))))
    ele (lc NIL) (mc NIL) (rc NIL))

(defun copy-triple-tree (v)
    (and v
        (make-node
            :ele (node-ele v)
            :lc (copy-triple-tree (node-lc v))
            :mc (copy-triple-tree (node-mc v))
            :rc (copy-triple-tree (node-rc v)))))

(defun triple-tree-find (obj v)
    (and v
        (or (eql obj (node-ele v))
            (triple-tree-find obj (node-lc v))
            (triple-tree-find obj (node-mc v))
            (triple-tree-find obj (node-rc v)))))
