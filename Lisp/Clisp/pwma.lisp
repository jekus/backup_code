(defvar *pw-db* nil)

(defun make-pwrd(ur pw sc vi) (list :USER ur :PASSWORD pw :SECURITY sc :VISUBLE vi))
(defun add-pwrd(ur pw sc vi) (push (make-pwrd ur pw sc vi) *pw-db*))
(defun display-pwrd() (format t "~{~{~a: ~a~%~}~%~}" *pw-db*))
(defun prompt-read(prompt)
    (format *query-io* "~a: " prompt)
    (force-output *query-io*)
    (read-line *query-io*))
(defun prompt-add() (add-pwrd
    (prompt-read "USER")
    (prompt-read "PASSWORD")
    (or (parse-integer (prompt-read "SECURITY") :junk-allowed t) 1)
    (y-or-n-p "VISUBLE[y/n]")))
(defun add-loop()
    (loop (prompt-add)
        (if (not (y-or-n-p "~%Another?[y/n]")) (return))))
(defun save-pwdb(file)
    (with-open-file (out file
        :direction :output
        :if-exists :supersede)
        (with-standard-io-syntax 
            (print *pw-db* out))))
(defun load-pwdb(file)
    (with-open-file (in file)
        (with-standard-io-syntax
            (setf *pw-db* (read in)))))
(defun select(selector)
    (remove-if-not 
        selector *pw-db*))
(defun where(&key usr pwd sec (vi nil vi-p))
    #'(lambda(pw-rd)
        (and
        (if usr (equal (getf pw-rd :USER) usr) t)
        (if pwd (equal (getf pw-rd :PASSWORD) pwd) t)
        (if sec (equal (getf pw-rd :SECURITY) sec) t)
        (if vi-p (equal (getf pw-rd :VISUBLE) vi) t))))
(defun update-pwrd(selector &key usr pwd sec (vi nil vi-p))
    (setf *pw-db*
        (mapcar
            #'(lambda(row)
                (when(funcall selector row)
                    (if usr (setf (getf row :USER) usr))
                    (if pwd (setf (getf row :PASSWORD) pwd))
                    (if sec (setf (getf row :SECURITY) sec))
                    (if vi-p (setf (getf row :VISUBLE) vi)))
                row)*pw-db*)))
(defun delete-pwrd(selector)
    (setf *pw-db* (remove-if selector *pw-db*)))
