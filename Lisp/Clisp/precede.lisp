#|
    Take an object and sequence ,
    return a list of all the objects
    that immediately precede 'obj' in 'l'.
|#
(defun precedes-iter (obj l)
    (let ((res NIL))
        (do ((i 1 (+ i 1)))
            ((= i (length l)))
            (if (eql obj (elt l i))
                (pushnew (elt l (- i 1)) res)))
        res))

(defun precedes-rec (obj l)
    (do-precedes-rec obj l 1))

(defun do-precedes-rec (obj l idx)
    (if (= idx (length l))
        NIL
        (let ((res (do-precedes-rec obj l (+ idx 1))))
            (if (eql obj (elt l idx))
                (adjoin (elt l (- idx 1)) res)
                res))))
