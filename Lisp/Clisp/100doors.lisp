#|
    There are one hundard doors ,
    the first time go through the doors and
    toggle them(if open then close , vice versa) ,
    then go through the 2th , 4th , 6th , ... and then
    3th , 6th , 9th , ... and so on.

    Q: What's the state of each door in the end?
|#

;;;; Solution 1
(setq *COUNTER* (make-list 99 :initial-element 1))

(defun count-doors nil
    (let ((rst 1))
        (loop as row from 2 to 100 do
            (loop with col = 2 do
                (setq rst (* row col))
                (if (> 100 rst)
                    (incf (nth (- rst 2) *COUNTER*))
                    (return))
                (incf col)))))
(defun output nil
    (loop as idx from 0 to 98 do
        (format t "~d : " (+ 2 idx))
        (if (evenp (nth idx *COUNTER*))
            (format t "~s~4@t" "open")
            (format t "~s~4@t" "close"))
        (if (zerop (mod (1+ idx) 4))
            (terpri))))

(count-doors)
(output)
