; Take a string like "16 Aug 1980"
; and return a list of integers representing
; the day , month and year
(defun tokenize (str &key (test #'valid-char) (start 0))
    (let ((tk_start (position-if test str :start start)))
        (if tk_start
            (let ((tk_end (position-if #'(lambda (x) (not (funcall test x))) str :start tk_start)))
                (cons (subseq str tk_start tk_end)
                    (if tk_end
                        (tokenize str :test test :start tk_end)
                        NIL)))
            NIL)))

(defun valid-char (c)
    (and (graphic-char-p c)
         (not (char= c #\ ))))

(defun parse-date (d)
    (let ((tks (tokenize d)))
        (list (parse-integer (first tks))
              (parse-month (second tks))
              (parse-integer (third tks)))))

(defconstant month-names
    #("Jan" "Feb" "Mar" "Apr" "May" "Jun"
      "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))

(defun parse-month (m)
    (let ((p (position m month-names :test #'string-equal)))
        (and p (+ p 1))))

; Alternative for 'parse-integer'
(defun get-integer (str)
    (and (every #'digit-char-p str)
         (let ((res 0))
            (dotimes (idx (length str))
                (setf res (+ (* res 10)
                             (digit-char-p (char str idx)))))
            res)))
