#|
    The functions take a list of numbers
    and an integer 'x'.
    Return TRUE iff the difference between
    each successive pair of them is 'x'
|#
(defun eq-dif-list-iter (l x)
    (do* ((i 1 (+ i 1)))
         ((>= i (length l)) t)
         (if (not (= x (abs (- (nth i l) (nth (- i 1) l)))))
            (return))))

(defun eq-dif-list-rec (l x)
    (do-eq-dif-list-rec l x 1))

(defun do-eq-dif-list-rec (l x idx)
    (if (= idx (length l))
        T
        (and (= x (abs (- (nth idx l) (nth (- idx 1) l))))
             (do-eq-dif-list-rec l x (+ idx 1)))))

(defun eq-dif-list-alter (l x)
    (let ((oplist (append (cdr l) (list (+ (car (last l)) x)))))
         (mapc #'(lambda (e1 e2)
                    (if (not (= x (abs (- e1 e2))))
                        (return-from eq-dif-list-alter NIL)))
               l oplist)
         t))
