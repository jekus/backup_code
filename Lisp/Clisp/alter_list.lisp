#|
    Implement an alternative version of list ,
    with 'car' point to the rest of the list ,
    and 'cdr' point to the first element.

    Define the following functions:
    'cons' , 'list' , 'length' , 'member'
|#

(defun alter-cons (l obj)
    (cons l (list obj)))

(defun alter-list (&rest args)
    (if (null args)
        NIL
        (let ((res NIL))
            (dolist (obj args)
                (setf res (alter-cons res obj)))
            res)))

(defun alter-length (l)
    (if (null l)
        0
        (+ 1 (alter-length (car l)))))

(defun alter-member (obj l)
    (if (null l)
        NIL
        (or
            (if (member obj l) l NIL)
            (alter-member obj (car l))
        )))
