(defun show-square-iter (start end)
    (do ((i start (+ i 1)))
        ((> i end) 'done)
        (format t "~A ~A~%" i (* i i))))

(defun show-square-rec (i end)
    (if (> i end)
        'done
        (progn
            (format t "~A ~A~%" i (* i i))
            (show-square (+ i 1) end))))
