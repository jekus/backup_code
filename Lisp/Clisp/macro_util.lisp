(defmacro while (con &rest body)
    `(loop until (not ,con) do
        ,@body))

(defmacro with-gensyms (syms &rest body)
    `(let ,(mapcar
        #'(lambda (s)
            `(,s (gensym))) syms)
        ,@body))

(defmacro my-cond (&rest con_unity)
    `(or ,@(mapcar #'(lambda (con)
        `(if ,@con)) con_unity)))

;; n start from 1
(defmacro nth_exp (n &rest exp_ut)
    (nth (1- n) exp_ut))

(defmacro double (x)
    `(setq ,x (* ,x 2)))

;; the value of variable in VAR_L will not
;; change after the evaluation of BODY
(defmacro do-const (var_l &rest body)
    `(let ((v_val ',(gensym)))
        (set v_val ',(mapcar
            #'(lambda (v) (symbol-value v))
                    var_l))
        ,@body
        (mapcar
            #'(lambda (var val) (set var val))
            ',var_l (symbol-value v_val)) nil))

;;; Alternative for 'do-const'
(defmacro var-expand (vars)
    `(mapcar (lambda (x) `(,x ,x)) ,vars))
(defmacro var-unchanged (vars body)
    `(let ,(var-expand vars)
        (,body)))

(defmacro n-of (n expr)
    `(let ((rst))
        (dotimes (,(gensym) ,n)
            (setq rst (append rst (list ,expr))))
        rst))

(defmacro ntimes (n &rest body)
    `(labels (
        (fun (x)
            (cond ((> x 0) ,@body
                (fun (1- x))))))
        (fun ,n)))
