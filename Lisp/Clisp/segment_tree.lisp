#|
    An implementation of segment tree ,
    with Lazy Propagation.
|#

(defstruct seg-tree st lazy ele_n)

(defun new-seg-tree (v)
    (let* ((vl (length v))
           (st_size (1- (* 2 (expt 2 (ceiling (log vl 2))))))
           (st (make-seg-tree
                    :st (make-array st_size)
                    :lazy (make-array st_size :initial-element 0)
                    :ele_n vl)))
        (cons-seg-tree st 0 (1- vl) 0 v)
        st))

(defun cons-seg-tree (st l r st_idx v)
    (let ((mid (floor (+ l r) 2)))
        (if (= l r)
            (setf (svref (seg-tree-st st) st_idx)
                  (svref v l))
            (setf (svref (seg-tree-st st) st_idx)
                  (+ (cons-seg-tree st l mid (1+ (* st_idx 2)) v)
                     (cons-seg-tree st (1+ mid) r (+ 2 (* st_idx 2)) v))))))

(defun update-seg-tree (st idx dif)
    (do-update-seg-tree st 0 (1- (seg-tree-ele_n st)) idx idx 0 dif))

(defun update-seg-tree-range (st l r dif)
    (do-update-seg-tree st 0 (1- (seg-tree-ele_n st)) l r 0 dif))

(defun do-update-seg-tree (st l r tl tr st_idx dif)
    (if (not (zerop (svref (seg-tree-lazy st) st_idx)))
        (let ((d (svref (seg-tree-lazy st) st_idx)))
            (progn
                (incf (svref (seg-tree-st st) st_idx) (* (1+ (- r l)) d))
                (if (not (= l r))
                    (progn
                        (incf (svref (seg-tree-lazy st) (1+ (* st_idx 2))) d)
                        (incf (svref (seg-tree-lazy st) (+ 2 (* st_idx 2))) d)))
                (setf (svref (seg-tree-lazy st) st_idx) 0))))
    (cond ((or (< r tl) (< tr l)) NIL) 
          ((and (<= tl l) (<= r tr))
            (progn
                (incf (svref (seg-tree-st st) st_idx) (* (1+ (- r l)) dif))
                (if (not (= l r))
                    (progn
                        (incf (svref (seg-tree-lazy st) (1+ (* st_idx 2))) dif)
                        (incf (svref (seg-tree-lazy st) (+ 2 (* st_idx 2))) dif)))))
          (t
            (let* ((mid (floor (+ l r) 2)))
                (do-update-seg-tree st l mid tl tr (1+ (* st_idx 2)) dif)
                (do-update-seg-tree st (1+ mid) r tl tr (+ 2 (* st_idx 2)) dif)
                (setf (svref (seg-tree-st st) st_idx)
                    (+ (svref (seg-tree-st st) (1+ (* st_idx 2)))
                       (svref (seg-tree-st st) (+ 2 (* st_idx 2)))))))))

(defun query-seg-tree (st l r)
    (do-query-seg-tree st 0 (1- (seg-tree-ele_n st)) l r 0))

(defun do-query-seg-tree (st l r tl tr st_idx)
    (if (not (zerop (svref (seg-tree-lazy st) st_idx)))
        (let ((d (svref (seg-tree-lazy st) st_idx)))
            (progn
                (incf (svref (seg-tree-st st) st_idx) (* (1+ (- r l)) d))
                (if (not (= l r))
                    (progn
                        (incf (svref (seg-tree-lazy st) (1+ (* st_idx 2))) d)
                        (incf (svref (seg-tree-lazy st) (+ 2 (* st_idx 2))) d)))
                (setf (svref (seg-tree-lazy st) st_idx) 0))))
    (cond ((or (< r tl) (< tr l)) 0)
          ((and (<= tl l) (<= r tr)) (svref (seg-tree-st st) st_idx))
          (t
            (let ((mid (floor (+ l r) 2)))
                (+ (do-query-seg-tree st l mid tl tr (1+ (* st_idx 2)))
                   (do-query-seg-tree st (1+ mid) r tl tr (+ 2 (* st_idx 2))))))))

(defun test-seg-tree ()
    (let* ((vec #(1 2 3 4 5 6 7 8 9 10))
           (st (new-seg-tree vec)))
        (format t "~A to ~A : ~A~%" 0 2 (query-seg-tree st 0 2))    ; RES: 6
        (format t "~A to ~A : ~A~%" 4 9 (query-seg-tree st 4 9))    ; RES: 45
        (format t "~A to ~A : ~A~%" 1 5 (query-seg-tree st 1 5))    ; RES: 20
        (format t "~A to ~A : ~A~%" 8 8 (query-seg-tree st 8 8))    ; RES: 9

        (update-seg-tree st 1 10)
        (update-seg-tree st 4 50)
        (update-seg-tree st 8 100)
        (terpri)
        (format t "~A to ~A : ~A~%" 0 2 (query-seg-tree st 0 2))    ; RES: 16
        (format t "~A to ~A : ~A~%" 4 9 (query-seg-tree st 4 9))    ; RES: 195
        (format t "~A to ~A : ~A~%" 1 5 (query-seg-tree st 1 5))    ; RES: 80
        (format t "~A to ~A : ~A~%" 8 8 (query-seg-tree st 8 8))    ; RES: 109
        
        (update-seg-tree st 1 -20)
        (update-seg-tree st 4 30)
        (update-seg-tree st 8 -40)
        (terpri)
        (format t "~A to ~A : ~A~%" 0 2 (query-seg-tree st 0 2))    ; RES: -4
        (format t "~A to ~A : ~A~%" 4 9 (query-seg-tree st 4 9))    ; RES: 185
        (format t "~A to ~A : ~A~%" 1 5 (query-seg-tree st 1 5))    ; RES: 90
        (format t "~A to ~A : ~A~%" 8 8 (query-seg-tree st 8 8))    ; RES: 69

        (update-seg-tree-range st 1 5 30)
        (update-seg-tree-range st 2 8 -10)
        (terpri)
        (format t "~A to ~A : ~A~%" 0 2 (query-seg-tree st 0 2))    ; RES: 46
        (format t "~A to ~A : ~A~%" 4 9 (query-seg-tree st 4 9))    ; RES: 195
        (format t "~A to ~A : ~A~%" 1 5 (query-seg-tree st 1 5))    ; RES: 200
        (format t "~A to ~A : ~A~%" 8 8 (query-seg-tree st 8 8))    ; RES: 59
        ))
