(defun compress (x)
    (if (listp x)
        (compr (car x) 1 (cdr x))
        x))

(defun compr (ele n l)
    (if (null l)
        (list (n-ele ele n))
        (let ((next_ele (car l)))
            (if (equal ele next_ele)
                (compr next_ele (+ n 1) (cdr l))
                (cons (n-ele ele n)
                      (compr next_ele 1 (cdr l)))))))

(defun n-ele (ele n)
    (if (> n 1)
        (cons n ele)
        ele))

(defun uncompress (x)
    (if (listp x)
        (uncompr x)
        x))

(defun uncompr (l)
    (if (null l)
        NIL
        (let ((ele (car l))
              (res (uncompress (cdr l))))
            (if (consp ele)
                (append (flat-list (car ele) (cdr ele)) res)
                (cons ele res)))))

(defun flat-list (n ele)
        (if (zerop n)
            NIL
            (cons ele (flat-list (- n 1) ele))))
