;;; Take an assoc-list and returns a
;;; corresponding hash table
(defun assoc-hash (al)
    (let ((ht (make-hash-table)))
        (dolist (p al ht)
            (setf (gethash (car p) ht) (cdr p)))))

;;; Take a hash table and returns a
;;; corresponding assoc-list
(defun hash-assoc (ht)
    (let ((al NIL))
        (maphash #'(lambda (k v) (push (cons k v) al)) ht)
        al))
