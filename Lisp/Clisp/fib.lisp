(defun fib-1 (n)
    (if (< n 3) 1
        (let ((last 1) (next 1))
            (loop
                (psetq next (+ last next) last next)
                (decf n)
                (if (< n 3) (return next))))))

(defun fib-2 (n)
    (if (< n 3) 1
        (+ (fib_2 (1- n)) (fib_2 (- n 2)))))

(defun fib-3 (n)
    ;; n > 1
    (setq mem (make-list n))
    (setf (nth 0 mem) 1 (nth 1 mem) 1)
    (go_fib n))
(defun go_fib (n)
    (let ((cur (nth (1- n) mem)))
        (cond
            (cur cur)
            (t (setf (nth (1- n) mem)
                    (+ (go_fib (1- n)) (go_fib (- n 2))))))))
