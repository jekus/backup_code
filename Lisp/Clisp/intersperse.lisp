#|
    Take an object and a list ,
    return a new list in which the object
    appears between each pair of elements
    in the original list.
|#
(defun intersperse-iter (obj l)
    (let ((res NIL))
        (dotimes (i (length l))
            (push (nth i l) res)
            (if (< (+ 1 i) (length l))
                (push obj res)))
        (reverse res)))

(defun intersperse-rec (obj l)
    (do-intersperse-rec obj l 0))

(defun do-intersperse-rec (obj l idx)
    (if (= (+ 1 idx) (length l))
        (list (nth idx l))
        (append (list (nth idx l) obj)
              (do-intersperse-rec obj l (+ idx 1)))))
