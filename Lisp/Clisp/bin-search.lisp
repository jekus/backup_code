; Do binary search in a sorted vector
(defun bin-search (obj vec)
    (let ((vl (length vec)))
        (and (> vl 0) (do-bin-search 0 (- vl 1) obj vec))))

(defun do-bin-search (begin end obj vec)
    (if (>= begin end)
        (and (eql obj (svref vec begin)) obj)
        (let ((mid (floor (/ (+ begin end) 2))))
            (if (> obj (svref vec mid))
                (do-bin-search (+ mid 1) end obj vec)
                (do-bin-search begin mid obj vec)))))
