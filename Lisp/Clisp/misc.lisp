#|
    Count the number of times an element
    appears , return an Assoc-list in sorted form ,
    from most common to least common.
|#
(defun occurrences (l)
    (let ((ele-tb NIL))
        (dolist (ele l)
            (let ((p (assoc ele ele-tb)))
                (if p
                    (setf (cdr p) (+ 1 (cdr p)))
                    (setf ele-tb (cons (cons ele 1) ele-tb)))))
        (sort ele-tb
            #'(lambda (p1 p2)
                (if (> (cdr p1) (cdr p2))
                    T
                    NIL)))))

#|
    Take a list and return a list of each element
    plus its position
    > (pos+ '(7 5 1 4))
    (7 6 3 7)
|#
(defun pos+-iter (l)
    (let ((res NIL)
          (c 0))
        (dolist (ele l)
            (setf res (append res (list (+ ele c))))
            (setf c (+ c 1)))
        res))

(defun pos+-rec (l)
    (dopos l 0))

(defun dopos (l c)
    (if (null l)
        NIL
        (cons (+ c (car l)) (dopos (cdr l) (+ c 1)))))

(defun pos+-map (l)
    (let ((c -1))
        (mapcar #'(lambda (n) (+ n (setf c (+ c 1)))) l)))

;;; Take a list and prints it in dot notation
(defun showdots (l)
    (if (null l)
        (format t "NIL")
        (progn
            (format t "(~A . " (car l))
            (showdots (cdr l))
            (format t ")"))))

;;; Take a square matrix and rotate it 90 degree clockwise ,
;;; return a new matrix
(defun rotate-square (sq)
    (let* ((sq_dim (array-dimension sq 0))
           (new_sq (make-array (list sq_dim sq_dim))))
        (do ((upb 0 (+ upb 1))
             (lwb (- sq_dim 1) (- lwb 1)))
            ((>= upb lwb) new_sq)
            (dotimes (offset (- lwb upb))
                (setf (aref new_sq (+ upb offset) lwb)
                      (aref sq upb (+ upb offset)))
                (setf (aref new_sq lwb (- lwb offset))
                      (aref sq (+ upb offset) lwb))
                (setf (aref new_sq (- lwb offset) upb)
                      (aref sq lwb (- lwb offset)))
                (setf (aref new_sq upb (+ upb offset))
                      (aref sq (- lwb offset) upb))))))

(defun print-matrix (m)
    (let* ((m_dim (array-dimensions m))
           (r (first m_dim))
           (c (second m_dim)))
        (dotimes (r_idx r)
            (format t "(")
            (dotimes (c_idx c)
                (let ((ele (aref m r_idx c_idx)))
                    (if (eql (+ c_idx 1) c)
                        (format t "~A" ele)
                        (format t "~A " ele))))
            (format t ")~%"))))

(defun m-copy-list (l)
    (reduce #'cons l :from-end t :initial-value NIL))

(defun m-reverse-list (l)
    (reduce #'(lambda (z x) (append (list x) z)) l :initial-value NIL))

#|
    Take a vector , return the maximum and minimum
    elements in it.
|#
(defun extreme-ele (v)
    (let ((fele (svref v 0)))
        (do-extreme-ele v 1 fele fele)))

(defun do-extreme-ele (v idx max-ele min-ele)
    (if (= idx (length v))
        (values max-ele min-ele)
        (let* ((cur-ele (svref v idx))
               (next-max (max cur-ele max-ele))
               (next-min (min cur-ele min-ele)))
              (do-extreme-ele v (+ idx 1) next-max next-min))))

#|
    The function takes a number and returns
    the greatest numbers passed to it so far.
|#
(let ((max-num NIL))
    (defun max-so-far (x)
        (if (null max-num)
            (setf max-num x)
            (setf max-num (max max-num x)))))

#|
    The function takes a number and returns
    TRUE if it is greater than the number passed
    to it the last time it was called.
    Return NIL the first time it was called.
|#
(let ((last-num NIL))
    (defun bigger-than-last (x)
        (let ((res (and last-num (> x last-num))))
            (setf last-num x)
            res)))

;;; Read the content of a file
(defun cat (file)
    (with-open-file (ins (make-pathname :name file)
                         :direction :input)
        (do ((line (read-line ins NIL 'EOF)
                   (read-line ins NIL 'EOF)))
            ((eql line 'EOF))
            (format t "~A~%" line))))

;;; Read a list of expression from a given file
(defun read-expr (file)
    (with-open-file (ins (make-pathname :name file)
                         :direction :input)
        (let ((res NIL))
             (do ((expr (read ins NIL 'EOF)
                       (read ins NIL 'EOF)))
                 ((eql expr 'EOF))
                 (push expr res))
             res)))

#|
    Take out all the comments in a file and write
    the content to a new file.
    Comment starts with '%' and everything from this
    to the end of line is ignored.
|#
(defun take-comment (old_f new_f)
    (with-open-file (ins (make-pathname :name old_f)
                         :direction :input)
        (with-open-file (ots (make-pathname :name new_f)
                             :direction :output
                             :if-exists :supersede)
            (do ((cur_c (read-char ins NIL 'EOF)
                        (read-char ins NIL 'EOF)))
                ((eql cur_c 'EOF))
                (if (eql cur_c #\%)
                    (do NIL
                        ((or (eql cur_c #\Newline)
                             (eql cur_c 'EOF)))
                        (setf cur_c (read-char ins NIL 'EOF))))
                (if (not (eql cur_c 'EOF))
                    (princ cur_c ots))))))
