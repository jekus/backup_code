; list all combination of N bits

(setq *BITS* NIL *POS_COUNT* 0)
(setq *OUTPUT* NIL)

(defun find-pos (n)
    (when (>= n 0)
        (find-pos (1- n))
        (setf (char *BITS* n) #\1)
        (find-pos (1- n))
        (princ *BITS* *OUTPUT*) (terpri *OUTPUT*) (incf *POS_COUNT*)
        (setf (char *BITS* n) #\0)) nil)

(defun bin_pos (bit_num)
    (setq
        *BITS* (make-string bit_num :initial-element #\0)
        *POS_COUNT* 0)
    (with-open-file (output "bin-pos.txt" :direction :output)
        (setq *OUTPUT* output)
        (find-pos (1- bit_num))))
