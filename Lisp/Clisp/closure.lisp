(defun make-password-keeper ()
    (let ((passwd nil) (secret nil))
        #'(lambda (op &rest arg)
            (ecase op
                (set-passwd
                    (let ((new-passwd (first arg)))
                        (if passwd
                            '|FAILED|
                            (progn (setq passwd new-passwd) '|SUCCEED|))))
                (change-passwd
                    (let (
                        (old-passwd (first arg))
                        (new-passwd (second arg)))
                        (if (eq old-passwd passwd)
                            (progn (setq passwd new-passwd) '|SUCCEED|)
                            '|FAILED|)))
                (set-secret
                    (let ((new-secret (first arg)))
                        (if secret
                            '|FAILED|
                            (progn (setq secret new-secret) '|SUCCEED|))))
                (change-secret
                    (let (
                        (old-secret (first arg))
                        (new-secret (second arg)))
                        (if (eq old-secret secret)
                            (progn (setq secret new-secret) '|SUCCEED|)
                            '|FAILED|)))
                (get-secret
                    (let ((pass (first arg)))
                        (if (eq pass passwd)
                            (print secret)
                            '|FAILED|)))))))
