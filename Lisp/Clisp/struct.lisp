(defstruct (point (:conc-name p-)
                  (:print-function print-point))
    (x 0)
    (y 0)
    (z 0))

(defun print-point (p stream depth)
    (format stream "(~A , ~A , ~A)" (p-x p) (p-y p) (p-z p)))
