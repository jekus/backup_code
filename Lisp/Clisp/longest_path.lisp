#|
    Find the longest finite path through a network ,
    the network may contain cycles.
    The network is a directed graph.

    network REP: '((a b c) (b c d) (d x y))
|#

(defun longest_path (net)
    ;; start from each vertex
    (let ((res NIL))
        (dolist (vs net)
            (setf res (append (do_find_lg vs net (list (car vs))) res)))
        (reverse (car (sort res
                        #'(lambda (p1 p2)
                            (> (length p1) (length p2))))))))

(defun do_find_lg (vs net path)
    (if (null vs)
        (list path)
        (let ((path_set NIL))
            (dolist (v (cdr vs))
                (if (not (member v path))
                    (setf path_set
                        (append
                            (do_find_lg (assoc v net) net (cons v path))
                            path_set))))
            path_set)))
