(setq *B_SIZE* 8 *QUEEN* (make-list *B_SIZE*) *S_N* 0)

(defun place_ok (cl rw) (block L
    (loop for r from 0 to (- rw 1)
        do (when (or (= cl (nth r *QUEEN*))
                    (= (- rw r) (abs (- cl (nth r *QUEEN*)))))
            (return-from L nil)))
    (return-from L t)))

(defun add_queen (rw)
    (if (= rw *B_SIZE*)
        (incf *S_N*)
        (loop for i from 0 to (- *B_SIZE* 1)
            do (when (place_ok i rw)
                (setf (nth rw *QUEEN*) i)
                (add_queen (+ rw 1))))))

(add_queen 0)
(princ *S_N*)
