<?php
    class Book
    {
       public $author , $price;

       function Book ($a , $p)
       {
            $this->author = $a;
            $this->price = $p;
       }

       function display ()
       {
            echo $this->author . "<br>";
            echo $this->price . "<br>";
       }
    }

    class Fiction extends Book
    {
        public $type;

        // Another kind of constructor
        function __construct ($a , $p , $t)
        {
            parent::Book($a , $p);
            $this->type = $t;
        }

        function __destruct ()
        {
        }

        function display ()
        {
            parent::display();
            echo $this->type . "<br>";
        }
    }

    $foo = new Book("Jekus Neky" , 9998);
    $bar = new Fiction("David Johnson" , 4324 , "BOMB");
    $foo->display();
    $bar->display();
?>
