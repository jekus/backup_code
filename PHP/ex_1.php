<?php
    class User
    {
        static $user_n = 0;       // Can't be accessed by an instance of the class
        const ALIAS = "USER";    // Can't change once defined

        public $name , $password;
        private $foo = 123;
        // private $bar = $foo * 123;  (Error , the value must be a constant value)

        // Constructor
        function User ($_name , $_passwd)
        {
            $this->name = $_name;
            $this->password = $_passwd;
        }

        // Static method
        static function pwd_string ()
        {
            echo "Please enter your password<br>";
        }

        static function lookup ()
        {
            echo self::ALIAS;
        }
    }

    $user_1 = new User("Jekus" , "123");
    $user_2 = clone $user_1;
    $user_2->name = "Neky";
    $user_1->age = 18;      // Declare a property implicitly

    print_r($user_1);
    echo "<br>";
    print_r($user_2);
    echo "<br>";
    User::pwd_string();
    echo User::ALIAS . " ";
    User::lookup();
    echo " " . User::$user_n;
?>
