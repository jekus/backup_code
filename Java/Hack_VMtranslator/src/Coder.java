import java.util.*;

class Coder {

	private static final HashMap<String , String> _nullaryCmdsMap;
	static {
		_nullaryCmdsMap = new HashMap<String , String>(15);

		_nullaryCmdsMap.put("add" , String.format("%n@SP%nAM=M-1%nD=M%nA=A-1%nM=D+M%n"));
		_nullaryCmdsMap.put("sub" , String.format("%n@SP%nAM=M-1%nD=M%nA=A-1%nM=M-D%n"));
		_nullaryCmdsMap.put("neg" , String.format("%n@SP%nA=M-1%nM=-M%n"));

		_nullaryCmdsMap.put("and" , String.format("%n@SP%nAM=M-1%nD=M%nA=A-1%nM=D&M%n"));
		_nullaryCmdsMap.put("or" , String.format("%n@SP%nAM=M-1%nD=M%nA=A-1%nM=D|M%n"));
		_nullaryCmdsMap.put("not" , String.format("%n@SP%nA=M-1%nM=!M%n"));

		_nullaryCmdsMap.put("eq" ,
				"%n@SP%nAM=M-1%nD=M%nA=A-1%nD=M-D%n@JMP_%1$d%nD;JEQ%n@SP%nA=M-1%nM=0%n@JMP_%2$d%n0;JMP%n(JMP_%1$d)%n@SP%nA=M-1%nM=-1%n(JMP_%2$d)%n");
		_nullaryCmdsMap.put("gt" ,
				"%n@SP%nAM=M-1%nD=M%nA=A-1%nD=M-D%n@JMP_%1$d%nD;JGT%n@SP%nA=M-1%nM=0%n@JMP_%2$d%n0;JMP%n(JMP_%1$d)%n@SP%nA=M-1%nM=-1%n(JMP_%2$d)%n");
		_nullaryCmdsMap.put("lt" ,
				"%n@SP%nAM=M-1%nD=M%nA=A-1%nD=M-D%n@JMP_%1$d%nD;JLT%n@SP%nA=M-1%nM=0%n@JMP_%2$d%n0;JMP%n(JMP_%1$d)%n@SP%nA=M-1%nM=-1%n(JMP_%2$d)%n");
	
		_nullaryCmdsMap.put("return" , String.format("%n@LCL%nD=M%n@5%nA=D-A%nD=M%n@R13%nM=D%n@SP%nA=M-1%nD=M%n@ARG%nA=M%nM=D%nD=A+1%n@SP%nM=D%n@LCL%nA=M-1%nD=M%n@THAT%nM=D%n@LCL%nD=M%n@2%nA=D-A%nD=M%n@THIS%nM=D%n@LCL%nD=M%n@3%nA=D-A%nD=M%n@ARG%nM=D%n@LCL%nD=M%n@4%nA=D-A%nD=M%n@LCL%nM=D%n@R13%nA=M;JMP%n"));
	}

	private static final HashMap<String , String> _unaryCmdsMap;
	static {
		_unaryCmdsMap = new HashMap<String , String>(10);
		_unaryCmdsMap.put("label" , "%n(%1$s)%n");
		_unaryCmdsMap.put("goto" , "%n@%1$s%n0;JMP%n");
		_unaryCmdsMap.put("if-goto" , "%n@SP%nAM=M-1%n%nD=M%n@%1$s%nD;JNE%n");
	}

	private static final HashMap<String , String> _pushSegMap;
	private static final HashMap<String , String> _popSegMap;
	private static final HashMap<String , String> _binaryCmdsMap;
	static {
		_binaryCmdsMap = new HashMap<String , String>(10);
		
		_binaryCmdsMap.put("call" , "%n@JMP_%3$d%nD=A%n@SP%nA=M%nM=D%n@LCL%nD=M%n@SP%nAM=M+1%nM=D%n@ARG%nD=M%n@SP%nAM=M+1%nM=D%n@THIS%nD=M%n@SP%nAM=M+1%nM=D%n@THAT%nD=M%n@SP%nAM=M+1%nM=D%n@SP%nMD=M+1%n@LCL%nM=D%n@%2$s%nD=D-A%n@5%nD=D-A%n@ARG%nM=D%n@%1$s%n0;JMP%n(JMP_%3$d)%n");
		_binaryCmdsMap.put("function" , "%n(%1$s)%n@%2$s%nD=A%n@JMP_%3$d%nD;JGT%n@JMP_%4$d%n0;JMP%n(JMP_%3$d)%n@SP%nA=M%nM=0%n@SP%nM=M+1%n@JMP_%3$d%nD=D-1;JGT%n(JMP_%4$d)%n");
		
		// Expect that the content need pushing has already been in D register
		_binaryCmdsMap.put("push" , String.format("%n@SP%nA=M%nM=D%n@SP%nM=M+1%n"));
		// Load the top element of stack into D register
		_binaryCmdsMap.put("pop" , String.format("%n@SP%nAM=M-1%nD=M%n"));
		
		// Load the content of the specific location into D register
		_pushSegMap = new HashMap<String , String>(15);
		_pushSegMap.put("local" , "%n@%1$s%nD=A%n@LCL%nA=D+M%nD=M%n");
		_pushSegMap.put("argument" , "%n@%1$s%nD=A%n@ARG%nA=D+M%nD=M%n");
		_pushSegMap.put("this" , "%n@%1$s%nD=A%n@THIS%nA=D+M%nD=M%n");
		_pushSegMap.put("that" , "%n@%1$s%nD=A%n@THAT%nA=D+M%nD=M%n");
		_pushSegMap.put("pointer" , "%n@%1$s%nD=A%n@THIS%nA=D+A%nD=M%n");
		_pushSegMap.put("temp" , "%n@%1$s%nD=A%n@R5%nA=D+A%nD=M%n");
		_pushSegMap.put("constant" , "%n@%1$s%nD=A%n");
		_pushSegMap.put("static" , "%n@%1$s.%2$s%nD=M%n");

		// Expect that the content need loading has already been in D register
		_popSegMap = new HashMap<String , String>(15);
		_popSegMap.put("local" , "%n@R13%nM=D%n@%1$s%nD=A%n@LCL%nD=D+M%n@R14%nM=D%n@R13%nD=M%n@R14%nA=M%nM=D%n");
		_popSegMap.put("argument" , "%n@R13%nM=D%n@%1$s%nD=A%n@ARG%nD=D+M%n@R14%nM=D%n@R13%nD=M%n@R14%nA=M%nM=D%n");
		_popSegMap.put("this" , "%n@R13%nM=D%n@%1$s%nD=A%n@THIS%nD=D+M%n@R14%nM=D%n@R13%nD=M%n@R14%nA=M%nM=D%n");
		_popSegMap.put("that" , "%n@R13%nM=D%n@%1$s%nD=A%n@THAT%nD=D+M%n@R14%nM=D%n@R13%nD=M%n@R14%nA=M%nM=D%n");
		_popSegMap.put("pointer" , "%n@R13%nM=D%n@%1$s%nD=A%n@THIS%nD=D+A%n@R14%nM=D%n@R13%nD=M%n@R14%nA=M%nM=D%n");
		_popSegMap.put("temp" , "%n@R13%nM=D%n@%1$s%nD=A%n@R5%nD=D+A%n@R14%nM=D%n@R13%nD=M%n@R14%nA=M%nM=D%n");
		_popSegMap.put("static" , "%n@%1$s.%2$s%nM=D%n");
	}


	private final String _filename;
	private final StringBuilder _code;
	private static int _jmp_idx = 1;
	private String _func;

	private final void geneNullaryCode (Command cmd) {
		String cmdstr = cmd.getCmd();
		String cmdcode = _nullaryCmdsMap.get(cmdstr);
		switch(cmdstr) {
			case "add":		case "sub":		case "neg":
			case "and":		case "or":		case "not":
			case "return":
				_code.append(cmdcode);
				break;
				
			case "eq":		case "gt":		case "lt":
				_code.append(String.format(cmdcode , _jmp_idx , _jmp_idx + 1));
				_jmp_idx += 2;
				break;
		}
	}

	private final void geneUnaryCode (Command cmd) {
		_code.append(String.format(_unaryCmdsMap.get(cmd.getCmd()) ,
				String.format("%1$s$%2$s" , _func , cmd.getArg_1())));
	}
	
	private final void genePushCode (Command cmd) {
		String segment = cmd.getArg_1();
		String index = cmd.getArg_2();
		String pushcode = _binaryCmdsMap.get(cmd.getCmd());
		String segcode = _pushSegMap.get(segment);
		switch (segment) {
			case "local":	case "argument":	case "this":
			case "that":	case "pointer":		case "temp":
			case "constant":
				_code.append(String.format("%s%s" ,
						String.format(segcode , index) , pushcode));
				break;
				
			case "static": {
				_code.append(String.format("%s%s" ,
						String.format(segcode , _filename , index) , pushcode));
				break;
			}
		}
	}

	private final void genePopCode (Command cmd) {
		String segment = cmd.getArg_1();
		String index = cmd.getArg_2();
		String popcode = _binaryCmdsMap.get(cmd.getCmd());
		String segcode = _popSegMap.get(segment);
		switch (segment) {
			case "local":	case "argument":	case "this":
			case "that":	case "pointer":		case "temp":
				_code.append(String.format("%s%s" ,
						popcode , String.format(segcode , index)));
				break;
				
			case "static":
				_code.append(String.format("%s%s" ,
						popcode , String.format(segcode , _filename , index)));
				break;
		}
	}

	private final void geneFuncCode (Command cmd) {
		_code.append(String.format(_binaryCmdsMap.get(cmd.getCmd()) ,
				cmd.getArg_1() , cmd.getArg_2() , _jmp_idx , _jmp_idx + 1));
		_jmp_idx += 2;
		_func = cmd.getArg_1();
	}

	private final void geneCallCode (Command cmd) {
		_code.append(String.format(_binaryCmdsMap.get(cmd.getCmd()) ,
				cmd.getArg_1() , cmd.getArg_2() , _jmp_idx));
		++_jmp_idx;
	}

	private final void geneBinaryCode (Command cmd) {
		switch (cmd.getCmd()) {
			case "push":
				genePushCode(cmd);
				break;

			case "pop":
				genePopCode(cmd);
				break;

			case "function":
				geneFuncCode(cmd);
				break;

			case "call":
				geneCallCode(cmd);
				break;
		}
	}

	Coder (String filename , LinkedList<Command> cmds) {
		_func = _filename = filename;
		_code = new StringBuilder(1024);

		for (Command cmd : cmds) {
			Command.CmdType ct = cmd.getCmdType();
			if (ct == Command.CmdType.C_ARITHMETIC || ct == Command.CmdType.C_RETURN) {
				geneNullaryCode(cmd);
			}
			else if (ct == Command.CmdType.C_LABEL || ct == Command.CmdType.C_GOTO || ct == Command.CmdType.C_IF) {
				geneUnaryCode(cmd);
			}
			else {
				geneBinaryCode(cmd);
			}
		}
	}

	String getCode () { return _code.toString(); }
	
	
	private static final String _bootstrap_code = String.format("%n@256%nD=A%n@SP%nM=D%n@JMP_0%nD=A%n@SP%nA=M%nM=D%n@LCL%nD=M%n@SP%nAM=M+1%nM=D%n@ARG%nD=M%n@SP%nAM=M+1%nM=D%n@THIS%nD=M%n@SP%nAM=M+1%nM=D%n@THAT%nD=M%n@SP%nAM=M+1%nM=D%n@SP%nMD=M+1%n@LCL%nM=D%n@5%nD=D-A%n@ARG%nM=D%n@Sys.init%n0;JMP%n(JMP_0)%n");
	
	static String getBootstrapCode () { return _bootstrap_code; }
}
