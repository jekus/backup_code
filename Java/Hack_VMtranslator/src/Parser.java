import java.io.*;
import java.util.*;

class Parser {

	private LinkedList<Command> _cmds;

	Parser (File file) throws IOException {
		try (BufferedReader rd = new BufferedReader(new FileReader(file))) {
			_cmds = new LinkedList<Command>();
			decompCmd(preProcess(rd.lines().toArray()));
		}
		catch (FileNotFoundException e) {
			System.out.printf("File '%s' doesn't exist" , e.getMessage());
			System.exit(0);
		}
		catch (UnknownTokenException e) {
			System.out.printf("Unknown token - '%s'%n" , e.getSym());
			System.exit(0);
		}
		catch (InvalidCmdException e) {
			System.out.printf("Invalid Command - '%s'%n" , e.getSym());
			System.exit(0);
		}
	}

	private static class SymException extends Exception {

		private static final long serialVersionUID = -2828254936903133776L;
		private final String sym;

		SymException (String in_sym) { sym = in_sym; }
		
		String getSym () { return sym; }

	}

	private static class UnknownTokenException extends SymException {
		private static final long serialVersionUID = -7600422960834115701L;
		UnknownTokenException (String sym) { super(sym); }
	}

	private static class InvalidCmdException extends SymException {
		private static final long serialVersionUID = 3876376376853725743L;
		InvalidCmdException (String sym) { super(sym); }
	}

	private static LinkedList<String> preProcess (Object[] lines) throws UnknownTokenException {
		LinkedList<String> new_lines = new LinkedList<String>();
		for (Object obj : lines) {
			String line = ((String)obj).trim();
			if (!line.startsWith("//") && !line.isEmpty()) {
				int inline_cmtidx = line.indexOf('/');
				if (inline_cmtidx == -1) {
					new_lines.addLast(line);
				}
				else {
					if (line.indexOf('/' , inline_cmtidx + 1) == -1) {
						throw new UnknownTokenException("/");
					}
					else {
						new_lines.addLast(line.substring(0 , inline_cmtidx).trim());
					}
				}
			}
		}

		return new_lines;
	}

	private void decompNullaryCmd (String in_cmd) throws InvalidCmdException {
		Command cmd = new Command(in_cmd);
		_cmds.addLast(cmd);

		switch (in_cmd) {
			case "add":	case "sub":	case "neg":
			case "eq":	case "gt":	case "lt":
			case "and":	case "or":	case "not":
				cmd.setCmdType(Command.CmdType.C_ARITHMETIC);
				break;

			case "return":
				cmd.setCmdType(Command.CmdType.C_RETURN);
				break;

			default:
				throw new InvalidCmdException(in_cmd);
		}
	}

	private void decompUnaryCmd (String[] cmdcomps) throws InvalidCmdException {
		Command cmd = new Command(cmdcomps[0] , cmdcomps[1]);
		_cmds.addLast(cmd);

		switch (cmdcomps[0]) {
			case "label":
				cmd.setCmdType(Command.CmdType.C_LABEL);
				break;

			case "goto":
				cmd.setCmdType(Command.CmdType.C_GOTO);
				break;

			case "if-goto":
				cmd.setCmdType(Command.CmdType.C_IF);
				break;

			default:
				throw new InvalidCmdException(cmdcomps[0]);
		}
	}

	private void decompBinaryCmd (String[] cmdcomps) throws InvalidCmdException {
		Command cmd = new Command(cmdcomps[0] , cmdcomps[1] , cmdcomps[2]);
		_cmds.addLast(cmd);

		switch (cmdcomps[0]) {
			case "push":
				cmd.setCmdType(Command.CmdType.C_PUSH);
				break;

			case "pop":
				cmd.setCmdType(Command.CmdType.C_POP);
				break;

			case "function":
				cmd.setCmdType(Command.CmdType.C_FUNCTION);
				break;

			case "call":
				cmd.setCmdType(Command.CmdType.C_CALL);
				break;

			default:
				throw new InvalidCmdException(cmdcomps[0]);
		}
	}

	private void decompCmd (LinkedList<String> lines) throws InvalidCmdException {
		for (String line : lines) {
			String[] cmdcomps = line.split("\\s+");
			switch (cmdcomps.length) {
				case 1:
					decompNullaryCmd(cmdcomps[0]);
					break;

				case 2:
					decompUnaryCmd(cmdcomps);
					break;

				case 3:
					decompBinaryCmd(cmdcomps);
					break;

				default:
					throw new InvalidCmdException(line);
			}
		}
	}

	LinkedList<Command> getCmds () { return _cmds; }

}
