
class Command {

	static enum CmdType {
		C_ARITHMETIC , C_PUSH , C_POP ,
		C_LABEL , C_GOTO , C_IF , C_FUNCTION ,
		C_CALL , C_RETURN
	}

	private final String _cmd;
	private final String _arg_1;
	private final String _arg_2;

	private CmdType _t;

	Command (String cmd) {
		_cmd = cmd;
		_arg_1 = _arg_2 = null;
	}

	Command (String cmd , String arg_1) {
		_cmd = cmd;
		_arg_1 = arg_1;
		_arg_2 = null;
	}

	Command (String cmd , String arg_1 , String arg_2) {
		_cmd = cmd;
		_arg_1 = arg_1;
		_arg_2 = arg_2;
	}
	
	void setCmdType (CmdType t) { _t = t; }
	
	String getCmd () { return _cmd; }
	String getArg_1 () { return _arg_1; }
	String getArg_2 () { return _arg_2; }
	CmdType getCmdType () { return _t; }

}
