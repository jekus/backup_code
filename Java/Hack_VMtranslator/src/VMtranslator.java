import java.io.*;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.DirectoryStream;

public class VMtranslator {

	private static void dealWithFile (Path path) throws IOException {
		String filename = path.getFileName().toString();
		filename = filename.substring(0 , filename.lastIndexOf('.'));
		String outfile = filename + ".asm";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(Paths.get(path.getParent().toString() , outfile).toFile()))) {
			Parser pser = new Parser(path.toFile());
			Coder coder = new Coder(filename , pser.getCmds());
			bw.write(Coder.getBootstrapCode());
			bw.write(coder.getCode());
		}
	}

	private static void dealWithDir (Path path) throws IOException {
		try (
				DirectoryStream<Path> stream = Files.newDirectoryStream(path);
				BufferedWriter bw = new BufferedWriter(new FileWriter(Paths.get(path.toString() , path.getFileName().toString() + ".asm").toFile()))
			)
		{
			bw.write(Coder.getBootstrapCode());
			for (Path p : stream) {
				if (Files.isRegularFile(p) && p.getFileName().toString().endsWith(".vm")) {
					String filename = p.getFileName().toString();
					filename = filename.substring(0 , filename.lastIndexOf('.'));
					Parser pser = new Parser(p.toFile());
					Coder coder = new Coder(filename , pser.getCmds());
					bw.write(coder.getCode());
				}
			}
		}
	}

	public static void main (String[] args) throws IOException {
		if (args.length != 1) {
			System.out.println("Require one argument");
			System.exit(0);
		}
		else {
			Path path = Paths.get(args[0]);
			if (Files.isDirectory(path)) {
				dealWithDir(path);
			}
			else {
				dealWithFile(path);
			}
		}
	}

}
