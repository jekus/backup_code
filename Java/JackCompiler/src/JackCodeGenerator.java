import java.io.*;
import java.util.LinkedList;

class JackCodeGenerator {
	
	private static int jmp_n;
	
	static void geneCode (
			JackAstNode ast , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		jmp_n = -1;
		geneClassCode((JackClassNode) ast , program_info , bw);
	}
	
	private static void geneClassCode (
			JackClassNode node , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		String class_name = node.getClassName();
		for (JackSubroutineDecNode subr_dec_node : node.getSubrDecList()) {
			geneSubroutineCode(class_name , subr_dec_node , program_info , bw);
		}
	}
	
	private static void geneSubroutineCode (
			String class_name , JackSubroutineDecNode subr_dec_node ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		String subroutine_name = subr_dec_node.getSubroutineName();
		JackSubroutineBodyNode body = subr_dec_node.getSubroutineBody();
		JackInfoCollector.ClassRecord.SubroutineRecord sr =
				program_info.getSubroutine(class_name , subroutine_name);
		
		if (sr instanceof JackInfoCollector.ClassRecord.ConstructorRecord) {
			geneConstructorCode(
					class_name , subroutine_name , body ,
					sr , program_info , bw);
		}
		else if (sr instanceof JackInfoCollector.ClassRecord.MethodRecord) {
			geneMethodCode(
					class_name , subroutine_name , body ,
					sr , program_info , bw);
		}
		else if (sr instanceof JackInfoCollector.ClassRecord.FunctionRecord) {
			geneFunctionCode(
					class_name , subroutine_name , body ,
					sr , program_info , bw);
		}
		else {
			return;
		}
	}
	
	private static void geneConstructorCode (
			String class_name , String subroutine_name , JackSubroutineBodyNode body ,
			JackInfoCollector.ClassRecord.SubroutineRecord sr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		geneSubroutineHeader(sr , bw);
		
		JackInfoCollector.ClassRecord cr = program_info.getClass(class_name);
		bw.write(String.format("push constant %d%n" , cr.getFieldNum()));
		bw.write(String.format("call Memory.alloc 1%n"));
		bw.write(String.format("pop pointer 0%n"));
		
		geneSubroutineBody(class_name , subroutine_name , body , program_info , bw);
	}
	
	private static void geneMethodCode (
			String class_name , String subroutine_name , JackSubroutineBodyNode body ,
			JackInfoCollector.ClassRecord.SubroutineRecord sr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		geneSubroutineHeader(sr , bw);
		
		bw.write(String.format("push argument 0%n"));
		bw.write(String.format("pop pointer 0%n"));
		
		geneSubroutineBody(class_name , subroutine_name , body , program_info , bw);
	}
	
	private static void geneFunctionCode (
			String class_name , String subroutine_name , JackSubroutineBodyNode body ,
			JackInfoCollector.ClassRecord.SubroutineRecord sr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		geneSubroutineHeader(sr , bw);
		geneSubroutineBody(class_name , subroutine_name , body , program_info , bw);
	}
	
	private static void geneSubroutineBody (
			String class_name , String subroutine_name ,
			JackSubroutineBodyNode body , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		for (JackStatementNode st_node : body.getStatementList()) {
			geneStatementCode(class_name , subroutine_name , st_node , program_info , bw);
		}
	}
	
	private static void geneExpressionCode (
			String class_name , String subroutine_name ,
			JackExpressionNode expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		if (expr instanceof JackAddExpressionNode) {
			geneAddExpressionCode(class_name , subroutine_name ,
					(JackAddExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackSubExpressionNode) {
			geneSubExpressionCode(class_name , subroutine_name ,
					(JackSubExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackMulExpressionNode) {
			geneMulExpressionCode(class_name , subroutine_name ,
					(JackMulExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackDivExpressionNode) {
			geneDivExpressionCode(class_name , subroutine_name ,
					(JackDivExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackBitWiseAndExpressionNode) {
			geneBitWiseAndExpressionCode(class_name , subroutine_name ,
					(JackBitWiseAndExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackBitWiseOrExpressionNode) {
			geneBitWiseOrExpressionCode(class_name , subroutine_name ,
					(JackBitWiseOrExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackLessThanExpressionNode) {
			geneLessThanExpressionCode(class_name , subroutine_name ,
					(JackLessThanExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackGreaterExpressionNode) {
			geneGreaterExpressionCode(class_name , subroutine_name ,
					(JackGreaterExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackEqualExpressionNode) {
			geneEqualExpressionCode(class_name , subroutine_name ,
					(JackEqualExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackIntegerNode) {
			geneIntegerExpressionCode(class_name , subroutine_name ,
					(JackIntegerNode) expr , program_info , bw);
		}
		else if (expr instanceof JackStringNode) {
			geneStringExpressionCode(class_name , subroutine_name ,
					(JackStringNode) expr , program_info , bw);
		}
		else if (expr instanceof JackKeywordConstNode) {
			geneKeywordConstCode(class_name , subroutine_name ,
					(JackKeywordConstNode) expr , program_info , bw);
		}
		else if (expr instanceof JackIdentifierNode) {
			geneIdentifierExpressionCode(class_name , subroutine_name ,
					(JackIdentifierNode) expr , program_info , bw);
		}
		else if (expr instanceof JackArrayAccessNode) {
			geneArrayAccessExpressionCode(class_name , subroutine_name ,
					(JackArrayAccessNode) expr , program_info , bw);
		}
		else if (expr instanceof JackNegateExpressionNode) {
			geneNegateExpressionCode(class_name , subroutine_name ,
					(JackNegateExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackLogicalNegateExpressionNode) {
			geneLogicalNegateExpressionCode(class_name , subroutine_name ,
					(JackLogicalNegateExpressionNode) expr , program_info , bw);
		}
		else if (expr instanceof JackSubroutineCallNode) {
			geneSubroutineCallCode(class_name , subroutine_name ,
					(JackSubroutineCallNode) expr , program_info , bw);
		}
	}
	
	private static void geneAddExpressionCode (
			String class_name , String subroutine_name ,
			JackAddExpressionNode add_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				add_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				add_expr.getRightHandSide() , program_info , bw);
		bw.write("add");
		bw.newLine();
	}
	
	private static void geneSubExpressionCode (
			String class_name , String subroutine_name ,
			JackSubExpressionNode sub_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				sub_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				sub_expr.getRightHandSide() , program_info , bw);
		bw.write("sub");
		bw.newLine();
	}
	
	private static void geneMulExpressionCode (
			String class_name , String subroutine_name ,
			JackMulExpressionNode mul_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				mul_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				mul_expr.getRightHandSide() , program_info , bw);
		bw.write("call Math.multiply 2");
		bw.newLine();
	}
	
	private static void geneDivExpressionCode (
			String class_name , String subroutine_name ,
			JackDivExpressionNode div_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				div_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				div_expr.getRightHandSide() , program_info , bw);
		bw.write("call Math.divide 2");
		bw.newLine();
	}
	
	private static void geneBitWiseAndExpressionCode (
			String class_name , String subroutine_name ,
			JackBitWiseAndExpressionNode bit_and_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				bit_and_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				bit_and_expr.getRightHandSide() , program_info , bw);
		bw.write("and");
		bw.newLine();
	}
	
	private static void geneBitWiseOrExpressionCode (
			String class_name , String subroutine_name ,
			JackBitWiseOrExpressionNode bit_or_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				bit_or_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				bit_or_expr.getRightHandSide() , program_info , bw);
		bw.write("or");
		bw.newLine();
	}
	
	private static void geneLessThanExpressionCode (
			String class_name , String subroutine_name ,
			JackLessThanExpressionNode lt_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				lt_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				lt_expr.getRightHandSide() , program_info , bw);
		bw.write("lt");
		bw.newLine();
	}
	
	private static void geneGreaterExpressionCode (
			String class_name , String subroutine_name ,
			JackGreaterExpressionNode gt_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				gt_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				gt_expr.getRightHandSide() , program_info , bw);
		bw.write("gt");
		bw.newLine();
	}
	
	private static void geneEqualExpressionCode (
			String class_name , String subroutine_name ,
			JackEqualExpressionNode eq_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name ,
				eq_expr.getLeftHandSide() , program_info , bw);
		geneExpressionCode(class_name , subroutine_name ,
				eq_expr.getRightHandSide() , program_info , bw);
		bw.write("eq");
		bw.newLine();
	}
	
	private static void geneIntegerExpressionCode (
			String class_name , String subroutine_name ,
			JackIntegerNode int_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		bw.write(String.format("push constant %d%n" , int_expr.getIntval()));
	}
	
	private static void geneStringExpressionCode (
			String class_name , String subroutine_name ,
			JackStringNode string_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		String str = string_expr.getStr();
		bw.write(String.format("push constant %d%n" , str.length()));
		bw.write(String.format("call String.new 1%n"));
		
		for (char ch : str.toCharArray()) {
			bw.write(String.format("push constant %d%n" , ch - '\0'));
			bw.write(String.format("call String.appendChar 2%n"));
		}
	}
	
	private static void geneKeywordConstCode (
			String class_name , String subroutine_name ,
			JackKeywordConstNode keyconst_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		JackTokenizer.TokenType keyword = keyconst_expr.getKeyword();
		switch (keyword) {
			case KW_NULL:	case KW_FALSE:
				bw.write("push constant 0");
				bw.newLine();
				break;
				
			case KW_TRUE:
				bw.write(String.format("push constant 1%n"));
				bw.write(String.format("neg%n"));
				break;
				
			case KW_THIS:
				bw.write("push pointer 0");
				bw.newLine();
				break;
				
			default: break;
		}
	}
	
	private static void geneIdentifierExpressionCode (
			String class_name , String subroutine_name ,
			JackIdentifierNode id_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		String id = id_expr.getRep();
		JackInfoCollector.ClassRecord.VarRecord svr =
				program_info.getSubroutineVar(class_name , subroutine_name , id);
		
		if (svr == null) {
			geneIdentifierExpressionOnClassVarCode(class_name , id , program_info , bw);
		}
		else {
			geneIdentifierExpressionOnSubrVarCode(class_name , subroutine_name , svr , program_info , bw);
		}
	}
	
	private static void geneIdentifierExpressionOnClassVarCode (
			String class_name , String id ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.VarRecord cvr = program_info.getClassVar(class_name , id);
		
		if (cvr instanceof JackInfoCollector.ClassRecord.ClassField) {
			bw.write(String.format("push this %d%n" , cvr.getIndex()));
		}
		else if (cvr instanceof JackInfoCollector.ClassRecord.ClassStaticField) {
			bw.write(String.format("push static %d%n" , cvr.getIndex()));
		}
		else {
			return;
		}
	}
	
	private static void geneIdentifierExpressionOnSubrVarCode (
			String class_name , String subroutine_name ,
			JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineParameter) {
			JackInfoCollector.ClassRecord.SubroutineRecord sr =
					program_info.getSubroutine(class_name , subroutine_name);
			
			if (sr instanceof JackInfoCollector.ClassRecord.MethodRecord) {
				bw.write(String.format("push argument %d%n" , svr.getIndex() + 1));
			}
			else {
				bw.write(String.format("push argument %d%n" , svr.getIndex()));
			}
		}
		else if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineLocalVar) {
			bw.write(String.format("push local %d%n" , svr.getIndex()));
		}
		else {
			return;
		}
	}
	
	private static void geneArrayAccessExpressionCode (
			String class_name , String subroutine_name ,
			JackArrayAccessNode araccess_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		String var_name = araccess_expr.getVarName();
		JackExpressionNode index_expr = araccess_expr.getIndexExpr();
		JackInfoCollector.ClassRecord.VarRecord svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		
		if (svr == null) {
			geneArrayAccessExpressionOnClassVarCode(
					class_name , subroutine_name , var_name ,
					index_expr , program_info , bw);
		}
		else {
			geneArrayAccessExpressionOnSubrVarCode(
					class_name , subroutine_name ,
					index_expr , svr , program_info , bw);
		}
	}
	
	private static void geneArrayAccessExpressionOnSubrVarCode (
			String class_name , String subroutine_name ,
			JackExpressionNode index_expr , JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineParameter) {
			geneArrayAccessExpressionOnSubrParamCode(
					class_name , subroutine_name ,
					index_expr , svr , program_info , bw);
		}
		else if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineLocalVar) {
			geneArrayAccessExpressionOnSubrLocalVarCode(
					class_name , subroutine_name ,
					index_expr , svr , program_info , bw);
		}
		else {
			return;
		}
	}
	
	private static void geneArrayAccessExpressionOnSubrParamCode (
			String class_name , String subroutine_name ,
			JackExpressionNode index_expr , JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.SubroutineRecord sr =
				program_info.getSubroutine(class_name , subroutine_name);
		
		if (sr instanceof JackInfoCollector.ClassRecord.MethodRecord) {
			bw.write(String.format("push argument %d%n" , svr.getIndex() + 1));
			geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
			bw.write(String.format("add%n"));
			bw.write(String.format("pop pointer 1%n"));
			bw.write(String.format("push that 0%n"));
		}
		else {
			bw.write(String.format("push argument %d%n" , svr.getIndex()));
			geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
			bw.write(String.format("add%n"));
			bw.write(String.format("pop pointer 1%n"));
			bw.write(String.format("push that 0%n"));
		}
	}
	
	private static void geneArrayAccessExpressionOnSubrLocalVarCode (
			String class_name , String subroutine_name ,
			JackExpressionNode index_expr , JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		bw.write(String.format("push local %d%n" , svr.getIndex()));
		geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
		bw.write(String.format("add%n"));
		bw.write(String.format("pop pointer 1%n"));
		bw.write(String.format("push that 0%n"));
	}
	
	private static void geneArrayAccessExpressionOnClassVarCode (
			String class_name , String subroutine_name , String var_name ,
			JackExpressionNode index_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.VarRecord cvr = program_info.getClassVar(class_name , var_name);
		
		if (cvr instanceof JackInfoCollector.ClassRecord.ClassField) {
			geneArrayAccessExpressionOnClassFieldCode(
					class_name , subroutine_name , var_name ,
					index_expr , cvr , program_info , bw);
		}
		else if (cvr instanceof JackInfoCollector.ClassRecord.ClassStaticField) {
			geneArrayAccessExpressionOnClassStaticFieldCode(
					class_name , subroutine_name , var_name ,
					index_expr , cvr , program_info , bw);
		}
		else {
			return;
		}
	}
	
	private static void geneArrayAccessExpressionOnClassFieldCode (
			String class_name , String subroutine_name , String var_name ,
			JackExpressionNode index_expr , JackInfoCollector.ClassRecord.VarRecord cvr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		bw.write(String.format("push this %d%n" , cvr.getIndex()));
		geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
		bw.write(String.format("add%n"));
		bw.write(String.format("pop pointer 1%n"));
		bw.write(String.format("push that 0%n"));
	}
	
	private static void geneArrayAccessExpressionOnClassStaticFieldCode (
			String class_name , String subroutine_name , String var_name ,
			JackExpressionNode index_expr , JackInfoCollector.ClassRecord.VarRecord cvr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		bw.write(String.format("push static %d%n" , cvr.getIndex()));
		geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
		bw.write(String.format("add%n"));
		bw.write(String.format("pop pointer 1%n"));
		bw.write(String.format("push that 0%n"));
	}
	
	private static void geneNegateExpressionCode (
			String class_name , String subroutine_name ,
			JackNegateExpressionNode neg_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name , neg_expr.getTerm() , program_info , bw);
		bw.write("neg");
		bw.newLine();
	}
	
	private static void geneLogicalNegateExpressionCode (
			String class_name , String subroutine_name ,
			JackLogicalNegateExpressionNode log_neg_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name , log_neg_expr.getTerm() , program_info , bw);
		bw.write("not");
		bw.newLine();
	}
	
	private static void geneSubroutineCallCode (
			String class_name , String subroutine_name ,
			JackSubroutineCallNode subr_call_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		if (subr_call_expr.getVarname() != null) {
			geneSubroutineCallOnVarCode(class_name , subroutine_name , subr_call_expr , program_info , bw);
		}
		else {
			geneSubroutineCallNotOnVarCode(class_name , subroutine_name , subr_call_expr , program_info , bw);
		}
	}
	
	private static void geneSubroutineCallArgsCode (
			String class_name , String subroutine_name ,
			LinkedList<JackExpressionNode> args , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		for (JackExpressionNode arg : args) {
			geneExpressionCode(class_name , subroutine_name , arg , program_info , bw);
		}
	}
	
	private static void geneSubroutineCallOnVarCode (
			String class_name , String subroutine_name ,
			JackSubroutineCallNode subr_call_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		String called_subr_class = subr_call_expr.getScopeIdentifier();
		String called_subr_name = subr_call_expr.getSubroutineName();
		LinkedList<JackExpressionNode> args = subr_call_expr.getArguments();
		String var_name = subr_call_expr.getVarname();
		JackInfoCollector.ClassRecord.VarRecord svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		
		if (svr == null) {
			geneSubroutineCallOnClassFieldCode(
					class_name , subroutine_name , var_name ,
					called_subr_class , called_subr_name , args ,
					program_info , bw);
		}
		else {
			geneSubroutineCallOnSubrVarCode(
					class_name , subroutine_name ,
					called_subr_class , called_subr_name , args ,
					svr , program_info , bw);
		}
	}
	
	private static void geneSubroutineCallOnClassFieldCode (
			String class_name , String subroutine_name , String var_name ,
			String called_subr_class , String called_subr_name ,
			LinkedList<JackExpressionNode> args , 
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.VarRecord cvr = program_info.getClassVar(class_name , var_name);
		
		if (cvr instanceof JackInfoCollector.ClassRecord.ClassField) {
			bw.write(String.format("push this %d%n" , cvr.getIndex()));
			geneSubroutineCallArgsCode(class_name , subroutine_name , args , program_info , bw);
			bw.write(String.format("call %s.%s %d%n" , called_subr_class , called_subr_name , args.size() + 1));
		}
		else {
			return;
		}
	}
	
	private static void geneSubroutineCallOnSubrVarCode (
			String class_name , String subroutine_name ,
			String called_subr_class , String called_subr_name ,
			LinkedList<JackExpressionNode> args , JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineParameter) {
			geneSubroutineCallOnSubrParamsCode(
					class_name , subroutine_name ,
					called_subr_class , called_subr_name , args ,
					svr , program_info , bw);
		}
		else if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineLocalVar) {
			geneSubroutineCallOnSubrLocalVarCode(
					class_name , subroutine_name ,
					called_subr_class , called_subr_name , args ,
					svr , program_info , bw);
		}
		else {
			return;
		}
	}
	
	private static void geneSubroutineCallOnSubrParamsCode (
			String class_name , String subroutine_name ,
			String called_subr_class , String called_subr_name ,
			LinkedList<JackExpressionNode> args , JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.SubroutineRecord sr =
				program_info.getSubroutine(class_name , subroutine_name);
		
		if (sr instanceof JackInfoCollector.ClassRecord.MethodRecord) {
			bw.write(String.format("push argument %d%n" , svr.getIndex() + 1));
			geneSubroutineCallArgsCode(class_name , subroutine_name , args , program_info , bw);
			bw.write(String.format("call %s.%s %d%n" , called_subr_class , called_subr_name , args.size() + 1));
		}
		else {
			bw.write(String.format("push argument %d%n" , svr.getIndex()));
			geneSubroutineCallArgsCode(class_name , subroutine_name , args , program_info , bw);
			bw.write(String.format("call %s.%s %d%n" , called_subr_class , called_subr_name , args.size() + 1));
		}
	}
	
	private static void geneSubroutineCallOnSubrLocalVarCode (
			String class_name , String subroutine_name ,
			String called_subr_class , String called_subr_name ,
			LinkedList<JackExpressionNode> args , JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		bw.write(String.format("push local %d%n" , svr.getIndex()));
		geneSubroutineCallArgsCode(class_name , subroutine_name , args , program_info , bw);
		bw.write(String.format("call %s.%s %d%n" , called_subr_class , called_subr_name , args.size() + 1));
	}
	
	private static void geneSubroutineCallNotOnVarCode (
			String class_name , String subroutine_name ,
			JackSubroutineCallNode subr_call_expr , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		String called_subr_class = subr_call_expr.getScopeIdentifier();
		String called_subr_name = subr_call_expr.getSubroutineName();
		LinkedList<JackExpressionNode> args = subr_call_expr.getArguments();
		JackInfoCollector.ClassRecord.SubroutineRecord sr =
				program_info.getSubroutine(called_subr_class , called_subr_name);
		
		if (sr instanceof JackInfoCollector.ClassRecord.ConstructorRecord ||
			sr instanceof JackInfoCollector.ClassRecord.FunctionRecord)
		{
			geneConsFuncCallNotOnVarCode(
					class_name , subroutine_name , called_subr_class , called_subr_name ,
					args , program_info , bw);
		}
		else
		{
			geneMethodCallNotOnVarCode(
					class_name , subroutine_name , called_subr_class , called_subr_name ,
					args , program_info , bw);
		}
	}
	
	private static void geneConsFuncCallNotOnVarCode (
			String class_name , String subroutine_name ,
			String called_subr_class , String called_subr_name ,
			LinkedList<JackExpressionNode> args ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		geneSubroutineCallArgsCode(class_name , subroutine_name , args , program_info , bw);
		bw.write(String.format("call %s.%s %d%n" , called_subr_class , called_subr_name , args.size()));
	}
	
	private static void geneMethodCallNotOnVarCode (
			String class_name , String subroutine_name ,
			String called_subr_class , String called_subr_name ,
			LinkedList<JackExpressionNode> args ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		bw.write(String.format("push pointer 0%n"));
		geneSubroutineCallArgsCode(class_name , subroutine_name , args , program_info , bw);
		bw.write(String.format("call %s.%s %d%n" , called_subr_class , called_subr_name , args.size() + 1));
	}
	
	private static void geneStatementCode (
			String class_name , String subroutine_name ,
			JackStatementNode st_node , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		if (st_node instanceof JackLetStatementNode) {
			geneLetStatementCode(class_name , subroutine_name , (JackLetStatementNode) st_node ,
					program_info , bw);
		}
		else if (st_node instanceof JackIfStatementNode) {
			geneIfStatementCode(class_name , subroutine_name , (JackIfStatementNode) st_node ,
					program_info , bw);
		}
		else if (st_node instanceof JackWhileStatementNode) {
			geneWhileStatementCode(class_name , subroutine_name , (JackWhileStatementNode) st_node ,
					program_info , bw);
		}
		else if (st_node instanceof JackDoStatementNode) {
			geneDoStatementCode(class_name , subroutine_name , (JackDoStatementNode) st_node ,
					program_info , bw);
		}
		else if (st_node instanceof JackReturnStatementNode) {
			geneReturnStatementCode(class_name , subroutine_name , (JackReturnStatementNode) st_node ,
					program_info , bw);
		}
		else {
			return;
		}
	}
	
	private static void geneLetStatementCode (
			String class_name , String subroutine_name ,
			JackLetStatementNode let_st , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name , let_st.getAssignExpr() , program_info , bw);
		geneVarAssignCode(class_name , subroutine_name , let_st.getVarName() ,
				let_st.getIndexExpr() , program_info , bw);
	}
	
	private static void geneVarAssignCode (
			String class_name , String subroutine_name , String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		if (index_expr == null) {
			geneCommonVarAssignCode(class_name , subroutine_name , var_name , program_info , bw);
		}
		else {
			geneArrayVarAssignCode(class_name , subroutine_name , var_name , index_expr , program_info , bw);
		}
	}
	
	private static void geneArrayVarAssignCode (
			String class_name , String subroutine_name , String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.VarRecord svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		
		if (svr == null) {
			geneArrayVarAssignOnClassVarCode(
					class_name , subroutine_name , var_name , index_expr , program_info , bw);
		}
		else {
			geneArrayVarAssignOnSubroutineVarCode(
					class_name , subroutine_name , svr , index_expr , program_info , bw);
		}
	}
	
	private static void geneArrayVarAssignOnClassVarCode (
			String class_name , String subroutine_name , String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.VarRecord cvr = program_info.getClassVar(class_name , var_name);
		
		if (cvr instanceof JackInfoCollector.ClassRecord.ClassField) {
			bw.write(String.format("push this %d%n" , cvr.getIndex()));
			geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
			bw.write(String.format("add%n"));
			bw.write(String.format("pop pointer 1%n"));
			bw.write(String.format("pop that 0%n"));
		}
		else if (cvr instanceof JackInfoCollector.ClassRecord.ClassStaticField) {
			bw.write(String.format("push static %d%n" , cvr.getIndex()));
			geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
			bw.write(String.format("add%n"));
			bw.write(String.format("pop pointer 1%n"));
			bw.write(String.format("pop that 0%n"));
		}
		else {
			return;
		}
	}
	
	private static void geneArrayVarAssignOnSubroutineVarCode (
			String class_name , String subroutine_name ,
			JackInfoCollector.ClassRecord.VarRecord svr , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineParameter) {
			JackInfoCollector.ClassRecord.SubroutineRecord sr =
					program_info.getSubroutine(class_name , subroutine_name);
			
			if (sr instanceof JackInfoCollector.ClassRecord.MethodRecord) {
				bw.write(String.format("push argument %d%n" , svr.getIndex() + 1));
				geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
				bw.write(String.format("add%n"));
				bw.write(String.format("pop pointer 1%n"));
				bw.write(String.format("pop that 0%n"));
			}
			else {
				bw.write(String.format("push argument %d%n" , svr.getIndex()));
				geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
				bw.write(String.format("add%n"));
				bw.write(String.format("pop pointer 1%n"));
				bw.write(String.format("pop that 0%n"));
			}
		}
		else if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineLocalVar) {
			bw.write(String.format("push local %d%n" , svr.getIndex()));
			geneExpressionCode(class_name , subroutine_name , index_expr , program_info , bw);
			bw.write(String.format("add%n"));
			bw.write(String.format("pop pointer 1%n"));
			bw.write(String.format("pop that 0%n"));
		}
		else {
			return;
		}
	}
	
	private static void geneCommonVarAssignCode (
			String class_name , String subroutine_name , String var_name ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.VarRecord svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		
		if (svr == null) {
			geneCommonVarAssignOnClassVarCode(
					class_name , subroutine_name , var_name , program_info , bw);
		}
		else {
			geneCommonVarAssignOnSubroutineVarCode(
					class_name , subroutine_name , svr , program_info , bw);
		}
	}
	
	private static void geneCommonVarAssignOnClassVarCode (
			String class_name , String subroutine_name , String var_name ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.VarRecord cvr = program_info.getClassVar(class_name , var_name);
		
		if (cvr instanceof JackInfoCollector.ClassRecord.ClassField) {
			bw.write(String.format("pop this %d%n" , cvr.getIndex()));
		}
		else if (cvr instanceof JackInfoCollector.ClassRecord.ClassStaticField) {
			bw.write(String.format("pop static %d%n" , cvr.getIndex()));
		}
		else {
			return;
		}
	}
	
	private static void geneCommonVarAssignOnSubroutineVarCode (
			String class_name , String subroutine_name ,
			JackInfoCollector.ClassRecord.VarRecord svr ,
			JackInfoCollector.ProgramInfo program_info , BufferedWriter bw) throws IOException
	{
		if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineParameter) {
			JackInfoCollector.ClassRecord.SubroutineRecord sr =
					program_info.getSubroutine(class_name , subroutine_name);
			
			if (sr instanceof JackInfoCollector.ClassRecord.MethodRecord) {
				bw.write(String.format("pop argument %d%n" , svr.getIndex() + 1));
			}
			else {
				bw.write(String.format("pop argument %d%n" , svr.getIndex()));
			}
		}
		else if (svr instanceof JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineLocalVar) {
			bw.write(String.format("pop local %d%n" , svr.getIndex()));
		}
		else {
			return;
		}
	}
	
	private static void geneIfStatementCode (
			String class_name , String subroutine_name ,
			JackIfStatementNode if_st , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		int true_jmp_idx = ++jmp_n;
		int false_jmp_idx = ++jmp_n;
		
		geneExpressionCode(class_name , subroutine_name , if_st.getCondition() , program_info , bw);
		bw.write(String.format("if-goto JMP_%d%n" , true_jmp_idx));
		
		for (JackStatementNode st_node : if_st.getFalseStatements()) {
			geneStatementCode(class_name , subroutine_name , st_node , program_info , bw);
		}
		bw.write(String.format("goto JMP_%d%n" , false_jmp_idx));
		
		bw.write(String.format("label JMP_%d%n" , true_jmp_idx));
		for (JackStatementNode st_node : if_st.getTrueStatements()) {
			geneStatementCode(class_name , subroutine_name , st_node , program_info , bw);
		}
		
		bw.write(String.format("label JMP_%d%n" , false_jmp_idx));
	}
	
	private static void geneWhileStatementCode (
			String class_name , String subroutine_name ,
			JackWhileStatementNode while_st , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		int start_idx = ++jmp_n;
		int true_jmp_idx = ++jmp_n;
		int false_jmp_idx = ++jmp_n;
		
		bw.write(String.format("label JMP_%d%n" , start_idx));
		geneExpressionCode(class_name , subroutine_name , while_st.getCondition() , program_info , bw);
		
		bw.write(String.format("if-goto JMP_%d%n" , true_jmp_idx));
		bw.write(String.format("goto JMP_%d%n" , false_jmp_idx));
		
		bw.write(String.format("label JMP_%d%n" , true_jmp_idx));
		for (JackStatementNode st_node : while_st.getBody()) {
			geneStatementCode(class_name , subroutine_name , st_node , program_info , bw);
		}
		bw.write(String.format("goto JMP_%d%n" , start_idx));
		
		bw.write(String.format("label JMP_%d%n" , false_jmp_idx));
	}
	
	private static void geneDoStatementCode (
			String class_name , String subroutine_name ,
			JackDoStatementNode do_st , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		geneExpressionCode(class_name , subroutine_name , do_st.getSubrcall() , program_info , bw);
		bw.write(String.format("pop temp 0%n"));
	}
	
	private static void geneReturnStatementCode (
			String class_name , String subroutine_name ,
			JackReturnStatementNode rt_st , JackInfoCollector.ProgramInfo program_info ,
			BufferedWriter bw) throws IOException
	{
		JackInfoCollector.ClassRecord.SubroutineRecord sr =
				program_info.getSubroutine(class_name , subroutine_name);
		JackExpressionNode ret_expr = rt_st.getReturnExpr();
		
		if (ret_expr != null) {
			geneExpressionCode(class_name , subroutine_name , ret_expr , program_info , bw);
			
			if (sr.getReturnType().equals("void")) {
				bw.write(String.format("push constant 0%n"));
				bw.write(String.format("return%n"));
			}
			else {
				bw.write(String.format("return%n"));
			}
		}
		else {
			if (sr.getReturnType().equals("void")) {
				bw.write(String.format("push constant 0%n"));
				bw.write(String.format("return%n"));
			}
			else {
				bw.write(String.format("return%n"));
			}
		}
	}
	
	private static void geneSubroutineHeader (
			JackInfoCollector.ClassRecord.SubroutineRecord sr ,
			BufferedWriter bw) throws IOException
	{
		bw.write(String.format("function %s.%s %d%n" ,
				sr.getClassName() , sr.getSubroutineName() , sr.getLocalVarNum()));
	}

}
