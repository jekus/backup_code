import java.io.*;
import java.util.*;

public class JackTokenizer {

	public static enum TokenType {
		KW_CLASS , KW_CONSTRUCTOR , KW_FUNC ,
		KW_METHOD , KW_FIELD , KW_STATIC ,
		KW_VAR , KW_INT , KW_CHAR ,
		KW_BOOL , KW_VOID , KW_TRUE ,
		KW_FALSE , KW_NULL , KW_THIS ,
		KW_LET , KW_DO , KW_IF ,
		KW_ELSE , KW_WHILE , KW_RETURN ,
		
		SYM_LBRACE , SYM_RBRACE ,
		SYM_LPAREN , SYM_RPAREN ,
		SYM_LBRACKET , SYM_RBRACKET ,
		SYM_PERIOD , SYM_COMMA , SYM_SEMICOLON ,
		SYM_PLUS , SYM_MINUS , SYM_MUL , SYM_DIV ,
		SYM_AND , SYM_OR , SYM_NOT ,
		SYM_LT , SYM_GT , SYM_EQ ,
		
		IDENTIFIER ,
		CONST_INTEGER , CONST_STRING ,
		EOF
	}
	
	private static HashSet<String> kwset = new HashSet<String>(
			Arrays.asList("class" , "constructor" , "function" , "method" ,
					"field" , "static" , "var" , "int" , "char" , "boolean" ,
					"void" , "true" , "false" , "null" , "this" ,
					"let" , "do" , "if" , "else" , "while" , "return"));
	
	private static HashSet<Character> symbolset = new HashSet<Character>(
			Arrays.asList('{' , '}' , '(' , ')' , '[' , ']' ,
					'.' , ',' , ';' , '+' , '-' , '*' , '/' ,
					'&' , '|' , '~' , '<' , '>' , '='));
	
	
	private TokenType tk_t;
	private String tk_rep;
	private StringBuilder buf;
	private int buf_idx;
	private char cur_ch;
	private boolean iseof;
	
	JackTokenizer (String infile) throws IOException {
		this.buf = new StringBuilder(2048);
		this.buf_idx = -1;
		this.iseof = false;
		
		try (BufferedReader br = new BufferedReader(new FileReader(infile))) {
			br.lines().forEach(line -> this.buf.append(line).append('\n'));
		}
		catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(0);
		}
		
		nextChar();
	}
	
	private static String takeToken (StringBuilder buf , int start , int end) {
		return buf.substring(start , end);
	}
	

	private void getKwClass 		() 		{ this.tk_t = JackTokenizer.TokenType.KW_CLASS;			}
	private void getKwConstructor	() 		{ this.tk_t = JackTokenizer.TokenType.KW_CONSTRUCTOR;	}
	private void getKwFunc 			()		{ this.tk_t = JackTokenizer.TokenType.KW_FUNC;			}
	private void getKwMethod		()		{ this.tk_t = JackTokenizer.TokenType.KW_METHOD;		}
	private void getKwField			()		{ this.tk_t = JackTokenizer.TokenType.KW_FIELD;			}
	private void getKwStatic		()		{ this.tk_t = JackTokenizer.TokenType.KW_STATIC;		}
	private void getKwVar			()		{ this.tk_t = JackTokenizer.TokenType.KW_VAR;			}
	private void getKwInt			()		{ this.tk_t = JackTokenizer.TokenType.KW_INT;			}
	private void getKwChar			()		{ this.tk_t = JackTokenizer.TokenType.KW_CHAR;			}
	private void getKwBool			()		{ this.tk_t = JackTokenizer.TokenType.KW_BOOL;			}
	private void getKwVoid			()		{ this.tk_t = JackTokenizer.TokenType.KW_VOID;			}
	private void getKwTrue			()		{ this.tk_t = JackTokenizer.TokenType.KW_TRUE;			}
	private void getKwFalse			()		{ this.tk_t = JackTokenizer.TokenType.KW_FALSE;			}
	private void getKwNull			()		{ this.tk_t = JackTokenizer.TokenType.KW_NULL;			}
	private void getKwThis			()		{ this.tk_t = JackTokenizer.TokenType.KW_THIS;			}
	private void getKwLet			()		{ this.tk_t = JackTokenizer.TokenType.KW_LET;			}
	private void getKwDo			()		{ this.tk_t = JackTokenizer.TokenType.KW_DO;			}
	private void getKwIf			()		{ this.tk_t = JackTokenizer.TokenType.KW_IF;			}
	private void getKwElse			()		{ this.tk_t = JackTokenizer.TokenType.KW_ELSE;			}
	private void getKwWhile			()		{ this.tk_t = JackTokenizer.TokenType.KW_WHILE;			}
	private void getKwReturn		()		{ this.tk_t = JackTokenizer.TokenType.KW_RETURN;		}
	
	private void getKeyword () {
		switch (tk_rep) {
			case "class":			getKwClass();			break;
			case "constructor":		getKwConstructor(); 	break;
			case "function":		getKwFunc();			break;
			case "method":			getKwMethod();			break;
			case "field":			getKwField();			break;
			case "static":			getKwStatic();			break;
			case "var":				getKwVar();				break;
			
			case "int":				getKwInt();				break;
			case "char":			getKwChar();			break;
			case "boolean":			getKwBool();			break;
			
			case "true":			getKwTrue();			break;
			case "false":			getKwFalse();			break;
			
			case "void":			getKwVoid();			break;
			case "null":			getKwNull();			break;
			case "this":			getKwThis();			break;
			case "let":				getKwLet();				break;
			case "do":				getKwDo();				break;
			case "if":				getKwIf();				break;
			case "else":			getKwElse();			break;
			case "while":			getKwWhile();			break;
			case "return":			getKwReturn();			break;
		}
	}
	

	private void getSymLbrace		()		{ this.tk_t = JackTokenizer.TokenType.SYM_LBRACE;		}
	private void getSymRbrace		()		{ this.tk_t = JackTokenizer.TokenType.SYM_RBRACE;		}
	private void getSymLparen		()		{ this.tk_t = JackTokenizer.TokenType.SYM_LPAREN;		}
	private void getSymRparen		()		{ this.tk_t = JackTokenizer.TokenType.SYM_RPAREN;		}
	private void getSymLbracket		()		{ this.tk_t = JackTokenizer.TokenType.SYM_LBRACKET;		}
	private void getSymRbracket		()		{ this.tk_t = JackTokenizer.TokenType.SYM_RBRACKET;		}
	private void getSymPeriod		()		{ this.tk_t = JackTokenizer.TokenType.SYM_PERIOD;		}
	private void getSymComma		()		{ this.tk_t = JackTokenizer.TokenType.SYM_COMMA;		}
	private void getSymSemicolon	()		{ this.tk_t = JackTokenizer.TokenType.SYM_SEMICOLON;	}
	private void getSymPlus			()		{ this.tk_t = JackTokenizer.TokenType.SYM_PLUS;			}
	private void getSymMinus		()		{ this.tk_t = JackTokenizer.TokenType.SYM_MINUS;		}
	private void getSymMul			()		{ this.tk_t = JackTokenizer.TokenType.SYM_MUL;			}
	private void getSymDiv			()		{ this.tk_t = JackTokenizer.TokenType.SYM_DIV;			}
	private void getSymAnd			()		{ this.tk_t = JackTokenizer.TokenType.SYM_AND;			}
	private void getSymOr			()		{ this.tk_t = JackTokenizer.TokenType.SYM_OR;			}
	private void getSymNot			()		{ this.tk_t = JackTokenizer.TokenType.SYM_NOT;			}
	private void getSymLt			()		{ this.tk_t = JackTokenizer.TokenType.SYM_LT;			}
	private void getSymGt			()		{ this.tk_t = JackTokenizer.TokenType.SYM_GT;			}
	private void getSymEq			()		{ this.tk_t = JackTokenizer.TokenType.SYM_EQ;			}
	
	private void getSymbol () {
		tk_rep = takeToken(buf , buf_idx , buf_idx + 1);
		switch (cur_ch) {
			case '{':	getSymLbrace();		break;
			case '}':	getSymRbrace();		break;
			case '(':	getSymLparen();		break;
			case ')':	getSymRparen();		break;
			case '[':	getSymLbracket();	break;
			case ']':	getSymRbracket();	break;
			
			case '.':	getSymPeriod();		break;
			case ',':	getSymComma();		break;
			case ';':	getSymSemicolon();	break;
			
			case '+':	getSymPlus(); 		break;
			case '-':	getSymMinus(); 		break;
			case '*':	getSymMul();	 	break;
			case '/':	getSymDiv();		break;
			case '&':	getSymAnd();		break;
			case '|':	getSymOr();			break;
			case '~':	getSymNot();		break;
			case '<':	getSymLt();			break;
			case '>':	getSymGt();			break;
			case '=':	getSymEq();			break;
		}
		
		nextChar();
	}

	private void getKeywordOrIdentifier () {
		int tk_start = buf_idx;
		while (!iseof && (cur_ch == '_' || Character.isLetterOrDigit(cur_ch))) {
			nextChar();
		}

		tk_rep = takeToken(buf , tk_start , buf_idx);
		if (kwset.contains(tk_rep)) {
			getKeyword();
		}
		else {
			tk_t = JackTokenizer.TokenType.IDENTIFIER;
		}
	}

	private void getInteger () {
		int tk_start = buf_idx;
		while (!iseof && Character.isDigit(cur_ch)) {
			nextChar();
		}
		
		tk_rep = takeToken(buf , tk_start , buf_idx);
		this.tk_t = JackTokenizer.TokenType.CONST_INTEGER;
	}

	private void getString () {
		int tk_start = buf_idx + 1;
		nextChar();
		while (!iseof && cur_ch != '"') {
			nextChar();
		}

		tk_rep = takeToken(buf , tk_start , buf_idx);
		this.tk_t = JackTokenizer.TokenType.CONST_STRING;
		nextChar();
	}
	
	private void getEof () {
		this.tk_t = JackTokenizer.TokenType.EOF;
	}
	
	private char nextChar () {
		if (buf_idx + 1 >= buf.length()) {
			iseof = true;
			return '\0';
		}
		else {
			return (cur_ch = buf.charAt(++buf_idx));
		}
	}

	private char peekChar () throws IndexOutOfBoundsException {
		return buf.charAt(buf_idx + 1);
	}

	private void skip_whitespace () {
		while (!iseof && Character.isWhitespace(cur_ch)) {
			nextChar();
		}
	}

	private void skip_linecomment () {
		while (!iseof && cur_ch != '\n') {
			nextChar();
		}
	}
	
	private void skip_blockcomment () {
		try {
			nextChar();
			nextChar();
			while (!iseof && (cur_ch != '*' || peekChar() != '/')) {
				nextChar();
			}
		}
		catch (IndexOutOfBoundsException e) {
		}

		nextChar();
		nextChar();
	}

	private void skip_comment () {
		try {
			if (cur_ch == '/' && peekChar() == '/') {
				skip_linecomment();
				skip_useless();
			}
			else if (cur_ch == '/' && peekChar() == '*') {
				skip_blockcomment();
				skip_useless();
			}
		}
		catch (IndexOutOfBoundsException e) {
		}
	}
	
	private void skip_useless () {
		skip_whitespace();
		skip_comment();
	}

	public void getNextToken ()
	{
		skip_useless();
		if (iseof) {
			getEof();
			return;
		}

		if (Character.isDigit(cur_ch)) {
			getInteger();
		}
		else if (cur_ch == '"') {
			getString();
		}
		else if (cur_ch == '_' || Character.isLetter(cur_ch)) {
			getKeywordOrIdentifier();
		}
		else if (symbolset.contains(cur_ch)) {
			getSymbol();
		}
		else {
			JackError.error(String.format("Unknown character '%c'" , cur_ch));
		}
	}

	public TokenType getTkType () {
		return tk_t;
	}

	public String getTkRep () {
		return tk_rep;
	}

	public void validateToken (TokenType require)
	{
		if (tk_t == require) {
			getNextToken();
		}
		else {
			JackError.error(String.format("Require [%s] , get [%s]" ,
								require.toString() , tk_t.toString()));
		}
	}

}
