import java.util.LinkedList;

class JackProgramValidator {
	
	static void validate (LinkedList<JackParser> parsers , JackInfoCollector.ProgramInfo program_info) {
		for (JackParser pser : parsers) {
			validateClass((JackClassNode) pser.getAst() , program_info);
		}
	}
	
	private static void validateClass (JackClassNode node , JackInfoCollector.ProgramInfo program_info) {
		for (JackSubroutineDecNode subr_node : node.getSubrDecList()) {
			validateSubroutineDec(node.getClassName() , subr_node , program_info);
		}
	}
	
	private static void validateSubroutineDec (
			String class_name , JackSubroutineDecNode node ,
			JackInfoCollector.ProgramInfo program_info)
	{
		validateSubroutineBody(
				program_info.getSubroutine(class_name , node.getSubroutineName()) ,
				node.getSubroutineBody() , program_info);
	}
	
	private static void validateSubroutineBody (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackSubroutineBodyNode node , JackInfoCollector.ProgramInfo program_info)
	{
		for (JackStatementNode st_node : node.getStatementList()) {
			validateStatement(subr_rec , st_node , program_info);
		}
	}
	
	private static void validateStatement (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackStatementNode st  , JackInfoCollector.ProgramInfo program_info)
	{
		if (st instanceof JackLetStatementNode) {
			validateLetStatement(subr_rec , (JackLetStatementNode) st , program_info);
		}
		else if (st instanceof JackIfStatementNode) {
			validateIfStatement(subr_rec , (JackIfStatementNode) st , program_info);
		}
		else if (st instanceof JackWhileStatementNode) {
			validateWhileStatement(subr_rec , (JackWhileStatementNode) st , program_info);
		}
		else if (st instanceof JackDoStatementNode) {
			validateDoStatement(subr_rec , (JackDoStatementNode) st , program_info);
		}
		else if (st instanceof JackReturnStatementNode) {
			validateReturnStatement(subr_rec , (JackReturnStatementNode) st , program_info);
		}
	}
	
	private static void validateLetStatement (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackLetStatementNode let_st , JackInfoCollector.ProgramInfo program_info)
	{
		validateVariableAccess(subr_rec , let_st.getVarName() , let_st.getIndexExpr() , program_info);
		validateExpression(subr_rec , let_st.getAssignExpr() , program_info);
		validateAssignmentTypeChecked(subr_rec , let_st.getVarName() , let_st.getAssignExpr() , program_info);
	}
	
	private static boolean isTypeChecked (String type_1 , String type_2)
	{
		if (!type_1.equals(type_2)) {
			if (type_1.equals("char") && !type_2.equals("int") && !type_2.equals("Array")) {
				return false;
			}
			else if (type_1.equals("int") && !type_2.equals("char") && !type_2.equals("Array")) {
				return false;
			}
			else if (type_1.equals("Array") && !type_2.equals("int") && !type_2.equals("boolean") && !type_2.equals("char")) {
				return true;
			}
			else if (!type_1.equals("int") && !type_1.equals("boolean") && !type_1.equals("char") &&
					 !type_2.equals("int") && !type_2.equals("Array") &&
					 !type_2.equals("char"))
			{
				return false;
			}
			else {
				return true;
			}
		}
		else {
			return true;
		}
	}
	
	private static void validateAssignmentTypeChecked (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackExpressionNode assign_expr ,
			JackInfoCollector.ProgramInfo program_info)
	{
		String var_type = getVariableType(subr_rec , var_name , program_info) ,
			   assign_expr_type = getExprType(subr_rec , assign_expr , program_info);
		if (!isTypeChecked(var_type , assign_expr_type)) {
			JackError.error(
					String.format("Class '%s' subroutine '%s' attempt to make an assignment on incompatible type" ,
							subr_rec.getClassName() , subr_rec.getSubroutineName()));
		}
	}
	
	private static void validateIfStatement (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackIfStatementNode if_st , JackInfoCollector.ProgramInfo program_info)
	{	
		JackExpressionNode cond_expr = if_st.getCondition();
		validateExpression(subr_rec , cond_expr , program_info);
		validateExprType(cond_expr , "boolean" , subr_rec , program_info ,
				String.format("The type of Condition in If-Statement must be boolean"));
		
		for (JackStatementNode st : if_st.getTrueStatements()) {
			validateStatement(subr_rec , st , program_info);
		}
		
		for (JackStatementNode st : if_st.getFalseStatements()) {
			validateStatement(subr_rec , st , program_info);
		}
	}
	
	private static void validateWhileStatement (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackWhileStatementNode while_st , JackInfoCollector.ProgramInfo program_info)
	{
		JackExpressionNode cond_expr = while_st.getCondition();
		validateExpression(subr_rec , cond_expr , program_info);
		validateExprType(cond_expr , "boolean" , subr_rec , program_info ,
				String.format("The type of Condition in While-Statement must be boolean"));
		
		for (JackStatementNode st : while_st.getBody()) {
			validateStatement(subr_rec , st , program_info);
		}
	}
	
	private static void validateDoStatement (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackDoStatementNode do_st , JackInfoCollector.ProgramInfo program_info)
	{
		validateSubroutineCallExpression(subr_rec , do_st.getSubrcall() , program_info);
	}
	
	private static void validateReturnStatement (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackReturnStatementNode rt_st , JackInfoCollector.ProgramInfo program_info)
	{
		validateExpression(subr_rec , rt_st.getReturnExpr() , program_info);
	}
	
	private static void validateExpression (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackExpressionNode expr , JackInfoCollector.ProgramInfo program_info)
	{
		if (expr instanceof JackArithmeticExpressionNode) {
			validateArithmeticExpression(subr_rec , (JackArithmeticExpressionNode) expr , program_info);
		}
		else if (expr instanceof JackLogicalExpressionNode) {
			validateLogicalExpression(subr_rec , (JackLogicalExpressionNode) expr , program_info);
		}
		else if (expr instanceof JackArrayAccessNode) {
			validateArrayAccessExpression(subr_rec , (JackArrayAccessNode) expr , program_info);
		}
		else if (expr instanceof JackUnaryExpressionNode) {
			validateUnaryExpression(subr_rec , (JackUnaryExpressionNode) expr , program_info);
		}
		else if (expr instanceof JackSubroutineCallNode) {
			validateSubroutineCallExpression(subr_rec , (JackSubroutineCallNode) expr , program_info);
		}
		else if (expr instanceof JackIdentifierNode) {
			validateIdentifierExpression(subr_rec , (JackIdentifierNode) expr , program_info);
		}
	}
	
	private static void validateArrayAccessExpression (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackArrayAccessNode aa_expr , JackInfoCollector.ProgramInfo program_info)
	{
		validateVariableAccess(subr_rec , aa_expr.getVarName() , aa_expr.getIndexExpr() , program_info);
	}
	
	private static void validateIdentifierExpression (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackIdentifierNode id_expr , JackInfoCollector.ProgramInfo program_info)
	{
		validateVariableAccess(subr_rec , id_expr.getRep() , null , program_info);
	}
	
	private static void validateSubroutineCallExpression (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackSubroutineCallNode sc_expr , JackInfoCollector.ProgramInfo program_info)
	{
		String target_subr_class = sc_expr.getScopeIdentifier() ,
			   target_subr_name = sc_expr.getSubroutineName();
		
		JackInfoCollector.ClassRecord.SubroutineRecord sr =
				program_info.getSubroutine(target_subr_class , target_subr_name);
		if (sr == null) {
			JackError.error(String.format("Class '%s' does not have a subroutine called '%s'" ,
					target_subr_class , target_subr_name));
		}
		else {
			LinkedList<JackExpressionNode> arguments = sc_expr.getArguments();
			arguments.forEach(arg -> validateExpression(subr_rec , arg , program_info));
			
			int idx = 0;
			String[] args_types = sr.getParametersType();
			for (JackExpressionNode arg : arguments) {
				if (!isTypeChecked(args_types[idx] , getExprType(subr_rec , arg , program_info))) {
					JackError.error(String.format("Class '%s' subroutine '%s' called with incompatible type of argument" ,
							sc_expr.getScopeIdentifier() , sc_expr.getSubroutineName()));
				}
				else {
					++idx;
				}
			}
		}
	}
	
	private static void validateUnaryExpression (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackUnaryExpressionNode u_expr , JackInfoCollector.ProgramInfo program_info)
	{
		JackExpressionNode term = u_expr.getTerm();
		if (u_expr instanceof JackNegateExpressionNode) {
			validateExpression(subr_rec , term , program_info);
			if (!getExprType(subr_rec , term , program_info).equals("int")) {
				JackError.error(String.format("Negate expression must take an integer operand"));
			}
		}
		else if (u_expr instanceof JackLogicalNegateExpressionNode) {
			validateExpression(subr_rec , term , program_info);
			
			String expr_t = getExprType(subr_rec , term , program_info);
			if (!expr_t.equals("boolean") && !expr_t.equals("int")) {
				JackError.error(String.format("Logical negate expression must take an int/boolean operand"));
			}
		}
	}

	private static void validateLogicalExpression (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackLogicalExpressionNode lg_expr , JackInfoCollector.ProgramInfo program_info)
	{
		validateExpression(subr_rec , lg_expr.getLeftHandSide() , program_info);
		validateExpression(subr_rec , lg_expr.getRightHandSide() , program_info);
	}

	private static void validateArithmeticExpression (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackArithmeticExpressionNode ar_expr , JackInfoCollector.ProgramInfo program_info)
	{
		JackExpressionNode left_hand_side = ar_expr.getLeftHandSide() ,
						   right_hand_side = ar_expr.getRightHandSide();
		
		validateExpression(subr_rec , left_hand_side , program_info);
		validateExpression(subr_rec , right_hand_side , program_info);
		
		NodeType nt = ar_expr.getNodeType();
		String lhs_type = getExprType(subr_rec , left_hand_side , program_info) ,
			   rhs_type = getExprType(subr_rec , right_hand_side , program_info);		
		if (nt == NodeType.BITWISE_AND_EXPRESSION || nt == NodeType.BITWISE_OR_EXPRESSION) {
			if ((!lhs_type.equals("boolean") || !rhs_type.equals("boolean")) &&
				(!lhs_type.equals("int") || !rhs_type.equals("int")))
			{
				JackError.error(String.format("Class '%s' subroutine '%s' , & and | operators need a pair of int/boolean operands" ,
						subr_rec.getClassName() , subr_rec.getSubroutineName()));
			}
		}
		else if ((!lhs_type.equals("int") && !lhs_type.equals("Array")) ||
				 (!rhs_type.equals("int") && !rhs_type.equals("Array")))
		{
			JackError.error(String.format("Class '%s' subroutine '%s' , Arithmetic operation needs Integer operands" ,
					subr_rec.getClassName() , subr_rec.getSubroutineName()));
		}
	}

	private static void validateVariableAccess (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info)
	{
		if (subr_rec instanceof JackInfoCollector.ClassRecord.ConstructorRecord ||
			subr_rec instanceof JackInfoCollector.ClassRecord.MethodRecord)
		{
			selectValidationMannerInConsOrMethod(subr_rec , var_name , index_expr , program_info);
		}
		else if (subr_rec instanceof JackInfoCollector.ClassRecord.FunctionRecord)
		{
			selectValidationMannerInStaticMethod(subr_rec , var_name , index_expr , program_info);
		}
		else
		{
			return;
		}
	}
	
	private static void selectValidationMannerInConsOrMethod (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info)
	{
		if (index_expr == null) {
			validateOrdinaryVariableAccessInConsOrMethod(subr_rec , var_name , program_info);
		}
		else {
			validateArrayAccessInConsOrMethod(subr_rec , var_name , index_expr , program_info);
		}
	}
	
	private static void selectValidationMannerInStaticMethod (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info)
	{
		if (index_expr == null) {
			validateOrdinaryVariableAccessInStaticMethod(subr_rec , var_name , program_info);
		}
		else {
			validateArrayAccessInStaticMethod(subr_rec , var_name , index_expr , program_info);
		}
	}
	
	private static void generateVariableAbsenceError (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec , String var_name)
	{
		String class_name = subr_rec.getClassName() ,
			   subroutine_name = subr_rec.getSubroutineName();
		
		if (subr_rec instanceof JackInfoCollector.ClassRecord.ConstructorRecord) {
			JackError.error(String.format("Undefined variable '%s' in class '%s' constructor" ,
					var_name , class_name));
		}
		else if (subr_rec instanceof JackInfoCollector.ClassRecord.MethodRecord) {
			JackError.error(String.format("Undefined variable '%s' in class '%s' method '%s'" ,
					var_name , class_name , subroutine_name));
		}
		else if (subr_rec instanceof JackInfoCollector.ClassRecord.FunctionRecord) {
			JackError.error(String.format("Undefined variable '%s' in class '%s' function '%s'" ,
					var_name , class_name , subroutine_name));
		}
		else {
			return;
		}
	}
	
	private static void validateOrdinaryVariableAccessInConsOrMethod (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackInfoCollector.ProgramInfo program_info)
	{
		String class_name = subr_rec.getClassName() ,
			   subroutine_name = subr_rec.getSubroutineName();
		
		JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineVar svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		if (svr == null) {
			JackInfoCollector.ClassRecord.VarRecord cvr =
					program_info.getClassVar(subr_rec.getClassName() , var_name);
			if (cvr == null) {
				generateVariableAbsenceError(subr_rec , var_name);
			}
		}
	}
	
	private static void validateOrdinaryVariableAccessInStaticMethod (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackInfoCollector.ProgramInfo program_info)
	{
		String class_name = subr_rec.getClassName() ,
			   subroutine_name = subr_rec.getSubroutineName();

		JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineVar svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		if (svr == null) {
			JackInfoCollector.ClassRecord.VarRecord cvr =
					program_info.getClassVar(subr_rec.getClassName() , var_name);
			if (cvr == null) {
				generateVariableAbsenceError(subr_rec , var_name);
			}
			else if (cvr instanceof JackInfoCollector.ClassRecord.ClassField) {
				JackError.error(String.format("Function '%s' in class '%s' accesses a field variable" ,
						subroutine_name , class_name));
			}
		}
	}
	
	private static String getVariableType (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackInfoCollector.ProgramInfo program_info)
	{
		String class_name = subr_rec.getClassName() ,
			   subroutine_name = subr_rec.getSubroutineName();

		JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineVar svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		if (svr == null) {
			JackInfoCollector.ClassRecord.VarRecord cvr =
					program_info.getClassVar(subr_rec.getClassName() , var_name);
			if (cvr == null) {
				generateVariableAbsenceError(subr_rec , var_name);
				return null;
			}
			else {
				return cvr.getType();
			}
		}
		else {
			return svr.getType();
		}
	}
	
	private static String getExprType (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackExpressionNode expr , JackInfoCollector.ProgramInfo program_info)
	{
		if (expr instanceof JackIntegerNode) {
			return "int";
		}
		else if (expr instanceof JackStringNode) {
			return "String";
		}
		else if (expr instanceof JackKeywordConstNode) {
			switch (((JackKeywordConstNode) expr).getKeyword()) {
				case KW_TRUE: case KW_FALSE:
					return "boolean";
					
				case KW_NULL: return "null";
				case KW_THIS: return subr_rec.getClassName();
				
				default: return null;
			}
		}
		else if (expr instanceof JackIdentifierNode) {
			return getVariableType(subr_rec , ((JackIdentifierNode) expr).getRep() , program_info);
		}
		else if (expr instanceof JackArrayAccessNode) {
			return "Array";
		}
		else if (expr instanceof JackNegateExpressionNode) {
			return "int";
		}
		else if (expr instanceof JackLogicalNegateExpressionNode) {
			return getExprType(subr_rec , ((JackLogicalNegateExpressionNode) expr).getTerm() , program_info);
		}
		else if (expr instanceof JackArithmeticExpressionNode) {
			return getExprType(subr_rec , ((JackArithmeticExpressionNode) expr).getLeftHandSide() , program_info);
		}
		else if (expr instanceof JackLogicalExpressionNode) {
			return "boolean";
		}
		else if (expr instanceof JackSubroutineCallNode) {
			JackSubroutineCallNode sc_expr = (JackSubroutineCallNode) expr;
			JackInfoCollector.ClassRecord.SubroutineRecord sr =
					program_info.getSubroutine(sc_expr.getScopeIdentifier() , sc_expr.getSubroutineName());
			return sr.getReturnType();
		}
		else {
			return null;
		}
	}
	
	private static void validateVariableType (String src_type , String target_type , String err_msg) {
		if (!src_type.equals(target_type)) {
			JackError.error(err_msg);
		}
	}
	
	private static void validateExprType (
			JackExpressionNode expr , String target_type ,
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackInfoCollector.ProgramInfo program_info , String err_msg)
	{
		if (!getExprType(subr_rec , expr , program_info).equals(target_type)) {
			JackError.error(err_msg);
		}
	}
	
	private static void doTypeCheckArrayAccess (
			String class_name , String subroutine_name , String var_name ,
			String var_type , JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackExpressionNode index_expr , JackInfoCollector.ProgramInfo program_info)
	{
		validateVariableType(var_type , "Array" ,
				String.format("Variable '%s' in class '%s' subroutine '%s' is not an Array" ,
						var_name , class_name , subroutine_name));
		
		validateExpression(subr_rec , index_expr , program_info);
		
		String index_expr_type = getExprType(subr_rec , index_expr , program_info);
		if (!index_expr_type.equals("int") && !index_expr_type.equals("Array")) {
			JackError.error(String.format("Index exprssion for variable '%s' in class '%s' subroutine '%s' is not an Integer" ,
					var_name , class_name , subroutine_name));
		}
	}
	
	private static void typecheckArrayAccessFromSubroutineVar (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineVar svr ,
			JackExpressionNode index_expr , JackInfoCollector.ProgramInfo program_info)
	{
		String class_name = svr.getClassName() ,
			   subroutine_name = svr.getSubroutineName() ,
			   var_name = svr.getVarName();
		
		doTypeCheckArrayAccess(class_name , subroutine_name , var_name ,
				svr.getType() , subr_rec , index_expr , program_info);
	}
	
	private static void typecheckArrayAccessFromClassVar (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			JackInfoCollector.ClassRecord.VarRecord cvr ,
			JackExpressionNode index_expr , JackInfoCollector.ProgramInfo program_info)
	{
		String class_name = cvr.getClassName() ,
			   subroutine_name = subr_rec.getSubroutineName() ,
			   var_name = cvr.getVarName();
		
		doTypeCheckArrayAccess(class_name , subroutine_name , var_name ,
				cvr.getType() , subr_rec , index_expr , program_info);
	}
	
	private static void validateArrayAccessInConsOrMethod (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info)
	{
		String class_name = subr_rec.getClassName() ,
			   subroutine_name = subr_rec.getSubroutineName();
		
		JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineVar svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		if (svr == null) {
			JackInfoCollector.ClassRecord.VarRecord cvr =
					program_info.getClassVar(subr_rec.getClassName() , var_name);
			if (cvr == null) {
				generateVariableAbsenceError(subr_rec , var_name);
			}
			else {
				typecheckArrayAccessFromClassVar(subr_rec , cvr , index_expr , program_info);
			}
		}
		else {
			typecheckArrayAccessFromSubroutineVar(subr_rec , svr , index_expr , program_info);
		}
	}
	
	private static void validateArrayAccessInStaticMethod (
			JackInfoCollector.ClassRecord.SubroutineRecord subr_rec ,
			String var_name , JackExpressionNode index_expr ,
			JackInfoCollector.ProgramInfo program_info)
	{
		String class_name = subr_rec.getClassName() ,
			   subroutine_name = subr_rec.getSubroutineName();

		JackInfoCollector.ClassRecord.SubroutineRecord.SubroutineVar svr =
				program_info.getSubroutineVar(class_name , subroutine_name , var_name);
		if (svr == null) {
			JackInfoCollector.ClassRecord.VarRecord cvr =
					program_info.getClassVar(subr_rec.getClassName() , var_name);
			if (cvr == null) {
				generateVariableAbsenceError(subr_rec , var_name);
			}
			else if (cvr instanceof JackInfoCollector.ClassRecord.ClassField) {
				JackError.error(
						String.format("Function '%s' in class '%s' accesses a field variable" ,
								subroutine_name , class_name));
			}
			else {
				typecheckArrayAccessFromClassVar(subr_rec , cvr , index_expr , program_info);
			}
		}
		else {
			typecheckArrayAccessFromSubroutineVar(subr_rec , svr , index_expr , program_info);
		}
	}
	
}
