import java.util.LinkedList;

public class JackParser {
	
	private final String source_file;
	private final JackTokenizer jtk;
	private final JackAstNode ast;

	JackParser (String source_file , JackTokenizer jtk)
	{
		jtk.getNextToken();
		this.jtk = jtk;
		ast = parseClass();
		this.source_file = source_file;
	}
	
	JackAstNode getAst () { return ast; }
	String getSourceFile () { return source_file; }

	
	private JackAstNode parseClass ()
	{
		jtk.validateToken(JackTokenizer.TokenType.KW_CLASS);
		JackClassNode node = new JackClassNode();
		node.setName(parseIdentifier());
		parseClassBody(node);
		return node;
	}

	private void parseClassBody (JackClassNode classnode)
	{
		jtk.validateToken(JackTokenizer.TokenType.SYM_LBRACE);
		classnode.setVarDecs(parseClassVarDecs());
		classnode.setSubroutineDecs(parseSubroutineDecs());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RBRACE);
	}
	
	private LinkedList<JackClassVarDecNode> parseClassVarDecs ()
	{
		LinkedList<JackClassVarDecNode> vardecs = new LinkedList<JackClassVarDecNode>();
		
		while (true) {
			switch(jtk.getTkType()) {
				case KW_STATIC: case KW_FIELD:
					vardecs.add(parseClassVarDec());
					break;
					
				default:
					return vardecs;
			}
		}
	}
	
	private LinkedList<JackSubroutineDecNode> parseSubroutineDecs ()
	{
		LinkedList<JackSubroutineDecNode> subrdecs = new LinkedList<JackSubroutineDecNode>();
		
		while (true) {
			switch (jtk.getTkType()) {
				case KW_CONSTRUCTOR: case KW_FUNC:
				case KW_METHOD:
					subrdecs.add(parseSubroutineDec());
					break;
					
				default:
					return subrdecs;
			}
		}
	}

	private JackClassVarDecNode parseClassVarDec ()
	{
		JackClassVarDecNode node = new JackClassVarDecNode();
		
		node.setModifier(parseClassVarModifier());
		node.setType(parseVarType());
		node.setVarnames(parseIdentifiers());
		jtk.validateToken(JackTokenizer.TokenType.SYM_SEMICOLON);
		
		return node;
	}

	private JackSubroutineDecNode parseSubroutineDec ()
	{
		JackSubroutineDecNode node = new JackSubroutineDecNode();
		
		node.setSubroutineType(parseSubroutineType());
		if (jtk.getTkType() == JackTokenizer.TokenType.KW_VOID) {
			node.setReturnType(jtk.getTkRep());
			jtk.validateToken(JackTokenizer.TokenType.KW_VOID);
		}
		else {
			node.setReturnType(parseVarType());
		}
		
		node.setSubroutineName(parseIdentifier());
		jtk.validateToken(JackTokenizer.TokenType.SYM_LPAREN);
		node.setParameterList(parseParamList());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RPAREN);
		node.setSubroutineBody(parseSubroutineBody());
		
		return node;
	}

	private JackParameterListNode parseParamList () 
	{
		JackParameterListNode node = new JackParameterListNode();
		
		switch (jtk.getTkType()) {
			case KW_INT: case KW_CHAR:
			case KW_BOOL: case IDENTIFIER:
				String type = parseVarType();
				String name = parseIdentifier();
				node.addParameterPair(new JackParameterListNode.ParaPair(type , name));
				while (jtk.getTkType() == JackTokenizer.TokenType.SYM_COMMA) {
					jtk.validateToken(JackTokenizer.TokenType.SYM_COMMA);
					JackParameterListNode.ParaPair pp = new JackParameterListNode.ParaPair();
					pp.setType(parseVarType());
					pp.setName(parseIdentifier());
					node.addParameterPair(pp);
				}
				break;
				
			default: break;
		}
		
		return node;
	}

	private JackSubroutineBodyNode parseSubroutineBody () 
	{
		JackSubroutineBodyNode node = new JackSubroutineBodyNode();
		
		jtk.validateToken(JackTokenizer.TokenType.SYM_LBRACE);
		node.setLocalVarDecs(parseLocalVarDecs());
		node.setStatements(parseStatements());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RBRACE);
		
		return node;
	}
	
	private LinkedList<JackLocalVarDecNode> parseLocalVarDecs ()
	{
		LinkedList<JackLocalVarDecNode> local_vardecs = new LinkedList<JackLocalVarDecNode>();
		
		while(jtk.getTkType() == JackTokenizer.TokenType.KW_VAR) {
			local_vardecs.add(parseLocalVarDec());
		}
		
		return local_vardecs;
	}

	private JackLocalVarDecNode parseLocalVarDec ()
	{
		JackLocalVarDecNode node = new JackLocalVarDecNode();
		
		jtk.validateToken(JackTokenizer.TokenType.KW_VAR);
		node.setVarType(parseVarType());
		node.setVarNames(parseIdentifiers());
		jtk.validateToken(JackTokenizer.TokenType.SYM_SEMICOLON);
		
		return node;
	}
	
	private LinkedList<String> parseIdentifiers ()
	{
		LinkedList<String> varnames = new LinkedList<String>();
		
		varnames.add(parseIdentifier());
		while (jtk.getTkType() == JackTokenizer.TokenType.SYM_COMMA) {
			jtk.validateToken(JackTokenizer.TokenType.SYM_COMMA);
			varnames.add(parseIdentifier());
		}
		
		return varnames;
	}

	private LinkedList<JackStatementNode> parseStatements ()
	{
		LinkedList<JackStatementNode> statements = new LinkedList<JackStatementNode>();
		
		while (true) {
			switch (jtk.getTkType()) {
				case KW_LET:		statements.add(parseLetStatement());		break;
				case KW_IF:			statements.add(parseIfStatement());			break;
				case KW_WHILE:		statements.add(parseWhileStatement());		break;
				case KW_DO:			statements.add(parseDoStatement());			break;
				case KW_RETURN:		statements.add(parseReturnStatement());		break;
					
				default:
					return statements;
			}
		}
	}

	private JackLetStatementNode parseLetStatement ()
	{
		JackLetStatementNode node = new JackLetStatementNode();
		
		jtk.validateToken(JackTokenizer.TokenType.KW_LET);
		node.setVarName(parseIdentifier());
		if (jtk.getTkType() == JackTokenizer.TokenType.SYM_LBRACKET) {
			jtk.validateToken(JackTokenizer.TokenType.SYM_LBRACKET);
			node.setIndexExpr(parseExpression());
			jtk.validateToken(JackTokenizer.TokenType.SYM_RBRACKET);
		}
		jtk.validateToken(JackTokenizer.TokenType.SYM_EQ);
		node.setAssignExpr(parseExpression());
		jtk.validateToken(JackTokenizer.TokenType.SYM_SEMICOLON);
		
		return node;
	}

	private JackIfStatementNode parseIfStatement ()
	{
		JackIfStatementNode node = new JackIfStatementNode();
		
		jtk.validateToken(JackTokenizer.TokenType.KW_IF);
		jtk.validateToken(JackTokenizer.TokenType.SYM_LPAREN);
		node.setCondition(parseExpression());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RPAREN);
		jtk.validateToken(JackTokenizer.TokenType.SYM_LBRACE);
		node.setTrueStatements(parseStatements());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RBRACE);
		if (jtk.getTkType() == JackTokenizer.TokenType.KW_ELSE) {
			jtk.validateToken(JackTokenizer.TokenType.KW_ELSE);
			jtk.validateToken(JackTokenizer.TokenType.SYM_LBRACE);
			node.setFalseStatements(parseStatements());
			jtk.validateToken(JackTokenizer.TokenType.SYM_RBRACE);
		}
		
		return node;
	}

	private JackWhileStatementNode parseWhileStatement ()
	{
		JackWhileStatementNode node = new JackWhileStatementNode();
		
		jtk.validateToken(JackTokenizer.TokenType.KW_WHILE);
		jtk.validateToken(JackTokenizer.TokenType.SYM_LPAREN);
		node.setCondition(parseExpression());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RPAREN);
		jtk.validateToken(JackTokenizer.TokenType.SYM_LBRACE);
		node.setBody(parseStatements());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RBRACE);
		
		return node;
	}

	private JackDoStatementNode parseDoStatement ()
	{
		JackDoStatementNode node = new JackDoStatementNode();
		
		jtk.validateToken(JackTokenizer.TokenType.KW_DO);
		String head_identifier = parseIdentifier();
		node.setSubroutineCall(parseSubroutineCall(head_identifier));
		jtk.validateToken(JackTokenizer.TokenType.SYM_SEMICOLON);
		
		return node;
	}

	private JackReturnStatementNode parseReturnStatement ()
	{
		JackReturnStatementNode node = new JackReturnStatementNode();
		jtk.validateToken(JackTokenizer.TokenType.KW_RETURN);
		
		switch (jtk.getTkType()) {
			case CONST_INTEGER: case CONST_STRING:
			case KW_TRUE: case KW_FALSE: case KW_NULL:
			case KW_THIS: case IDENTIFIER: case SYM_LPAREN:
			case SYM_MINUS: case SYM_NOT:
				node.setReturnExpr(parseExpression());
				break;
				
			default: break;
		}
		
		jtk.validateToken(JackTokenizer.TokenType.SYM_SEMICOLON);
		return node;
	}

	private JackExpressionNode parseExpression ()
	{
		JackExpressionNode left_hand_side = parseTerm();
		
		switch (jtk.getTkType()) {
			case SYM_PLUS:	return parseAddExpression(left_hand_side);
			case SYM_MINUS:	return parseSubExpression(left_hand_side);
			case SYM_MUL:	return parseMulExpression(left_hand_side);
			case SYM_DIV:	return parseDivExpression(left_hand_side);
			case SYM_AND:	return parseBitWiseAndExpression(left_hand_side);
			case SYM_OR:	return parseBitWiseOrExpression(left_hand_side);
			case SYM_LT:	return parseLessThanExpression(left_hand_side);
			case SYM_GT:	return parseGreaterExpression(left_hand_side);
			case SYM_EQ:	return parseEqualExpression(left_hand_side);

			default:
				return left_hand_side;
		}
	}
	
	private JackExpressionNode parseAddExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_PLUS);
		JackAddExpressionNode node = new JackAddExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseSubExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_MINUS);
		JackSubExpressionNode node = new JackSubExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseMulExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_MUL);
		JackMulExpressionNode node = new JackMulExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseDivExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_DIV);
		JackDivExpressionNode node = new JackDivExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseBitWiseAndExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_AND);
		JackBitWiseAndExpressionNode node = new JackBitWiseAndExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseBitWiseOrExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_OR);
		JackBitWiseOrExpressionNode node = new JackBitWiseOrExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseLessThanExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_LT);
		JackLessThanExpressionNode node = new JackLessThanExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseEqualExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_EQ);
		JackEqualExpressionNode node = new JackEqualExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}
	
	private JackExpressionNode parseGreaterExpression (JackExpressionNode left_hand_side) {
		jtk.validateToken(JackTokenizer.TokenType.SYM_GT);
		JackGreaterExpressionNode node = new JackGreaterExpressionNode();
		node.setLeftHandSide(left_hand_side);
		node.setRightHandSide(parseExpression());
		return node;
	}

	private JackExpressionNode parseTerm ()
	{	
		switch(jtk.getTkType()) {
			case CONST_INTEGER:	return parseConstInteger();
			case CONST_STRING:	return parseConstString();
			case IDENTIFIER:	return parseHeaderTerm();
			case SYM_LPAREN:	return parseNestedExpr();
				
			case KW_TRUE: case KW_FALSE:
			case KW_NULL: case KW_THIS:
				return parseKeywordConst();
				
			case SYM_MINUS: case SYM_NOT:
				return parseUnaryTerm();
				
			default:
				JackError.error(
						String.format("Require [%s | %s | %s | %s | %s | %s | %s | %s | %s | %s] , get [%s]" ,
								JackTokenizer.TokenType.CONST_INTEGER , JackTokenizer.TokenType.CONST_STRING ,
								JackTokenizer.TokenType.KW_TRUE , JackTokenizer.TokenType.KW_FALSE ,
								JackTokenizer.TokenType.KW_NULL , JackTokenizer.TokenType.KW_THIS ,
								JackTokenizer.TokenType.IDENTIFIER , JackTokenizer.TokenType.SYM_LPAREN ,
								JackTokenizer.TokenType.SYM_MINUS , JackTokenizer.TokenType.SYM_NOT ,
								jtk.getTkType()));
				return null;
		}
	}
	
	private JackExpressionNode parseHeaderTerm ()
	{
		String header = parseIdentifier();
		switch (jtk.getTkType()) {
			case SYM_LBRACKET:
				return parseArrayAccess(header);
				
			case SYM_LPAREN: case SYM_PERIOD:
				return parseSubroutineCall(header);
				
			default:
				JackIdentifierNode id = new JackIdentifierNode();
				id.setRep(header);
				return id;
		}
	}
	
	private JackExpressionNode parseNestedExpr ()
	{
		jtk.validateToken(JackTokenizer.TokenType.SYM_LPAREN);
		JackExpressionNode expr = parseExpression();
		jtk.validateToken(JackTokenizer.TokenType.SYM_RPAREN);
		return expr;
	}
	
	private JackExpressionNode parseUnaryTerm ()
	{
		switch(parseUnaryOp()) {
			case SYM_MINUS: {
				JackNegateExpressionNode node = new JackNegateExpressionNode();
				node.setTerm(parseTerm());
				return node;
			}
				
			case SYM_NOT: {
				JackLogicalNegateExpressionNode node = new JackLogicalNegateExpressionNode();
				node.setTerm(parseTerm());
				return node;
			}
			
			default:
				return null;
		}
	}
	
	private JackArrayAccessNode parseArrayAccess (String varname)
	{
		JackArrayAccessNode node = new JackArrayAccessNode();
		
		node.setVarName(varname);
		jtk.validateToken(JackTokenizer.TokenType.SYM_LBRACKET);
		node.setIndexExpr(parseExpression());
		jtk.validateToken(JackTokenizer.TokenType.SYM_RBRACKET);
		
		return node;
	}

	private JackSubroutineCallNode parseSubroutineCall (String header)
	{
		JackSubroutineCallNode node = new JackSubroutineCallNode();
		
		switch (jtk.getTkType()) {
			case SYM_LPAREN:
				node.setSubroutineName(header);
				jtk.validateToken(JackTokenizer.TokenType.SYM_LPAREN);
				node.setArguments(parseExpressionList());
				jtk.validateToken(JackTokenizer.TokenType.SYM_RPAREN);
				break;
				
			case SYM_PERIOD:
				node.setScopeIdentifier(header);
				jtk.validateToken(JackTokenizer.TokenType.SYM_PERIOD);
				node.setSubroutineName(parseIdentifier());
				jtk.validateToken(JackTokenizer.TokenType.SYM_LPAREN);
				node.setArguments(parseExpressionList());
				jtk.validateToken(JackTokenizer.TokenType.SYM_RPAREN);
				break;
				
			default:
				JackError.error(String.format("Require [%s | %s] , get [%s]" ,
						JackTokenizer.TokenType.SYM_LPAREN ,
						JackTokenizer.TokenType.SYM_PERIOD ,
						jtk.getTkType()));
				return null;
		}
		
		return node;
	}

	private LinkedList<JackExpressionNode> parseExpressionList ()
	{
		LinkedList<JackExpressionNode> expr_list = new LinkedList<JackExpressionNode>();
		
		switch(jtk.getTkType()) {
			case CONST_INTEGER: case CONST_STRING:
			case KW_TRUE: case KW_FALSE: case KW_NULL:
			case KW_THIS: case IDENTIFIER: case SYM_LPAREN:
			case SYM_MINUS: case SYM_NOT:
				expr_list.add(parseExpression());
				while (jtk.getTkType() == JackTokenizer.TokenType.SYM_COMMA) {
					jtk.getNextToken();
					expr_list.add(parseExpression());
				}
				break;
				
			default: break;
		}
		
		return expr_list;
	}

	private JackTokenizer.TokenType parseUnaryOp ()
	{
		switch (jtk.getTkType()) {
			case SYM_MINUS: case SYM_NOT:
				JackTokenizer.TokenType op = jtk.getTkType();
				jtk.getNextToken();
				return op;
				
			default:
				JackError.error(
						String.format("Require [%s | %s] , get [%s]" ,
								JackTokenizer.TokenType.SYM_MINUS ,
								JackTokenizer.TokenType.SYM_NOT ,
								jtk.getTkType()));
				return null;
		}
	}

	private JackKeywordConstNode parseKeywordConst ()
	{
		switch (jtk.getTkType()) {
			case KW_TRUE: case KW_FALSE:
			case KW_NULL: case KW_THIS:
				JackKeywordConstNode node = new JackKeywordConstNode();
				node.setKeyword(jtk.getTkType());
				jtk.getNextToken();
				return node;
				
			default:
				JackError.error(
						String.format("Require [%s | %s | %s | %s] , get [%s]" ,
								JackTokenizer.TokenType.KW_TRUE ,
								JackTokenizer.TokenType.KW_FALSE ,
								JackTokenizer.TokenType.KW_NULL ,
								JackTokenizer.TokenType.KW_THIS ,
								jtk.getTkType()));
				return null;
		}
	}
	
	private JackIntegerNode parseConstInteger ()
	{
		JackIntegerNode node = new JackIntegerNode();
		node.setIntval(Integer.parseInt(jtk.getTkRep()));
		jtk.validateToken(JackTokenizer.TokenType.CONST_INTEGER);
		return node;
	}
	
	private JackStringNode parseConstString ()
	{
		JackStringNode node = new JackStringNode();
		node.setStr(jtk.getTkRep());
		jtk.validateToken(JackTokenizer.TokenType.CONST_STRING);
		return node;
	}
	
	private String parseVarType ()
	{
		switch (jtk.getTkType()) {
			case KW_INT: case KW_CHAR:
			case KW_BOOL: case IDENTIFIER:
				String type = jtk.getTkRep();
				jtk.getNextToken();
				return type;
				
			default:
				JackError.error(String.format("Require [%s | %s | %s | %s] , get [%s]" ,
						JackTokenizer.TokenType.KW_INT , JackTokenizer.TokenType.KW_CHAR ,
						JackTokenizer.TokenType.KW_BOOL , JackTokenizer.TokenType.IDENTIFIER ,
						jtk.getTkType()));
				return null;
		}
	}
	
	private String parseIdentifier ()
	{
		String id = jtk.getTkRep();
		jtk.validateToken(JackTokenizer.TokenType.IDENTIFIER);
		return id;
	}
	
	private JackTokenizer.TokenType parseClassVarModifier ()
	{
		switch(jtk.getTkType()) {
			case KW_STATIC: case KW_FIELD:
				JackTokenizer.TokenType modifier = jtk.getTkType();
				jtk.getNextToken();
				return modifier;
				
			default:
				JackError.error(String.format("Require [%s | %s] , get [%s]" ,
						JackTokenizer.TokenType.KW_STATIC ,
						JackTokenizer.TokenType.KW_FIELD ,
						jtk.getTkType()));
				return null;
		}
	}
	
	private JackTokenizer.TokenType parseSubroutineType ()
	{
		switch(jtk.getTkType()) {
			case KW_CONSTRUCTOR: case KW_FUNC:
			case KW_METHOD:
				JackTokenizer.TokenType type = jtk.getTkType();
				jtk.getNextToken();
				return type;
				
			default:
				JackError.error(String.format("Require [%s | %s | %s] , get [%s]" ,
						JackTokenizer.TokenType.KW_CONSTRUCTOR ,
						JackTokenizer.TokenType.KW_FUNC ,
						JackTokenizer.TokenType.KW_METHOD ,
						jtk.getTkType()));
				return null;
		}
	}

}
