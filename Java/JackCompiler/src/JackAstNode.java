import java.util.*;

enum NodeType {
	COSNT_INTEGER , CONST_STRING ,
	IDENTIFIER , KEYWORD_CONST ,
	
	CLASS , CLASS_VARDEC , CLASS_SUBROUTINEDEC ,
	PARAMETERLIST , SUBROUTINE_BODY ,
	LOCAL_VARDEC ,
	LET_STATEMENT ,
	IF_STATEMENT , WHILE_STATEMENT ,
	DO_STATEMENT , RETURN_STATEMENT ,
	
	EXPRESSION ,
	ADD_EXPRESSION , SUB_EXPRESSION ,
	MUL_EXPRESSION , DIV_EXPRESSION ,
	BITWISE_AND_EXPRESSION , BITWISE_OR_EXPRESSION ,
	LESSTHAN_EXPRESSION , GREATER_EXPRESSION ,
	EQUAL_EXPRESSION , NEGATE_EXPRESSION ,
	LOGICAL_NEGATE_EXPRESSION ,
	
	ARRAY_ACCESS , UNARY_TERM ,
	SUBROUTINE_CALL
}

abstract class JackAstNode {
	
	private final NodeType type;
	
	JackAstNode (NodeType type) { this.type = type; }
	
	NodeType getNodeType () { return type; }
	
}

class JackClassNode extends JackAstNode {
	
	private String classname;
	private LinkedList<JackClassVarDecNode> vardecs;
	private LinkedList<JackSubroutineDecNode> subrdecs;
	
	JackClassNode () {
		super(NodeType.CLASS);
		classname = null;
		vardecs = null;
		subrdecs = null;
	}
	
	String getClassName () { return classname; }
	LinkedList<JackClassVarDecNode> getVarDecList () { return vardecs; }
	LinkedList<JackSubroutineDecNode> getSubrDecList () { return subrdecs; }
	
	void setName (String name) { classname = name; }
	void setVarDecs (LinkedList<JackClassVarDecNode> vardecs) { this.vardecs = vardecs; }
	void setSubroutineDecs (LinkedList<JackSubroutineDecNode> subrdecs) { this.subrdecs = subrdecs; }
	
}

class JackClassVarDecNode extends JackAstNode {
	
	private JackTokenizer.TokenType modifier;
	private String type;
	private LinkedList<String> varnames;
	
	JackClassVarDecNode () {
		super(NodeType.CLASS_VARDEC);
		modifier = null;
		type = null;
		varnames = null;
	}
	
	JackTokenizer.TokenType getModifier () { return modifier; }
	String getType () { return type; }
	LinkedList<String> getVarnames () { return varnames; }
	
	void setModifier (JackTokenizer.TokenType modifier) { this.modifier = modifier; }
	void setType (String type) { this.type = type; }
	void setVarnames (LinkedList<String> varnames) { this.varnames = varnames; }
	
}

class JackSubroutineDecNode extends JackAstNode {
	
	private JackTokenizer.TokenType subr_type;
	private String ret_type;
	private String subr_name;
	private JackParameterListNode paralist_node;
	private JackSubroutineBodyNode subroutinebody_node;
	
	JackSubroutineDecNode () {
		super(NodeType.CLASS_SUBROUTINEDEC);
		subr_type = null;
		ret_type = subr_name = null;
		paralist_node = null;
		subroutinebody_node = null;
	}
	
	JackTokenizer.TokenType getSubroutineType () { return subr_type; }
	String getReturnType () { return ret_type; }
	String getSubroutineName () { return subr_name; }
	JackParameterListNode getParameterlist () { return paralist_node; }
	JackSubroutineBodyNode getSubroutineBody () { return subroutinebody_node; }
	
	void setSubroutineType (JackTokenizer.TokenType type) { this.subr_type = type; }
	void setReturnType (String ret_type) { this.ret_type = ret_type; }
	void setSubroutineName (String name) { this.subr_name = name; }
	void setParameterList (JackParameterListNode node) { this.paralist_node = node; }
	void setSubroutineBody (JackSubroutineBodyNode node) { this.subroutinebody_node = node; }
	
}

class JackParameterListNode extends JackAstNode {
	
	static class ParaPair {
		
		private String type;
		private String name;
		
		ParaPair () {
			type = name = null;
		}
		
		ParaPair (String type , String name) {
			this.type = type;
			this.name = name;
		}
		
		String getType () { return type; }
		String getName () { return name; }
		
		void setType (String type) { this.type = type; }
		void setName (String name) { this.name = name; }
		
	}
	
	private LinkedList<ParaPair> paralist;
	
	JackParameterListNode () {
		super(NodeType.PARAMETERLIST);
		paralist = new LinkedList<ParaPair>();
	}
	
	LinkedList<ParaPair> getParameterList () { return paralist; }
	
	void addParameterPair (ParaPair p) { paralist.add(p); }
	
}

class JackSubroutineBodyNode extends JackAstNode {
	
	private LinkedList<JackLocalVarDecNode> vardec_list;
	private LinkedList<JackStatementNode> statement_list;
	
	JackSubroutineBodyNode () {
		super(NodeType.SUBROUTINE_BODY);
		vardec_list = null;
		statement_list = null;
	}
	
	LinkedList<JackLocalVarDecNode> getVarDecList () { return vardec_list; }
	LinkedList<JackStatementNode> getStatementList () { return statement_list; }
	
	void setLocalVarDecs (LinkedList<JackLocalVarDecNode> local_vardecs) { vardec_list = local_vardecs; }
	void setStatements (LinkedList<JackStatementNode> statements) { statement_list = statements; }
	
}

class JackLocalVarDecNode extends JackAstNode {
	
	private String var_type;
	private LinkedList<String> varnames;

	JackLocalVarDecNode () {
		super(NodeType.LOCAL_VARDEC);
		var_type = null;
		varnames = null;
	}
	
	String getVarType () { return var_type; }
	LinkedList<String> getVarnames () { return varnames; }
	
	void setVarType (String type) { this.var_type = type; }
	void setVarNames (LinkedList<String> varnames) { this.varnames = varnames; }
	
}


abstract class JackStatementNode extends JackAstNode {
	
	JackStatementNode (NodeType type) {
		super(type);
	}
	
}

class JackLetStatementNode extends JackStatementNode {
	
	private String varname;
	private JackExpressionNode index_expr;
	private JackExpressionNode assign_expr;
	
	JackLetStatementNode () {
		super(NodeType.LET_STATEMENT);
		varname = null;
		index_expr = assign_expr = null;
	}
	
	String getVarName () { return varname; }
	JackExpressionNode getIndexExpr () { return index_expr; }
	JackExpressionNode getAssignExpr () { return assign_expr; }
	
	void setVarName (String varname) { this.varname = varname; }
	void setIndexExpr (JackExpressionNode node) { index_expr = node; }
	void setAssignExpr (JackExpressionNode node) { assign_expr = node; }
	
}

class JackIfStatementNode extends JackStatementNode {
	
	private JackExpressionNode condition;
	private LinkedList<JackStatementNode> true_body;
	private LinkedList<JackStatementNode> false_body;
	
	JackIfStatementNode () {
		super(NodeType.IF_STATEMENT);
		condition = null;
		true_body = new LinkedList<JackStatementNode>();
		false_body = new LinkedList<JackStatementNode>();
	}
	
	JackExpressionNode getCondition () { return condition; }
	LinkedList<JackStatementNode> getTrueStatements () { return true_body; }
	LinkedList<JackStatementNode> getFalseStatements () { return false_body; }
	
	void setCondition (JackExpressionNode node) { condition = node; }
	void setTrueStatements (LinkedList<JackStatementNode> statements) { true_body = statements; }
	void setFalseStatements (LinkedList<JackStatementNode> statements) { false_body = statements; }
	
}

class JackWhileStatementNode extends JackStatementNode {
	
	private JackExpressionNode condition;
	private LinkedList<JackStatementNode> body;
	
	JackWhileStatementNode () {
		super(NodeType.WHILE_STATEMENT);
		condition = null;
		body = new LinkedList<JackStatementNode>();
	}
	
	JackExpressionNode getCondition () { return condition; }
	LinkedList<JackStatementNode> getBody () { return body; }
	
	void setCondition (JackExpressionNode node) { condition = node; }
	void setBody (LinkedList<JackStatementNode> node) { body = node; }
	
}

class JackDoStatementNode extends JackStatementNode {
	
	private JackSubroutineCallNode subr_call;
	
	JackDoStatementNode () {
		super(NodeType.DO_STATEMENT);
		subr_call = null;
	}
	
	JackSubroutineCallNode getSubrcall () { return subr_call; }
	
	void setSubroutineCall (JackSubroutineCallNode node) { subr_call = node; }
	
}

class JackReturnStatementNode extends JackStatementNode {
	
	private JackExpressionNode ret_expr;
	
	JackReturnStatementNode () {
		super(NodeType.RETURN_STATEMENT);
		ret_expr = null;
	}
	
	JackExpressionNode getReturnExpr () { return ret_expr; }
	
	void setReturnExpr (JackExpressionNode node) { ret_expr = node; }
	
}

abstract class JackExpressionNode extends JackAstNode {

	JackExpressionNode (NodeType type) {
		super(type);
	}
	
}

abstract class JackBinaryExpressionNode extends JackExpressionNode {
	
	private JackExpressionNode left_hand_side;
	private JackExpressionNode right_hand_side;
	
	JackBinaryExpressionNode (NodeType type) {
		super(type);
	}
	
	JackExpressionNode getLeftHandSide () { return left_hand_side; }
	JackExpressionNode getRightHandSide () { return right_hand_side; }
	
	void setLeftHandSide (JackExpressionNode expr) { left_hand_side = expr; }
	void setRightHandSide (JackExpressionNode expr) { right_hand_side = expr; }
	
}

abstract class JackArithmeticExpressionNode extends JackBinaryExpressionNode {

	JackArithmeticExpressionNode (NodeType type) {
		super(type);
	}
	
}

class JackAddExpressionNode extends JackArithmeticExpressionNode {
	
	JackAddExpressionNode () {
		super(NodeType.ADD_EXPRESSION);
	}
	
}

class JackSubExpressionNode extends JackArithmeticExpressionNode {
	
	JackSubExpressionNode () {
		super(NodeType.SUB_EXPRESSION);
	}
	
}

class JackMulExpressionNode extends JackArithmeticExpressionNode {
	
	JackMulExpressionNode () {
		super(NodeType.MUL_EXPRESSION);
	}
	
}

class JackDivExpressionNode extends JackArithmeticExpressionNode {
	
	JackDivExpressionNode () {
		super(NodeType.DIV_EXPRESSION);
	}
	
}

class JackBitWiseAndExpressionNode extends JackArithmeticExpressionNode {
	
	JackBitWiseAndExpressionNode () {
		super(NodeType.BITWISE_AND_EXPRESSION);
	}
	
}

class JackBitWiseOrExpressionNode extends JackArithmeticExpressionNode {
	
	JackBitWiseOrExpressionNode () {
		super(NodeType.BITWISE_OR_EXPRESSION);
	}
	
}

abstract class JackLogicalExpressionNode extends JackBinaryExpressionNode {
	
	JackLogicalExpressionNode (NodeType type) {
		super(type);
	}
	
}

class JackLessThanExpressionNode extends JackLogicalExpressionNode {
	
	JackLessThanExpressionNode () {
		super(NodeType.LESSTHAN_EXPRESSION);
	}
	
}

class JackGreaterExpressionNode extends JackLogicalExpressionNode {
	
	JackGreaterExpressionNode () {
		super(NodeType.GREATER_EXPRESSION);
	}
	
}

class JackEqualExpressionNode extends JackLogicalExpressionNode {
	
	JackEqualExpressionNode () {
		super(NodeType.EQUAL_EXPRESSION);
	}
	
}

class JackIntegerNode extends JackExpressionNode {
	
	private int int_val;
	
	JackIntegerNode () {
		super(NodeType.COSNT_INTEGER);
	}
	
	int getIntval () { return int_val; }
	
	void setIntval (int val) { int_val = val; }
	
}

class JackStringNode extends JackExpressionNode {
	
	private String str;
	
	JackStringNode () {
		super(NodeType.CONST_STRING);
		str = null;
	}
	
	String getStr () { return str; }
	
	void setStr (String str) { this.str = str; }
	
}

class JackKeywordConstNode extends JackExpressionNode {
	
	private JackTokenizer.TokenType keyword;
	
	JackKeywordConstNode () {
		super(NodeType.KEYWORD_CONST);
		keyword = null;
	}
	
	JackTokenizer.TokenType getKeyword () { return keyword; }
	
	void setKeyword (JackTokenizer.TokenType keyword) { this.keyword = keyword; }
	
}

class JackIdentifierNode extends JackExpressionNode {
	
	private String rep;
	
	JackIdentifierNode () {
		super(NodeType.IDENTIFIER);
		rep = null;
	}
	
	String getRep () { return rep; }
	
	void setRep (String rep) { this.rep = rep; }
	
}

class JackArrayAccessNode extends JackExpressionNode {
	
	private String varname;
	private JackExpressionNode index_expr;
	
	JackArrayAccessNode () {
		super(NodeType.ARRAY_ACCESS);
		varname = null;
		index_expr = null;
	}
	
	String getVarName () { return varname; }
	JackExpressionNode getIndexExpr () { return index_expr; }
	
	void setVarName (String name) { varname = name; }
	void setIndexExpr (JackExpressionNode node) { index_expr = node; }
	
}

abstract class JackUnaryExpressionNode extends JackExpressionNode {
	
	private JackExpressionNode term;
	
	JackUnaryExpressionNode (NodeType type) {
		super(type);
	}
	
	JackExpressionNode getTerm () { return term; }
	
	void setTerm (JackExpressionNode term) { this.term = term; }
	
}

class JackNegateExpressionNode extends JackUnaryExpressionNode {
	
	JackNegateExpressionNode () {
		super(NodeType.NEGATE_EXPRESSION);
	}
	
}

class JackLogicalNegateExpressionNode extends JackUnaryExpressionNode {
	
	JackLogicalNegateExpressionNode () {
		super(NodeType.LOGICAL_NEGATE_EXPRESSION);
	}
	
}

class JackSubroutineCallNode extends JackExpressionNode {
	
	private String scope_identifier;
	private String var_name;
	private String subr_name;
	private LinkedList<JackExpressionNode> arguments;
	
	JackSubroutineCallNode () {
		super(NodeType.SUBROUTINE_CALL);
		scope_identifier = var_name = subr_name = null;
		arguments = new LinkedList<JackExpressionNode>();
	}
	
	String getScopeIdentifier () { return scope_identifier; }
	String getVarname () { return var_name; }
	String getSubroutineName () { return subr_name; }
	LinkedList<JackExpressionNode> getArguments () { return arguments; }
	
	void setScopeIdentifier (String si) { scope_identifier = si; }
	void setVarname (String vn) { var_name = vn; }
	void setSubroutineName (String name) { subr_name = name; }
	void setArguments (LinkedList<JackExpressionNode> arguments) { this.arguments = arguments; }
	
}