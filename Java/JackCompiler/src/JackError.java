class JackError {
	
	private static final String errmsg_prefix = "[ERROR] -> ";

	static void error (String msg) {
		System.err.println(errmsg_prefix + msg);
		System.exit(0);
	}

}
