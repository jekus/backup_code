import java.util.HashMap;
import java.util.LinkedList;

class JackInfoCollector {
	
	static class ClassRecord {
		
		static class VarRecord {
			
			private final String class_name;
			private final String var_name;
			private final String type;
			private final int index;
			
			VarRecord (String cn , String vn , String tp , int idx) {
				class_name = cn;
				var_name = vn;
				type = tp;
				index = idx;
			}
			
			String getClassName () { return class_name; }
			String getVarName () { return var_name; }
			String getType () { return type; }
			int getIndex () { return index; }
			
		}
		
		static class ClassStaticField extends VarRecord {
			
			ClassStaticField (String class_name , String var_name , String type , int idx) {
				super(class_name , var_name , type , idx);
			}
			
		}
		
		static class ClassField extends VarRecord {
			
			ClassField (String class_name , String var_name , String type , int idx) {
				super(class_name , var_name , type , idx);
			}
			
		}
		
		static class SubroutineRecord {
			
			static class SubroutineVar extends VarRecord {
				
				private final String subroutine_name;
				
				SubroutineVar (
						String class_name , String subr_name , String var_name ,
						String type , int idx)
				{
					super(class_name , var_name , type , idx);
					subroutine_name = subr_name;
				}
				
				String getSubroutineName () { return subroutine_name; }
				
			}
			
			static class SubroutineParameter extends SubroutineVar {

				SubroutineParameter (
						String class_name , String subr_name , String var_name , 
						String type , int idx)
				{
					super(class_name , subr_name , var_name , type , idx);
				}

			}
			
			static class SubroutineLocalVar extends SubroutineVar {

				SubroutineLocalVar (
						String class_name , String subr_name , String var_name , 
						String type , int idx)
				{
					super(class_name , subr_name , var_name , type , idx);
				}

			}
			
			
			private final String class_name;
			private final String subroutine_name;
			private final String ret_type;
			private final String[] params_type;
			
			private int params_n;
			private int locals_n;
			
			private final HashMap<String , SubroutineParameter> subr_params;
			private final HashMap<String , SubroutineLocalVar> subr_locals;
			
			SubroutineRecord (
					String cn , String sn , String ret_tp ,
					LinkedList<JackParameterListNode.ParaPair> paralist)
			{
				class_name = cn;
				subroutine_name = sn;
				ret_type = ret_tp;
				params_type = new String[paralist.size()];
				
				int idx = 0;
				for (JackParameterListNode.ParaPair p : paralist) {
					params_type[idx] = p.getType();
					++idx;
				}
				
				params_n = locals_n = 0;
				subr_params = new HashMap<String , SubroutineParameter>();
				subr_locals = new HashMap<String , SubroutineLocalVar>();
			}
			
			String getClassName () { return class_name; }
			String getSubroutineName () { return subroutine_name; }
			String getReturnType () { return ret_type; }
			String[] getParametersType () { return params_type; }
			SubroutineParameter getParameter (String var_name) { return subr_params.get(var_name); }
			SubroutineLocalVar getLocalVar (String var_name) { return subr_locals.get(var_name); }
			int getParameterNum () { return params_n; }
			int getLocalVarNum () { return locals_n; }
			
			private void addParameter (String var_name , String type) {
				if (subr_params.containsKey(var_name)) {
					JackError.error(String.format("Class '%s' subroutine '%s' parameter '%s' is defined" ,
							class_name , subroutine_name , var_name));
				}
				else {
					subr_params.put(var_name ,
							new SubroutineParameter(class_name , subroutine_name , var_name , type , params_n));
					++params_n;
				}
			}
			
			private void addLocalVar (String var_name , String type) {
				if (subr_params.containsKey(var_name)) {
					JackError.error(String.format("Class '%s' subroutine '%s' local variable '%s' is defined" ,
							class_name , subroutine_name , var_name));
				}
				else {
					subr_locals.put(var_name ,
							new SubroutineLocalVar(class_name , subroutine_name , var_name , type , locals_n));
					++locals_n;
				}
			}
			
		}
		
		static class ConstructorRecord extends SubroutineRecord {
			
			ConstructorRecord (
					String class_name , String cons_name , String return_type ,
					LinkedList<JackParameterListNode.ParaPair> paralist)
			{
				super(class_name , cons_name , return_type , paralist);
			}
			
		}
		
		static class MethodRecord extends SubroutineRecord {
			
			MethodRecord (
					String class_name , String cons_name , String return_type ,
					LinkedList<JackParameterListNode.ParaPair> paralist)
			{
				super(class_name , cons_name , return_type , paralist);
			}
			
		}
		
		static class FunctionRecord extends SubroutineRecord {
			
			FunctionRecord (
					String class_name , String cons_name , String return_type ,
					LinkedList<JackParameterListNode.ParaPair> paralist)
			{
				super(class_name , cons_name , return_type , paralist);
			}
			
		}
		
		
		private final String class_name;
		
		private int field_n;
		private int static_field_n;
		private final HashMap<String , ClassField> class_fields;
		private final HashMap<String , ClassStaticField> class_staticfields;
		
		private final HashMap<String , ConstructorRecord> constructors;
		private final HashMap<String , MethodRecord> methods;
		private final HashMap<String , FunctionRecord> functions;
		
		ClassRecord (String cn) {
			class_name = cn;
			
			field_n = static_field_n = 0;
			class_fields = new HashMap<String , ClassField>();
			class_staticfields = new HashMap<String , ClassStaticField>();
			
			constructors = new HashMap<String , ConstructorRecord>();
			methods = new HashMap<String , MethodRecord>();
			functions = new HashMap<String , FunctionRecord>();
		}
		
		int getFieldNum () { return field_n; }
		int getStaticFieldNum () { return static_field_n; }
		
		VarRecord getClassVar (String var_name) {
			ClassField cf = getClassField(var_name);
			if (cf == null) {
				return getClassStaticField(var_name);
			}
			else {
				return cf;
			}
		}
		
		ClassField getClassField (String var_name) {
			return class_fields.get(var_name);
		}
		
		ClassStaticField getClassStaticField (String var_name) {
			return class_staticfields.get(var_name);
		}
		
		private void addClassVar (String var_name , JackTokenizer.TokenType scope_modifier , String type)
		{
			if (scope_modifier == JackTokenizer.TokenType.KW_STATIC) {
				addClassStaticField(var_name , type , static_field_n);
				++static_field_n;
			}
			else {
				addClassField(var_name , type , field_n);
				++field_n;
			}
		}
		
		private void addClassStaticField (String var_name , String type , int idx)
		{
			if (class_staticfields.containsKey(var_name)) {
				JackError.error(String.format("Class '%s' static field '%s' is defined" ,
						class_name , var_name));
			}
			else {
				class_staticfields.put(var_name ,
						new ClassStaticField(class_name , var_name , type , idx));
			}
		}
		
		private void addClassField (String var_name , String type , int idx)
		{
			if (class_fields.containsKey(var_name)) {
				JackError.error(String.format("Class '%s' field '%s' is defined" ,
						class_name , var_name));
			}
			else {
				class_fields.put(var_name ,
						new ClassField(class_name , var_name , type , idx));
			}
		}
		
		SubroutineRecord getSubroutine (String subroutine_name) {
			ConstructorRecord consr = getConstructor(subroutine_name);
			if (consr == null) {
				MethodRecord mr = getMethod(subroutine_name);
				if (mr == null) {
					return getFunction(subroutine_name);
				}
				else {
					return mr;
				}
			}
			else {
				return consr;
			}
		}
		
		private void addSubroutine (
				String subroutine_name , JackTokenizer.TokenType subr_type , String return_type ,
				LinkedList<JackParameterListNode.ParaPair> paralist)
		{
			if (subr_type == JackTokenizer.TokenType.KW_CONSTRUCTOR) {
				addConstructor(subroutine_name , return_type , paralist);
			}
			else if (subr_type == JackTokenizer.TokenType.KW_METHOD) {
				addMethod(subroutine_name , return_type , paralist);
			}
			else if (subr_type == JackTokenizer.TokenType.KW_FUNC) {
				addFunction(subroutine_name , return_type , paralist);
			}
			else {
				JackError.error(
						String.format("Class '%s' subroutine '%s' has unknown type [%s]" ,
								class_name , subroutine_name , subr_type));
			}
		}
		
		private ConstructorRecord getConstructor (String cons_name) {
			return constructors.get(cons_name);
		}
		
		private MethodRecord getMethod (String method_name) {
			return methods.get(method_name);
		}
		
		private FunctionRecord getFunction (String func_name) {
			return functions.get(func_name);
		}
		
		private void addConstructor (
				String cons_name , String return_type ,
				LinkedList<JackParameterListNode.ParaPair> paralist)
		{
			if (constructors.containsKey(cons_name)) {
				JackError.error(
						String.format("Class '%s' constructor '%s' is defined" ,
								class_name , cons_name));
			}
			else {
				constructors.put(cons_name ,
						new ConstructorRecord(class_name , cons_name , return_type , paralist));
			}
		}
		
		private void addMethod (
				String method_name , String return_type ,
				LinkedList<JackParameterListNode.ParaPair> paralist)
		{
			if (methods.containsKey(method_name)) {
				JackError.error(
						String.format("Class '%s' method '%s' is defined" ,
								class_name , method_name));
			}
			else {
				methods.put(method_name ,
						new MethodRecord(class_name , method_name , return_type , paralist));
			}
		}
		
		private void addFunction (
				String func_name , String return_type ,
				LinkedList<JackParameterListNode.ParaPair> paralist)
		{
			if (functions.containsKey(func_name)) {
				JackError.error(
						String.format("Class '%s' function '%s' is defined" ,
								class_name , func_name));
			}
			else {
				functions.put(func_name ,
						new FunctionRecord(class_name , func_name , return_type , paralist));
			}
		}
		
		SubroutineRecord.SubroutineVar getSubroutineVar (String subroutine_name , String var_name) {
			SubroutineRecord.SubroutineVar sv = getSubroutineLocalVar(subroutine_name , var_name);
			if (sv == null) {
				return getSubroutineParameter(subroutine_name , var_name);
			}
			else {
				return sv;
			}
		}
		
		SubroutineRecord.SubroutineParameter getSubroutineParameter (String subroutine_name , String var_name) {
			SubroutineRecord sr = getSubroutine(subroutine_name);
			if (sr == null) {
				JackError.error(String.format("Class '%s' does not have a subroutine called '%s'" ,
						class_name , subroutine_name));
				return null;
			}
			else {
				return sr.getParameter(var_name);
			}
		}
		
		SubroutineRecord.SubroutineLocalVar getSubroutineLocalVar (String subroutine_name , String var_name) {
			SubroutineRecord sr = getSubroutine(subroutine_name);
			if (sr == null) {
				JackError.error(String.format("Class '%s' does not have a subroutine called '%s'" ,
						class_name , subroutine_name));
				return null;
			}
			else {
				return sr.getLocalVar(var_name);
			}
		}
		
		private void addSubroutineParameter (String subroutine_name , String var_name , String type) {
			SubroutineRecord sr = getSubroutine(subroutine_name);
			if (sr == null) {
				JackError.error(String.format("Class '%s' does not have a subroutine called '%s'" ,
						class_name , subroutine_name));
			}
			else {
				sr.addParameter(var_name , type);
			}
		}
		
		private void addSubroutineLocalVar (String subroutine_name , String var_name , String type) {
			SubroutineRecord sr = getSubroutine(subroutine_name);
			if (sr == null) {
				JackError.error(String.format("Class '%s' does not have a subroutine called '%s'" ,
						class_name , subroutine_name));
			}
			else {
				sr.addLocalVar(var_name , type);
			}
		}
		
	}
	
	
	static class ProgramInfo {
		
		private final HashMap<String , ClassRecord> class_set;
		
		ProgramInfo () {
			class_set = new HashMap<String , ClassRecord>();
		}
		
		ClassRecord getClass (String class_name) {
			return class_set.get(class_name);
		}
		
		ClassRecord CheckClassExistence (String class_name)
		{
			ClassRecord cr = getClass(class_name);
			if (cr == null) {
				JackError.error(String.format("Unknown Class '%s'" , class_name));
				return null;
			}
			else {
				return cr;
			}
		}
		
		void CheckClassAbsence (String class_name)
		{
			if (getClass(class_name) != null) {
				JackError.error(String.format("Class '%s' is defined" , class_name));
			}
		}
		
		private void addClass (String class_name)
		{
			CheckClassAbsence(class_name);
			class_set.put(class_name , new ClassRecord(class_name));
		}
		
		private void addClassVar (
				String class_name , String var_name ,
				JackTokenizer.TokenType scope_modifier , String type)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			cr.addClassVar(var_name , scope_modifier , type);
		}
		
		private void addSubroutineParameter (
				String class_name , String subroutine_name ,
				String var_name , String type)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			cr.addSubroutineParameter(subroutine_name , var_name , type);
		}
		
		private void addSubroutineLocalVar (
				String class_name , String subroutine_name ,
				String var_name , String type)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			cr.addSubroutineLocalVar(subroutine_name , var_name , type);
		}
		
		private void addSubroutine (
				String class_name , String subroutine_name ,
				JackTokenizer.TokenType type ,
				String return_type , LinkedList<JackParameterListNode.ParaPair> paralist)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			cr.addSubroutine(subroutine_name , type , return_type , paralist);
		}
		
		ClassRecord.VarRecord getClassVar (String class_name , String var_name)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			return cr.getClassVar(var_name);
		}
		
		ClassRecord.SubroutineRecord.SubroutineVar getSubroutineVar (
				String class_name , String subroutine_name , String var_name)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			return cr.getSubroutineVar(subroutine_name , var_name);
		}
		
		ClassRecord.SubroutineRecord.SubroutineLocalVar getSubroutineLocalVar (
				String class_name , String subroutine_name , String var_name)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			return cr.getSubroutineLocalVar(subroutine_name , var_name);
		}
		
		ClassRecord.SubroutineRecord.SubroutineParameter getSubroutineParameter (
				String class_name , String subroutine_name , String var_name)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			return cr.getSubroutineParameter(subroutine_name , var_name);
		}
		
		ClassRecord.SubroutineRecord getSubroutine (String class_name , String subroutine_name)
		{
			ClassRecord cr = CheckClassExistence(class_name);
			return cr.getSubroutine(subroutine_name);
		}

	}
	
	
	private final ProgramInfo program_info;
	
	private static final LinkedList<JackParameterListNode.ParaPair> math_init_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> math_abs_params;
	static {
		math_abs_params = new LinkedList<JackParameterListNode.ParaPair>();
		math_abs_params.add(new JackParameterListNode.ParaPair("int" , "x"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> math_multiply_params;
	static {
		math_multiply_params = new LinkedList<JackParameterListNode.ParaPair>();
		math_multiply_params.add(new JackParameterListNode.ParaPair("int" , "x"));
		math_multiply_params.add(new JackParameterListNode.ParaPair("int" , "y"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> math_divide_params;
	static {
		math_divide_params = new LinkedList<JackParameterListNode.ParaPair>();
		math_divide_params.add(new JackParameterListNode.ParaPair("int" , "x"));
		math_divide_params.add(new JackParameterListNode.ParaPair("int" , "y"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> math_min_params;
	static {
		math_min_params = new LinkedList<JackParameterListNode.ParaPair>();
		math_min_params.add(new JackParameterListNode.ParaPair("int" , "x"));
		math_min_params.add(new JackParameterListNode.ParaPair("int" , "y"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> math_max_params;
	static {
		math_max_params = new LinkedList<JackParameterListNode.ParaPair>();
		math_max_params.add(new JackParameterListNode.ParaPair("int" , "x"));
		math_max_params.add(new JackParameterListNode.ParaPair("int" , "y"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> math_sqrt_params;
	static {
		math_sqrt_params = new LinkedList<JackParameterListNode.ParaPair>();
		math_sqrt_params.add(new JackParameterListNode.ParaPair("int" , "x"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_new_params;
	static {
		string_new_params = new LinkedList<JackParameterListNode.ParaPair>();
		string_new_params.add(new JackParameterListNode.ParaPair("int" , "maxLength"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_dispose_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_length_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_charAt_params;
	static {
		string_charAt_params = new LinkedList<JackParameterListNode.ParaPair>();
		string_charAt_params.add(new JackParameterListNode.ParaPair("int" , "j"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_setCharAt_params;
	static {
		string_setCharAt_params = new LinkedList<JackParameterListNode.ParaPair>();
		string_setCharAt_params.add(new JackParameterListNode.ParaPair("int" , "j"));
		string_setCharAt_params.add(new JackParameterListNode.ParaPair("char" , "c"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_appendChar_params;
	static {
		string_appendChar_params = new LinkedList<JackParameterListNode.ParaPair>();
		string_appendChar_params.add(new JackParameterListNode.ParaPair("char" , "c"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_eraseLastChar_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_intValue_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_setInt_params;
	static {
		string_setInt_params = new LinkedList<JackParameterListNode.ParaPair>();
		string_setInt_params.add(new JackParameterListNode.ParaPair("int" , "j"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_backSpace_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_doubleQuote_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> string_newLine_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> array_new_params;
	static {
		array_new_params = new LinkedList<JackParameterListNode.ParaPair>();
		array_new_params.add(new JackParameterListNode.ParaPair("int" , "size"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> array_dispose_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> output_init_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> output_moveCursor_params;
	static {
		output_moveCursor_params = new LinkedList<JackParameterListNode.ParaPair>();
		output_moveCursor_params.add(new JackParameterListNode.ParaPair("int" , "i"));
		output_moveCursor_params.add(new JackParameterListNode.ParaPair("int" , "j"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> output_printChar_params;
	static {
		output_printChar_params = new LinkedList<JackParameterListNode.ParaPair>();
		output_printChar_params.add(new JackParameterListNode.ParaPair("char" , "c"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> output_printString_params;
	static {
		output_printString_params = new LinkedList<JackParameterListNode.ParaPair>();
		output_printString_params.add(new JackParameterListNode.ParaPair("String" , "s"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> output_printInt_params;
	static {
		output_printInt_params = new LinkedList<JackParameterListNode.ParaPair>();
		output_printInt_params.add(new JackParameterListNode.ParaPair("int" , "i"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> output_println_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> output_backSpace_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> screen_init_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> screen_clearScreen_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> screen_setColor_params;
	static {
		screen_setColor_params = new LinkedList<JackParameterListNode.ParaPair>();
		screen_setColor_params.add(new JackParameterListNode.ParaPair("boolean" , "b"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> screen_drawPixel_params;
	static {
		screen_drawPixel_params = new LinkedList<JackParameterListNode.ParaPair>();
		screen_drawPixel_params.add(new JackParameterListNode.ParaPair("int" , "x"));
		screen_drawPixel_params.add(new JackParameterListNode.ParaPair("int" , "y"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> screen_drawLine_params;
	static {
		screen_drawLine_params = new LinkedList<JackParameterListNode.ParaPair>();
		screen_drawLine_params.add(new JackParameterListNode.ParaPair("int" , "x1"));
		screen_drawLine_params.add(new JackParameterListNode.ParaPair("int" , "y1"));
		screen_drawLine_params.add(new JackParameterListNode.ParaPair("int" , "x2"));
		screen_drawLine_params.add(new JackParameterListNode.ParaPair("int" , "y2"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> screen_drawRectangle_params;
	static {
		screen_drawRectangle_params = new LinkedList<JackParameterListNode.ParaPair>();
		screen_drawRectangle_params.add(new JackParameterListNode.ParaPair("int" , "x1"));
		screen_drawRectangle_params.add(new JackParameterListNode.ParaPair("int" , "y1"));
		screen_drawRectangle_params.add(new JackParameterListNode.ParaPair("int" , "x2"));
		screen_drawRectangle_params.add(new JackParameterListNode.ParaPair("int" , "y2"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> screen_drawCircle_params;
	static {
		screen_drawCircle_params = new LinkedList<JackParameterListNode.ParaPair>();
		screen_drawCircle_params.add(new JackParameterListNode.ParaPair("int" , "x"));
		screen_drawCircle_params.add(new JackParameterListNode.ParaPair("int" , "y"));
		screen_drawCircle_params.add(new JackParameterListNode.ParaPair("int" , "r"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> keyboard_init_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> keyboard_keyPressed_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> keyboard_readChar_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> keyboard_readLine_params;
	static {
		keyboard_readLine_params = new LinkedList<JackParameterListNode.ParaPair>();
		keyboard_readLine_params.add(new JackParameterListNode.ParaPair("String" , "message"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> keyboard_readInt_params;
	static {
		keyboard_readInt_params = new LinkedList<JackParameterListNode.ParaPair>();
		keyboard_readInt_params.add(new JackParameterListNode.ParaPair("String" , "message"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> memory_init_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> memory_peek_params;
	static {
		memory_peek_params = new LinkedList<JackParameterListNode.ParaPair>();
		memory_peek_params.add(new JackParameterListNode.ParaPair("int" , "address"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> memory_poke_params;
	static {
		memory_poke_params = new LinkedList<JackParameterListNode.ParaPair>();
		memory_poke_params.add(new JackParameterListNode.ParaPair("int" , "address"));
		memory_poke_params.add(new JackParameterListNode.ParaPair("int" , "value"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> memory_alloc_params;
	static {
		memory_alloc_params = new LinkedList<JackParameterListNode.ParaPair>();
		memory_alloc_params.add(new JackParameterListNode.ParaPair("int" , "size"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> memory_deAlloc_params;
	static {
		memory_deAlloc_params = new LinkedList<JackParameterListNode.ParaPair>();
		memory_deAlloc_params.add(new JackParameterListNode.ParaPair("Array" , "o"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> sys_init_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> sys_halt_params =
			new LinkedList<JackParameterListNode.ParaPair>();
	
	private static final LinkedList<JackParameterListNode.ParaPair> sys_error_params;
	static {
		sys_error_params = new LinkedList<JackParameterListNode.ParaPair>();
		sys_error_params.add(new JackParameterListNode.ParaPair("int" , "errorCode"));
	}
	
	private static final LinkedList<JackParameterListNode.ParaPair> sys_wait_params;
	static {
		sys_wait_params = new LinkedList<JackParameterListNode.ParaPair>();
		sys_wait_params.add(new JackParameterListNode.ParaPair("int" , "duration"));
	}

	
	
	JackInfoCollector () {
		program_info = new ProgramInfo();
		
		program_info.addClass("Math");
		program_info.addSubroutine("Math" , "init" , JackTokenizer.TokenType.KW_FUNC , "void" , math_init_params);
		program_info.addSubroutine("Math" , "abs" , JackTokenizer.TokenType.KW_FUNC , "int" , math_abs_params);
		program_info.addSubroutine("Math" , "multiply" , JackTokenizer.TokenType.KW_FUNC , "int" , math_multiply_params);
		program_info.addSubroutine("Math" , "divide" , JackTokenizer.TokenType.KW_FUNC , "int" , math_divide_params);
		program_info.addSubroutine("Math" , "min" , JackTokenizer.TokenType.KW_FUNC , "int" , math_min_params);
		program_info.addSubroutine("Math" , "max" , JackTokenizer.TokenType.KW_FUNC , "int" , math_max_params);
		program_info.addSubroutine("Math" , "sqrt" , JackTokenizer.TokenType.KW_FUNC , "int" , math_sqrt_params);
		
		program_info.addClass("String");
		program_info.addSubroutine("String" , "new" , JackTokenizer.TokenType.KW_CONSTRUCTOR , "String" , string_new_params);
		program_info.addSubroutine("String" , "dispose" , JackTokenizer.TokenType.KW_METHOD , "void" , string_dispose_params);
		program_info.addSubroutine("String" , "length" , JackTokenizer.TokenType.KW_METHOD , "int" , string_length_params);
		program_info.addSubroutine("String" , "charAt" , JackTokenizer.TokenType.KW_METHOD , "char" , string_charAt_params);
		program_info.addSubroutine("String" , "setCharAt" , JackTokenizer.TokenType.KW_METHOD , "void" , string_setCharAt_params);
		program_info.addSubroutine("String" , "appendChar" , JackTokenizer.TokenType.KW_METHOD , "String" , string_appendChar_params);
		program_info.addSubroutine("String" , "eraseLastChar" , JackTokenizer.TokenType.KW_METHOD , "void" , string_eraseLastChar_params);
		program_info.addSubroutine("String" , "intValue" , JackTokenizer.TokenType.KW_METHOD , "int" , string_intValue_params);
		program_info.addSubroutine("String" , "setInt" , JackTokenizer.TokenType.KW_METHOD , "void" , string_setInt_params);
		program_info.addSubroutine("String" , "backSpace" , JackTokenizer.TokenType.KW_METHOD , "char" , string_backSpace_params);
		program_info.addSubroutine("String" , "doubleQuote" , JackTokenizer.TokenType.KW_METHOD , "char" , string_doubleQuote_params);
		program_info.addSubroutine("String" , "newLine" , JackTokenizer.TokenType.KW_METHOD , "char" , string_newLine_params);
		
		program_info.addClass("Array");
		program_info.addSubroutine("Array" , "new" , JackTokenizer.TokenType.KW_FUNC , "Array" , array_new_params);
		program_info.addSubroutine("Array" , "dispose" , JackTokenizer.TokenType.KW_METHOD , "void" , array_dispose_params);
		
		program_info.addClass("Output");
		program_info.addSubroutine("Output" , "init" , JackTokenizer.TokenType.KW_FUNC , "void" , output_init_params);
		program_info.addSubroutine("Output" , "moveCursor" , JackTokenizer.TokenType.KW_FUNC , "void" , output_moveCursor_params);
		program_info.addSubroutine("Output" , "printChar" , JackTokenizer.TokenType.KW_FUNC , "void" , output_printChar_params);
		program_info.addSubroutine("Output" , "printString" , JackTokenizer.TokenType.KW_FUNC , "void" , output_printString_params);
		program_info.addSubroutine("Output" , "printInt" , JackTokenizer.TokenType.KW_FUNC , "void" , output_printInt_params);
		program_info.addSubroutine("Output" , "println" , JackTokenizer.TokenType.KW_FUNC , "void" , output_println_params);
		program_info.addSubroutine("Output" , "backSpace" , JackTokenizer.TokenType.KW_FUNC , "void" , output_backSpace_params);
		
		program_info.addClass("Screen");
		program_info.addSubroutine("Screen" , "init" , JackTokenizer.TokenType.KW_FUNC , "void" , screen_init_params);
		program_info.addSubroutine("Screen" , "clearScreen" , JackTokenizer.TokenType.KW_FUNC , "void" , screen_clearScreen_params);
		program_info.addSubroutine("Screen" , "setColor" , JackTokenizer.TokenType.KW_FUNC , "void" , screen_setColor_params);
		program_info.addSubroutine("Screen" , "drawPixel" , JackTokenizer.TokenType.KW_FUNC , "void" , screen_drawPixel_params);
		program_info.addSubroutine("Screen" , "drawLine" , JackTokenizer.TokenType.KW_FUNC , "void" , screen_drawLine_params);
		program_info.addSubroutine("Screen" , "drawRectangle" , JackTokenizer.TokenType.KW_FUNC , "void" , screen_drawRectangle_params);
		program_info.addSubroutine("Screen" , "drawCircle" , JackTokenizer.TokenType.KW_FUNC , "void" , screen_drawCircle_params);
		
		program_info.addClass("Keyboard");
		program_info.addSubroutine("Keyboard" , "init" , JackTokenizer.TokenType.KW_FUNC , "void" , keyboard_init_params);
		program_info.addSubroutine("Keyboard" , "keyPressed" , JackTokenizer.TokenType.KW_FUNC , "char" , keyboard_keyPressed_params);
		program_info.addSubroutine("Keyboard" , "readChar" , JackTokenizer.TokenType.KW_FUNC , "char" , keyboard_readChar_params);
		program_info.addSubroutine("Keyboard" , "readLine" , JackTokenizer.TokenType.KW_FUNC , "String" , keyboard_readLine_params);
		program_info.addSubroutine("Keyboard" , "readInt" , JackTokenizer.TokenType.KW_FUNC , "int" , keyboard_readInt_params);
		
		program_info.addClass("Memory");
		program_info.addSubroutine("Memory" , "init" , JackTokenizer.TokenType.KW_FUNC , "void" , memory_init_params);
		program_info.addSubroutine("Memory" , "peek" , JackTokenizer.TokenType.KW_FUNC , "int" , memory_peek_params);
		program_info.addSubroutine("Memory" , "poke" , JackTokenizer.TokenType.KW_FUNC , "void" , memory_poke_params);
		program_info.addSubroutine("Memory" , "alloc" , JackTokenizer.TokenType.KW_FUNC , "Array" , memory_alloc_params);
		program_info.addSubroutine("Memory" , "deAlloc" , JackTokenizer.TokenType.KW_FUNC , "void" , memory_deAlloc_params);
		
		program_info.addClass("Sys");
		program_info.addSubroutine("Sys" , "init" , JackTokenizer.TokenType.KW_FUNC , "void" , sys_init_params);
		program_info.addSubroutine("Sys" , "halt" , JackTokenizer.TokenType.KW_FUNC , "void" , sys_halt_params);
		program_info.addSubroutine("Sys" , "error" , JackTokenizer.TokenType.KW_FUNC , "void" , sys_error_params);
		program_info.addSubroutine("Sys" , "wait" , JackTokenizer.TokenType.KW_FUNC , "void" , sys_wait_params);
	}
	
	ProgramInfo getProgramInfo () { return program_info; }
	
	private void collectClassVarDec (String class_name , LinkedList<JackClassVarDecNode> vardecs)
	{
		for (JackClassVarDecNode node : vardecs) {
			for (String var_name : node.getVarnames()) {
				program_info.addClassVar(class_name , var_name , node.getModifier() , node.getType());
			}
		}
	}
	
	private void collectSubroutineParameter (
			String class_name , String subroutine_name ,
			LinkedList<JackParameterListNode.ParaPair> paralist)
	{
		for (JackParameterListNode.ParaPair p : paralist) {
			program_info.addSubroutineParameter(class_name , subroutine_name , p.getName() , p.getType());
		}
	}
	
	private void collectSubroutineLocalVar (
			String class_name , String subroutine_name ,
			LinkedList<JackLocalVarDecNode> vardecs)
	{
		for (JackLocalVarDecNode node : vardecs) {
			for (String varname : node.getVarnames()) {
				program_info.addSubroutineLocalVar(class_name , subroutine_name , varname , node.getVarType());
			}
		}
	}
	
	private void collectSubroutineCall (
			String class_name , String subroutine_name ,
			JackSubroutineCallNode sc_expr)
	{
		String sc_id = sc_expr.getScopeIdentifier();
		if (sc_id == null) {
			sc_expr.setScopeIdentifier(class_name);
		}
		else {
			ClassRecord.SubroutineRecord.SubroutineVar sv =
					program_info.getSubroutineVar(class_name , subroutine_name , sc_id);
			if (sv != null) {
				sc_expr.setVarname(sc_id);
				sc_expr.setScopeIdentifier(sv.getType());
			}
			else {
				ClassRecord.VarRecord cvr = program_info.getClassVar(class_name , sc_id);
				if (cvr != null) {
					sc_expr.setVarname(sc_id);
					sc_expr.setScopeIdentifier(cvr.getType());
				}
			}
		}
		
		for (JackExpressionNode expr : sc_expr.getArguments()) {
			collectExpression(class_name , subroutine_name , expr);
		}
	}
	
	private void collectExpression (
			String class_name , String subroutine_name ,
			JackExpressionNode expr)
	{
		if (expr instanceof JackSubroutineCallNode) {
			collectSubroutineCall(class_name , subroutine_name , (JackSubroutineCallNode) expr);
		}
	}
	
	private void collectLetStatement (
			String class_name , String subroutine_name ,
			JackLetStatementNode let_st)
	{
		collectExpression(class_name , subroutine_name , let_st.getIndexExpr());
		collectExpression(class_name , subroutine_name , let_st.getAssignExpr());
	}
	
	private void collectIfStatement (
			String class_name , String subroutine_name ,
			JackIfStatementNode if_st)
	{
		collectExpression(class_name , subroutine_name , if_st.getCondition());
		collectStatement(class_name , subroutine_name , if_st.getTrueStatements());
		collectStatement(class_name , subroutine_name , if_st.getFalseStatements());
	}
	
	private void collectWhileStatement (
			String class_name , String subroutine_name ,
			JackWhileStatementNode while_st)
	{
		collectExpression(class_name , subroutine_name , while_st.getCondition());
		collectStatement(class_name , subroutine_name , while_st.getBody());
	}
	
	private void collectDoStatement (
			String class_name , String subroutine_name ,
			JackDoStatementNode do_st)
	{
		collectSubroutineCall(class_name , subroutine_name , do_st.getSubrcall());
	}
	
	private void collectReturnStatement (
			String class_name , String subroutine_name ,
			JackReturnStatementNode rt_st)
	{	
		collectExpression(class_name , subroutine_name , rt_st.getReturnExpr());
	}
	
	private void collectStatement (
			String class_name , String subroutine_name ,
			LinkedList<JackStatementNode> statements)
	{
		for (JackStatementNode st : statements) {
			if (st instanceof JackLetStatementNode) {
				collectLetStatement(class_name , subroutine_name , (JackLetStatementNode) st);
			}
			else if (st instanceof JackIfStatementNode) {
				collectIfStatement(class_name , subroutine_name , (JackIfStatementNode) st);
			}
			else if (st instanceof JackWhileStatementNode) {
				collectWhileStatement(class_name , subroutine_name , (JackWhileStatementNode) st);
			}
			else if (st instanceof JackDoStatementNode) {
				collectDoStatement(class_name , subroutine_name , (JackDoStatementNode) st);
			}
			else if (st instanceof JackReturnStatementNode) {
				collectReturnStatement(class_name , subroutine_name , (JackReturnStatementNode) st);
			}
		}
	}
	
	private void collectSubroutineBody (
			String class_name , String subroutine_name ,
			JackSubroutineBodyNode subr_body)
	{
		collectSubroutineLocalVar(class_name , subroutine_name , subr_body.getVarDecList());
		collectStatement(class_name , subroutine_name , subr_body.getStatementList());
	}
	
	private void collectSubroutineDec (String class_name , LinkedList<JackSubroutineDecNode> subrdecs)
	{
		for (JackSubroutineDecNode node : subrdecs) {
			String subr_name = node.getSubroutineName();
			LinkedList<JackParameterListNode.ParaPair> paralist = node.getParameterlist().getParameterList();
			program_info.addSubroutine(class_name , subr_name , node.getSubroutineType() , node.getReturnType() , paralist);
			collectSubroutineParameter(class_name , subr_name , paralist);
			collectSubroutineBody(class_name , subr_name , node.getSubroutineBody());
		}
	}
	
	private void collectClass (JackClassNode root)
	{
		String class_name = root.getClassName();
		program_info.addClass(class_name);
		collectClassVarDec(class_name , root.getVarDecList());
		collectSubroutineDec(class_name , root.getSubrDecList());
	}

	void collectInfo (JackAstNode root) {
		collectClass((JackClassNode) root);
	}
	
}
