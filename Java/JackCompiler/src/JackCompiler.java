import java.io.*;
import java.util.LinkedList;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JackCompiler {
	
	private static JackParser parseFile (Path path) throws IOException
	{
		String infile = path.toString();
		return new JackParser(infile , new JackTokenizer(infile));
	}

	private static LinkedList<JackParser> parseDir (Path path) throws IOException
	{
		LinkedList<JackParser> parsers = new LinkedList<JackParser>();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
			for (Path p : stream) {
				if (Files.isRegularFile(p) && p.getFileName().toString().endsWith(".jack")) {
					parsers.add(parseFile(p));
				}
			}
		}
		
		return parsers;
	}
	
	private static JackInfoCollector collectInfo (LinkedList<JackParser> parsers)
	{
		JackInfoCollector jic = new JackInfoCollector();
		for (JackParser pser : parsers) {
			jic.collectInfo(pser.getAst());
		}
		return jic;
	}
	
	private static void geneCode (
			LinkedList<JackParser> parsers ,
			JackInfoCollector.ProgramInfo program_info) throws IOException
	{
		for (JackParser pser : parsers) {
			String infile = pser.getSourceFile();
			String infile_prefix = infile.substring(0 , infile.lastIndexOf('.'));
			String outfile = infile_prefix + ".vm";
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(outfile))) {
				JackCodeGenerator.geneCode(pser.getAst() , program_info , bw);
			}
		}
	}

	public static void main (String[] args) throws IOException
	{
		if (args.length != 1) {
			System.out.println("Require one argument");
			System.exit(0);
		}
		else {
			Path path = Paths.get(args[0]);
			if (Files.isDirectory(path)) {
				LinkedList<JackParser> parsers = parseDir(path);
				JackInfoCollector jic = collectInfo(parsers);
				JackInfoCollector.ProgramInfo program_info = jic.getProgramInfo();
				JackProgramValidator.validate(parsers , program_info);
				geneCode(parsers , program_info);
			}
			else {
				LinkedList<JackParser> parsers = new LinkedList<JackParser>();
				parsers.add(parseFile(path));
				JackInfoCollector jic = collectInfo(parsers);
				JackInfoCollector.ProgramInfo program_info = jic.getProgramInfo();
				JackProgramValidator.validate(parsers , program_info);
				geneCode(parsers , program_info);
			}
		}
	}

}
