import java.io.*;
import java.util.*;

class Coder {

	private static HashMap<String , String> InstrMap = new HashMap<String , String>(64);
	private static HashMap<String , String> DestMap = new HashMap<String , String>(15);
	private static HashMap<String , String> JmpMap = new HashMap<String , String>(15);

	static {
		InstrMap.put("0" , "0101010");
		InstrMap.put("1" , "0111111");
		InstrMap.put("-1" , "0111010");
		InstrMap.put("D" , "0001100");
		InstrMap.put("A" , "0110000");
		InstrMap.put("!D" , "0001101");
		InstrMap.put("!A" , "0110001");
		InstrMap.put("-D" , "0001111");
		InstrMap.put("-A" , "0110011");
		InstrMap.put("D+1" , "0011111");
		InstrMap.put("A+1" , "0110111");
		InstrMap.put("D-1" , "0001110");
		InstrMap.put("A-1" , "0110010");
		InstrMap.put("D+A" , "0000010");
		InstrMap.put("D-A" , "0010011");
		InstrMap.put("A-D" , "0000111");
		InstrMap.put("D&A" , "0000000");
		InstrMap.put("D|A" , "0010101");

		InstrMap.put("M" , "1110000");
		InstrMap.put("!M" , "1110001");
		InstrMap.put("-M" , "1110011");
		InstrMap.put("M+1" , "1110111");
		InstrMap.put("M-1" , "1110010");
		InstrMap.put("D+M" , "1000010");
		InstrMap.put("D-M" , "1010011");
		InstrMap.put("M-D" , "1000111");
		InstrMap.put("D&M" , "1000000");
		InstrMap.put("D|M" , "1010101");
	}

	static {
		DestMap.put(null , "000");
		DestMap.put("M" , "001");
		DestMap.put("D" , "010");
		DestMap.put("MD" , "011");
		DestMap.put("A" , "100");
		DestMap.put("AM" , "101");
		DestMap.put("AD" , "110");
		DestMap.put("AMD" , "111");
	}

	static {
		JmpMap.put(null , "000");
		JmpMap.put("JGT" , "001");
		JmpMap.put("JEQ" , "010");
		JmpMap.put("JGE" , "011");
		JmpMap.put("JLT" , "100");
		JmpMap.put("JNE" , "101");
		JmpMap.put("JLE" , "110");
		JmpMap.put("JMP" , "111");
	}

	void geneCode (String outFile , Parser pser) throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(outFile))) {
			for (Instr inst : pser.instrs) {
				if (inst.getType() == Instr.InstrType.A_Instr) {
					String addr = Integer.toBinaryString((pser.symtb.get(inst.getSymbol())));
					for (int i = 16 - addr.length(); i > 0; --i) {
						addr = "0" + addr;
					}
					bw.write(addr);
				}
				else {
					String dest = inst.getDest();
					dest = (dest == null) ? "000" : DestMap.get(dest);

					String jmp = inst.getJmp();
					jmp = (jmp == null) ? "000" : JmpMap.get(jmp);

					bw.write("111" + InstrMap.get(inst.getComp()) + dest + jmp);
				}
				bw.newLine();
			}
		}
	}

}
