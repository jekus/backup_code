import java.io.*;
import java.util.*;

class Parser {

	private int instr_n;
	private int next_var_addr;
	private BufferedReader rd;

	LinkedList<Instr> instrs;
	HashMap<String , Integer> symtb;

	Parser (String infile) {
		try {
			instr_n = 0;
			next_var_addr = 16;
			rd = new BufferedReader(new FileReader(infile));
			instrs = new LinkedList<Instr>();
			symtb = new HashMap<String , Integer>(64);

			symtb.put("SP" , 0);
			symtb.put("LCL" , 1);
			symtb.put("ARG" , 2);
			symtb.put("THIS" , 3);
			symtb.put("THAT" , 4);
			symtb.put("SCREEN" , 16384);
			symtb.put("KBD " , 24576);

			for (int i = 0; i < 16; ++i) {
				symtb.put("R" + i , i);
			}
		}
		catch (FileNotFoundException e) {
			System.out.printf("File '%s' doesn't exist%n" , infile);
			System.exit(0);
		}
	}

	private static class SymException extends Exception {

		private String _sym;

		SymException (String sym) {
			_sym = sym;
		}

		String getSym () {
			return _sym;
		}

	}

	private static class InvalidSymException extends SymException {
		InvalidSymException (String sym) {
			super(sym);
		}
	}

	private static class RedefinedLabelException extends SymException {
		RedefinedLabelException (String sym) {
			super(sym);
		}
	}

	private static class UnknownTokenException extends SymException {
		UnknownTokenException (String sym) {
			super(sym);
		}
	}

	private static boolean validSym (char[] sym) {
		if (sym.length < 1 || Character.isDigit(sym[0])) {
			return false;
		}
		else {
			for (char c : sym) {
				if (!(Character.isLetterOrDigit(c) || "_.$:".indexOf(c) != -1)) {
					return false;
				}
			}

			return true;
		}
	}

	private Instr consAInstr (String sym) {
		Instr it = new Instr();
		it.setSymbol(sym);
		it.setType(Instr.InstrType.A_Instr);

		if (validSym(sym.toCharArray())) {
			if (!symtb.containsKey(sym)) {
				symtb.put(sym , next_var_addr++);
			}
		}
		else {
			try {
				symtb.putIfAbsent(sym , Integer.parseInt(sym));
			}
			catch (NumberFormatException e) {
				System.out.printf("Invalid A-Instruction - '@$s'%n" , sym);
				System.exit(0);
			}
		}

		return it;
	}

	private Instr consCInstr (String inst) {
		Instr it = new Instr();
		it.setType(Instr.InstrType.C_Instr);

		int eq_idx = inst.indexOf('=') , semi_idx = inst.indexOf(';');
		if (eq_idx != -1 && semi_idx != -1) {
			it.setDest(inst.substring(0 , eq_idx));
			it.setComp(inst.substring(eq_idx + 1 , semi_idx));
			it.setJmp(inst.substring(semi_idx + 1));
		}
		else if (eq_idx != -1) {
			it.setDest(inst.substring(0 , eq_idx));
			it.setComp(inst.substring(eq_idx + 1));
		}
		else if (semi_idx != -1) {
			it.setJmp(inst.substring(semi_idx + 1));
			it.setComp(inst.substring(0 , semi_idx));
		}
		else {
			it.setComp(inst);
		}

		return it;
	}

	private Instr consInstr (String s) {
		if (s.charAt(0) == '@') {
			return consAInstr(s.substring(1));
		}
		else {
			return consCInstr(s);
		}
	}

	private static String label2symbol (String s) throws InvalidSymException {
		String sym = s.substring(s.indexOf('(') + 1 , s.lastIndexOf(')'));
		if (validSym(sym.toCharArray())) {
			return sym;
		}
		else {
			throw new Parser.InvalidSymException(sym);
		}
	}

	private static LinkedList<String> preProcess (Object[] lines) throws UnknownTokenException {
		LinkedList<String> new_lines = new LinkedList<String>();
		for (Object obj : lines) {
			String line = ((String) obj).trim();
			if (!line.startsWith("//") && !line.isEmpty()) {
				int inline_cmtidx = line.indexOf('/');
				if (inline_cmtidx == -1) {
					new_lines.addLast(line);
				}
				else {
					if (line.indexOf('/' , inline_cmtidx + 1) == -1) {
						throw new UnknownTokenException("/");
					}
					else {
						new_lines.addLast(line.substring(0 , inline_cmtidx).trim());
					}
				}
			}
		}

		return new_lines;
	}

	private void recordLabel (String lb) {
		try {
			String sym = label2symbol(lb);
			if (symtb.containsKey(sym)) {
				throw new RedefinedLabelException(sym);
			}
			else {
				symtb.put(sym , instr_n);
			}
		}
		catch (InvalidSymException e) {
			System.out.printf("Invalid symbol - '%s'%n" , e.getSym());
			System.exit(0);
		}
		catch (RedefinedLabelException e) {
			System.out.printf("Redefined label - '%s'%n" , e.getSym());
			System.exit(0);
		}
	}

	private void collectLabel (LinkedList<String> lines) {
		for (String line : lines) {
			if (line.charAt(0) == '(') {
				recordLabel(line);
			}
			else {
				++instr_n;
			}
		}
	}

	private void decompInstr (LinkedList<String> lines) {
		for (String line : lines) {
			if (line.charAt(0) != '(') {
				instrs.addLast(consInstr(line));
			}
		}
	}

	void parse () throws IOException {
		try {
			LinkedList<String> lines = preProcess(rd.lines().toArray());
			collectLabel(lines);
			decompInstr(lines);
		}
		catch (UnknownTokenException e) {
			System.out.printf("Unknown token - '%s'%n" , e.getSym());
			System.exit(0);
		}
	}

}
