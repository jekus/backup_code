import java.io.*;

public class Hackas {

	public static void main (String[] args) throws IOException {
		if (args.length != 1) {
			System.out.println("Require one input file");
			System.exit(0);
		}
		else {
			Parser pser = new Parser(args[0]);
			Coder cder = new Coder();
			pser.parse();
			cder.geneCode(args[0].substring(0 , args[0].lastIndexOf('.') + 1) + "hack" , pser);
		}
	}

}
