
class Instr {

	static enum InstrType {
		A_Instr , C_Instr
	}

	private String sym;
	private String dest;
	private String comp;
	private String jmp;

	private InstrType t;

	Instr () {
		t = null;
		sym = dest = comp = jmp = null;
	}

	void setSymbol (String in_sym) { sym = in_sym; }
	void setDest (String in_dest) { dest = in_dest; }
	void setComp (String in_comp) { comp = in_comp; }
	void setJmp (String in_jmp) { jmp = in_jmp; }
	void setType (InstrType in_t) { t = in_t; }

	String getSymbol () { return sym; }
	String getDest () { return dest; }
	String getComp () { return comp; }
	String getJmp () { return jmp; }
	InstrType getType () { return t; }

}
