function allwords ()
	local line = io.read()		-- current line
	local pos = 1				-- current position in the line

	return function ()			-- iterator function
		while line do
			local s , e = string.find(line , "%w+" , pos)

			if s then								-- found a word?
				pos = e + 1
				return string.sub(line , s , e)		-- return the word
			else
				line = io.read()		-- try next line
				pos = 1
			end
		end
	end
end

function prefix (w1 , w2)
	return w1 .. ' ' .. w2
end

local statetab = {}
function insert (index , value)
	local list = statetab[index]

	if not list then
		statetab[index] = {value}
	else
		table.insert(list , value)
	end
end


local N = 2
local MAXGEN = 10000
local NOWORD = '\n'

-- Build Table
local w1 , w2 = NOWORD , NOWORD
for w in allwords() do
	insert(prefix(w1 , w2) , w)
	w1 , w2 = w2 , w
end
insert(prefix(w1 , w2) , NOWORD)

-- Generate Text
io.write('\n')
w1 , w2 = NOWORD , NOWORD
for i = 1 , MAXGEN do
	local list = statetab[prefix(w1 , w2)]

	-- Choose a random item from list
	local r = math.random(#list)
	local nextword = list[r]
	if nextword == NOWORD then return end
	io.write(nextword , ' ')
	w1 , w2 = w2 , nextword
end
io.write('\n')
