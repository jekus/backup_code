Stack = {}

function Stack:new ()
	local new_st = {}
	setmetatable(new_st , self)
	self.__index = self
	return new_st
end

function Stack:push (item)
	table.insert(self , item)
end

function Stack:pop ()
	return table.remove(self)
end

function Stack:isempty ()
	return #self == 0
end
