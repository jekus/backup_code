function fromto (n , m , step)
	local step = step or 1

	return
		function (m , n)
			local rst = n + step

			if rst <= m then
				return rst
			end
		end , m , n-step
end
