local foo

do
	local _ENV = _ENV
	function foo () print(X) end
end

X = 13		-- X is declared in '_ENV'
_ENV = nil
foo()		-- foo is not a free name.
X = 0
