Graph = {}


function Graph.new ()
	return {}
end


--[[
	'adj_list' is a table contains
	the name of all adjacent of the NODE.
--]]
function Graph.add_node (graph , node_name , adj_list)
	Graph.add_adj(graph , node_name , adj_list)
end


function Graph.add_adj (graph , node_name , adj_list)
	if type(node_name) ~= 'string' then
		error('node_name must be a string')
	end

	local adjlist_t = type(adj_list)
	if not string.find('table nil' , adjlist_t) then
		error('adj_list must be NIL or a table')
	end

	--[[
		If the NODE doesn't exist , 
		create one.

		If 'adj_list' is NIL , do nothing.
	--]]
	if not graph[node_name] then
		graph[node_name] = {name = node_name , adj = {}}
	end
	if not adj_list then return true end

	for _ , adj in pairs(adj_list) do
		graph[node_name].adj[adj] = true
	end
	return true
end


function Graph.del_node (graph , node_name)
	graph[node_name] = nil
	return true
end


local function
do_func (func , node_ct)
	local func_t = type(func)

	if func_t == 'nil' then return end
	if func_t ~= 'function' then
		error('func is not a function' , 2)
	end

	func(node_ct)
end

local visit = {}
local function
go_dfs (graph , st_node , ed_node , func)
	if st_node == ed_node then return true end

	local rst = false
	for adj , _ in pairs(graph[st_node].adj) do
		if not visit[adj] then
			visit[adj] = true
			do_func(func , graph[adj])
			rst = go_dfs(graph , adj , ed_node)
			if rst then return true end
		end
	end

	return false
end

--[[
	Search a node in the GRAPH.

	FUNC is optional.
	If provided , the current NODE(a table) will
	be passed as argument.
--]]
function Graph.dfs (graph , ed_node , func)
	visit = {}
	local rst = false

	for cur_node , node_ct in pairs(graph) do
		if not visit[cur_node] then
			visit[cur_node] = true
			do_func(func , node_ct)
			rst = go_dfs(graph , cur_node , ed_node , func)
			if rst then return true end
		end
	end

	return false
end
