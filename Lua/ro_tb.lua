--[[
	The implement of
	readonly table.
--]]


-- Each table has its own metatable
function readonly_1 (t)
	local mt = {}
	local proxy = {}

	mt.__index = t
	mt.__newindex = function ()
		error("attempt to assign to a readonly table" , 2)
	end

	return setmetatable(proxy , mt)
end


-- All the table share the same metatable
local _t = {}
local mt = {
	__index = function (proxy , k)
		return _t[proxy][k]
	end ,
	__newindex = function ()
		error("attempt to assign to a readonly table" , 2)
	end
}
function readonly_2 (t)
	local proxy = {}
	_t[proxy] = t
	return setmetatable(proxy , mt)
end
