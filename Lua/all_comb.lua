--[[
	Print all combination of
	the elements in an array.

	Assume that all elements are
	different from each other.
--]]


function all_comb (ar , n)
	n = n or #ar

	if n <= 1 then
		print(table.unpack(ar))
	else
		for fir = 1 , n do
			ar[fir] , ar[n] =
				ar[n] , ar[fir]

			all_comb(ar , n - 1)
		
			ar[fir] , ar[n] =
				ar[n] , ar[fir]
		end
	end
end
