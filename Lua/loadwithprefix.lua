--[[
	'load' can be called with
	a reader function.

	A reader function can return
	the chunk in parts.

	'load' calls the reader function
	successively until it returns NIL ,
	which signals the chunk's end.
--]]


function loadwithprefix (prefix , code_src)
	local idx = 1
	local arg_tb = {prefix .. ' ' , code_src}
	local reader = function ()
		local rst = arg_tb[idx]
		idx = idx + 1
		return rst
	end

	return load(reader)
end
