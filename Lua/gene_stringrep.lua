--[[
	Generate a function that
	can repeat a string N times.
--]]


function gene_stringrep (n)
	if n < 0 then
		error("N must greater than 0")
	end

	local rst_code = '\z
		local str = ... \z
		local err_msg = "string expected" \z

		assert(str , err_msg) \z
		if type(str) ~= "string" then \z
			error(err_msg) \z
		end \z

		local rst = "" \z
	'

	while n > 1 do
		if n % 2 ~= 0 then
			rst_code =
				rst_code ..
				'rst = rst .. str '
		end

		rst_code =
			rst_code ..
			'str = str .. str '
		n = n // 2
	end

	rst_code =
		rst_code ..
		'return rst .. str'

	return load(rst_code)
end
