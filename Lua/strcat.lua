function strcat (...)
	local str = ""
	for k , v in pairs({...}) do
		str = str .. v
	end

	return str
end
