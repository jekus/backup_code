function getfield (f)
	local t = _G

	for w , d in string.gmatch(f , '([%w_]+)(%.?)') do
		if d == '.' or d == '' then
			t = t[w]
			if not t then return nil end
		else
			error('not a valid field name')
		end
	end

	return t
end

function setfield (f , v)
	local t = _G					-- start with the table of globals

	for w , d in string.gmatch(f , '([%w_]+)(%.?)') do
		if d == '.' then			-- not last name?
			t[w] = t[w] or {}		-- create table if absent
			t = t[w]				-- get the table
		else						-- last name
			t[w] = v
		end
	end
end

setfield("t.x.y" , 10)
print(getfield("t.x.y"))
print(getfield("a.x.y"))
print(getfield("a?fxv"))
