-- concatenate a table of strings(support nest tables)

function rconcat_1 (l , sp)
	if type(l) ~= 'table' then return l end
	local res = {}
	for i = 1 , #l do
		res[i] = rconcat_1(l[i] , sp) .. (sp and i ~= #l and sp or '')
	end

	return table.concat(res)
end


function rconcat_2 (l , sp)
	if type(l) ~= 'table' then return l end
	local res = ''
	for i = 1 , #l do
		res = res .. rconcat_2(l[i] , sp) .. (sp and i ~= #l and sp or '')
	end

	return res
end
