List = {}

function List.new ()
	return {first = 0 , last = -1}
end


function List.isempty (list)
	return list.first > list.last
end


function List.pushfirst (list , value)
	local first = list.first - 1
	
	list.first = first
	list[first] = value
end


function List.pushlast (list , value)
	local last = list.last + 1

	list.last = last
	list[last] = value
end


function List.popfirst (list)
	local first = list.first

	if List.isempty(list) then
		return nil
	end

	local value = list[first]
	list[first] = nil
	list.first = first + 1
	return value
end


function List.poplast (list)
	local last = list.last

	if List.isempty(list) then
		return nil
	end

	local value = list[last]
	list[last] = nil
	list.last = last - 1
	return value
end
