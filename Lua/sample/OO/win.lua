widget = {height = 100 , width = 100 , position = {30 , 30}}
win = {}

function widget:new (height , width , position)
	local new_wt = {}
	setmetatable(new_wt , self)
	self.__index = self
	new_wt.height = height
	new_wt.width = width
	new_wt.position = position
	return new_wt
end


function win:new (height , width , position)
	local new_win = widget:new(height , width , position)
	-- some other codes
	return new_win
end
