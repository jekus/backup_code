Stack = {}
function Stack:new ()
	local stack = {}
	setmetatable(stack , self)
	self.__index = self
	stack.idx = 0
	return stack
end

function Stack:push (v)
	self.idx = self.idx + 1
	self[self.idx] = v
end

function Stack:pop ()
	if not self:isempty() then
		local v = self[self.idx]
		self[self.idx] = nil
		self.idx = self.idx - 1
		return v
	end
end

function Stack:isempty ()
	return not self[self.idx]
end

function Stack:top ()
	return self[self.idx]
end

StackQueue = {}
function StackQueue:new ()
	local stackq = Stack:new()
	stackq.insertbottom = self.insertbottom
	stackq.bt = 0
	return stackq
end

function StackQueue:insertbottom (v)
	local bt = self.bt
	self[bt] = v
	self.bt = bt - 1
end
