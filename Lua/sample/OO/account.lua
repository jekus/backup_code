local ACC = {}
local fc = {}

function newAccount (init_bal)
	local proxy = {}
	setmetatable(proxy , {__index = fc})
	ACC[proxy] = {balance = init_bal}
	return proxy
end


local function withdraw (self , n)
	local bal = ACC[self].balance
	ACC[self].balance = bal - n
end


local function deposit (self , n)
	local bal = ACC[self].balance
	ACC[self].balance = bal + n
end


local function getbal (self)
	return ACC[self].balance
end

fc.withdraw = withdraw
fc.getbal = getbal
fc.deposit = deposit
setmetatable(ACC , {__mode = "k"})
