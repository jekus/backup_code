Stack = {}
StackQueue = {}

function Stack.new ()
	local stack = {}
	local s_op = {
		push = function (v)
			table.insert(stack , v)
		end ,
		pop = function ()
			local v = stack[#stack]
			stack[#stack] = nil
			return v
		end ,
		isempty = function ()
			return #stack == 0
		end ,
		top = function ()
			return stack[#stack]
		end
	}
	
	return s_op
end
