-- the implement of binary tree

btree = {}

function btree.new (val_type)
	if val_type ~= "number" and
		val_type ~= "string" then
			error("error type")
	end

	local bt = {}
	local mt = {__index = btree ,
				__val_t = val_type}
	mt.__metatable = mt
	
	return setmetatable(bt , mt)
end


local function checktype (tree , val)
	local mt = getmetatable(tree)

	if type(val) ~= mt.__val_t then
		error("error type for this TREE")
	end
end
	

local function nextdir (node , val)
	if node.val > val then
		return 0	-- to right
	elseif node.val < val then
		return 1	-- to left
	else
		return -1
	end
end


local function where (node , val)
	if not node.val then
		return node , 2
	end

	while true do
		local dir = nextdir(node , val)
	
		if dir == 0 then
			if node.right then
				node = node.right
			else
				return node , 0
			end
		elseif dir == 1 then
			if node.left then
				node = node.left
			else
				return node , 1
			end
		else
			return node , -1
		end
	end
end


local function add (node , val)
	local node , dir = where(node , val)

	if dir == 0 then
		node.right = {val = val , last = node}
	elseif dir == 1 then
		node.left = {val = val , last = node}
	elseif dir == 2 then
		node.val = val
	else
		return
	end

	return true
end


function btree:add (val)
	if not val then
		return
	else
		checktype(self , val)
	end

	return add(self , val)
end


local function remove (node , val)
	local node , dir = where(node , val)
	
	if dir ~= -1 then return end
	
	if node.left and (not node.right) then
		node.val = node.left.val
		node.right = node.left.right
		node.left = node.left.left
	elseif node.right and (not node.left) then
		node.val = node.right.val
		node.left = node.right.left
		node.right = node.right.right
	elseif node.left and node.right then
		local right = node.right

		node.val = node.left.val
		node.right = node.left.right
		node.left = node.left.left

		local tmp = node
		while tmp.right do
			tmp = tmp.right
		end
		tmp.right = right
	else
		if node.last then
			if node.last.left == node then
				node.last.left = nil
			else
				node.last.right = nil
			end
		end
	end
end


function btree:remove (val)
	if not val then
		return
	else
		checktype(self , val)
	end

	return remove(self , val)
end


local function go (node , func)
	if not node then return end

	go(node.left , func)
	if func then
		func(node.val)
	end
	go(node.right , func)
end


function btree:traverse (func)
	if type(func) ~= "function" then
		error("function expected , got " .. type(func))
	end

	go(self , func)
end
