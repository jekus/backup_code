-- print the last N lines of a file

local input = assert(io.open(arg[1]))
local last_n = arg[2] and tonumber(arg[2]) or 10
local l_count = 0

input:seek("end" , -1)
while input:seek() > 1 do
	if input:read(1) == '\n' then
		l_count = l_count + 1
	end
	input:seek("cur" , -2)

	if l_count == last_n then
		while input:read(1) ~= '\n' do
			if not input:seek("cur" , -2) then
				input:seek("cur" , -input:seek())
			end
		end

		io.write(input:read("a"))
		goto JUMP
	end
end

input:seek("cur" , -input:seek())
io.write(input:read("a"))

::JUMP::
input:close()
return true
