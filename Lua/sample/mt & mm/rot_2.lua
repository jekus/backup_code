-- read-only table version 2
-- multiple tables can be read-only at the same time

function read_only (t)
	local proxy = {}
	local mt = {
		__index = t ,
		__newindex = function ()
			error("Attempt to update a read-only table")
		end
	}

	setmetatable(proxy , mt)
	return proxy
end
