-- read-only table version 1
-- only a table can be read-only at the same time

local mt = {
	__newindex = function ()
		error("Attempt to update a read-only table")
	end
}

function read_only (t)
	local proxy = {}
	
	function mt.__index (_ , k)
		return t[k]
	end

	setmetatable(proxy , mt)
	return proxy
end
