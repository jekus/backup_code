local count = 0

local function comb (ar_list , len)
	if len > 1 then
		for i = 1 , len do
			ar_list[i] , ar_list[len] = ar_list[len] , ar_list[i]
			comb(ar_list , len-1)
			ar_list[i] , ar_list[len] = ar_list[len] , ar_list[i]
		end
	else
		count = count + 1
		for i = 1 , #ar_list do
			io.write(ar_list[i] , " ")
		end
		io.write("\n")
	end
end

function permu (ar_list)
	count = 0
	comb(ar_list , #ar_list)
	io.write("\nTotal : " , count , "\n")
end
