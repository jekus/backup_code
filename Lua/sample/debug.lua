ldebug = {}
function ldebug.getvarval (thread , name , level)
	local td_ch = type(thread) == "thread"
	local val , found
	local n , v
	
	level = td_ch and (level or 2) or (name or 2)
	name = td_ch and name or thread
	thread = td_ch and thread or coroutine.running()

	if type(name) ~= "string" or 
		tonumber(name:sub(1 , 1)) then
			error("not a valid SYMBOL")
	end
	
	if not debug.getinfo(thread , level) then
		error("LEVEL out of range")
	end

	for idx = 1 , math.huge do
		n , v = debug.getlocal(thread , level , idx)
		
		if not n then break end
		if n == name then
			val = v
			found = true
		end
	end
	if found then return val end

	local func = debug.getinfo(thread , level , "f").func
	for idx = 1 , math.huge do
		n , v = debug.getupvalue(func , idx)
			
		if not n then break end
		if n == name then	-- why just the first ?
			return v
		end
	end

	local env = ldebug.getvarval(thread , "_ENV" , level)
	return env[name]
end


function ldebug.getallvars ()
	local rst = {}
	local n , v
	local func

	for idx = 1 , math.huge do
		n , v = debug.getlocal(2 , idx)

		if not n then return rst end
		rst[n] = v

		func = debug.getinfo(2 , "f").func
		n , v = debug.getupvalue(func , idx)

		if not n then return rst end
		rst[n] = v
	end

	return rst
end


function ldebug.edebug ()
	local mt = {__index = function (_ , k)
		return ldebug.getvarval(k , 3) end ,
				__newindex = env}
	local debug = debug.debug

	-- why stackoverflow would happen without the LOCAL ?
	local _ENV = setmetatable({} , mt)
	
	debug()
end


local bp = {}
local findfunc
local function findline ()
	local inf = debug.getinfo(2 , "flS")
	local line = inf.currentline - inf.linedefined

	if line == bp[inf.func] then
		debug.debug()
		debug.sethook(findfunc , "c")
	end
end


function findfunc ()
	local func = debug.getinfo(2 , "f").func

	if bp[func] then
		debug.sethook(findline , "l")
	end
end


function ldebug.setbreakpoint (func , line)
	bp[func] = line
	debug.sethook(findfunc , "c")
	return func		-- a handle
end


function ldebug.removebreakpoint (func)
	bp[func] = nil
end
