local m_count = 0
local q_table = {}
local N = 8		-- board size

function place_ok (column , row)
	for i = 1 , row-1 do
		if column == q_table[i] or
			math.abs(column - q_table[i]) == (row - i) then
			return false
		end
	end

	return true
end

function e_queen (q_r)
	if q_r > N then
		m_count = m_count + 1
	else
		for i = 1 , N do
			if place_ok(i , q_r) then
				q_table[q_r] = i
				e_queen(q_r+1)
			end
		end
	end
end

e_queen(1)
print(m_count)
