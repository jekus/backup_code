Counters = {}
Name = {}

local function hook ()
	local f = debug.getinfo(2 , "f").func
	local count = Counters[f]

	if not count then
		Counters[f] = 1
		Name[f] = debug.getinfo(2 , "Sn")
	else
		Counters[f] = count + 1
	end
end


local function getname (func)
	local n = Name[func]
	if n.what == "C" then
		return n.name
	end
	
	local ot = string.format("[%s]:%d" , n.short_src , n.linedefined)
	if n.what ~= "main" and n.namewhat ~= "" then
		return string.format("%s (%s)" , ot , n.name)
	else
		return ot
	end
end


local function prst ()
	local ot = {}
	for func , count in pairs(Counters) do
		ot[#ot + 1] = string.format("%s %d" , getname(func) , count)
	end

	table.sort(ot)
	for _ , v in pairs(ot) do
		print(v)
	end
end

local f = assert(loadfile(arg[1]))
debug.sethook(hook , "c")
f()
debug.sethook()
prst()
