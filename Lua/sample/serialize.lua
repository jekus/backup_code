local init_it_lev	-- for custom indentation

function serialize (obj , it_lev)
	it_lev = it_lev or 0
	local obj_t = type(obj)
	local it_str = string.rep(" " , it_lev)

	if not init_it_lev then
		init_it_lev = it_lev
	end

	if obj_t == "number" then
		io.write(it_str , obj)
	elseif obj_t == "string" then
		io.write(it_str , string.format("%q" , obj))
	elseif obj_t == "table" then
		if it_lev == init_it_lev then
			io.write("{\n")
		else
			io.write(it_str , "{\n")
		end

		for k , v in pairs(obj) do	-- if the key is a table ?
			if type(k) == "string" then
				io.write(it_str , " [")
				serialize(k , 0)
				io.write("] = ")

				if type(v) == "table" then
					io.write("\n")
					serialize(v , it_lev+2)
					goto T_JMP
				end
			end

			serialize(v , it_lev+1)

			::T_JMP::
			io.write(",\n")
		end

		if it_lev == init_it_lev then
			io.write("}\n")
		else
			io.write(it_str , "}")
		end
	else
		error("Can't serialize a " .. obj_t)
	end
end
