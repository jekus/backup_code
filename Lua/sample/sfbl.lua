-- sort a file by lines

local lines = {}
local input = arg[1] and assert(io.open(arg[1])) or io.stdin
local output = arg[2] and assert(io.open(arg[2] , "w+")) or io.stdout

for l in input:lines("L") do
	lines[#lines + 1] = l
end

table.sort(lines)
for _ , l in pairs(lines) do
	output:write(l)
end
