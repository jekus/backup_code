--[[
	Generate a iterator that
	return all non-empty substrings of
	a given string.
--]]


function all_substr (str)
	local idx = 1
	local sec_idx = 1
	local slen = string.len(str)

	return function ()
		while idx <= slen do
			if sec_idx <= slen then
				local rst = string.sub(str , idx , sec_idx)
				sec_idx = sec_idx + 1
				return rst
			else
				idx = idx + 1
				sec_idx = idx
			end
		end
	end
end
