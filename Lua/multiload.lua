--[[
	General version of the
	'loadwithprefix' ,
	can load multiple parts of
	code.
--]]


local function adjust_str (tb)
	local arg_tb = {}

	for k , v in pairs(tb) do
		if type(v) == 'string' then
			arg_tb[k] = ' ' .. v .. ' '
		else
			arg_tb[k] = v
		end
	end

	return arg_tb
end

function multiload (...)
	local idx = 1
	local tmp_tb = {...}
	local arg_tb = adjust_str(tmp_tb)

	local reader = function ()
		local rst = arg_tb[idx]
		idx = idx + 1
		return rst
	end

	return load(reader)
end
