#include <iostream>

using namespace std;

int
main ()
{
	string str_1{"Hello"};
	string str_2{"World"};
	string &&str_3{str_1 + str_2};

	cout << str_1 + str_2 << endl;
	cout << str_3 << endl;
	cout << str_1 + " Jekus" << endl;

	return 0;
}
