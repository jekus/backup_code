#include <iostream>
#include <vector>
#include <string.h>

using namespace std;

class Vec_str{
	public:
		Vec_str (initializer_list<string> list)
			: _vec_str(list) {}

		void add (string & str) {_vec_str.push_back(str);}
		string * begin () {return &_vec_str[0];}
		string * end () {return begin() + size();}
		int size () {return _vec_str.size();}
		
	private:
		vector<string> _vec_str;
};

int
main ()
{
	Vec_str v{"HHL" , "XYT" , "HelloWorld"};

	for(auto &str : v)
		cout << str << endl;
	
	return 0;
}
