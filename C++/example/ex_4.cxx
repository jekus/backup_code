#include <iostream>

using namespace std;

template<typename T>
void
my_swap (T &a , T &b)
{
	// move(x) produces a rvalue reference to x
	T tmp{move(a)};	// equal to T tmp{static_cast<T &&>(a)}
	a = move(b);	// equal to a = static_cast<T &&>(b)
	b = move(tmp);	// equal to b = static_cast<T &&>(tmp)
}

int
main ()
{
	int a , b;

	cout << "Enter two number: ";
	cin >> a >> b;

	cout << "-Before" << endl;
	cout << "a: " << a << endl
		 << "b: " << b << endl << endl;

	my_swap(a , b);
	
	cout << "-After" << endl;
	cout << "a: " << a << endl
		 << "b: " << b << endl << endl;

	return 0;
}
