// Variadic Templates

#include <iostream>

using namespace std;

void f () {cout << "- Done" << endl << endl;}

template<typename T , typename... Tail>
void f (T first , Tail... tail)
{
	cout << first << endl;
	f(tail...);
}

int
main ()
{
	f("HHL" , "XYT" , 123 ,"Hello World" , "I Love You XYT");
}
