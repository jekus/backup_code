#ifndef _UP_H_
#define _UP_H_

#include <iostream>
#include <string>

#define DEF_USER "guest"

class UserProfile{
	public:
		enum uLevel{
			BEGINNER , MEDIUM ,
			ADVANCED , EXPERT
		};

		UserProfile ();
		UserProfile (const std::string & user , uLevel = BEGINNER);

		// read data
		std::string dump_user () const {return _user;}
		int dump_rep_usr () const {return _rep_usr;}
		int dump_level () const {return _level;}
		int dump_lg_times () const {return _lg_times;}
		int dump_gs_times () const {return _gs_times;}
		int dump_cr_times () const {return _cr_times;}
		double dump_cr_per () const {return _cr_per;}

		// set data
		void set_user (const std::string & user) {_user = user;}
		void set_level (int level) {_level = level;}
		void set_lg_times (int lt) {_lg_times = lt;}
		void set_gs_times (int gt) {_gs_times = gt;}
		void set_cr_times (int ct) {_cr_times = ct;}
		void set_cr_per (double cp) {_cr_per = cp;}

		bool operator== (const UserProfile &) const;
		bool operator!= (const UserProfile &) const;

	private:
		std::string _user;			// default "guest"
		static int _gt_max;		// the number of "guest"
		int _rep_usr;			// identification for each "guest"
		int _level;				// skill level
		int _lg_times;			// login times
		int _gs_times;			// guess times
		int _cr_times;			// correct times
		double _cr_per;			// correct percentage
};

int UserProfile::_gt_max;

inline
UserProfile::UserProfile ()
	: _lg_times(1) , _gs_times(0) ,
	  _cr_times(0) , _level(BEGINNER) ,
	  _cr_per(0)
{
	_user = DEF_USER;
	_rep_usr = ++_gt_max;
}

// assume that there is no the same USER except "guest"
inline
UserProfile::
UserProfile (const std::string & user , uLevel level)
	: _lg_times(1) , _gs_times(0) ,
	  _cr_times(0) , _level(level) ,
	  _cr_per(0) , _user(user) ,
	  _rep_usr(0)
{}

inline bool
UserProfile::
operator== (const UserProfile & user_p) const
{
	if(_user == user_p._user &&
		_rep_usr == user_p._rep_usr)
			return true;
	return false;
}

inline bool
UserProfile::
operator!= (const UserProfile & user_p) const
{
	return !(*this == user_p);
}

std::ostream &
operator<< (std::ostream & os , const UserProfile & user_p)
{
	os << "User: " << user_p.dump_user();
	if(user_p.dump_user() == DEF_USER)
		os << '-' << user_p.dump_rep_usr();
	os << ' ';

	os << user_p.dump_level() << ' '
	   << user_p.dump_lg_times() << ' '
	   << user_p.dump_gs_times() << ' '
	   << user_p.dump_cr_times() << ' '
	   << user_p.dump_cr_per() << std::endl;

	return os;
}

std::istream &
operator>> (std::istream & is , UserProfile & user_p)
{
	std::string user;
	std::cin >> user;
	user_p.set_user(user);

	int level , lt , gt , ct;
	double cp;
	std::cin >> level >> lt >> gt
		>> ct >> cp;
	user_p.set_level(level);
	user_p.set_lg_times(lt);
	user_p.set_gs_times(gt);
	user_p.set_cr_times(ct);
	user_p.set_cr_per(cp);

	return is;
}

#endif
