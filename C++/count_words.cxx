// word is separated by blank

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <unordered_set>

using namespace std;

static const unordered_set<string> Words_exc{
    "a" , "an" , "the" ,
    "or" , "and" , "but"
};

template<typename CITER_T>
void
display_list (CITER_T iter , CITER_T iter_end , char *file)
{
	cout << '\n'
		 << file << ":\n";
	while(iter != iter_end)
	{
		cout << "\t"
			 << iter->first
			 << "\t"
			 << iter->second << '\n';
		++iter;
	}
}

template<typename INS>
void
count_words (INS &input , char *file)
{
	string word;
	unordered_map<string , int> words_count;
	while(input >> word)
		if(!Words_exc.count(word))
			++words_count[word];

	display_list(
        words_count.cbegin() ,
        words_count.cend() , file);
}

void
read_file (char **file)
{
	ifstream infile;

	do
	{
		infile.open(*file);
		if(!infile)
		{
			cerr << "Can't open file '"
			 	 << *file << '\'' << endl;
		}
		else
			count_words(infile , *file);

		infile.close();
		++file;
	}while(*file);
}

int
main (int argc , char **argv)
{
	if(argc < 2)
		count_words(cin , "stdin");
	else
		read_file(++argv);

	return 0;
}
