#ifndef _INH_H_
#define _INH_H_

#include <iostream>
#include <string>

class WidgetBase{
    public:
        WidgetBase() :
            _x_pos{0} , _y_pos{0} ,
            _width{800} , _height{600}
        {
            std::cout << "WidgetBase()"
                      << std::endl;
        }

        WidgetBase(
            int x_pos , int y_pos ,
            int width , int height) :
                _x_pos{x_pos} , _y_pos{y_pos} ,
                _width{width} , _height{height}
        {
            std::cout << "WidgetBase(int , int , int , int)"
                      << std::endl;
        }

        virtual ~WidgetBase ()
        {
            std::cout << "~WidgetBase()"
                      << std::endl;
        }

        virtual void print () const     /* 'virtual' is for dynamic binding */
        {
            std::cout << "- In WidgetBase::print()"
                      << std::endl;
            std::cout << "x: " << _x_pos << std::endl
                      << "y: " << _y_pos << std::endl
                      << "width: " << _width << std::endl
                      << "height: " << _height << std::endl;
        }

    protected:
        int _x_pos;
        int _y_pos;
        int _width;
        int _height;
};

class Button : public WidgetBase{
    public:
        Button(std::string text = "") :
            _text{text}
        {
            std::cout << "Button()"
                      << std::endl;
        }

        Button(
            int x_pos , int y_pos ,
            int width , int height ,
            std::string text = "") :
                WidgetBase(         /* !!! */
                    x_pos , y_pos ,
                    width , height) ,
                _text{text}
        {
            std::cout << "Button(int , int , int , int)"
                      << std::endl;
        }

        virtual ~Button()
        {
            std::cout << "~Button()"
                      << std::endl;
        }

        virtual void print () const
        {
            WidgetBase::print();
            std::cout << "- In Button::print()"
                      << std::endl;
            std::cout << "text: "
                      << '"' << _text << '"'
                      << std::endl;
        }

    private:
        std::string _text;
};

#endif
