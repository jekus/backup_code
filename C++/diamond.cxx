#include <iostream>
#include <error.h>
#include <errno.h>

#define UP 1
#define DOWN 0

using namespace std;

extern int errno;

int
main ()
{
	char ch;

	cout << "Enter a character: ";
	cin >> ch;

	if(!isalpha(ch))
		error(1 , errno , "Not an alpha");

	int up_or_down = UP;
	char base = islower(ch) ? 'a' : 'A';
	char idx = base;
	while(idx >= base)
	{
		for(int i = 0;i < ch - idx;++i)
			cout << ' ';

		for(char pos = base;pos <= idx;++pos)
			cout << pos;
		for(char pos = idx - 1;pos >= base;--pos)
			cout << pos;
		cout << endl;

		if(idx == ch)
			up_or_down = DOWN;
		
		if(up_or_down == UP)
			++idx;
		else if(up_or_down == DOWN)
			--idx;
	}

	return 0;
}
