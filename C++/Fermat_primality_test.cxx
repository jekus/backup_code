#include <bits/stdc++.h>

using namespace std;

using LLI = long long int;
using ULLI = unsigned long long int;

LLI
m_pow (LLI base , LLI N , LLI M)
{
    LLI res = 1;
    while(N)
    {
        if(N & 1) res = (res * base) % M;
        base *= base;
        N >>= 1;
    }
    return res;
}

// Fermat Method
bool
is_prime_1 (LLI n)
{
    if(n <= 1) return false;
    if(n <= 3) return true;

    LLI iter_n = sqrt(n);
    while(iter_n--)
        if(m_pow(2+rand()%(n-2) , n-1 , n) != 1) return false;
    return true;
}

void
solve ()
{
    LLI n;
    while(cin >> n)
        cout << (is_prime_1(n) ? "YES" : "NO") << endl;
}

int
main ()
{
    solve();
    return 0;
}
