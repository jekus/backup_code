#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

using namespace std;

int
main ()
{
    istream_iterator<string> is(cin);
    istream_iterator<string> eof;
    ostream_iterator<string> os(cout , "\n");
    vector<string> buf;

    copy(is , eof , back_inserter(buf));
    copy(buf.begin() , buf.end() , os);

    return 0;
}
