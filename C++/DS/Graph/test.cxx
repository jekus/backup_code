#include <iostream>
#include <chrono>
#include <random>
#include <vector>
#include <utility>
#include <algorithm>
#include <functional>
#include "graph.h"

using namespace std;

using ULLI = unsigned long long int;
using Ver_dat_T = ULLI;

constexpr ULLI ran_begin{0};
constexpr ULLI ran_end{(ULLI)2<<30};

static default_random_engine generator;
static uniform_int_distribution<ULLI> distribution{ran_begin , ran_end};


inline static void
print_graph (Graph<Ver_dat_T> &graph)
{
	Graph<Ver_dat_T>::print_vertex(graph);
}


/*
	Test 1
*/
static void
add_vertex (Graph<Ver_dat_T> &graph)
{
	graph.insert_vertex(1 , 54345);
	graph.insert_vertex(2 , 847895);
	graph.insert_vertex(3 , 8967);
	graph.insert_vertex(4 , 324);
	graph.insert_vertex(5 , 666);
	graph.insert_vertex(6 , 999);
}

static void
add_edge (Graph<Ver_dat_T> &graph)
{
	graph.insert_edge(1 , 2);
	graph.insert_edge(1 , 3);
	graph.insert_edge(1 , 1);
	graph.insert_edge(1 , 6);
	graph.insert_edge(2 , 1);
	graph.insert_edge(2 , 4);
	graph.insert_edge(3 , 3);
	graph.insert_edge(3 , 6);
	graph.insert_edge(4 , 1);
	graph.insert_edge(5 , 3);
	graph.insert_edge(5 , 5);
	graph.insert_edge(5 , 6);
	graph.insert_edge(6 , 3);
	graph.insert_edge(6 , 2);
}

static void
test_1 ()
{
	cout << "--- Test 1 ---\n\n";
	Graph<Ver_dat_T> graph;
	add_vertex(graph);
	add_edge(graph);
	print_graph(graph);
	graph.dfs(1);
	cout << endl;
	graph.dfs(1);
	cout << endl;
	graph.bfs(1);
	cout << endl;
	graph.bfs(1);
	cout << "\n--- END ---\n\n\n";
}
/* --- test 1 --- */


/*
	Test 2
*/
inline static ULLI
clock_seed ()
{
	return
		std::chrono::system_clock::now().time_since_epoch().count();
}

/*
	Whether the random-generated id
	is in the 'id_set'.
*/
inline static bool
id_in (ULLI id , vector<ULLI> &id_set)
{
	return
		find(
			id_set.begin() , 
			id_set.end() , id) != id_set.end();
}

/*
	Insert data into the Graph , and
	make some edges in the Graph.
*/
static Graph<Ver_dat_T>
build_graph (
	vector<pair<ULLI , Ver_dat_T> > &data_set ,
	vector<ULLI> &id_set)
{
	default_random_engine generator{clock_seed()};
	uniform_int_distribution<ULLI> distribution{0 , id_set.size()-1};
	auto gen_random = bind(distribution , generator);

	Graph<Ver_dat_T> graph;
	for(auto &data_pair : data_set)
	{
		graph.insert_vertex(
			data_pair.first ,
			data_pair.second);
	}
	
	ULLI from , to;
	register ULLI vertex_num = id_set.size();
	for(int idx = 0;idx < vertex_num;++idx)
	{
		/* Make some edges */
		from = id_set[gen_random()];
		to = id_set[gen_random()];
		graph.insert_edge(from , to);
	}

	return graph;
}

static ULLI
gen_id (
	vector<ULLI> &id_set ,
	auto &gen_random)
{
	/* ensure ID is unique */
	ULLI cur_id = gen_random();
	while(id_in(cur_id , id_set))
		cur_id = gen_random();

	id_set.push_back(cur_id);
	return cur_id;
}

static void
gen_data (
	vector<pair<ULLI , Ver_dat_T> > &data_set ,
	vector<ULLI> &id_set ,
	auto &gen_random)
{
	/* Clear the record first */
	data_set.clear();
	id_set.clear();
	
	ULLI cur_id;
	for(int idx = 0;idx < 1000;++idx)
	{
		cur_id = gen_id(id_set , gen_random);
		data_set.push_back(make_pair(cur_id , gen_random()));
	}
}

static void
test_2 ()
{
	cout << "--- Test 2 ---" << endl;
	vector<pair<ULLI , Ver_dat_T> > data_set;
	vector<ULLI> id_set;
	Graph<Ver_dat_T> graph;

	for(int i = 0;i < 100;++i)
	{
		if(!(i % 5))
			generator.seed(clock_seed());

		auto gen_random = bind(distribution , generator);
		gen_data(data_set , id_set , gen_random);
		graph = build_graph(data_set , id_set);

		cout << "Generation " << i+1 << ':' << endl;
		print_graph(graph);
		cout << "\n\n\n";
	}

	cout << "\n--- END ---\n\n\n";
}
/* --- test 2 --- */


int
main ()
{
	test_1();
	test_2();

	return 0;
}
