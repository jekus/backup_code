/*
	An implement of directed Graph.
*/
#ifndef _GRAPH_H_
#define _GRAPH_H_

#define DEBUG

#include <ostream>
#include <forward_list>
#include <queue>
#include <algorithm>

template<typename Data_t>
class Graph;

template<typename Data_t>
class Vertex{
	public:
		Vertex (const int id , const Data_t data)
		: _id{id} , _data{data} ,
		  _state{Graph<Data_t>::NOT_DISCOVERED}
		{}

		bool operator== (const Vertex<Data_t> &vertex) const
		{return vertex._id == _id;}

		int _id;
		Data_t _data;
		int _state;		// used for searching
		std::forward_list<Vertex<Data_t> *> _adjacency_list;
};

template<typename Data_t>
class Graph{
	public:
		/*
			State used for searching to color
			the vertex.
		*/
		enum State{
			NOT_DISCOVERED , DISCOVERED ,
			FINISHED
		};

		Graph (): _vertex_num{0} , _edge_num{0} {}
		Graph (Graph<Data_t> &&graph);

		Graph<Data_t> & operator= (Graph<Data_t> &&graph);

		void insert_vertex (const int id , const Data_t data);
		void remove_vertex (const int id);

		void insert_edge (const int from , const int to);
		void remove_edge (const int from , const int to);

		Vertex<Data_t> * dfs (const int id);		// Deep-First search
		Vertex<Data_t> * bfs (const int id);		// Breath-First search

		/* print the data of each vertex */
		static void print_vertex (Graph<Data_t> &graph);


	private:
		/*
			Helper function for 'dfs' and 'bfs'.
		*/
		static Vertex<Data_t> * start_dfs (
			const int id , Vertex<Data_t> *cur_vertex);
		static Vertex<Data_t> * start_bfs (
			const int id , Vertex<Data_t> *cur_vertex);

		/* Helper function for 'print_vertex' */
		static void print_adjacency (Vertex<Data_t> &vertex);

		Vertex<Data_t> * find_vertex (const int id);
		std::forward_list<Vertex<Data_t> > _vertex_list;
		mutable int _vertex_num;
		mutable int _edge_num;
};


template<typename Data_t>
Graph<Data_t>::Graph (Graph<Data_t> &&graph)
:
	_vertex_list{std::move(graph._vertex_list)} ,
	_vertex_num{std::move(graph._vertex_num)} ,
	_edge_num{std::move(graph._edge_num)}
{}

template<typename Data_t>
Graph<Data_t> & Graph<Data_t>::
operator= (Graph<Data_t> &&graph)
{
	_vertex_list = std::move(graph._vertex_list);
	_vertex_num = std::move(graph._vertex_num);
	_edge_num = std::move(graph._edge_num);

	return *this;
}

template<typename Data_t>
inline void Graph<Data_t>::
print_adjacency (Vertex<Data_t> &vertex)
{
	std::cout << "\tAdjacency ID:\n";
	for(auto adj : vertex._adjacency_list)
		std::cout << "\t\t" << adj->_id << std::endl;
}

template<typename Data_t>
inline void Graph<Data_t>::
print_vertex (Graph<Data_t> &graph)
{
	for(auto &vertex : graph._vertex_list)
	{
		std::cout << vertex._id << ": "
				  << vertex._data << std::endl;
		print_adjacency(vertex);
	}
}

template<typename Data_t>
inline Vertex<Data_t> * Graph<Data_t>::
find_vertex (const int id)
{
	auto rst = std::find_if(
		_vertex_list.begin() , _vertex_list.end() ,
		[&id](Vertex<Data_t> &vertex){return id == vertex._id;});
	
	return (rst == _vertex_list.end() ? nullptr : &(*rst));
}

template<typename Data_t>
void Graph<Data_t>::
insert_vertex (const int id , const Data_t data)
{
	if(find_vertex(id))
		return;

	Vertex<Data_t> new_vertex{id , data};
	_vertex_list.push_front(new_vertex);
	++_vertex_num;
}

/*
	Remove the vertex if it doesn't
	have any adjacency.
*/
template<typename Data_t>
void Graph<Data_t>::
remove_vertex (const int id)
{
	Vertex<Data_t> *vertex_del;
	if(!(vertex_del = find_vertex(id)) ||
		!(vertex_del->_adjacency_list.empty()))
		return;

	_vertex_list.remove(*vertex_del);
	--_vertex_num;
}

/*
	Build an edge between 'from' and 'to'
	if they don't have one.
*/
template<typename Data_t>
void Graph<Data_t>::
insert_edge (const int from , const int to)
{
	Vertex<Data_t> *vertex_from , *vertex_to;
	if(!(vertex_from = find_vertex(from)) || 
		!(vertex_to = find_vertex(to)) ||
		find(
			vertex_from->_adjacency_list.begin() ,
			vertex_from->_adjacency_list.end() ,
			vertex_to) !=
				vertex_from->_adjacency_list.end())
			return;

	vertex_from->_adjacency_list.push_front(vertex_to);
	++_edge_num;
}

/*
	Remove an edge between 'from' and 'to'
	if they really have one.
*/
template<typename Data_t>
void Graph<Data_t>::
remove_edge (const int from , const int to)
{
	Vertex<Data_t> *vertex_from , *vertex_to;
	if(!(vertex_from = find_vertex(from)) ||
		!(vertex_to = find_vertex(to)) ||
		find(
			vertex_from->_adjacency_list.begin() ,
			vertex_from->_adjacency_list.end() ==
				vertex_from->_adjacency_list.end()))
			return;

	vertex_from._adjacency_list.remove(vertex_to);
	--_edge_num;
}

template<typename Data_t>
inline Vertex<Data_t> * Graph<Data_t>::
dfs (const int id)
{
	Vertex<Data_t> *rst = nullptr;
	for(auto &vertex : _vertex_list)
		if((rst = start_dfs(id , &vertex)))
			break;

	return rst;
}

template<typename Data_t>
inline Vertex<Data_t> * Graph<Data_t>::
bfs (const int id)
{
	Vertex<Data_t> *rst = nullptr;
	for(auto &vertex : _vertex_list)
		if((rst = start_bfs(id , &vertex)))
			break;
	
	for(auto &vertex : _vertex_list)
		vertex._state = NOT_DISCOVERED;

	return rst;
}

template<typename Data_t>
inline Vertex<Data_t> * Graph<Data_t>::
start_dfs (
	const int id ,
	Vertex<Data_t> *cur_vertex)
{
	if(cur_vertex->_state == FINISHED)
		return nullptr;

	#ifdef DEBUG
		std::cout << cur_vertex->_id << std::endl;
	#endif
	if(cur_vertex->_id == id)
	{
		cur_vertex->_state = NOT_DISCOVERED;
		return cur_vertex;
	}
	cur_vertex->_state = DISCOVERED;

	Vertex<Data_t> *rst = nullptr;
	for(auto adj : cur_vertex->_adjacency_list)
	{
		#ifdef DEBUG
			std::cout << adj->_id << std::endl;
		#endif
		if((rst = start_dfs(id , adj)))
			break;
	}

	if(rst)
		cur_vertex->_state = NOT_DISCOVERED;
	else
		cur_vertex->_state = FINISHED;
	return rst;
}

template<typename Data_t>
inline Vertex<Data_t> * Graph<Data_t>::
start_bfs (
	const int id ,
	Vertex<Data_t> *cur_vertex)
{
	if(cur_vertex->_state == FINISHED)
		return nullptr;

	#ifdef DEBUG
		if(cur_vertex->_state == NOT_DISCOVERED)
			std::cout << cur_vertex->_id << std::endl;
	#endif
	if(cur_vertex->_id == id)
		return cur_vertex;
	cur_vertex->_state = DISCOVERED;

	for(auto adj : cur_vertex->_adjacency_list)
	{
		if(adj->_state == NOT_DISCOVERED)
		{
			#ifdef DEBUG
				std::cout << adj->_id << std::endl;
			#endif
			adj->_state = DISCOVERED;
		}

		if(adj->_id == id)
			return adj;
	}

	cur_vertex->_state = FINISHED;
	return nullptr;
}

#endif
