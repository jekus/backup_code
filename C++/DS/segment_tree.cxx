/*
    An implement of Segment Tree ,
    with Lazy Propagation.
*/

#include <bits/stdc++.h>

using namespace std;

template<typename VAL_T>
class seg_tree{
    public:
        seg_tree (vector<VAL_T> &Ar)
        {
            int AL = Ar.size();
            int N = 2*pow(2 , ceil(log2(AL)))-1;
            st = new VAL_T[N];
            cons_st(Ar , 0 , 0 , AL-1);

            ele_n = AL;
            lazy = new VAL_T[N];
            memset(lazy , 0 , sizeof(VAL_T) * N);
        }

        ~seg_tree () { free(st);}

        void update (int idx , VAL_T dif)
        { do_update_range(0 , ele_n-1 , idx , idx , 0 , dif);}

        void update_range (int l , int r , VAL_T dif)
        { do_update_range(0 , ele_n-1 , l , r , 0 , dif);}

        VAL_T query (int l , int r)
        { return do_query(0 , ele_n-1 , l , r , 0);}

    private:
        int ele_n;
        VAL_T *st , *lazy;

        VAL_T
        cons_st (vector<VAL_T> &Ar , int idx , int l , int r)
        {
            if(l == r) return st[idx] = Ar[l];

            int mid = l + (r - l) / 2;
            return st[idx] =
                cons_st(Ar , idx*2+1 , l , mid) +
                cons_st(Ar , idx*2+2 , mid+1 , r);
        }

        void
        do_update_range (
            int l , int r ,
            int tl , int tr ,
            int st_idx , VAL_T dif)
        {
            if(lazy[st_idx])
            {
                VAL_T d = lazy[st_idx];
                st[st_idx] += (r-l+1)*d;
                if(l != r)
                {
                    lazy[st_idx*2+1] += d;
                    lazy[st_idx*2+2] += d;
                }
                lazy[st_idx] = 0;
            }

            if(r < tl || tr < l) return;
          
            if(tl <= l && r <= tr)
            {
                st[st_idx] += (r-l+1)*dif;
                if(l != r)
                {
                    lazy[st_idx*2+1] += dif;
                    lazy[st_idx*2+2] += dif;
                }
            }
            else
            {
                int mid = l + (r - l) / 2;
                do_update_range(l , mid , tl , tr , st_idx*2+1 , dif);
                do_update_range(mid+1 , r , tl , tr , st_idx*2+2 , dif);
                st[st_idx] = st[st_idx*2+1] + st[st_idx*2+2];
            }
        }

        VAL_T
        do_query (
            int l , int r ,
            int tl , int tr ,
            int st_idx)
        {
            if(lazy[st_idx])
            {
                VAL_T dif = lazy[st_idx];
                st[st_idx] += (r-l+1)*dif;
                if(l != r)
                {
                    lazy[st_idx*2+1] += dif;
                    lazy[st_idx*2+2] += dif;
                }
                lazy[st_idx] = 0;
            }
            
            if(r < tl || tr < l) return 0;

            if(tl <= l && r <= tr) return st[st_idx];
           
            int mid = l + (r - l) / 2;
            return do_query(l , mid , tl , tr , st_idx*2+1) +
                   do_query(mid+1 , r , tl , tr , st_idx*2+2);
        }
};

int
main ()
{
    vector<int> Ar({1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10});
    seg_tree<int> st(Ar);
    cout << st.query(0 , 2) << endl;    /* RES: 6  */
    cout << st.query(4 , 9) << endl;    /* RES: 45 */
    cout << st.query(1 , 5) << endl;    /* RES: 20 */
    cout << st.query(8 , 8) << endl;    /* RES: 9  */

    st.update(1 , 10);
    st.update(4 , 50);
    st.update(8 , 100);
    cout << endl;

    cout << st.query(0 , 2) << endl;    /* RES: 16  */
    cout << st.query(4 , 9) << endl;    /* RES: 195 */
    cout << st.query(1 , 5) << endl;    /* RES: 80  */
    cout << st.query(8 , 8) << endl;    /* RES: 109 */

    st.update(1 , -20);
    st.update(4 , 30);
    st.update(8 , -40);
    cout << endl;
    
    cout << st.query(0 , 2) << endl;    /* RES: -4  */
    cout << st.query(4 , 9) << endl;    /* RES: 185 */
    cout << st.query(1 , 5) << endl;    /* RES: 90  */
    cout << st.query(8 , 8) << endl;    /* RES: 69  */

    st.update_range(1 , 5 , 30);
    st.update_range(2 , 8 , -10);
    cout << endl;
    
    cout << st.query(0 , 2) << endl;    /* RES: 46  */
    cout << st.query(4 , 9) << endl;    /* RES: 195 */
    cout << st.query(1 , 5) << endl;    /* RES: 200 */
    cout << st.query(8 , 8) << endl;    /* RES: 59  */
    
    return 0;
}
