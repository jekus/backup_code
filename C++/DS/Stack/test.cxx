#include <iostream>
#include <string>
#include "stack.h"

using namespace std;

int
main ()
{
	string item;
	Stack<string> s;

	s.push("HHL");
	s.push("XYT");
	s.push("Jekus Neky");

	s.peek(item);
	cout << item << '\n';
	s.pop(item);
	cout << item << '\n';

	s.push("Hello World");
	s.peek(item);
	cout << item << '\n';

    cout << (s.find("BOOM") ? "YES" : "NO")
         << endl;

	return 0;
}
