#include <vector>
#include <algorithm>

#ifndef _STACK_H_
#define _STACK_H_

template<typename E_T>
class Stack{
	public:
		bool push (const E_T &);
		bool pop (E_T &);
		bool peek (E_T &);

        bool find (E_T &);
        bool find (E_T &&);

		bool empty ();
		bool full ();

		int size () {return _stack.size();}

	private:
		std::vector<E_T> _stack;
};

template<typename E_T>
inline bool
Stack<E_T>::
empty ()
{
	return _stack.empty();
}

template<typename E_T>
inline bool
Stack<E_T>::
full ()
{
	return _stack.size() == _stack.max_size();
}

template<typename E_T>
bool
Stack<E_T>::
push (const E_T &item)
{
	if(full())
		return false;

	_stack.push_back(item);
	return true;
}

template<typename E_T>
bool
Stack<E_T>::
pop (E_T &item)
{
	if(empty())
		return false;
	
	item = _stack.back();
	_stack.pop_back();
	return true;
}

template<typename E_T>
bool
Stack<E_T>::
peek (E_T &item)
{
	if(empty())
		return false;

	item = _stack.back();
	return true;
}

template<typename E_T>
bool
Stack<E_T>::
find (E_T &item)
{
    return
        std::find(_stack.begin() , _stack.end() ,
            item) != _stack.end();
}

template<typename E_T>
bool
Stack<E_T>::
find (E_T &&item)
{
    return
        std::find(_stack.begin() , _stack.end() ,
            item) != _stack.end();
}

#endif
