#include <iostream>
#include <vector>

using namespace std;

class T_node{
    public:
        T_node ()
        {par = this; rank = 0;}

        T_node *par;
        int rank;
};

T_node * make_set ()
{return new T_node;}

T_node * find_set (T_node *obj)
{
    if(obj != obj->par)
        obj->par = find_set(obj->par);
    return obj->par;
}

void union_set (T_node *A , T_node *B)
{
    T_node *A_rep = find_set(A) ,
           *B_rep = find_set(B);
    if(A_rep->rank > B_rep->rank)
        B_rep->par = A_rep;
    else
    {
        A_rep->par = B_rep;
        if(A_rep->rank == B_rep->rank)
            ++(B_rep->rank);
    }
}

int
main ()
{
    vector<T_node *> node_S;
    for(int i = 1;i <= 10;++i)
        node_S.push_back(make_set());

    auto same_set = [](T_node *A , T_node *B)
    {return find_set(A) == find_set(B);};
    cout << (same_set(node_S[0] , node_S[5]) ? "YES" : "NO") << endl;
    union_set(node_S[0] , node_S[5]);
    cout << (same_set(node_S[0] , node_S[5]) ? "YES" : "NO") << endl;
    cout << (same_set(node_S[0] , node_S[9]) ? "YES" : "NO") << endl;
    union_set(node_S[0] , node_S[9]);
    cout << (same_set(node_S[0] , node_S[9]) ? "YES" : "NO") << endl;
    cout << (same_set(node_S[5] , node_S[9]) ? "YES" : "NO") << endl;
    cout << (same_set(node_S[1] , node_S[9]) ? "YES" : "NO") << endl;

    return 0;
}
