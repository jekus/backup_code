#include <iostream>
#include "disjoint_set_list.h"

using namespace std;

void
print_s (set_node<int> *ptr)
{
    while(ptr)
    {
        cout << ptr->_val << ' ';
        ptr = ptr->_next;
    }
    cout << endl;
}

void
print_djs (disjoint_set<int> &DJS)
{
    cout << endl;
    for(auto S : DJS._S)
        print_s(S->_head);
    cout << endl;
}

int
main ()
{
    vector<set_node<int> *> S_obj;
    disjoint_set<int> DJS;
    for(int i = 1;i <= 10;++i)
        S_obj.push_back(DJS.make_set(i));

    print_djs(DJS);
    DJS.union_set(S_obj[0] , S_obj[5]);
    print_s(DJS.find_set(S_obj[0]));
    print_s(DJS.find_set(S_obj[5]));
    print_djs(DJS);

    auto same_set = [&DJS](set_node<int> *A , set_node<int> *B)
    {return DJS.find_set(A) == DJS.find_set(B);};
    cout << (same_set(S_obj[0] , S_obj[5]) ? "YES" : "NO") << endl;
    cout << (same_set(S_obj[0] , S_obj[4]) ? "YES" : "NO") << endl;
    DJS.union_set(S_obj[0] , S_obj[4]);
    cout << (same_set(S_obj[0] , S_obj[4]) ? "YES" : "NO") << endl;
    print_s(DJS.find_set(S_obj[0]));
    print_djs(DJS);

    return 0;
}
