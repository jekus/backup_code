#ifndef _DJ_SET_LIST_H_
#define _DJ_SET_LIST_H_

#include <unordered_set>
#include <vector>
#include <algorithm>
using namespace std;

template<typename T>
class set_head;

template<typename T>
class set_node{
    public:
        set_node (set_head<T> *par , T &val) :
            _val(val)
        {_par = par; _next = nullptr;}
        const T & get_val ()
        {return _val;}

        T _val;
        set_head<T> *_par;
        set_node<T> *_next;
};

template<typename T>
class set_head{
    public:
        set_head (T &);
        ~set_head ();

        set_node<T> * rep ()
        {return _head;}

        set_node<T> *_head;
        set_node<T> *_tail;
        int _set_l;
};

template<typename T>
class disjoint_set{
    public:
        set_node<T> * make_set (T &val)
        {
            set_head<T> *ptr = new set_head<T>(val);
            _S.insert(ptr);
            return ptr->_head;
        }

        set_node<T> * find_set (set_node<T> *) const;
        void union_set (set_node<T> * , set_node<T> *);

        unordered_set<set_head<T> *> _S;
};

template<typename T>
set_head<T>::set_head (T &val)
{
    _head = _tail = new set_node<T>(this , val);
    _set_l = 1;
}

template<typename T>
set_head<T>::~set_head ()
{
    set_node<T> *ptr = _head , *next = _head->_next;
    while(ptr)
    {
        if(ptr->_par == this)
            delete ptr;
        ptr = next;
        if(ptr) next = ptr->_next;
    }
}

template<typename T>
set_node<T> *
disjoint_set<T>::find_set (set_node<T> *X) const
{
    return X->_par->_head;
}

template<typename T>
void
disjoint_set<T>::union_set (set_node<T> *A , set_node<T> *B)
{
    set_head<T> *A_par = A->_par , *B_par = B->_par;
    set_head<T> *new_par = (A_par->_set_l > B_par->_set_l ? A_par : B_par);
    set_head<T> *old_par = (new_par == A_par ? B_par : A_par);
    new_par->_tail->_next = old_par->_head;
    new_par->_tail = old_par->_tail;
    new_par->_set_l += old_par->_set_l;

    set_node<T> *ptr = old_par->_head;
    while(ptr)
    {
        ptr->_par = new_par;
        ptr = ptr->_next;
    }
    delete old_par;
    _S.erase(old_par);
}

#endif
