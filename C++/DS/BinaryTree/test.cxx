#include <iostream>
#include <string>
#include "bint.h"

using namespace std;

int
main ()
{
	BinaryTree<string> tree;
	tree.add("Jekus");
	tree.add("Neky");
	tree.add("Hello World");
	tree.add("I Love You XYT");
	tree.print();

	return 0;
}
