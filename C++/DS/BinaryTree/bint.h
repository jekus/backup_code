#ifndef _BINT_H_
#define _BINT_H_

#include <iostream>

template<typename VAL_T>
class BT_Node{
	public:
		BT_Node (VAL_T &value)
			: _value(value) ,
			  _left(nullptr),
			  _right(nullptr)
		{}

		VAL_T & value () {return _value;}
		BT_Node * & left () {return _left;}
		BT_Node * & right () {return _right;}

	private:
		VAL_T _value;
		BT_Node *_left;
		BT_Node *_right;
};

template<typename VAL_T>
class BinaryTree{
	public:
		typedef void (*BT_Func) (BT_Node<VAL_T> *);
		enum{STAY , LEFT , RIGHT};
		
		BinaryTree () {_root = nullptr;}
		~BinaryTree ();

		// assume that VAL_T is built-in type
		void add (VAL_T);
		void del (VAL_T);
		bool find (VAL_T);

		void preorder_traverse (BT_Func);
		void inorder_traverse (BT_Func);
		void postorder_traverse (BT_Func);
		
		void print ()
		{
			auto print_func =
				[](BT_Node<VAL_T> *node)
				{std::cout << (node->value)() << std::endl;};

			inorder_traverse(print_func);
		}
	
	private:
		int	go_to (BT_Node<VAL_T> * , VAL_T &);

		void pre_go (BT_Node<VAL_T> * , BT_Func);
		void in_go (BT_Node<VAL_T> * , BT_Func);
		void post_go (BT_Node<VAL_T> * , BT_Func);

		BT_Node<VAL_T> * new_node (VAL_T &);
		BT_Node<VAL_T> * & find_pos (VAL_T &);

		BT_Node<VAL_T> *_root;
};

template<typename VAL_T>
inline BinaryTree<VAL_T>::
~BinaryTree ()
{
	postorder_traverse(
		[](BT_Node<VAL_T> *node) {delete node;});
}

template<typename VAL_T>
void BinaryTree<VAL_T>::
add (VAL_T value)
{
	BT_Node<VAL_T> * &pos = find_pos(value);
	pos = new_node(value);
}

template<typename VAL_T>
void BinaryTree<VAL_T>::
del (VAL_T value)
{
	BT_Node<VAL_T> * &n_ptr = find_pos(value);
	BT_Node<VAL_T> * n_del = n_ptr;
	BT_Node<VAL_T> * &n_right = (n_ptr->right)();

	n_ptr = (n_ptr->left)();
	while(n_ptr)
		n_ptr = (n_ptr->right)();
	n_ptr = n_right;

	delete n_del;
}

template<typename VAL_T>
inline bool BinaryTree<VAL_T>::
find (VAL_T value)
{
	return find_pos(value);
}

template<typename VAL_T>
inline BT_Node<VAL_T> * BinaryTree<VAL_T>::
new_node (VAL_T &value)
{
	return new BT_Node<VAL_T>(value);
}

template<typename VAL_T>
inline int BinaryTree<VAL_T>::
go_to (BT_Node<VAL_T> *node , VAL_T &value)
{
	VAL_T &val = (node->value)();

	if(val > value)
		return RIGHT;
	else if(val < value)
		return LEFT;
	else
		return STAY;
}

template<typename VAL_T>
BT_Node<VAL_T> * & BinaryTree<VAL_T>::
find_pos (VAL_T &value)
{
	int dir;
	BT_Node<VAL_T> **n_ptr = &_root;

	while(*n_ptr)
	{
		dir = go_to(*n_ptr , value);

		if(dir == LEFT)
			n_ptr = &((*n_ptr)->left)();
		else if(dir == RIGHT)
			n_ptr = &((*n_ptr)->right)();
		else
			break;
	}

	return *n_ptr;
}

template<typename VAL_T>
void BinaryTree<VAL_T>::
pre_go (BT_Node<VAL_T> *node , BT_Func func)
{
	if(node)
	{
		func(node);
		pre_go((node->left)() , func);
		pre_go((node->right)() , func);
	}
}

template<typename VAL_T>
inline void BinaryTree<VAL_T>::
preorder_traverse (BT_Func func)
{
	pre_go(_root , func);
}

template<typename VAL_T>
void BinaryTree<VAL_T>::
in_go (BT_Node<VAL_T> *node , BT_Func func)
{
	if(node)
	{
		in_go((node->left)() , func);
		func(node);
		in_go((node->right)() , func);
	}
}

template<typename VAL_T>
inline void BinaryTree<VAL_T>::
inorder_traverse (BT_Func func)
{
	in_go(_root , func);
}

template<typename VAL_T>
void BinaryTree<VAL_T>::
post_go (BT_Node<VAL_T> *node , BT_Func func)
{
	if(node)
	{
		pre_go((node->left)() , func);
		pre_go((node->right)() , func);
		func(node);
	}
}

template<typename VAL_T>
inline void BinaryTree<VAL_T>::
postorder_traverse (BT_Func func)
{
	post_go(_root , func);
}

#endif
