#ifndef _MAT_H_
#define _MAT_H_

#include <iostream>
#include <algorithm>
#include <initializer_list>
#include <cstring>

/*
    Assume that all the following matrix
    operations are valid.
*/
template<typename ELE_T>
class Matrix{
    public:
        Matrix (int , int);
        Matrix (int , int , std::initializer_list<ELE_T>);
        Matrix (const Matrix<ELE_T> &);
        ~Matrix () {delete [] _matrix;}

        /*
            Access the element in the Matrix.
            Index start from 1.
        */
        ELE_T & operator() (int row , int col) const
        {return _matrix[(row-1)*_col+(col-1)];}

        Matrix operator+ (const Matrix<ELE_T> &) const;
        Matrix operator- (const Matrix<ELE_T> &) const;
        Matrix operator* (const Matrix<ELE_T> &) const;
        Matrix & operator+= (const Matrix<ELE_T> &);
        Matrix & operator-= (const Matrix<ELE_T> &);
        Matrix & operator= (const Matrix<ELE_T> &);
        
        int row () const {return _row;}
        int col () const {return _col;}
        ELE_T * mat () const {return _matrix;}

        std::ostream & print (std::ostream &) const;

    private:
        ELE_T *_matrix;
        int _row;
        int _col;
};

template<typename ELE_T>
inline Matrix<ELE_T>::
Matrix (int row , int col) :
    _row(row) , _col(col)
{
    int mat_size = row * col;
    _matrix = new ELE_T[mat_size];
    std::memset(_matrix , 0 , sizeof(ELE_T) * mat_size);
}

/*
    User must ensure 'ROW * COL == l.size()'.
*/
template<typename ELE_T>
inline Matrix<ELE_T>::
Matrix (int row , int col , std::initializer_list<ELE_T> l) :
    _row(row) , _col(col)
{
    _matrix = new ELE_T[row * col];
    std::copy(l.begin() , l.end() , _matrix);
}

template<typename ELE_T>
inline Matrix<ELE_T>::
Matrix (const Matrix<ELE_T> &mat) :
    _row(mat.row()) , _col(mat.col())
{
    int mat_size = mat.row() * mat.col();
    _matrix = new ELE_T[mat_size];
    std::memcpy(_matrix , mat.mat() , sizeof(ELE_T) * mat_size);
}

template<typename ELE_T>
inline Matrix<ELE_T>
Matrix<ELE_T>::
operator+ (const Matrix<ELE_T> &mat) const
{
    Matrix<ELE_T> rst(*this);
    rst += mat;
    return rst;
}

template<typename ELE_T>
inline Matrix<ELE_T>
Matrix<ELE_T>::
operator- (const Matrix<ELE_T> &mat) const
{
    Matrix<ELE_T> rst(*this);
    rst -= mat;
    return rst;
}

template<typename ELE_T>
inline Matrix<ELE_T>
Matrix<ELE_T>::
operator* (const Matrix<ELE_T> &mat) const
{
    int mat_c = mat.col();
    Matrix<ELE_T> rst(_row , mat_c);

    for(int rst_r = 1;rst_r <= _row;++rst_r)
        for(int rst_c = 1;rst_c <= mat_c;++rst_c)
            for(int i = 1;i <= _col;++i)
                rst(rst_r , rst_c) += ((*this)(rst_r , i) * mat(i , rst_c));

    return rst;
}

template<typename ELE_T>
inline Matrix<ELE_T> &
Matrix<ELE_T>::
operator+= (const Matrix<ELE_T> &mat)
{
    int mat_size = _row * _col;
    ELE_T *src_mat = mat.mat();

    for(int i = 0;i < mat_size;++i)
        _matrix[i] += src_mat[i];
    
    return *this;
}

template<typename ELE_T>
inline Matrix<ELE_T> &
Matrix<ELE_T>::
operator-= (const Matrix<ELE_T> &mat)
{
    int mat_size = _row * _col;
    ELE_T *src_mat = mat.mat();

    for(int i = 0;i < mat_size;++i)
        _matrix[i] -= src_mat[i];

    return *this;
}

template<typename ELE_T>
inline Matrix<ELE_T> &
Matrix<ELE_T>::
operator= (const Matrix<ELE_T> &mat)
{
    int mat_size = _row * _col;
    if(this != &mat)
        std::memcpy(_matrix , mat.mat() , sizeof(ELE_T) * mat_size);
    return *this;
}

template<typename ELE_T>
inline std::ostream &
Matrix<ELE_T>::
print (std::ostream &os) const
{
    int mat_size = _row * _col;
    for(int i = 0;i < mat_size;++i)
    {
        if(!(i % _col))
            os << std::endl;
        os << '\t' << _matrix[i];
    }
    os << std::endl;
    return os;
}

template<typename ELE_T>
inline std::ostream &
operator<< (std::ostream &os , const Matrix<ELE_T> &mat)
{
    return mat.print(os);
}

#endif
