#include <iostream>
#include "matrix.h"

using namespace std;

int
main ()
{
	Matrix<double> mat_1(4 , 4);
	Matrix<double> mat_2(4 , 4);
	mat_1(1 , 1) = 123.1;
	mat_1(3 , 2) = 111.1;
	mat_1(4 , 4) = 13;
	mat_2(1 , 1) = 1;
	mat_2(3 , 2) = 7;
	mat_2(4 , 4) = 9;
	cout << "mat_1:"
		 << mat_1 << endl;
	cout << "mat_2:"
		 << mat_2 << endl;

	Matrix<double> mat_3(4 , 4);
	mat_3 += mat_1;
	mat_3 += mat_2;
	cout << "mat_3:"
		 << mat_3 << endl;

    Matrix<double> mat_4(mat_3);
	cout << "Addr:\n"
		 << "\tmat_1: " << mat_1.mat() << '\n'
		 << "\tmat_2: " << mat_2.mat() << '\n'
		 << "\tmat_3: " << mat_3.mat() << '\n'
		 << "\tmat_4: " << mat_4.mat() << endl;

    Matrix<int> p(2 , 2);
    Matrix<int> fib(1 , 2);
    p(1 , 1) = 0;
    p(1 , 2) = p(2 , 1) = p(2 , 2) = 1;
    fib(1 , 1) = 0;
    fib(1 , 2) = 1;
    cout << p << endl;
    cout << fib << endl;

    Matrix<int> X(fib * p);
    cout << X << endl;

    Matrix<int> A(3 , 2 , {1 , 2 , 3 , 4 , 5 , 6});
    cout << A << endl;

	return 0;
}
