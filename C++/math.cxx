#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

struct Ques{
	int opd_l;
	int opd_r;
	int answer;
};

void
gene_ques (Ques ques_set[])
{
	for(int idx = 0;idx < 10;++idx)
	{
		int opd_l = rand() % 100 ,
			opd_r = rand() % 100;
		int answer = opd_l + opd_r;

		ques_set[idx].opd_l = opd_l;
		ques_set[idx].opd_r = opd_r;
		ques_set[idx].answer = answer;
	}	
}

int
no_continue ()
{
	char choice;

	cout << "Wrong!" << endl;
	cout << "Continue ? (y/n) ";
	cin >> choice;
	choice = tolower(choice);

	while(choice != 'y' && choice != 'n')
	{
		cout << "Y/n ? ";
		cin >> choice;
		choice = tolower(choice);
	}

	if(choice == 'n')
		return 1;

	return 0;
}

int
ask_user (Ques ques_set[])
{
	int mark = 0;

	for(int idx = 0;idx < 10;++idx)
	{
		int answer;
		Ques &cur_ques = ques_set[idx];

		cout << cur_ques.opd_l << " + "
			 << cur_ques.opd_r << " = ";
		cin >> answer;

		if(answer == cur_ques.answer)
		{
			cout << "Right!" << endl;
			mark += 10;
		}
		else if(no_continue())
			break;
	}

	return mark;
}

int
main ()
{
	srand(time(NULL));

	Ques ques_set[10];
	gene_ques(ques_set);

	cout << "Your mark: "
		 << ask_user(ques_set) << endl;

	return 0;
}
