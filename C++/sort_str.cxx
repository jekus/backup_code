#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

bool
len_less (string str1 , string str2)
{
	return str1.size() < str2.size() ? true : false;
}

int
main ()
{
	string str;
	vector<string> cont;

	while((cin >> str))
		cont.push_back(str);

	vector<string>::iterator iter = cont.begin() ,
							 iter_end = cont.end();
	sort(iter , iter_end , len_less);

	while(iter != iter_end)
	{
		cout << *iter << '\n';
		++iter;
	}

	return 0;
}
